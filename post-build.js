const fs = require('fs');
const projectName = '/sdo/instruction/'
var pack = fs.readFileSync("./dist/index.html", "utf8");
pack = pack.replace('/../../static/js4app/runtime', projectName+'runtime');
pack = pack.replace('/../../static/js4app/main', projectName+'main');
pack = pack.replace('/../../static/js4app/polyfills', projectName+'polyfills');
pack = pack.replace('/../../static/js4app/scripts', projectName+'scripts');
pack = pack.replace(/<link(.*?)js4app\//g, '<link rel="stylesheet" href="' + projectName);
require('fs').writeFileSync('./dist/index.html', pack);
