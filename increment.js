
let pack = require('./package.json');
let packNumbers;
let suffixIndex = pack.version.indexOf('-');
let packSuffix;
if(suffixIndex > 0) {
  packNumbers = pack.version.slice(0, suffixIndex);
  packSuffix = pack.version.slice(suffixIndex + 1);
} else {
  packNumbers = pack.version;
}
//console.log('packNumbers=' + packNumbers);
//console.log('packSuffix=' + packSuffix);

let ver = packNumbers.split('.');
//console.log('old ver=' + ver);
ver[ver.length - 1] = parseInt(ver[ver.length - 1]) + 1;
let newVer = ver.join('.') + (packSuffix ? '-' + packSuffix : '');
//console.log('new ver=' + newVer);

console.log('ver ' + newVer);
require('fs').writeFileSync('./ver.txt', newVer);

let paths = process.argv.length > 2 ? process.argv.slice(2) : [];
paths.push('./package.json');

paths.forEach(path => {
  pack = require(path);
pack.version = newVer;
require('fs').writeFileSync(path, JSON.stringify(pack, null, '\t'));
})
