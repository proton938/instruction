module.exports = {
	"globDirectory": "dist",
	"globPatterns": [
	  "**/*.{txt,png,gif,ico,eot,svg,woff2,ttf,woff,jpg,html,js,css}"
	],
	"swDest": "dist/sw.js",
	"maximumFileSizeToCacheInBytes": 7000000,
	"importWorkboxFrom": "local",
	"cleanupOutdatedCaches": true
  };
  