export interface IXmppServiceSettings {
   xmpp_BOSH_SERVICE: string; // Адрес сервиса BOSH для работы по протоколу HTTP
   xmpp_sender_jid: string; // JID аккаунта для рассылки
   xmpp_sender_pswd: string; // Пароль аккаунта для рассылки
   xmpp_start_timeout_sec: number; // Таймаут в секундах ожидания соединения
   xmpp_close_timeout_sec: number; // Таймаут в секундах простоя (после выполнения последней рассылки)
   xmpp_domain: string; // домен для формирования аккаунтов пользователей
   xmpp_switch: string; // on/off включить/выключить рассылку уведомлений
}
