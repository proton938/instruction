import {XmppErrCode} from "./XmppErrCode";

export class XmppError {
   code: XmppErrCode;
   message: string;

   constructor(code: XmppErrCode = XmppErrCode.ERR_UNDEFINED, message: string = "") {
      this.code = code;
      this.message = message;
   }
}
