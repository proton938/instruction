import Strophe from '@reinform-cdp/legacy-libs/strophe.js';
import {XmppError} from './XmppError';
import {XmppErrCode} from './XmppErrCode';
import {IXmppServiceSettings} from './IXmppServiceSettings';
import * as _ from 'lodash';
import {Injectable} from '@angular/core';
import {Observable, Observer} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class XmppService {
   xmpp_connection = null;
   xmpp_last_use: Date;
   settings: IXmppServiceSettings;

   constructor() {
   }

   setSetting(settings: IXmppServiceSettings) {
      this.settings = settings;
   }

   notify(receivers: string[], message: string): Observable<void> {
      return new Observable((observer: Observer<void>) => {
        const _receivers = _.map(receivers, r => `${r}@${this.settings.xmpp_domain}`);
        if (this.xmpp_connection && this.xmpp_connection.connected) {
          this.send(_receivers, message);
          observer.next(null);
        } else {
          this.connection().subscribe(() => {
            this.send(_receivers, message);
            observer.next(null);
          }, error => {
            console.log(error);
            observer.error(error);
          });
        }
      });
   }

   connection(): Observable<void> {
     return new Observable((observer: Observer<void>) => {
       this.xmpp_connection = new Strophe.Strophe.Connection(this.settings.xmpp_BOSH_SERVICE);
       this.xmpp_connection.connect(this.settings.xmpp_sender_jid, this.settings.xmpp_sender_pswd, (status) => {
         if (status === Strophe.Strophe.Status.ERROR) {
           console.log('ERROR');
           observer.error(new XmppError(XmppErrCode.ERR_NO_CONNECT, 'Не удалось установить соединение с сервером рассылки'));
         } else if (status === Strophe.Strophe.Status.CONNECTING) {
           console.log('CONNECTING');
         } else if (status === Strophe.Strophe.Status.CONNFAIL) {
           console.log('CONNFAIL');
           observer.error(new XmppError(XmppErrCode.ERR_NO_CONNECT, 'Не удалось установить соединение с сервером рассылки'));
         } else if (status === Strophe.Strophe.Status.AUTHENTICATING) {
           console.log('AUTHENTICATING');
         } else if (status === Strophe.Strophe.Status.AUTHFAIL) {
           console.log('AUTHFAIL');
           observer.error(new XmppError(XmppErrCode.ERR_AUTH, 'Ошибка авторизации на сервере рассылки'));
         } else if (status === Strophe.Strophe.Status.CONNECTED) {
           console.log('CONNECTED');
           this.xmpp_connection.send(Strophe.$pres());
           this.xmpp_last_use = new Date();
           this.close();
           observer.next(null);
         } else if (status === Strophe.Strophe.Status.DISCONNECTED) {
           console.log('DISCONNECTED');
         } else if (status === Strophe.Strophe.Status.DISCONNECTING) {
           console.log('DISCONNECTING');
         } else if (status === Strophe.Strophe.Status.ATTACHED) {
           console.log('ATTACHED');
         } else if (status === Strophe.Strophe.Status.REDIRECT) {
           console.log('REDIRECT');
         } else if (status === Strophe.Strophe.Status.CONNTIMEOUT) {
           observer.error(new XmppError(XmppErrCode.ERR_TIMEOUT, 'Сервер рассылки уведомлений не отвечает'));
         } else {
           console.log('UNKNOWN');
           observer.error(new XmppError(XmppErrCode.ERR_UNKNOWN, 'Не удалось установить соединение с сервером рассылки'));
         }
       }, this.settings.xmpp_start_timeout_sec);
     });
   }

   close() {
      if (this.settings.xmpp_close_timeout_sec > 0) {
         const curTime = new Date();
         const checkTimeMsec = this.xmpp_last_use.getTime() + (this.settings.xmpp_close_timeout_sec * 1000) - curTime.getTime();
         if (checkTimeMsec > 0) {
            setTimeout(this.close.bind(this), checkTimeMsec);
         } else {
            if (this.xmpp_connection && this.xmpp_connection.connected) {
               this.xmpp_connection.disconnect();
               console.log('disconeted');
            }
         }
      }
   }

   send(receivers: string[], message: string) {
      this.xmpp_last_use = new Date();
      for (let i = 0; i < receivers.length; i++) {
         const m = Strophe.$msg({to: receivers[i],
            from: this.settings.xmpp_sender_jid,
            type: 'chat'
         }).c('body').t(message);
         this.xmpp_connection.send(m);
      }
   }
}
