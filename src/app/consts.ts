export const dayInMs = 24 * 60 * 60 * 1000;
export const sevenDaysInMs = 7 * dayInMs;
