import { CoreModule } from '@reinform-cdp/core';
import { NgxMaskModule } from 'ngx-mask'
import { DropzoneModule } from "ngx-dropzone-wrapper";
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import {TextMaskModule} from 'angular2-text-mask';
// import {DropdownModule} from "ngx-dropdown";
import {ClickOutsideModule} from 'ng-click-outside';
import {
  BsDatepickerModule, BsDropdownModule, ButtonsModule, PaginationModule, TabsModule,
  TimepickerModule
} from 'ngx-bootstrap';
import { ShowcaseBuilderModule } from '@reinform-cdp/showcase-builder';
import * as angular from 'angular';
import { AppComponent } from './app.component';
import { ModuleWithProviders, Compiler, CompilerFactory, COMPILER_OPTIONS, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AttachFileModalComponent } from "./components/commons/attach-file-modal/attach-file-modal.component";
import { states } from './states.config';
import { UIRouterUpgradeModule } from '@uirouter/angular-hybrid';
import { WidgetsModule } from '@reinform-cdp/widgets';
import { FormsModule } from '@angular/forms';
import { LaddaModule } from 'angular2-ladda';
import { CdpBpmComponentsModule } from '@reinform-cdp/bpm-components';
import { ToastrModule } from 'ngx-toastr';
import { SearchResourceModule, SolarResourceServiceProvider } from '@reinform-cdp/search-resource';
import { NgSelectModule } from '@ng-select/ng-select';
import { BlockUIModule } from 'ng-block-ui';
import { CreateOrderComponent } from './components/order/create-order/create-order.component';
import { OrderNsiService } from './services/order-nsi.service';
import { OrderActivitiService } from './services/order-activiti.service';
import { OrderDictsService } from "./services/order-dicts.service";
import { NsiResourceServiceProvider } from "@reinform-cdp/nsi-resource";
import { OrderDocumentService } from "./services/order-document.service";
import { OrderEditorComponent } from './components/order/order-editor/order-editor.component';
import { AddThemeModalComponent } from './components/order/add-theme/add-theme.modal';
import { OrderModalService } from "./services/order-modal.service";
import { FileResourceModule, FileResourceServiceProvider } from "@reinform-cdp/file-resource";
import { CdpReporterPreviewServiceProvider, CdpReporterResourceModule } from "@reinform-cdp/reporter-resource";
import { OrderCardComponent } from './components/order/order-card/order-card.component';
import { OrderDesktopCardComponent } from './components/order/order-card/desktop/desktop.component';
import { MemoRestService } from "./services/memo-rest.service";
import { MemoActivitiService } from "./services/memo-activiti.service";
import { FileService } from "./services/file.service";
import { InstructionRestService } from "./services/instruction-rest.service";
import './extensions';
import { OrderResultsComponent } from './components/order/order-card/order-results/order-results.component';
import { OrderCommentsComponent } from './components/order/order-card/order-comments/order-comments.component';
import { AddCommentModalComponent } from './components/order/order-card/order-comments/add-comment.modal';
import { AddEmployeeModalComponent } from "./components/order/tasks/review-mka-doc/add-employee/add-employee.modal"
import { PlanDateHistoryComponent } from './components/order/plan-date-history/plan-date-history.component';
import { OrderCardMenuComponent } from './components/order/order-card/order-card-menu/order-card-menu.component';
import { CopyOrderModalComponent } from './components/order/order-card/order-card-menu/copy-order/copy-order.modal';
import { CopyOrderComponent } from './components/order/copy-order/copy-order.component';
import { CancelOrderModalComponent } from './components/order/order-card/order-card-menu/cancel-order/cancel-order.modal';
import { EditOrderModalComponent } from './components/order/order-card/order-card-menu/edit-order/edit-order.modal';
import { ReCreateOrderModalComponent } from './components/order/order-card/order-card-menu/re-create-order/re-create-order.modal';
import { CreateLinkedOrderComponent } from './components/order/create-linked-order/create-linked-order.component';
import { CreateSubTaskComponent } from './components/order/create-sub-task/create-sub-task.component';
import { OrderSubtasksComponent } from './components/order/order-card/order-subtasks/order-subtasks.component';
import { OrderSubtasksDocComponent } from './components/order/order-card/order-subtasks/order-subtasks-doc.component';
import { OrderMobileCardComponent } from './components/order/order-card/mobile/mobile.component';
import { MemoEditorComponent } from './components/memo/memo-editor/memo-editor.component';
import { MemoMkaShowcaseComponent } from './components/memo/memo-showcase/memo-mka-showcase.component';
import { CreateMemoComponent } from './components/memo/create-memo/create-memo.component';
import { MainOrderInfoComponent } from './components/commons/main-order-info/main-order-info.component';
import { SelectLdapUsersComponent } from './components/commons/select-ldap-users/select-ldap-users.component';
import { MemoCardComponent } from './components/memo/memo-card/memo-card.component';
import { DocumentsLogComponent } from './components/commons/documents-log/documents-log.component';
import { MemoCommentsComponent } from './components/memo/memo-card/memo-comments/memo-comments.component';
import { OrderJsonEditorComponent } from './components/order/order-card/order-json-editor/order-json-editor.component';
import { MemoJsonEditorComponent } from './components/memo/memo-card/memo-json-editor/memo-json-editor.component';
import { MemoCardMenuComponent } from './components/memo/memo-card/memo-card-menu/memo-card-menu.component';
import { SendMailModalComponent } from './components/memo/memo-card/memo-card-menu/send-mail/send-mail.modal';
import { CopyMemoComponent } from './components/memo/copy-memo/copy-memo.component';
import { CreateOrderFromMemoComponent } from './components/order/create-order-from-memo/create-order-from-memo.component';
import { RenameFileModalComponent } from './components/commons/rename-file/rename-file.modal';
import { DropzoneComponent } from './components/commons/dropzone/dropzone.component';
import { CreateOrderFromMeetingComponent } from './components/order/create-order-from-meeting/create-order-from-meeting.component';
import { ApprovalListAddComponent } from './components/memo/tasks/approval-list-add/approval-list-add.component';
import { AddApproversModalComponent } from './components/memo/tasks/add-approvers/add-approvers.modal';
import { ReviewMemoComponent } from './components/memo/tasks/review-memo/review-memo.component';
import { AcceptMemoComponent } from './components/memo/tasks/accept-memo/accept-memo.component';
import { ApproveMemoComponent } from './components/memo/tasks/approve-memo/approve-memo.component';
import { ReworkMemoComponent } from './components/memo/tasks/rework-memo/rework-memo.component';
import { ScanMemoComponent } from './components/memo/tasks/scan-memo/scan-memo.component';
import { RegisterMemoComponent } from './components/memo/tasks/register-memo/register-memo.component';
import { ReviewMemoMKADocComponent } from "./components/memo/tasks/review-mka-doc/review-mka-doc.component";
import { IssueAssignmentComponent } from './components/order/tasks/issue-assignment/issue-assignment.component';
import { ReviewAssignmentComponent } from './components/order/tasks/review-assignment/review-assignment.component';
import { AcceptExecutionComponent } from './components/order/tasks/accept-execution/accept-execution.component';
import { CorrectAssignmentComponent } from './components/order/tasks/correct-assignment/correct-assignment.component';
import { AcceptIssueAssignmentComponent } from './components/order/tasks/accept-issue-assignment/accept-issue-assignment.component';
import { ShowcaseDesktopComponent } from './components/showcase/desktop/desktop.component';
import { AssigmentListComponent } from './components/list/assigment-list/assigment-list.component';
import { AssigmentListFiltersComponent } from './components/list/assigment-list/assigment-list-filters.component';
import { MemoListFiltersComponent } from './components/memo/list/memo-list-filters.component';
import { MemoShowcaseComponent } from './components/memo/memo-showcase/memo-showcase.component';
import { MemoListComponent } from './components/memo/list/memo-list.component';
import { ShowcaseMobileComponent } from './components/showcase/mobile/mobile.component';
import { AssignmentListMobileComponent } from './components/list/assigment-list/mobile/mobile.component';
import { MobileSelectComponent } from './components/commons/mobile-select/mobile-select.component';
import { ShowcaseComponent } from './components/showcase/showcase.component';
import { ReviewMKADocComponent } from "./components/order/tasks/review-mka-doc/review-mka-doc.component";
import { OrderReviewComponent } from "./components/order/order-card/order-review/order-review.component";
import { OrderCoexecutorComponent } from "./components/order/order-card/order-coexecutor/order-coexecutor.component";
import { OrderForInformationComponent } from "./components/order/order-card/order-for-information/order-for-information.component";
import { ApprovalListComponent } from './components/commons/approval-list/approval-list.component';
import { ApprovalListEditComponent } from './components/commons/approval-list-edit/approval-list-edit.component';
import { MobileApprovalListComponent } from './components/commons/approval-list/mobile/mobile.component';
import { DesktopApprovalListComponent } from './components/commons/approval-list/desktop/desktop.component';
import { CreateOrderFromTaskComponent } from './components/order/create-order-from-task/create-order-from-task.component';
import { XmppService } from './xmpp/XmppService';
import { ExcelService } from "./services/excel.service";
import * as JSONEditor from 'jsoneditor';
import { ShowcaseInstructionsComponent } from "./components/showcase/instructions/instructions.component";
import { MemoVisaCopyComponent } from "./components/memo/memo-card/memo-visa-copy/memo-visa-copy.component";
import { CreateMemoShortComponent } from './components/memo/create-memo-short/create-memo-short.component';
import { CreateMemoFullComponent } from './components/memo/create-memo-full/create-memo-full.component';
import { ScanSignedMemoComponent } from './components/memo/tasks/scan-signed-memo/scan-signed-memo.component';
import {setAngularJSGlobal, UpgradeModule} from '@angular/upgrade/static';
import {UrlService, TransitionService, Transition} from '@uirouter/core';
import {SkeletonModule} from '@reinform-cdp/skeleton';
import {CdpLoggerModule} from '@reinform-cdp/logger';
import {MobileMemoListComponent} from "./components/memo/memo-showcase/mobile/mobile-memo-list/mobile-memo-list.component";
import {ShowcaseMemoMobileComponent} from "./components/memo/memo-showcase/mobile/mobile.component";
import {BsModalService, BsModalRef} from "ngx-bootstrap/modal";
import {AddEmployeeModalMemoComponent} from "./components/memo/tasks/review-mka-doc/add-employee/add-employee.modal";
import { ApprovalListHistoryComponent } from './components/commons/approval-list-history/approval-list-history.component';
import { MemoEditComponent } from './components/memo/memo-edit/memo-edit.component';
import {JitCompilerFactory} from '@angular/platform-browser-dynamic';
import {CheckboxSigningElectronicallyComponent} from './components/commons/controls/checkbox-signing-electronically/checkbox-signing-electronically.component';
import { BtnRevokeExecutionComponent } from './components/commons/btn-revoke-execution/btn-revoke-execution.component';
import {CloseMemoConfirmModalComponent} from './components/order/tasks/accept-issue-assignment/close-memo-confirm-modal.component';
declare let document: any;
import {SdoComponents} from '@sdo/components';
import {RedirectMemoModalComponent} from './components/memo/tasks/review-memo/redirect-memo/redirect-memo.modal';
import {FormRenameFileModalComponent} from "./components/forms/rename-file/form-rename-file-modal.component";
import {OrderRepeatEditorComponent} from './components/order/order-editor/order-repeat-editor.component';
import {CommonSearchComponent} from "./components/showcase/parts/common-search/common-search.component";
import {ShowcaseMemoDesktopComponent} from "./components/memo/memo-showcase/desktop/desktop.component";
import {ShowcaseSearchResultTableComponent} from "./components/showcase/parts/showcase-search-result-table/showcase-search-result-table.component";
import {ShowcaseSearchResultTableCellComponent} from "./components/showcase/parts/showcase-search-result-table-cell/showcase-search-result-table-cell.component";
import {ShowcasePagingComponent} from "./components/showcase/parts/showcase-paging/showcase-paging.component";
import {ShowcaseFilterComponent} from "./components/showcase/parts/showcase-filter/showcase-filter.component";
import {SelectAsyncComponent} from "./components/commons/select-async/select-async.component";
import {RadioComponent} from "./components/commons/radio/radio.component";
import {CheckboxComponent} from "./components/commons/checkbox/checkbox.component";
import {CreateInstructionFromOrderComponent} from "./components/order/create-from-order/create-from-order.component";
setAngularJSGlobal(angular);

export function createCompiler(compilerFactory: CompilerFactory) {
  return compilerFactory.createCompiler();
}

@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    LaddaModule,
    ShowcaseBuilderModule,
    ToastrModule.forRoot(),
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    UIRouterUpgradeModule.forRoot({ states: states }),
    WidgetsModule.forRoot(),
    CdpBpmComponentsModule.forRoot(),
    SearchResourceModule.forRoot(),
    FileResourceModule.forRoot(),
    NgSelectModule,
    TooltipModule.forRoot(),
    ButtonsModule.forRoot(),
    BlockUIModule.forRoot(),
    NgxMaskModule.forRoot(),
    DropzoneModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    CoreModule.forRoot('SDO', '/main/', 'Главная', '/app/sdo/bpm'),
    CdpReporterResourceModule.forRoot(),
    TextMaskModule,
    SkeletonModule.forRoot(),
    CdpLoggerModule.forRoot(),
    SdoComponents.forRoot(),
    ClickOutsideModule
  ],
  declarations: [
    AppComponent,
    CreateOrderComponent,
    OrderEditorComponent,
    OrderRepeatEditorComponent,
    AddThemeModalComponent,
    OrderCardComponent,
    OrderDesktopCardComponent,
    OrderResultsComponent,
    OrderCommentsComponent,
    AddCommentModalComponent,
    AddEmployeeModalMemoComponent,
    AddEmployeeModalComponent,
    PlanDateHistoryComponent,
    OrderCardMenuComponent,
    CopyOrderModalComponent,
    CopyOrderComponent,
    CancelOrderModalComponent,
    EditOrderModalComponent,
    ReCreateOrderModalComponent,
    CreateLinkedOrderComponent,
    CreateSubTaskComponent,
    OrderSubtasksComponent,
    OrderReviewComponent,
    OrderCoexecutorComponent,
    OrderForInformationComponent,
    OrderSubtasksDocComponent,
    OrderMobileCardComponent,
    MemoEditorComponent,
    CreateMemoComponent,
    CreateMemoShortComponent,
    CreateMemoFullComponent,
    MemoMkaShowcaseComponent,
    SelectLdapUsersComponent,
    AttachFileModalComponent,
    MemoCardComponent,
    DocumentsLogComponent,
    MemoCommentsComponent,
    ReviewMemoMKADocComponent,
    OrderJsonEditorComponent,
    MemoJsonEditorComponent,
    MemoCardMenuComponent,
    SendMailModalComponent,
    CopyMemoComponent,
    CreateOrderFromMemoComponent,
    RenameFileModalComponent,
    DropzoneComponent,
    CreateOrderFromMeetingComponent,
    AddApproversModalComponent,
    ApprovalListAddComponent,
    ReviewMemoComponent,
    AcceptMemoComponent,
    ApproveMemoComponent,
    ReworkMemoComponent,
    ScanMemoComponent,
    RegisterMemoComponent,
    IssueAssignmentComponent,
    ReviewAssignmentComponent,
    AcceptExecutionComponent,
    ReviewMKADocComponent,
    CorrectAssignmentComponent,
    AcceptIssueAssignmentComponent,
    CreateSubTaskComponent,
    ShowcaseDesktopComponent,
    AssigmentListComponent,
    AssigmentListFiltersComponent,
    MemoShowcaseComponent,
    MemoListComponent,
    MemoListFiltersComponent,
    ShowcaseMobileComponent,
    AssignmentListMobileComponent,
    MobileSelectComponent,
    ShowcaseComponent,
    ApprovalListComponent,
    MobileApprovalListComponent,
    DesktopApprovalListComponent,
    ApprovalListEditComponent,
    MainOrderInfoComponent,
    CreateOrderFromTaskComponent,
    ShowcaseInstructionsComponent,
    MemoVisaCopyComponent,
    ScanSignedMemoComponent,
    MobileMemoListComponent,
    ShowcaseMemoMobileComponent,
    MemoVisaCopyComponent,
    ScanSignedMemoComponent,
    ApprovalListHistoryComponent,
    MemoEditComponent,
    CheckboxSigningElectronicallyComponent,
    BtnRevokeExecutionComponent,
    CloseMemoConfirmModalComponent,
    RedirectMemoModalComponent,
    FormRenameFileModalComponent,
    ShowcaseMemoDesktopComponent,
    CommonSearchComponent,
    ShowcaseSearchResultTableComponent,
    ShowcaseSearchResultTableCellComponent,
    ShowcasePagingComponent,
    ShowcaseFilterComponent,
    SelectAsyncComponent,
    RadioComponent,
    CheckboxComponent,
    CreateInstructionFromOrderComponent
  ],
  exports: [AppComponent, CdpBpmComponentsModule],
  entryComponents: [
    AddApproversModalComponent, AddThemeModalComponent, AddCommentModalComponent,
    AddEmployeeModalComponent, CancelOrderModalComponent, CopyOrderModalComponent,
    EditOrderModalComponent, SendMailModalComponent, AttachFileModalComponent, RenameFileModalComponent,
    ReCreateOrderModalComponent,  AssigmentListComponent,
    CloseMemoConfirmModalComponent, RedirectMemoModalComponent, FormRenameFileModalComponent
  ],
  providers: [
    {provide: COMPILER_OPTIONS, useValue: {useJit: true}, multi: true},
    {
      provide: CompilerFactory,
      useClass: JitCompilerFactory,
      deps: [COMPILER_OPTIONS]
    },
    {provide: Compiler, useFactory: createCompiler, deps: [CompilerFactory]}
  ]
})
export class InstructionSystem {
  constructor(private upgrade: UpgradeModule) { }
  ngDoBootstrap() {
    this.upgrade.bootstrap(document, ['instruction.system'], { strictDi: false });
  }
}


export const ng1Module = angular.module('instruction.system', [
  'ui.router',
  'ui.router.upgrade',
  'cdp.skeleton',
  'cdp.core',
  'cdp.bpm.components',
  'cdp.widgets',
  'cdp.logger',
  'cdp.security'])
  .config(['cdpNsiResourceServiceProvider', (nsiResourceServiceProvider: NsiResourceServiceProvider) => {
    nsiResourceServiceProvider.root = '/mdm/api/v1';
  }])
  .config(['cdpFileResourceServiceProvider', (fileResourceServiceProvider: FileResourceServiceProvider) => {
    fileResourceServiceProvider.root = '/filestore/v1';
  }])
  .config(['cdpSolarResourceServiceProvider', (solarResourceServiceProvider: SolarResourceServiceProvider) => {
    solarResourceServiceProvider.root = '/app/sdo/search';
  }])
  .config(['cdpReporterPreviewServiceProvider', (cdpReporterPreviewServiceProvider: CdpReporterPreviewServiceProvider) => {
    cdpReporterPreviewServiceProvider.rootReporter = '/reporter/v1';
  }]);


// перезагрузать, когда обновится кеш сервис-воркера ( workbox). смотри index.html
ng1Module.run([
  '$transitions',
  '$q',
  function($transitions: TransitionService, $q: ng.IQService) {
    $transitions.onStart({}, function(trans: Transition) {
      if (window.sessionStorage && window.sessionStorage['cdp-reload']) {
        delete window.sessionStorage['cdp-reload'];
       // debugger;
        //window.location.reload();
        console.log('New version arrived');
      } else {
        trans.options().reload = false;
      }
    });
  }
]);

