import {NsiResourceService, UserBean} from '@reinform-cdp/nsi-resource'
import * as _ from 'lodash';
import {StateService} from '@uirouter/core';
import {ToastrService} from 'ngx-toastr';
import {Component, ViewEncapsulation} from '@angular/core';
import {AssigmentListComponent} from '../assigment-list.component';
import {SearchResultDocument} from '@reinform-cdp/search-resource';
import {SessionStorage} from '@reinform-cdp/security';
import {formatDate} from '@angular/common';
import {ExcelService} from '../../../../services/excel.service';
import { InstructionFilterService } from '../../../../services/instruction-filter.service';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {LinkRestService} from "../../../../services/link-rest.service";

export interface MobileSortings {
  id: number;
  direction: string;
  name: string;
}

export interface ContentTable {
  id: string;
  title: string;
  fn: (data: any) => string;
}

@Component({
  selector: 'mggt-assignment-list-mobile',
  templateUrl: './mobile.component.html',
  encapsulation: ViewEncapsulation.None
})
export class AssignmentListMobileComponent extends AssigmentListComponent {

  mobileSortings: MobileSortings[];
  isDocsLoaded: boolean = false;
  filtersCount: number = 0;
  contentTable: ContentTable[];
  appendBlock: boolean = false;
  extSearchCollapsed = true;

  cancelation;
  authors: UserBean[] = [];
  assistants: UserBean[] = [];
  executors: UserBean[] = [];
  coExecutors: UserBean[] = [];
  forInformationLogins: UserBean[] = [];

  constructor($state: StateService,
              solrMediator: SolrMediatorService,
              session: SessionStorage,
              toastr: ToastrService,
              private nsiResourceService: NsiResourceService,
              excelService: ExcelService,
              filterService:InstructionFilterService,
              linkService: LinkRestService) {
    super($state, solrMediator, session, toastr, excelService,filterService, linkService);
    this.mobileSortings = [
      {id: 1, direction: 'asc', name: 'Сначала старые'},
      {id: 1, direction: 'desc', name: 'Сначала новые'},
      {id: 7, direction: 'asc', name: 'Срок исполнения раньше'},
      {id: 7, direction: 'desc', name: 'Срок исполнения позже'}
    ];
    this.contentTable = [
      {id: 'instructionNumber', title: 'Номер', fn: data => data},
      {id: 'creatorFioFull', title: 'Автор', fn: data => data},
      {id: 'executorFioFull', title: 'Отв.', fn: data => data},
      {id: 'reportDate', title: 'Факт', fn: data => {
        return data && data.length ? formatDate(_.last(data), 'dd.MM.yyyy', 'en') : ''
      }}
    ]

    if (localStorage.getItem('assigment') !== null) {
      let documentId = localStorage.getItem('assigment');
      let url = `/sdo/instruction/#/app/instruction/instruction/card/${documentId}`;
      window.open(url, '_self');
      localStorage.removeItem('assigment');
    }

  }

  saveLinkForNew() {
    if (localStorage.getItem('onSaveLink') === null) {
      localStorage.setItem('newAssigment', 'new');
      localStorage.setItem('onSaveLink', 'on');
    }
  }

  mobileChangeSorting(sort: MobileSortings): void {
    let baseSort = this.sortings[sort.id];
    if (baseSort.value == sort.direction) {
      return;
    }
    baseSort.value = sort.direction === 'asc' ? 'desc' : 'asc';
    this.sortingChanged(baseSort);
  }

  mobileLoadMore() {
    if (this.isDocsLoaded) {
      return;
    }
    this.appendBlock = true;
    let searchRequest = _.clone(this.searchRequest);
    searchRequest.page = (this.searchResult.docs.length / this.searchResult.pageSize);
    this.solrMediator.query(searchRequest).subscribe(response => {
      let loadedDocs = <SearchResultDocument[]>response.docs;
      loadedDocs.map(d => this.extendDoc(d));
      this.searchResult.docs = this.searchResult.docs.concat(loadedDocs);
      this.isDocsLoaded = this.searchResult.docs.length === this.searchResult.numFound;
      this.searchResult.numFound = response.numFound;
      this.searchResult.pageSize = response.pageSize;
      this.appendBlock = false;
    }, error => {
      console.log(error);
      this.appendBlock = false;
    });
  }

  mobileFilterChange() {
    this.filtersCount = this.filter.count();
    this.onFilterChange();
  }

  mobileClearFilters() {
    this.filter.clear();
    this.mobileFilterChange();
  }

  solicitorSearch(data: {search: string, users: UserBean[]}) {
    if (data.search && data.search.length > 2) {
      if (this.cancelation) {
        clearTimeout(this.cancelation);
      }
      this.cancelation = setTimeout(() => {
        this.nsiResourceService.searchUsers({fio: data.search}).then(_response => {
          data.users.length = 0;
          _.forEach(_response, r => {
            data.users.push(r);
          });
        }).catch(error => {
          this.toastr.error('Ошибка при получении информации из справочника!');
        });
      }, 500);
    }
  }

}
