import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsLocaleService } from "ngx-bootstrap/datepicker";
import { UserBean } from '@reinform-cdp/nsi-resource';
import { val } from '@uirouter/core';
import { formatDate } from "@angular/common";

@Component({
    selector: 'assigment-list-filters',
    templateUrl: './assigment-list-filters.component.html',
    styleUrls: ['./assigment-list-filters.component.scss']
})
export class AssigmentListFiltersComponent implements OnInit {
    @Input() filter: any;
    @Input() overdue: {value: boolean};
    @Input() dicts: any;
    @Input() isMka?: boolean;
    @Output() filterChanged: EventEmitter<any> = new EventEmitter();
    @Output() overdueChanged: EventEmitter<any> = new EventEmitter();
    @Output() filterReset: EventEmitter<any> = new EventEmitter();

    isExtendedSearch: boolean = false;

    constructor() {
    }
    test = '';

    ngOnInit() {
        if(!!this.filter&&!!this.filter.isEmpty&&!this.filter.isEmpty())
            this.isExtendedSearch=true;
    }

    onFilterChange() {
        this.filterChanged.emit(this.filter);
    }

    onOverdueChange(value) {
      this.overdueChanged.emit({value: value});
    }

    changeLdapField(user: UserBean, field: string) {
        this.filter[field] = user;
        this.onFilterChange();
    }

    changeCheckbox(value: any, field: string) {
        this.filter[field] = value;
        this.onFilterChange();
    }

    changeDateField(value: any, field: string, direction: string) {
        if (value !== undefined) {
          this.filter[field] = this.filter[field] || {};
          if (value) {
            const date = new Date(value);
            this.filter[field][direction] = formatDate(date, 'yyyy-MM-dd', 'en');
          }
          else  {
            this.filter[field][direction] = undefined
          }
          this.onFilterChange();
        }
    }

    changeTextField(value: any, field: string) {
        console.log(value);
        this.filter[field] = value;
        this.onFilterChange();
    }

    clear() {
        this.filterReset.emit();
    }

    toggleExtendSearch() {
        this.isExtendedSearch = !this.isExtendedSearch;
    }
}
