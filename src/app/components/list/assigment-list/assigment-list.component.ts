import * as _ from 'lodash';
import {SessionStorage} from '@reinform-cdp/security';
import {StateService} from '@uirouter/core';
import {Component, Input, OnInit} from '@angular/core';
import {formatDate} from '@angular/common';
import {
  FacetedSearchResult, QuerySearchData, SearchExtData, SearchExtDataItem, SearchResultDocument
} from '@reinform-cdp/search-resource';
import {forkJoin, from, Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {ShowcaseSorting} from '../../../models/sorting/ShowcaseSorting';
import {ShowcaseInstructionSortingBuilder} from '../../../models/sorting/ShowcaseInstructionSortingBuilder';
import {InstructionFilter} from '../../../models/instruction/InstructionFilter';
import {UserBean} from '@reinform-cdp/nsi-resource';
import {InstructionTableFilter} from '../../../models/table-filters/InstructionTableFilter';
import {ExcelService} from '../../../services/excel.service';
import {InstructionFilterService} from '../../../services/instruction-filter.service';
import {SolrMediatorService} from '../../../services/solr-mediator.service';
import {InstructionDocument} from "../../../models/instruction-document/InstructionDocument";
import {LinkRestService} from "../../../services/link-rest.service";

@Component({
  selector: 'assigment-list',
  templateUrl: './assigment-list.component.html',
  styleUrls: ['./assigment-list.component.scss']
})
export class AssigmentListComponent implements OnInit {
  @Input() filter: InstructionFilter;
  @Input() overdue: {value: boolean};
  @Input() dicts: any;
  @Input() preFilter: any;
  @Input() filterChanged: Subject<any>;
  @Input() isMka?: boolean;
  tableFilter: InstructionTableFilter = new InstructionTableFilter();

  document: InstructionDocument;
  isLoading: boolean;
  searchRequest: SearchExtData;
  searchResult: FacetedSearchResult;
  currentPage: number = 1;
  sortings: ShowcaseSorting[] = [];
  links: any = {};

  constructor(public $state: StateService,
              public solrMediator: SolrMediatorService,
              private session: SessionStorage,
              public toastr: ToastrService,
              public excelService: ExcelService,
              public filterService:InstructionFilterService,
              public linkService: LinkRestService) {
    this.isLoading = true;
  }

  ngOnInit() {

    if(!this.isMka)this.tableFilter = this.filterService.getAssignmentTableFilter();
    let sortingBuilder = new ShowcaseInstructionSortingBuilder();
    this.sortings = sortingBuilder.build();
    this.getSortingFromStorage();
    this.saveSortingToStorage();

    this.filterChanged.observers.length = 0;
    this.filterChanged.subscribe(() => {
      this.onFilterChange();
    });
    this.searchRequest = {
      common: '',
      page: this.currentPage - 1,
      pageSize: 10,
      sort: this.sortingsToString(),
      types: [ this.solrMediator.types.instruction ]
    };
    if (!this.isMka&&!!this.filterService.getAssignmentSearch())
      this.searchRequest.common=this.filterService.getAssignmentSearch();
    this.search();
  }

  saveLink(documentId) {
    if (localStorage.getItem('onSaveLink') === null) {
      localStorage.setItem('assigment', documentId);
      localStorage.setItem('onSaveLink', 'on');
    }
    localStorage.setItem('linkCardOrder', documentId);
  }


  testVar: any = 'test';

  search() {
    this.isLoading = true;
    const req: QuerySearchData = this.searchRequest;
    req.query = this.buildFilter();
    req.sort = this.sortingsToString();
    this.testVar = JSON.stringify(req);
    this.solrMediator.query(req).subscribe(resp => {
      this.searchResult = resp;
      this.searchResult.docs = this.searchResult.docs || [];
      this.searchResult.docs.map(d => this.extendDoc(d));
      this.updateLinks();
      this.isLoading = false;
    }, err => {
      this.toastr.error(err && err.data ? err.data.exception : 'Произошла ошибка!');
      console.log(err);
      this.isLoading = false;
    });
  }

  updateLinks() {
    if (this.searchResult && this.searchResult.docs && this.searchResult.docs.length) {
      this.updateLinksByIds(this.searchResult.docs.map(i => i.documentId).filter(i => !this.links[i]));
    }
  }

  updateLinksByIds(ids: string[] = []) {
    if (ids.length) {
      forkJoin(ids.map(i => this.linkService.findSdoLinks(i))).subscribe(res => {
        if (res && res.length) {
          res.forEach((r, index) => {
            const id = ids[index];
            this.links[id] = r.map(i => {
              const link = i;
              link.targetId = _.find([i.link.id1, i.link.id2], linkId => linkId !== id);
              link.targetType = [i.link.type1, i.link.type2][+(link.targetId === i.link.id2)];
              return link;
            });
          });
        }
      });
    }
  }

  commonSearch(common: string) {
    this.searchRequest.common = common;
    this.searchRequest.page = 0;
    this.search();
  }

  buildFilter(): string {
    let result = '', _filter = '';
    if (this.preFilter) {
      result += this.preFilter;
    }
    if (!this.filter.isEmpty()) {
      this.filterService.login = this.session.login();
      _filter = this.filter.toString(this.filterService.login);
      result += result ? ' AND ' : '';
      result += _filter;
    }

    if (!this.tableFilter.isEmpty()) {
      let _filter = this.tableFilter.toString();
      result += (result) ? " AND " : "";
      result += _filter;
    }
    if (this.overdue && this.overdue.value) {
      result += (result) ? " AND " : "";
      result += `(-factDate:[* TO *] AND -reportDate:[* TO *] AND planDate:[* TO NOW])`
        + ` OR (factDate:[* TO *] AND -reportDate:[* TO *] AND q=\"{!frange}ms(factDate, planDate)\")`
        + ` OR (-factDate:[* TO *] AND reportDate:[* TO *] AND q=\"{!frange}ms(reportDate, planDate)\")`
        + ` OR (factDate:[* TO *] AND reportDate:[* TO *] AND q=\"{!frange}ms(reportDate, planDate)\")`;
    }
    return result;
  }

  changeDateRangeField = function () {
    this.onFilterChange();
  }.bind(this);

  isDateRangeEmpty(obj: any): boolean {
    return obj.from === null && obj.start === null;
  }

  changePage() {
    this.searchRequest.page = this.currentPage - 1;
    this.search();
  }

  clearTableFilter() {
    this.tableFilter.clear();
  }

  onFilterChange() {
    this.searchRequest.page = 0;
    console.log('Change: ', this.searchRequest);
    this.search();
  }

  sortingChanged(s: ShowcaseSorting) {
    this.sortings.forEach(_s => {
      if (s !== _s) {
        _s.value = 'none';
      } else {
        _s.value = (_s.value === 'none') ? 'desc' : ((_s.value === 'desc') ? 'asc' : 'desc');
      }
    });
    this.saveSortingToStorage();
    this.search();
  }

  saveSortingToStorage(): void {
    let sorting = _.find(this.sortings, (s: ShowcaseSorting) => {
      return (s.value === 'desc' || s.value === 'asc');
    });
    localStorage.setItem(`${this.session.login()}.instruction.sorting`, JSON.stringify({
      code: sorting.code,
      value: sorting.value
    }));
  }

  changeLdapField(user: UserBean, field: string) {
    this.filter[field] = user;
    this.onFilterChange();
  }

  getSortingFromStorage(): void {
    let sorting = localStorage[`${this.session.login()}.instruction.sorting`];
    if (sorting) {
      sorting = JSON.parse(sorting);
      if (_.some(this.sortings, s => s.code === sorting.code)) {
        this.sortings.forEach(s => {
          if (s.code === sorting.code) {
            s.value = sorting.value;
          } else {
            if (s.value === 'desc' || s.value === 'asc') {
              s.value = 'none';
            }
          }
        });
      }

    }
  }

  sortingsToString(): string {
    let result = '';
    this.sortings.forEach(s => {
      if (s.value !== 'none') {
        result = `${s.code} ${s.value}`;
      }
    });
    return result;
  }

  extendDoc(d: any) {
    const status = this.dicts.statuses.find(s => s.name === d.statusName);
    const priority = this.dicts.priorities.find(s => s.name === d.priorityName);
    d['statusColor'] = status ? status.color : 'default';
    d['priorityColor'] = priority ? priority.color : 'default';
    d['isOverdue'] = this._isOverdue(d);
    d['isOverdueTwoDays'] = this._isOverdueTwoDays(d);
    if (d['agendaMKAID']) {
      this.solrMediator.getByIds([
        { ids: [d['agendaMKAID']], type: this.solrMediator.types.agenda }
      ]).subscribe(res => {
        const docs = this.solrMediator.getDocs(res);
        if (docs.length) {
          const agenda = docs[0];
          const isExpress = this.dicts.types.find((el) => el.meetingType === agenda['meetingType']).Express;
          d['isExpress'] = isExpress;
        }
      });
    }
  }

  createReport(): any { // it must return the Promise
    const numFound = this.searchResult.numFound;
    const req = this.searchRequest;
    req.pageSize = numFound;

    return this.solrMediator.query(req).toPromise().then(resp => {
      const docs = resp.docs;
      return this.excelService.exportAsExcelFile(docs, 'Отчет');
    }).catch((err) => {
      this.toastr.error(err && err.data ? err.data.exception : 'Произошла ошибка!');
      console.log(err);
    });
  }

  getAcceptDate(item: any): string {
    let r = '';
    if (item && item.acceptDate && item.acceptDate.length) {
      const lastDateString: string = _.last(item.acceptDate)
      let lastDate: Date = new Date(lastDateString);
      if (lastDateString.indexOf('T') < 0) { lastDate.setHours(0, 0, 0) }
      if (lastDate && lastDate.getFullYear() !== 1900) {
        r = formatDate(lastDate, 'dd.MM.yyyy', 'en') +
          "\n" + formatDate(lastDate, 'HH:mm', 'en');
      }
    }
    return r;
  }

  getAcceptStatus(item: any) {
    let r = '';
    if (item && item.acceptStatus && item.acceptStatus.length) {
      r = _.last(item.acceptStatus);
    }
    return r;
  }

  private _isOverdue(d: any): boolean {
    let currentDateMs = new Date().valueOf();
    let planDateInstMs = d.planDate ? new Date(d.planDate).valueOf() : undefined;
    let factDateInstMs = d.factDate ? new Date(d.factDate).valueOf() : undefined;
    let reportDateInst = d.reportDate ? new Date(d.reportDate).valueOf() : undefined;

    return ((!reportDateInst && !factDateInstMs && (planDateInstMs < currentDateMs)) ||
      // (!reportDateInst && factDateInstMs && (planDateInstMs < factDateInstMs)) ||
      (reportDateInst && !factDateInstMs && (planDateInstMs < reportDateInst)));
      // (reportDateInst && factDateInstMs && (planDateInstMs < reportDateInst)));
  }

  private _isOverdueTwoDays(d: any): boolean {
    let currentDate = new Date();
    currentDate.setDate(currentDate.getDate()+2);
    let currentDateMs = currentDate.valueOf();
    let planDateInstMs = d.planDate ? new Date(d.planDate).valueOf() : undefined;
    let factDateInstMs = d.factDate ? new Date(d.factDate).valueOf() : undefined;
    let reportDateInst = d.reportDate ? new Date(d.reportDate).valueOf() : undefined;

    return ((!reportDateInst && !factDateInstMs && (planDateInstMs < currentDateMs)) ||
      (!reportDateInst && factDateInstMs && (planDateInstMs < factDateInstMs)) ||
      (reportDateInst && !factDateInstMs && (planDateInstMs < reportDateInst)) ||
      (reportDateInst && factDateInstMs && (planDateInstMs < reportDateInst)));
  }

  changeDateField(value: any, field: string, direction: string) {
    if (value !== undefined) {
      this.tableFilter[field] = this.tableFilter[field] || {};
      if (value) {
        const date = new Date(value);
        this.tableFilter[field][direction] = formatDate(date, 'yyyy-MM-dd', 'en');
      }
      else  {
        this.tableFilter[field][direction] = undefined
      }
      this.onFilterChange();
    }
  }

}
