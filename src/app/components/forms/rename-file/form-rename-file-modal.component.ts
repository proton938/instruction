import {Component, Input} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import {BlockUI, NgBlockUI} from 'ng-block-ui';

@Component({
  selector: 'form-instruction-rename-file-modal',
  templateUrl: './form-rename-file-modal.component.html'
})
export class FormRenameFileModalComponent {
  @Input() file: any;
  @Input() saveCallback: (result: string) => void;
  @Input() rejectCallback: () => void;

  newName: string = '';

  @BlockUI('modal-block-ui') blockUI: NgBlockUI;


  constructor(private bsModalRef: BsModalRef) {}

  ngOnInit() {
    this.newName = this.file.oldName.substring(0, this.file.oldName.lastIndexOf('.')) + '(' + this.file.numberOfFile + ')';
  }

  rename() {
    if (this.saveCallback) this.saveCallback(this.newName);
    this.bsModalRef.hide();
  }

  cancel() {
    if (this.rejectCallback) this.rejectCallback();
    this.bsModalRef.hide();
  }
}
