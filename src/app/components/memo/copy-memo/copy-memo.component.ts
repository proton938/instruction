import * as angular from "angular";
import * as _ from "lodash";
import {ToastrService} from "ngx-toastr";
import {StateService, Transition} from '@uirouter/core';
import {Component, Inject, OnInit} from '@angular/core';
import {EditMemoModel} from "../../../models/memo/EditMemoModel";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {MemoDocument} from "../../../models/memo-document/MemoDocument";
import {MemoDictsModel} from "../../../models/memo/MemoDictsModel";
import {FileService} from "../../../services/file.service";
import {MemoDictsService} from "../../../services/memo-dicts.service";
import {MemoDocumentService} from "../../../services/memo-document.service";
import {RouterStateWrapperService} from "@reinform-cdp/skeleton";
import {MemoActivitiService} from "../../../services/memo-activiti.service";
import {MemoRestService} from "../../../services/memo-rest.service";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {catchError, mergeMap, tap} from "rxjs/internal/operators";
import {from, throwError} from "rxjs/index";
import {MemoBreadcrumbsService} from "../../../services/memo-breadcrumbs.service";

@Component({
  selector: 'mggt-copy-memo',
  templateUrl: './copy-memo.component.html'
})
export class CopyMemoComponent implements OnInit {

  dicts: MemoDictsModel;
  document: MemoDocument;
  copiedDocument: MemoDocument;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  model: EditMemoModel;

  @BlockUI('newMemo') blockUI: NgBlockUI;

  constructor(private memoRestService: MemoRestService,
              private breadcrumbsService: MemoBreadcrumbsService,
              private memoDictsService: MemoDictsService,
              private memoActivityService: MemoActivitiService,
              private fileService: FileService,
              private memoDocumentService: MemoDocumentService,
              private $state: StateService,
              private toastr: ToastrService,
              private routerStateWrapper: RouterStateWrapperService,
              private transition: Transition,
              private fileHttpService: FileResourceService) {
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    this.memoDictsService.getDicts().pipe(
      mergeMap(response => {
        this.dicts = response;
        this.model = new EditMemoModel();
        this.model.assistant = this.memoDocumentService.getCurrentUser();
        return this.memoRestService.get(this.transition.params()['copiedId']).pipe(
          mergeMap(response => {
            this.copiedDocument = new MemoDocument();
            this.copiedDocument.build(response.document);
            this.document = new MemoDocument();
            this.copyFields(this.copiedDocument);
            return from(this.fileHttpService.getFullFolderInfo(this.copiedDocument.folderId, false)).pipe(
              tap(response => {
                this.fileService.ex.files = response.files.map(f => this.fileService.convertToExFile(f));
              })
            );
          })
        );
      }), tap(response => {
        this.breadcrumbsService.newMemo();
        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      }), catchError(error => {
        this.loadingStatus = LoadingStatus.ERROR;
        return throwError(error);
      })
    ).subscribe();
  }

  private copyFields(document: MemoDocument) {
    this.model.content = document.content;
    this.model.description = document.description;
  }

  create() {
    let ids = [];
    if (this.model.isValid()) {
      this.blockUI.start();
      this.model.updateDocumentFromModel(this.document);
      this.document.beginDate = this.dateWithoutTime(new Date());
      this.memoDocumentService.updateDocumentStatus(this.document, this.dicts.statuses, 'send');
      this.memoDocumentService.createDocument(this.document).pipe(
        mergeMap(response => {
          //Копирование файлов из родительского поручения
          let files = _.map(this.fileService.ex.files, f => f.idFile);
          return this.fileHttpService.copyFiles(files, this.document.folderId)
        }),
        mergeMap(response => {
          ids = _.map(response, f => {
            return f.versionSeriesGuid;
          });

          //Сохранение загруженных файлов
          return this.fileService.syncExFiles(this.document.documentId, this.document.folderId, _ => {}).pipe(
            mergeMap(response => {
              let document = angular.copy(this.document);
              //Сохранение списка файлов в документ
              this.document.fileId = ids.concat(response);
              return this.memoDocumentService.updateDocument(this.document.documentId, document, this.document);
            })
          );
        }),
        // TODO: memo_ReviewMemo_prc
        // mergeMap(() => {
        //   return this.memoActivityService.startProcess('memo_ReviewMemo_prc', this.document);
        // }),
        tap(response => {
          this.goBack();
          this.blockUI.stop();
        }), catchError(error => {
          console.log(error);
          this.blockUI.stop();
          return throwError(error);
        })
      ).subscribe();
    } else {
      this.toastr.warning('Не заполнены обязательные поля!');
    }
  }

  goBack() {
    this.routerStateWrapper.getPrevState().goTo('app.instruction.memo-card', {id: this.document.documentId});
  }

  private dateWithoutTime(date: Date): Date {
    let _date = date.setHours(0, 0, 0, 0).valueOf();
    return new Date(_date);
  }

}
