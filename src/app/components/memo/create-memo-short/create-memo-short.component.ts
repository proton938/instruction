import * as angular from "angular";
import { PlatformConfig } from '@reinform-cdp/core';
import { ToastrService } from "ngx-toastr";
import { Component, Inject, OnInit } from '@angular/core';
import { StateService } from '@uirouter/core';
import { ActivityResourceService, IProcessDefinition, ITaskVariable } from "@reinform-cdp/bpm-components";
import { NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import { EditMemoModel } from "../../../models/memo/EditMemoModel";
import { LoadingStatus } from "@reinform-cdp/widgets";
import { MemoDocument } from "../../../models/memo-document/MemoDocument";
import { MemoDictsModel } from "../../../models/memo/MemoDictsModel";
import { FileService } from "../../../services/file.service";
import { MemoActivitiService } from "../../../services/memo-activiti.service";
import { MemoDictsService } from "../../../services/memo-dicts.service";
import { MemoRestService } from "../../../services/memo-rest.service";
import { MemoDocumentService } from "../../../services/memo-document.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { catchError, mergeMap, tap, map } from "rxjs/internal/operators";
import { Observable, throwError, of, forkJoin, from } from 'rxjs/index';
import { MemoBreadcrumbsService } from "../../../services/memo-breadcrumbs.service";
import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { MemoKind } from "./../../../models/memo/MemoKind";

@Component({
  selector: 'mggt-create-memo-short',
  templateUrl: './create-memo-short.component.html',
  styleUrls: ['./create-memo-short.component.scss']
})
export class CreateMemoShortComponent implements OnInit {

  dicts: MemoDictsModel;
  document: MemoDocument;
  prevDocument: MemoDocument;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;
  model: EditMemoModel;
  memoKinds: MemoKind[];
  memoKindName = null;
  creator = null;
  creators: UserBean[] = [];

  @BlockUI('newShortMemo') blockUI: NgBlockUI;

  constructor(public $state: StateService,
    private memoRestService: MemoRestService,
    private activityResourceService: ActivityResourceService,
    private breadcrumbsService: MemoBreadcrumbsService,
    private memoDictsService: MemoDictsService,
    private memoActivityService: MemoActivitiService,
    private fileService: FileService,
    private memoDocumentService: MemoDocumentService,
    private reporterResourceService: CdpReporterResourceService,
    private toastr: ToastrService,
    public platformConfig: PlatformConfig,
    private nsiResourceService: NsiResourceService) {
  }

  ngOnInit() {
    this.memoDictsService.getDicts().pipe(
      mergeMap((response: any) => {
        this.dicts = response;
        return this.loadDictionaries();
      }),
      mergeMap(() => {
        return this.nsiResourceService.searchUsers({group: ['MGGT_MEMO_VIEW']});
      }),
      tap((res) => {
        this.creators = res;
        this.model = new EditMemoModel();
        this.model.assistant = this.memoDocumentService.getCurrentUser();
        this.document = new MemoDocument();
        this.prevDocument = new MemoDocument();
        this.breadcrumbsService.newMemo();
        this.model.reportTerm = this.dicts.reportTerms.find(rt => rt.Default);
        this.model.signingElectronically = true;
        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      }), catchError(error => {
        this.loadingStatus = LoadingStatus.ERROR;
        return throwError(error);
      })
    ).subscribe();

    this.loadingStatus = LoadingStatus.LOADING;
    this.loadDictionaries().subscribe((result: any) => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, error => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  isValid(): boolean {
    return !!(this.model.creator && this.model.content && this.model.memoKind && this.model.memoKind.name);
  }

  create() {
    if (this.isValid()) {
      this.blockUI.start();
      this._saveDocument('initial').pipe(
        mergeMap(() => {
          const processVariables = [{
            'name': 'EntityIdVar',
            'value': this.document.documentId
          }];
          return this.memoActivityService.startProcess('sdomemo_CreateMemo', this.document, processVariables);
        }),
        mergeMap((processId) => {
          return this.getTaskId(processId);
        }),
        tap((taskId) => {
          this.blockUI.stop();
          this.gotToTask(taskId);
          // this.goToCard();
        }), catchError(error => {
          console.log(error);
          this.blockUI.stop();
          return throwError(error);
        })
      ).subscribe();

    } else {
      this.toastr.warning('Не заполнены обязательные поля!');
    }
  }


  _resolveDocumentState(): Observable<any> {
    if (this.document.documentId) {
      return of([]);

    } else {
      return this.memoDocumentService.createDocumentInitial(this.document);
    }
  }

  _saveDocument(status: string): Observable<any> {

    this.blockUI.start();
    this.model.updateDocumentFromModel(this.document);
    this.memoDocumentService.updateDocumentStatus(this.document, this.dicts.statuses, status);

    return this._resolveDocumentState().pipe(
      mergeMap((response) => {
        return of([]);
      }), catchError(error => {
        console.log(error);
        this.blockUI.stop();
        return throwError(error);
      })
    );
  }

  getTaskId(processId: any): Observable<string> {
    return from(this.activityResourceService.getTasks({ processInstanceId: processId })).pipe(
      map(result => {
        return result.data[0].id;
      })
    );
  }

  gotToTask(taskId) {
    const url = `${window.location.protocol}//${window.location.host}/` +
      `sdo/instruction/#/app/execution/sdomemo/${taskId}/sdomemomemoCreate?systemCode=${this.platformConfig.systemCode.toUpperCase()}`;

    window.location.href = url;
  }

  goToCard() {
    this.$state.go('app.instruction.memo-new-full', { id: this.document.documentId });
  }

  goBack() {
    this.$state.go('app.instruction.showcase-memo');
  }

  memoKindChanged(mk) {
    this.model.memoKind = this.memoKinds.find(memoKind => memoKind.code === mk.code);
    if (mk.code !== '16') {
      this.model.approvalElectronically = false;
    } else {
      this.model.approvalElectronically = true;
    }
    this.refreshModel();
  }

  creatorChanged(creator) {
    this.model.creator = this.creators.find(c => c.accountName === creator.accountName);
    this.refreshModel();
  }

  refreshModel() {
    this.model = this.model ? this.model : null;
  }

  loadDictionaries(): Observable<any> {
    return forkJoin([
      this.nsiResourceService.get('MemoKind')
    ]).pipe(
      map((result: [MemoKind[]]) => {
        this.memoKinds = result[0];
        return true;
      })
    );
  }

  changeCheckbox(value: any) {
    this.model.approvalElectronically = value;
  }

  private dateWithoutTime(date: Date): Date {
    let _date = date.setHours(0, 0, 0, 0).valueOf();
    return new Date(_date);
  }

}
