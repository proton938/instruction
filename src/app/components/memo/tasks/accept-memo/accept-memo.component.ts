import * as angular from 'angular';
import * as _ from 'lodash';
import { StateService } from '@uirouter/core';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SearchResultDocument } from '@reinform-cdp/search-resource';
import { AlertService, LoadingStatus } from '@reinform-cdp/widgets';
import { ActiveTaskService, ActivityResourceService, ITask } from '@reinform-cdp/bpm-components';
import { MemoDictsModel } from '../../../../models/memo/MemoDictsModel';
import { MemoDocument } from '../../../../models/memo-document/MemoDocument';
import { SessionStorage } from '@reinform-cdp/security';
import { MemoModalService } from '../../../../services/memo-modal.service';
import { MemoDocumentService } from '../../../../services/memo-document.service';
import { FileService } from '../../../../services/file.service';
import { OrderDocumentService } from '../../../../services/order-document.service';
import { MemoRestService } from '../../../../services/memo-rest.service';
import { MemoActivitiService } from '../../../../services/memo-activiti.service';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { catchError, mergeMap, tap } from 'rxjs/internal/operators';
import { Observable, from, of, throwError } from 'rxjs';
import { DocumentUser } from '../../../../models/core/DocumentUser';
import { MemoDictsService } from '../../../../services/memo-dicts.service';
import { ExFileType } from '@reinform-cdp/widgets';
import {HelperService} from '../../../../services/helper.service';
import {MemoDocumentResult} from '../../../../models/memo-document/MemoDocumentResult';

@Component({
  selector: 'mggt-accept-memo',
  templateUrl: './accept-memo.component.html',
  styleUrls: ['./accept-memo.component.scss']
})
export class AcceptMemoComponent implements OnInit, OnDestroy {

  document: MemoDocument;
  copyDocument: MemoDocument;
  dicts: MemoDictsModel;
  task: ITask;
  files: ExFileType[];
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;

  relatedMemos: MemoDocument[];
  instructions: SearchResultDocument[];
  instructionDicts: any;
  comment: string = '';
  selectedTab = 0;

  acceptContent = '';
  acceptFile: ExFileType[] = [];

  hideOvers = true;

  resultExpanded = true;
  showLinkedElements = false;

  resultFiles: { [key: string] : ExFileType } = {};

  @BlockUI('acceptMemo') blockUI: NgBlockUI;

  constructor(private fileService: FileService,
    public helper: HelperService,
    private activeTaskService: ActiveTaskService,
    private instructionDocumentService: OrderDocumentService,
    private memoRestService: MemoRestService,
    private memoDictsService: MemoDictsService,
    private memoDocumentService: MemoDocumentService,
    private memoActivityService: MemoActivitiService,
    private memoModalService: MemoModalService,
    private toastr: ToastrService,
    public $state: StateService,
    private session: SessionStorage,
    private activityRestService: ActivityResourceService,
    private nsiRestService: NsiResourceService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.memoDictsService.getDicts();
      }),
      mergeMap(response => {
        this.dicts = response;
        return this.nsiRestService.getDictsFromCache(['InstructionStatus', 'InstructionPriority']);
      }),
      mergeMap(response => {
        this.instructionDicts = {
          statuses: response.InstructionStatus,
          priorities: response.InstructionPriority
        };
        return this.memoActivityService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.memoRestService.get(resp);
      }),
      mergeMap(response => {
        this.document = new MemoDocument();
        this.document.build(response.document);
        this.copyDocument = angular.copy(this.document);

        return this.fileService.getRealInfoAboutFilesByIds(this.document.fileId);
      }),
      mergeMap(response => {
        this.files = response;
        return this.memoDocumentService.getInstructions(this.document.documentId);
      }),
      mergeMap(response => {
        this.instructions = response;
        return this.fileService.updateInfoAboutFilesFromFolder(this.document.folderId);
      }),
      mergeMap(response => {
        this.document.result.forEach(r => {
          if (r.reportFileId) {
            this.fileService.getInfoAboutFilesByIds(r.reportFileId).forEach(f => this.resultFiles[f.idFile] = f);
          }
          if (r.acceptFileId) {
            this.fileService.getInfoAboutFilesByIds(r.acceptFileId).forEach(f => this.resultFiles[f.idFile] = f);
          }
        });

        const relatedIds = this.document.relateId && this.document.relateId.length ? this.document.relateId : [];

        return this.memoDocumentService.getRelatedMemos(relatedIds);
      }),
      tap(response => {
        this.relatedMemos = response;

        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      }),
      catchError(error => {
        console.log(error);
        this.loadingStatus = LoadingStatus.ERROR;
        return throwError(error);
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  getAlt(onCondition: boolean, hintId: string, on?: boolean, condition?: boolean ) {
    if (onCondition) {
      let hint = document.getElementById(hintId);
      if (on) {
        document.addEventListener('mousemove', function (event) {
          hint.style.left = event.pageX + 10 + 'px';
          hint.style.top = event.pageY - window.pageYOffset + 10 + 'px';
        });
        hint.style.display = 'block';

      } else {
        hint.style.left = -1000 + 'px';
        hint.style.top = -1000 + 'px';
        hint.style.display = 'none';
      }
    }
  }

  mail() {
    this.memoModalService.mail(this.document).subscribe(() => {
    }, (error) => {
      console.log(error);
    });
  }

  finishTask() {
    this.helper.beforeUnload.start();
    this.blockUI.start();

    /*if (this.acceptContent.trim() === '') {
      this.toastr.error('Не указан комментарий!');
      this.blockUI.stop();
      return;
    }*/

    this.memoRestService.get(this.document.documentId).subscribe(
      response => {
        this.document.build(response.document);
        let r: MemoDocumentResult = _.find(this.document.result, i => i.processId === (<any>this.task).processInstanceId);
        // #MGGT-2039
        // if (!r) { // Если нет document.result с таким processId = создаем его, и добавляем в конец массива
        //   r = new MemoDocumentResult();
        //   this.document.result.push(r);
        //   r.processId = (<any>this.task).processInstanceId;
        // }
        if (r) {
          r.reportAcceptor = new DocumentUser();
          r.reportAcceptor.login = this.session.login();
          r.reportAcceptor.fioFull = this.session.fullName();
          r.reportAcceptor.post = this.session.post();
          r.acceptDate = new Date();
          r.acceptContent = this.acceptContent;
          r.acceptFileId = this.acceptFile.map(f => f.idFile);
        }

        this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document)
          .pipe(
            mergeMap(() => this._finishTask())
          ).subscribe(
            () => {
              this.helper.beforeUnload.stop();
              window.location.href = '/main/#/app/tasks';
            },
            error => {
              this.helper.beforeUnload.stop();
              this.blockUI.stop();
              console.log(error);
              this.loadingStatus = LoadingStatus.ERROR;
            });
      },
      (error) => {
        this.helper.beforeUnload.stop();
        this.blockUI.stop();
        console.log(error);
        this.loadingStatus = LoadingStatus.ERROR;
      }
    );

  }

  getFiles(files): any[] {
    return this.fileService.getInfoAboutFilesByIds(files);
  }

  private _finishTask(): Observable<any> {
    return from(this.activityRestService.getFormData({ taskId: this.task.id })).pipe(
      mergeMap(response => {
        return this.activityRestService.finishTask(parseInt(this.task.id, 10));
      }), tap(response => {
        this.toastr.success('Задача успешно завершена!');
      }), catchError(error => {
        this.toastr.error('Ошибка при завершении задачи!');
        return throwError(error);
      })
    );
  }

  isActive(index: number): boolean {
    return index === this.selectedTab;
  }

  onTabSelected(index: number) {
    if (this.selectedTab === index) {
      this.hideOvers = !this.hideOvers;
    } else {
      this.hideOvers = true;
    }
    this.selectedTab = index;
  }

}
