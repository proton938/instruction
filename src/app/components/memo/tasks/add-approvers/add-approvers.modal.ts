import * as _ from "lodash";
import { from } from "rxjs/index";
import { AlertService } from "@reinform-cdp/widgets";
import { BsModalRef } from "ngx-bootstrap";
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { AddedInstructionApprovalListItem } from "../../../../models/instruction/InstructionApprovalList";

@Component({
    selector: 'order-modal-content',
    templateUrl: './add-approvers.modal.html',
    styleUrls: ['./add-approvers.modal.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AddApproversModalComponent implements OnInit {

    agreed: AddedInstructionApprovalListItem[] = [];
    @Input() except: string[];
    @Input() dueDate: Date;
    @Input() approvalNum: string;
    @Input() alreadyAddedCounter: number;

    submit: boolean = false;

    constructor(private bsModalRef: BsModalRef, private alertService: AlertService) {
    }

    ngOnInit() {
        this.agreed = [];
    }

    isValid() {
        return !_.some(this.agreed, _ => {
            return !_.agreedBy || !_.approvalTypeCode;
        });
    }

    add() {
        this.submit = true;
        this.bsModalRef.hide();
    }

    cancel() {
        if (this.agreed.length > 0) {
            from(this.alertService.confirm({
                message: 'Добавленные данные не сохранятся. Продолжить без сохранения?',
                type: 'default',
                okButtonText: 'Да',
                windowClass: 'zindex'
            })).subscribe(_ => {
                this.bsModalRef.hide();
            })
        } else {
            this.bsModalRef.hide();
        }
    }

}
