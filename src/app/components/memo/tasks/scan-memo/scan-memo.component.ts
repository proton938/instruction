import * as angular from "angular";
import * as _ from 'lodash';
import { StateService } from '@uirouter/core';
import { ToastrService } from "ngx-toastr";
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertService, LoadingStatus, ExFileType } from "@reinform-cdp/widgets";
import { ActiveTaskService, ActivityResourceService, ITask } from "@reinform-cdp/bpm-components";
import { MemoDictsModel } from "../../../../models/memo/MemoDictsModel";
import { MemoDocument } from "../../../../models/memo-document/MemoDocument";
import { MemoModalService } from "../../../../services/memo-modal.service";
import { MemoDocumentService } from "../../../../services/memo-document.service";
import { FileService } from "../../../../services/file.service";
import { MemoRestService } from "../../../../services/memo-rest.service";
import { MemoActivitiService } from "../../../../services/memo-activiti.service";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { catchError, mergeMap, tap } from "rxjs/internal/operators";
import { from, throwError } from "rxjs";
import { Observable } from "rxjs/Rx";
import { MemoDictsService } from "../../../../services/memo-dicts.service";
import {HelperService} from '../../../../services/helper.service';

@Component({
  selector: 'mggt-scan-memo',
  templateUrl: './scan-memo.component.html',
  styleUrls: ['./scan-memo.component.scss']
})
export class ScanMemoComponent implements OnInit, OnDestroy {
  document: MemoDocument;
  copyDocument: MemoDocument;
  dicts: MemoDictsModel;
  relatedMemos: MemoDocument[];
  task: ITask;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;

  files: ExFileType[];

  sendingToReview = false;
  executing = false;
  saving = false;

  isFullDescription = false;
  showLinkedElements = false;

  uploading = false;

  @BlockUI('scanMemo') blockUI: NgBlockUI;

  constructor(public fileService: FileService,
    private activeTaskService: ActiveTaskService,
    private memoRestService: MemoRestService,
    private memoDictsService: MemoDictsService,
    private memoDocumentService: MemoDocumentService,
    private memoActivityService: MemoActivitiService,
    private memoModalService: MemoModalService,
    private toastr: ToastrService,
    public $state: StateService,
    private activityRestService: ActivityResourceService,
    private nsiRestService: NsiResourceService,
    private alertService: AlertService,
    public helper: HelperService) {
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.memoDictsService.getDicts();
      }),
      mergeMap(response => {
        this.dicts = response;
        return this.nsiRestService.getDictsFromCache(['InstructionStatus', 'InstructionPriority']);
      }),
      mergeMap(response => {
        return this.memoActivityService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.memoRestService.get(resp);
      }),
      mergeMap((response) => {
        this.document = new MemoDocument();
        this.document.build(response.document);
        this.copyDocument = angular.copy(this.document);

        return this.fileService.updateInfoAboutFilesFromFolder(this.document.folderId);
      }),
      mergeMap(response => {
        this.files = this.fileService.getInfoAboutFilesByIds(this.document.fileId);

        const relatedIds = this.document.relateId && this.document.relateId.length ? this.document.relateId : [];
        return this.memoDocumentService.getRelatedMemos(relatedIds);
      }),

      tap(response => {
        this.relatedMemos = response;

        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      }),
      catchError(error => {
        console.log(error);
        this.loadingStatus = LoadingStatus.ERROR;
        return throwError(error);
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  getAlt(onCondition: boolean, hintId: string, on?: boolean, condition?: boolean ) {
    if (onCondition) {
      let hint = document.getElementById(hintId);
      if (on) {
        document.addEventListener('mousemove', function (event) {
          hint.style.left = event.pageX + 10 + 'px';
          hint.style.top = event.pageY - window.pageYOffset + 10 + 'px';
        });
        hint.style.display = 'block';

      } else {
        hint.style.left = -1000 + 'px';
        hint.style.top = -1000 + 'px';
        hint.style.display = 'none';
      }
    }
  }

  toggleDescription(): void {
    this.isFullDescription = !this.isFullDescription;
  }

  mail() {
    this.memoModalService.mail(this.document).subscribe(() => {
    }, (error) => {
      console.log(error);
    });
  }

  execute() {
    if (this.files.length === 0) {
      this.toastr.error('Не добавлена Отсканированная копия документа');
      return;
    }

    this.executing = true;
    this.blockUI.start();
    this.helper.beforeUnload.start();

    const fileId = this.files.map((f: any) => (f.idFile));
    const fileIdsToDelete = _.difference(this.document.fileId, fileId);
    this.document.fileId = fileId;

    fileIdsToDelete.forEach((id) => {
      this.fileService.deleteFile(id);
    });

    this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document).pipe(
      mergeMap(() => {
        return this._finishTask();
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      window.location.href = '/main/#/app/tasks';
      this.executing = false;
      this.blockUI.stop();
    }, (error) => {
      this.helper.beforeUnload.stop();
      console.log(error);
      this.executing = false;
      this.blockUI.stop();
    });
  }

  sendToReview() {
    if (this.files.length === 0) {
      this.toastr.error('Не добавлена Отсканированная копия документа');
      return;
    }

    this.helper.beforeUnload.start();
    this.sendingToReview = true;
    this.blockUI.start();

    const fileId = this.files.map((f: any) => (f.idFile));
    const fileIdsToDelete = _.difference(this.document.fileId, fileId);
    this.document.fileId = fileId;

    fileIdsToDelete.forEach((id) => {
      this.fileService.deleteFile(id);
    });

    this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document).pipe(
      mergeMap(() => {
        return this._finishTask();
      }),
      tap(() => {
        const processVariables = [{
          'name': 'EntityIdVar',
          'value': this.document.documentId
        }, {
          'name': 'ResponsibleExecutor_VAR',
          'value': this.document.executor.map((ex) => (ex.login)).join(',')
        }, {
          'name': 'OwnerExecutorVar',
          'value': this.document.assistant.login
        }];
        return this.memoActivityService.startProcess('sdomemo_StartMemo_prc', this.document, processVariables);
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      window.location.href = '/main/#/app/tasks';
      this.executing = false;
      this.blockUI.stop();
    }, (error) => {
      this.helper.beforeUnload.stop();
      console.log(error);
      this.sendingToReview = false;
      this.blockUI.stop();
    });
  }

  save() {
    this.helper.beforeUnload.start();
    this.saving = true;
    this.blockUI.start();

    const fileId = this.files.map((f: any) => (f.idFile));
    this.document.fileId = fileId;


    this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.saving = false;
      this.blockUI.stop();
    }, (error) => {
      this.helper.beforeUnload.stop();
      console.log(error);
      this.saving = false;
      this.blockUI.stop();
    });
  }

  cancel() {
    this.alertService.confirm({
      message: 'Вы уверены, что хотите закрыть задачу без сохранения введенных данных?',
      okButtonText: 'Ок'
    }).then(() => {
      this.$state.go('app.instruction.showcase-memo');
      // window.location.href = '/main/#/app/tasks';
    }).catch(() => {
      console.log('cancel');
    });
  }

  deleteDocument({ fileInfo }) {
    let ind = this.files.findIndex(el => el.idFile === fileInfo.idFile);
    this.files.splice(ind, 1);
  }

  private _finishTask(): Observable<any> {
    return from(this.activityRestService.getFormData({ taskId: this.task.id })).pipe(
      mergeMap(response => {
        return this.activityRestService.finishTask(parseInt(this.task.id));
      }), tap(response => {
        this.toastr.success('Задача успешно завершена!');
      }), catchError(error => {
        this.toastr.error('Ошибка при завершении задачи!');
        return throwError(error);
      })
    );
  }
}
