import {Component, OnInit} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";

@Component({
  selector: 'add-employee-modal',
  templateUrl: './add-employee.modal.html'
})
export class AddEmployeeModalMemoComponent implements OnInit {

  theme: string = '';
  submit: boolean;
  employee: any;

  constructor(private bsModalRef: BsModalRef) {
  }

  ngOnInit() {
    this.submit = false;
  }

  create() {
    this.submit = true;
    this.bsModalRef.hide();
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

}
