import * as angular from 'angular';
import * as _ from 'lodash';
import { StateService, Transition } from '@uirouter/core';
import { ActiveTaskService, ITask, ActivityResourceService } from '@reinform-cdp/bpm-components';
import { SearchExtDataItem, SolarResourceService } from '@reinform-cdp/search-resource';
import { LoadingStatus } from '@reinform-cdp/widgets';
import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { catchError, map, mergeMap, tap } from 'rxjs/internal/operators';
import { EMPTY, from, throwError, of } from 'rxjs/index';
import { Observable } from 'rxjs/Rx';
import { OrderDocumentService } from '../../../../services/order-document.service';
import { DocumentUser } from '../../../../models/core/DocumentUser';
import { ToastrService } from 'ngx-toastr';
import { OrderModalService } from './../../../../services/order-modal.service';
import { MemoDocumentReview } from '../../../../models/memo-document/MemoDocumentReview';
import { MemoDocument } from '../../../../models/memo-document/MemoDocument';
import { MemoMkaDocumentService } from '../../../../services/memo-mka-document.service';
import { MemoActivitiService } from '../../../../services/memo-activiti.service';
import { MemoMkaRestService } from '../../../../services/memo-mka-rest.service';
import { MemoDocumentService } from '../../../../services/memo-document.service';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import {HelperService} from '../../../../services/helper.service';

@Component({
  selector: 'mggt-review-mka-doc',
  templateUrl: './review-mka-doc.component.html'
})
export class ReviewMemoMKADocComponent implements OnInit {

  document: MemoDocument;
  reviewBy: MemoDocumentReview[];
  task: ITask;
  docType: string;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  copyDocument: MemoDocument;
  reviewNote = '';
  currentUser: DocumentUser = new DocumentUser();
  listEmpl: any;
  processId: string;
  sentForReviewExpanded = false;
  internalInstructionsExpanded = false;
  instructionDicts: any;

  assignments: any[] = [];

  @BlockUI('ReviewMKADoc') blockUI: NgBlockUI;

  constructor(
    private toastr: ToastrService,
    private activityRestService: ActivityResourceService,
    private activeTaskService: ActiveTaskService,
    public instructionDocumentService: OrderDocumentService,
    public $state: StateService,
    public transition: Transition,
    private memoActivityService: MemoActivitiService,
    private memoMkaRestService: MemoMkaRestService,
    private memoMkaDocumentService: MemoMkaDocumentService,
    private memoModalService: OrderModalService,
    private memoDocumentService: MemoDocumentService,
    private solarResourceService: SolarResourceService,
    private nsiRestService: NsiResourceService,
    public helper: HelperService
  ) {
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    const user = this.memoMkaDocumentService.getCurrentUser();
    this.currentUser.login = user.accountName;
    this.currentUser.fioFull = user.displayName;
    this.currentUser.post = user.post;


    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.memoActivityService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.memoMkaRestService.get(resp);
      }),
      mergeMap(response => {
        this.document = new MemoDocument();
        this.document.build(response.document);
        this.copyDocument = angular.copy(this.document);

        return from(this.nsiRestService.getDictsFromCache(['InstructionStatus', 'InstructionPriority']));
      }),
      mergeMap(response => {
        this.instructionDicts = {
          statuses: response.InstructionStatus,
          priorities: response.InstructionPriority
        };
        return of(this.document);
      }),
      mergeMap(order => {
        return this.memoDocumentService.getInsideInstructions(this.document.documentId, 'memomka');
      }),
      tap(response => {
        this.assignments = response;
      })
    ).subscribe(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, error => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  creteOrder() {
    this.$state.go('app.instruction.instruction-new', { field: 'memoMKAID', id: this.document.documentId, backUrl: window.location.href });
  }

  addEmpl() {
    this.helper.beforeUnload.start();
    this.memoModalService.addEmployee().pipe(
      mergeMap(response => {
        this.listEmpl = response;
        return this.getProcessId('sdomemomka_reviewMkaDocumentHand');
      }),
      mergeMap(response => {
        this.processId = response.data[0].id;
        return this.memoMkaRestService.get(this.document.documentId);
      }),
      mergeMap(response => {
        this.document.build(response.document);
        this.copyDocument = angular.copy(this.document);
        if (this.listEmpl.length > 0) {
          this.listEmpl.forEach(l => {
            const reviewBy = new DocumentUser();
            reviewBy.login = l.accountName;
            reviewBy.post = l.post;
            reviewBy.fioFull = l.displayName;

            const newReview = new MemoDocumentReview();
            newReview.reviewPlanDate = this.task.dueDate;
            newReview.sentReviewBy = this.currentUser;
            newReview.reviewBy = reviewBy;
            this.document.review.push(newReview);

            this.activityRestService.initProcess({
              processDefinitionId: this.processId,
              variables: [
                { 'name': 'EntityIdVar', 'value': this.document.documentId },
                { 'name': 'sdoUserVar', 'value': l.accountName }
              ]
            });
          });
        }
        return this.memoMkaDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document);
      }), catchError(error => {
        console.log(error);
        this.blockUI.stop();
        return throwError(error);
      }), catchError(error => {
        return EMPTY;
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
    }, error => {
      this.helper.beforeUnload.stop();
    });
  }


  finishTask() {
    this.helper.beforeUnload.start();
    this.blockUI.start();
    this.memoMkaRestService.get(this.document.documentId).pipe(
      mergeMap(response => {
        this.document.build(response.document);
        this.copyDocument = new MemoDocument();
        this.copyDocument = angular.copy(this.document);

        let review: MemoDocumentReview = this.document.review.find(r => r.reviewBy.login === this.currentUser.login && !r.factReviewBy.login);

        if (review) {
          review.reviewFactDate = new Date();
          review.reviewNote = this.reviewNote;
          review.factReviewBy = this.currentUser;
        } else {
          review = new MemoDocumentReview();
          review.reviewFactDate = new Date();
          review.reviewNote = this.reviewNote;
          review.factReviewBy = this.currentUser;
          this.document.review.push(review);
        }

        return this.memoMkaDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document);
      }), mergeMap(response => {
        return this.activityRestService.finishTask(parseInt(this.task.id));
      }), tap(response => {
        this.toastr.success('Задача успешно завершена!');
        this.blockUI.stop();
        window.location.href = '/main/#/app/tasks';
      }), catchError(error => {
        this.toastr.error('Ошибка при завершении задачи!');
        console.log(error);
        this.blockUI.stop();
        return throwError(error);
      })
    ).subscribe(response => {
      this.helper.beforeUnload.stop();
    }, error => {
      this.helper.beforeUnload.stop();
      console.log(error);
      this.blockUI.stop();
    });
  }

  getProcessId(name: string): Observable<any> {
    return from(this.activityRestService.getProcessDefinitions({ key: name, latest: true })).pipe(
      map(id => {
        return id;
      })
    );
  }
}
