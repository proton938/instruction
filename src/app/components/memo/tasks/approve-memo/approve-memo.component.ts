import * as angular from 'angular';
import { StateService } from '@uirouter/core';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import {copy} from 'angular';
import { formatDate } from '@angular/common';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { SearchResultDocument } from '@reinform-cdp/search-resource';
import { BsModalService } from 'ngx-bootstrap';
import { AlertService, LoadingStatus, ExFileType } from '@reinform-cdp/widgets';
import { ActiveTaskService, ActivityResourceService, ITask } from '@reinform-cdp/bpm-components';
import { MemoDictsModel } from '../../../../models/memo/MemoDictsModel';
import { MemoDocument } from '../../../../models/memo-document/MemoDocument';
import { SignService, ICertificateInfoEx, SessionStorage } from '@reinform-cdp/security';
import { MemoDocumentService } from '../../../../services/memo-document.service';
import { CdpReporterResourceService, PdfService, WordService, WordReporterService } from "@reinform-cdp/reporter-resource";
import { FileService } from '../../../../services/file.service';
import { MemoRestService } from '../../../../services/memo-rest.service';
import { OrderNsiService } from '../../../../services/order-nsi.service';
import { MemoActivitiService } from '../../../../services/memo-activiti.service';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { catchError, mergeMap, tap, map } from 'rxjs/internal/operators';
import { from, throwError, of, forkJoin } from 'rxjs';
import * as moment_ from "moment";
import { Observable } from 'rxjs/Rx';
import { MemoDictsService } from '../../../../services/memo-dicts.service';
import { FileType } from '../../../../models/instruction/FileType';
import { ApprovalType } from '../../../../models/instruction/ApprovalType';
import { AddApproversModalComponent } from '../add-approvers/add-approvers.modal';
import {HelperService} from '../../../../services/helper.service';
import {
  MemoApprovalAdded,
  MemoApprovalHistory,
  MemoApprovalHistoryList, MemoApprovalListItem, MemoApprovalListItemSignature,
  MemoApprover
} from '../../../../models/memo-document/MemoApproval';
import {MemoDs} from '../../../../models/memo-document/MemoDs';

const moment = moment_;
@Component({
  selector: 'mggt-approve-memo',
  templateUrl: './approve-memo.component.html',
  styleUrls: ['./approve-memo.component.scss']
})
export class ApproveMemoComponent implements OnInit, OnDestroy {
  document: MemoDocument;
  copyDocument: MemoDocument;
  dicts: MemoDictsModel;
  task: ITask;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  tempDraftFile: FileType;
  approvalCycleCopy;

  isFullDescription = false;
  showLinkedElements = false;

  instructions: SearchResultDocument[];
  instructionDicts: any;

  relatedMemos: MemoDocument[];

  loopCounter: number;
  isApproval: boolean;
  isAssent: boolean;
  isAgreed: boolean;
  isLastApprovalListItem: boolean;

  openSignDialog: () => void;
  openMobileSignDialog: () => void;

  openApproveDialog: () => void;
  openMobileApproveDialog: () => void;

  agreeing = false;
  approving = false;
  addingApprovals = false;
  sendingBackToWork = false;

  approvalNote = '';

  files: ExFileType[] = [];

  checkSign: any;

  uploading = false;

  showApprovalList: boolean = false;

  isAllowedAddingApprovals: boolean = true;

  isMobile: boolean = false;

  draftFiles: any[] = [];

  @BlockUI('approveMemo') blockUI: NgBlockUI;

  constructor(public fileService: FileService,
              private activeTaskService: ActiveTaskService,
              private memoRestService: MemoRestService,
              private memoDictsService: MemoDictsService,
              private memoDocumentService: MemoDocumentService,
              private memoActivityService: MemoActivitiService,
              private toastr: ToastrService,
              private session: SessionStorage,
              public $state: StateService,
              private activityRestService: ActivityResourceService,
              private reporterResourceService: CdpReporterResourceService,
              private nsiResourceService: NsiResourceService,
              private signService: SignService,
              private modalService: BsModalService,
              private nsiRestService: NsiResourceService,
              private orderNsiService: OrderNsiService,
              private pdfService: PdfService,
              private wordService: WordService,
              private wordReporter: WordReporterService,
              public helper: HelperService,
              private alertService: AlertService,
              @Inject('$localStorage') public $localStorage: any) {
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.memoDictsService.getDicts();
      }),
      mergeMap(response => {
        this.dicts = response;
        return this.nsiRestService.getDictsFromCache(['InstructionStatus', 'InstructionPriority']);
      }),
      mergeMap(response => {
        this.instructionDicts = {
          statuses: response.InstructionStatus,
          priorities: response.InstructionPriority
        };
        return this.memoActivityService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.memoRestService.get(resp);
      }),
      tap(response => {
        this.document = new MemoDocument();
        this.document.build(response.document);
        this.copyDocument = angular.copy(this.document);
        this.approvalCycleCopy = angular.copy(this.document.approval.approvalCycle);
        let taskOwnerLogin: string;
        // Определяем реального владельца задачи
        if (!!this.task && !!this.task.owner) {
          taskOwnerLogin = this.task.owner; // Владелец делегированной задачи
        } else if (!!this.task && !!this.task.assignee && this.task.assignee !== this.session.login()) {
          taskOwnerLogin = this.task.assignee; // Задача делегирована, но только взята в работу (не перезагружена)
        } else taskOwnerLogin = this.session.login();
        this.loopCounter = this.document.approval.approvalCycle.agreed.findIndex(agree => {
          return agree.agreedBy.accountName === taskOwnerLogin && !agree.approvalResult;
          //this.session.login();
        });
        if (this.loopCounter === undefined||this.loopCounter<0) {
          this.toastr.error('Не удалось определить номер согласующего в листе согласования.');
          this.loadingStatus = LoadingStatus.ERROR;

          return throwError('Не удалось определить номер согласующего в листе согласования.');
        }
        // this.loopCounter = this.document.approval.approvalCycle.agreed.findIndex((agree) => {
        //   return agree.agreedBy.accountName === this.session.login() && !agree.approvalResult;
        // });

        if (this.document.draftFiles) {
          this.draftFiles = copy(this.document.draftFiles);
        }

        const mainApprovals = this.document.approval.approvalCycle.agreed.filter((a) => (a.approvalNum.split('.').length === 1));
        let currentIndex = mainApprovals.findIndex(a => a.agreedBy.accountName === taskOwnerLogin && !a.approvalResult);
        this.isLastApprovalListItem = currentIndex === mainApprovals.length - 1;

        this.tempDraftFile = _.clone(this.document.draftFiles[0]);

        this.isApproval = this.document.approval.approvalCycle.agreed[this.loopCounter].approvalTypeCode === 'approval' && this.isLastApprovalListItem;
        this.isAgreed = !this.isApproval && this.document.approval.approvalCycle.agreed[this.loopCounter].approvalTypeCode === 'agreed';
        this.isAssent = !this.isApproval && this.document.approval.approvalCycle.agreed[this.loopCounter].approvalTypeCode === 'assent';
      }),
      mergeMap(() => {
        const agreed = this.document.approval.approvalCycle.agreed[this.loopCounter];
        if (agreed.approvalPlanDate) {
          return of('');
        }
        return this.fillApprovalPlanDate(agreed);
      }),
      mergeMap(() => {
        if (this.document.approval.approvalCycle.agreed[this.loopCounter].added && this.document.approval.approvalCycle.agreed[this.loopCounter].added.length > 0) {
          this.isAllowedAddingApprovals = false;
        }

        return this.signService.checkSign();
      }),
      mergeMap(res => {
        this.checkSign = res;

        const relatedIds = this.document.relateId && this.document.relateId.length ? this.document.relateId : [];
        return this.memoDocumentService.getRelatedMemos(relatedIds);
      }),
      tap(response => {
        this.relatedMemos = response;
        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      }),
      catchError(error => {
        console.log(error);
        this.loadingStatus = LoadingStatus.ERROR;
        return throwError(error);
      })
    ).subscribe(() => {

    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  getAlt(onCondition: boolean, hintId: string, on?: boolean, condition?: boolean ) {
    if (onCondition) {
      let hint = document.getElementById(hintId);
      if (on) {
        document.addEventListener('mousemove', function (event) {
          hint.style.left = event.pageX + 10 + 'px';
          hint.style.top = event.pageY - window.pageYOffset + 10 + 'px';
        });
        hint.style.display = 'block';

      } else {
        hint.style.left = -1000 + 'px';
        hint.style.top = -1000 + 'px';
        hint.style.display = 'none';
      }
    }
  }


  toggleDescription(): void {
    this.isFullDescription = !this.isFullDescription;
  }

  agreeAndSendForward() {
    this.helper.beforeUnload.start();
    this.agreeing = true;

    switch (this.document.approval.approvalCycle.agreed[this.loopCounter].approvalTypeCode) {
      case 'assent':
        this.processAgreeing();
        this.blockUI.start();
        break;
      case 'agreed':
        if (this.isMobile) {
          this.openMobileSignDialog();
        } else {
          this.openSignDialog();
        }
        break;
    }
  }

  // tslint:disable-next-line: member-ordering
  afterSign = function (result: any) {
    if (result.error) {
      this.agreeing = false;
      return;
    }

    const certEx: ICertificateInfoEx = result.result.certEx;

    if (result.result && result.result.isDss) {
      this.document.draftFiles.forEach((f) => (f.signed = false));
    } else {
      this.document.draftFiles.forEach((f) => (f.signed = true));
    }


    const agreedDs = {
      agreedDsLastName: certEx.lastName,
      agreedDsName: certEx.firstName,
      agreedDsPosition: certEx.position,
      agreedDsCN: certEx.serialNumber,
      agreedDsFrom: moment(certEx.from, 'DD-MM-YYYY').format('YYYY-MM-DD'),
      agreedDsTo: moment(certEx.till, 'DD-MM-YYYY').format('YYYY-MM-DD')
    };

    this.processAgreeing(agreedDs);
  }.bind(this);

  processAgreeing(agreedDs?) {
    this.blockUI.start();
    this.approve(this.document, this.loopCounter, this.task.id, true, {
      approvalNote: this.approvalNote,
      agreedDs
    }).pipe(
      mergeMap(() => {
        return this.fillApprovalPlanDates();
      }),
      mergeMap(() => {
        return this.updateMemo();
      }),
      mergeMap(() => {
        if (this.isLastApprovalListItem) {
          return this.registerMemo();
        }

        return of('');
      }),
      // mergeMap(()=>{
      //   let newApprovals = angular.copy(this.document.approval.approvalCycle.agreed);
      //   const sortByParts = (partsA, partsB) => {
      //     const [firstA, ...restA] = partsA;
      //     const [firstB, ...restB] = partsB;

      //     if (+firstB < +firstA) {
      //       return 1;
      //     } else if (+firstB === +firstA) {
      //       if (partsB.length === partsA.length) {
      //         return sortByParts(restA, restB);
      //       }

      //       if (partsB.length + 1 === partsA.length) {
      //         return sortByParts(restA, restB);
      //       }

      //       return partsB.length < partsA.length ? -1 : 1;
      //     } else {
      //       return -1;
      //     }
      //   };

      //   newApprovals = newApprovals.sort((a, b) => {
      //     const partsA = a.approvalNum.split('.');
      //     const partsB = b.approvalNum.split('.');

      //     return sortByParts(partsA, partsB);
      //   });

      //   return this.memoActivityService.addApprovers(this.task.id, newApprovals);
      // }),
      mergeMap(() => {
        return this.activityRestService.finishTask(parseInt(this.task.id), [
          { name: 'ApprovalApprovedVar', value: true },
          { name: 'CreatorVar', value: this.document.creator.login }
        ]);
      })
    ).subscribe((result: any) => {
      this.helper.beforeUnload.stop();
      this.agreeing = false;
      this.blockUI.stop();
      this.toastr.success('Документ успешно согласован!');
      window.location.href = '/main/#/app/tasks';
    }, error => {
      this.helper.beforeUnload.stop();
      console.log(error);
      this.toastr.error('Ошибка в процессе согласования!');
      this.agreeing = false;
      this.blockUI.stop();
    });
  }

  processApproving() {
    this.blockUI.start();
    this.document.fileId = [this.tempDraftFile.idFile];
    this.fillApprovalPlanDates().pipe(
      mergeMap(() => {
        return this.updateMemo();
      }),
      mergeMap(() => {
        return from(this.activityRestService.finishTask(parseInt(this.task.id), [
          { name: 'ApprovalApprovedVar', value: true }
        ]))
      })
    ).subscribe((result: any) => {
      this.helper.beforeUnload.stop();
      this.approving = false;
      this.blockUI.stop();
      this.toastr.success('Электронный документ успешно создан и подписан!');
      window.location.href = '/main/#/app/tasks';
    }, error => {
      this.helper.beforeUnload.stop();
      console.log(error);
      this.blockUI.stop();
      this.toastr.error('Ошибка в процессе утверждения документа!');
      this.approving = false;
    });
  }

  registerMemo() {
    let mergedPDF: any, draftWithoutAtt: FileType, memoLS: FileType, jsonTxt = '';
    // 1. запускается сервис генерации рег номера MGGT-988, сохраняется номер в document/number
    return this.memoRestService.generateNumber(this.document.documentId).pipe(
      mergeMap((res: any) => {
        this.document.number = res.regNumber;
        // 2. сохраняется дата регистрации document/beginDate - текущая дата
        this.document.beginDate = this.dateWithoutTime(new Date());
        jsonTxt = JSON.stringify({ document: this.document.min() });
        return this.convertToPdfIfNecessary(this.document.draftWithoutAtt[0]);
      }),
      mergeMap((res: FileType) => {
        draftWithoutAtt = this.document.draftWithoutAtt[0] = res;
        // 3. Формируем ПФ с рег номером и датой по шаблону «MemoWithoutSignRegInfo_Проект внутреннего документа без ЭП рег данные.docx», переводим в пдф
        const request: any = {
          jsonTxt,
          options: {
            jsonSourceDescr: '',
            onProcess: 'Согласование проекта служебной записки',
            placeholderCode: 'sn',
            strictCheckMode: true,
            xwpfResultDocumentDescr: `MemoWithoutSignRegInfo_Проект внутреннего документа без ЭП рег данные.docx`,
            xwpfTemplateDocumentDescr: ''
          },
          rootJsonPath: '$',
          nsiTemplate: {
            templateCode: 'MemoWithoutSignRegInfo'
          },
          filenetDestination: {
            fileName: 'Проект внутреннего документа без ЭП рег данные.docx',
            fileType: 'draftWithoutAtt',
            folderGuid: this.document.folderId,
            fileAttrs: this.getFileAttrs()
          }
        };
        return this.reporterResourceService.nsi2Filenet(request);
      }),
      mergeMap((res: any) => {
        const file = FileType.create(res.versionSeriesGuid, res.fileName, res.fileSize.toString(), false, res.dateCreated, 'draftWithoutAtt', res.mimeType);
        // 3. переводим в пдф
        return this.convertToPdfIfNecessary(file, 'draftFiles');
      }),
      mergeMap((res: FileType) => {
        const request: any = {
          addPageAs: 'PDF_ADD_PAGE_AS_BACKGROUND',
          filenetDestination: {
            fileAttrs: this.getFileAttrs(),
            fileName: `Служебная записка № ${this.document.number} от ${moment(this.document.beginDate).format('DD.MM.YYYY')}.pdf`,
            fileType: 'draftFiles',
            folderGuid: this.document.folderId,
          },
          mergedPdfPageNum: 1,
          pdfPageNum: 1,
          versionSeriesGuid: this.document.draftFiles[0].idFile,
          mergedVersionSeriesGuid: res.idFile,
          zeroPointAtLeftUpperCorner: true
        };
        // 4. Накладываем пдф с рег данными на document/draftFiles (наложение свержу первого листа)
        return this.pdfService.mergePdfPagesUsingPUT_1(request);
      }),
      mergeMap((response) => {
        mergedPDF = response;
        return this.createMemoLS();
      }),
      mergeMap((mls: FileType) => {
        memoLS = mls;
        return this.concatenatePDF([mergedPDF.versionSeriesGuid, memoLS.idFile]);
      }),
      mergeMap((res: FileType) => {
        this.document.IdDraftFilesRegInfo = res.idFile;
        return this.updateMemo();
      })
    );
  }

  createMemoLS(): Observable<FileType> {
    const request: any = {
      jsonTxt: JSON.stringify({ document: this.document.min() }),
      filenetDestination: {
        fileName: 'Лист согласования.docx',
        fileType: 'draftWithoutAtt',
        folderGuid: this.document.folderId
      },
      wordReporterParts: [{templateCode: 'MemoLS', sectionStart: ''}],
      options: {
        onProcess: 'Сформировать проект СЗ',
        placeholderCode: '',
        strictCheckMode: true,
        xwpfResultDocumentDescr: 'Лист согласования.docx',
        xwpfTemplateDocumentDescr: ''
      },
      rootJsonPath: '$'
    };
    return this.wordReporter.compoundReportNsi2FilenetPdfUsingPOST(request).pipe(
      mergeMap((res: any) => {
        return of(FileType.create(res.versionSeriesGuid, res.fileName,
          res.fileSize.toString(), false, res.dateCreated, res.typeFile, res.mimeType));
      })
    );
  }

  concatenatePDF(ids): Observable<FileType> {
    return (this.document.attachment && this.document.attachment.length
      ? forkJoin(this.document.attachment.map(i => this.convertToPdfIfNecessary(i)))
      : of([])).pipe(
      mergeMap((attachments: FileType[]) => {
        const request: any = {
          filenetDestination: {
            fileAttrs: this.getFileAttrs(),
            fileName: `${this.document.memoKind.name} № ${this.document.number} от ${moment(this.document.beginDate).format('DD.MM.YYYY')}.pdf`,
            fileType: 'draftFiles',
            folderGuid: this.document.folderId,
          },
          versionSeriesGuids: [...ids, ...attachments.map(a => a.idFile)]
        };

        return from(this.reporterResourceService.pdfConcatenateFiles(request));
      }),
      mergeMap((response: any) => {
        return of(FileType.create(response.versionSeriesGuid, response.fileName,
          response.fileSize.toString(), false, response.dateCreated, 'draftFiles', response.mimeType))
      })
    )
  }

  getFileAttrs(): any[] {
    return [
      { attrName: 'docEntityID', attrValues: [this.document.documentId] },
      { attrName: 'docSourceReference', attrValues: ['UI'] }
    ]
  }

  approveAndSendForward() {
    this.helper.beforeUnload.start();
    this.approving = true;

    if (this.isMobile) {
      this.openMobileApproveDialog();
    } else {
      this.openApproveDialog();
    }
  }

  beforeApproveSign = function (fileInfo: ExFileType, folderGuid, certEx) {
    this.document.ds = this.makeMemoDs(certEx);
    return this.approve(this.document, this.loopCounter, this.task.id, true, {
      approvalNote: this.approvalNote,
      agreedDs: this.makeMemoApprovalListItemSignature(certEx)
    }).pipe(
      mergeMap(() => {
        return this.convertApprovalFile();
      })
    ).toPromise();
  }.bind(this);

  afterApproveSign = function (result: any) {
    if (result.error) {
      this.helper.beforeUnload.stop();
      this.approving = false;
      return;
    }
    this.processApproving();
  }.bind(this);

  convertApprovalFile(): Observable<FileType> {
    let draftWithoutAtt: FileType, memoLS: FileType;
    let jsonTxt = '';
    return this.memoRestService.generateNumber(this.document.documentId).pipe(
      mergeMap((res: any) => {
        this.document.number = res.regNumber;
        this.document.beginDate = this.dateWithoutTime(new Date());
        jsonTxt = JSON.stringify({ document: this.document.min() })
        const draftFile = this.document.draftWithoutAtt[0];
        const request: any = {
          jsonTxt,
          filenetTemplate: {
            templateVersionSeriesGuid: draftFile.idFile
          },
          filenetDestination: {
            reportFileName: draftFile.nameFile,
            reportFileType: draftFile.typeFile,
            reportFolderGuid: this.document.folderId,
            fileName: `${this.document.memoKind.name} № ${this.document.number} от ${moment(this.document.beginDate).format('DD.MM.YYYY')}.docx`,
            fileType: this.document.draftWithoutAtt[0].typeFile,
            folderGuid: this.document.folderId
          },
          rootJsonPath: '$',
          options: {
            onProcess: this.getFileName(draftFile.nameFile),
            placeholderCode: 'sn',
            strictCheckMode: true,
            xwpfResultDocumentDescr: draftFile.nameFile,
            xwpfTemplateDocumentDescr: ''
          }
        };
        return this.reporterResourceService.filenet2Filenet(request);
      }),
      mergeMap((res: any) => {
        return this.convertToPdfIfNecessary(FileType.create(res.versionSeriesGuid, res.fileName, res.fileSize.toString(),
          false, res.dateCreated, 'draftWithoutAtt', res.mimeType));
      }),
      mergeMap((res: FileType) => {
        draftWithoutAtt = res;
        // make MemoLS
        return this.createMemoLS();
      }),
      mergeMap((mls: FileType) => {
        memoLS = mls;
        return this.concatenatePDF([draftWithoutAtt.idFile, memoLS.idFile]);
      }),
      tap((response: FileType) => {
        this.tempDraftFile = FileType.create(response.idFile, response.nameFile,
          response.sizeFile, response.signed, response.dateFile, 'draftFiles', response.classFile);
      })
    );
  }

  convertToPdfIfNecessary(file: FileType, fileType?: string, namePrefix?: string): Observable<FileType> {
    if (file.nameFile.indexOf('.pdf') !== -1) {
      return of(file);
    }

    const request: any = {
      filenetDestination: {
        fileAttrs: this.getFileAttrs(),
        fileName: `${this.getFileName(file.nameFile)}${namePrefix ? namePrefix : ''}.pdf`,
        fileType: fileType ? fileType : file.typeFile,
        folderGuid: this.document.folderId
      },
      wordVersionSeriesGuid: file.idFile
    };

    return from(this.reporterResourceService.word2pdf(request)).pipe(
      map((response) => {
        return FileType.create(response.versionSeriesGuid, response.fileName,
          response.fileSize.toString(), false, response.dateCreated, 'draftFiles', response.mimeType);
      })
    );
  }

  sendBackToWork() {
    if (!this.approvalNote) {
      this.toastr.error('Не указан комментарий!');
      return;
    }
    this.helper.beforeUnload.start();
    this.blockUI.start();
    this.sendingBackToWork = true;
    this.approve(this.document, this.loopCounter, this.task.id, false, {
      approvalNote: this.approvalNote
    }).pipe(
      mergeMap(() => {
        return this.updateMemo();
      }),
      mergeMap(() => {
        return this.activityRestService.finishTask(parseInt(this.task.id), [
          { name: 'ApprovalApprovedVar', value: false }
        ]);
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.sendingBackToWork = false;
      this.blockUI.stop();
      this.toastr.success('Документ отправлен на доработку!');
      window.location.href = '/main/#/app/tasks';
    }, () => {
      this.helper.beforeUnload.stop();
      this.sendingBackToWork = false;
      this.blockUI.stop();
    });
  }

  addApproval() {
    this.helper.beforeUnload.start();
    const approval = this.document.approval.approvalCycle.agreed[this.loopCounter];
    const alreadyAddedCounter = approval.added ? approval.added.length : 0;
    const options = {
      initialState: {
        dueDate: this.task.dueDate,
        except: this.getExistingApprovers(),
        approvalNum: approval.approvalNum,
        alreadyAddedCounter: alreadyAddedCounter,
      },
      'class': 'modal-xl modal-lg'
    };
    const ref = this.modalService.show(AddApproversModalComponent, options);
    this.modalService.onHide.pipe(
      mergeMap(() => {
        this.blockUI.start();
        const component = <AddApproversModalComponent>ref.content;

        if (!component.submit) {
          return throwError('cancelled');
        }
        if (component.agreed.length == 0) {
          return throwError('no added approvers');
        }

        this.addingApprovals = true;
        const agreed = this.document.approval.approvalCycle.agreed[this.loopCounter];

        agreed.added = agreed.added || [];
        component.agreed.forEach(added => {
          let addedItem = new MemoApprovalAdded();
          addedItem.build({
            additionalAgreed: {
              accountName: added.agreedBy.accountName,
              fio: added.agreedBy.fioFull,
              iofShort: added.agreedBy.iofShort
            },
            addedNote: added.note,
          });
          agreed.added.push(addedItem);
        });
        let componentAgreed = component.agreed.map(added => {
          return <any>{
            approvalNum: added.approvalNum,
            approvalTime: added.approvalTime,
            agreedBy: added.agreedBy,
            approvalType: added.approvalType,
            approvalTypeCode: added.approvalTypeCode,
            approvalPlanDate: formatDate(added.approvalPlanDate, 'yyyy-MM-ddTHH:mm:ss', 'en')
          };
        });
        const newApprovers: MemoApprovalListItem[] = MemoApprovalListItem.buildArray(componentAgreed);

        const currentApprover = this.document.approval.approvalCycle.agreed.indexOf(agreed);
        this.document.approval.approvalCycle.agreed.splice(currentApprover + alreadyAddedCounter + 1, 0, ...newApprovers);

        return this.fillUserInfo(agreed).pipe(
          mergeMap(() => {
            return this.fillApprovalPlanDate(newApprovers[0]);
          }),
          mergeMap(() => {
            return this.updateMemo().pipe(
              mergeMap(() => {
                let newApprovals = angular.copy(this.document.approval.approvalCycle.agreed);
                const sortByParts = (partsA, partsB) => {
                  const [firstA, ...restA] = partsA;
                  const [firstB, ...restB] = partsB;

                  if (+firstB < +firstA) {
                    return 1;
                  } else if (+firstB === +firstA) {
                    if (partsB.length === partsA.length) {
                      return sortByParts(restA, restB);
                    }

                    if (partsB.length + 1 === partsA.length) {
                      return sortByParts(restA, restB);
                    }

                    return partsB.length < partsA.length ? -1 : 1;
                  } else {
                    return -1;
                  }
                };

                newApprovals = newApprovals.sort((a, b) => {
                  const partsA = a.approvalNum.split('.');
                  const partsB = b.approvalNum.split('.');

                  return sortByParts(partsA, partsB);
                });

                return this.memoActivityService.addApprovers(this.task.id, newApprovals);
              }),
              mergeMap(() => {
                const logins = _.compact((component.agreed || [])
                  .map(i => i && i.agreedBy ? i.agreedBy.accountName : null));
                return logins.length ? this.memoRestService.notifyAdditionalApproval([logins[0]], this.task.id) : of('')
              }),
              tap(() => {
                this.helper.beforeUnload.stop();
                this.blockUI.stop();
                this.addingApprovals = false;
                window.location.href = '/main/#/app/tasks';
              })
            );
          })
        );
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.blockUI.stop();
      this.addingApprovals = false;
    }, () => {
      this.helper.beforeUnload.stop();
      this.blockUI.stop();
      this.addingApprovals = false;
    });
  }

  getExistingApprovers(): Array<string> {
    return this.document.approval.approvalCycle.agreed.map(agree => agree.agreedBy.accountName);
  }

  approve(document: MemoDocument, agreeIndex: number, taskId, approve: boolean, approvalParams: { approvalNote: string, agreedDs?: MemoApprovalListItemSignature }): Observable<any> {
    if (!document.approval || !document.approval.approvalCycle || !document.approval.approvalCycle.agreed || document.approval.approvalCycle.agreed.length === 0) {
      return throwError('Лист согласования не заполнен.');
    }

    const agreeds = document.approval.approvalCycle.agreed;

    if (agreeIndex < 0 || agreeIndex >= agreeds.length) {
      return throwError('Некорректный индекс согласующего.');
    }

    const agreed = agreeds[agreeIndex];

    if (!approve && !approvalParams.approvalNote) {
      return throwError('Для отрицательного решения необходимо заполнить комментарий.');
    }

    return from(this.nsiResourceService.get('mggt_memo_ApprovalType')).pipe(
      mergeMap((approvalTypes: ApprovalType[]) => {
        const approvalType = approvalTypes.find(at => at.approvalTypeCode === agreed.approvalTypeCode);
        agreed.approvalResult = approve ? approvalType.buttonYes : approvalType.buttonNo;

        agreed.approvalNote = approvalParams.approvalNote;
        agreed.agreedDs = approvalParams.agreedDs || null;
        agreed.approvalFactDate = new Date();
        agreed.fileApproval = this.files.length > 0 ? this.files.map(file =>
          FileType.create(file.idFile, file.nameFile, file.sizeFile.toString(), file.signed, new Date(Number(file.dateFile)), file.typeFile, file.mimeType)) : null;
        return this.fillUserInfo(agreed);
      }),
      mergeMap(() => {
        if (!approve) {
          return this.closeCycle(document);
        }

        return of('');
      })/*,
      mergeMap(() => {
        return this.updateMemo();
      })*/
    );
  }

  closeCycle(document: MemoDocument): Observable<any> {
    document.approvalHistory = document.approvalHistory || new MemoApprovalHistory();
    const approvalCycle = <MemoApprovalHistoryList>angular.copy(document.approval.approvalCycle);

    approvalCycle.approvalCycleFile = document.draftFiles.map((draftFile: FileType) => ({
      ...draftFile,
      nameFile: `${this.getFileName(draftFile.nameFile)}-согласование-${formatDate(new Date(draftFile.dateFile), 'yyyy-MM-dd HH:mm:ss', 'en')}.${this.getFileExtension(draftFile.nameFile)}`
    }));

    document.approvalHistory.approvalCycle = document.approvalHistory.approvalCycle || [];
    document.approvalHistory.approvalCycle.push(approvalCycle);
    document.approval.approvalCycle = null;

    return of('');
  }

  fillUserInfo(agreed: MemoApprovalListItem): Observable<any> {
    const plannedAgreedBy = agreed.agreedBy.accountName;
    const login = this.session.login();

    if (login !== plannedAgreedBy) {
      return this.getMemoApprover(login).pipe(
        tap(approver => agreed.factAgreedBy = approver),
        mergeMap(() => this.fillAgreedBy(agreed))
      );
    } else {
      return this.fillAgreedBy(agreed);
    }
  }

  fillAgreedBy(agreed: MemoApprovalListItem): Observable<any> {
    if (!agreed.agreedBy.fioShort) {
      return this.getMemoApprover(agreed.agreedBy.accountName).pipe(
        tap(approver => {
          agreed.agreedBy = approver;
        })
      );
    } else {
      return of('');
    }
  }

  getMemoApprover(login: string): Observable<MemoApprover> {
    return from(this.nsiResourceService.ldapUser(login)).pipe(
      map((userBean) => {
        return MemoApprover.fromUserBean(userBean);
      })
    );
  }

  updateMemo(): Observable<any> {
    return this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document);
  }

  getFileName(fullFileName: string): string {
    let dotIndex = fullFileName.lastIndexOf('.');
    if (dotIndex < 0) {
      return fullFileName;
    } else {
      return fullFileName.substring(0, dotIndex);
    }
  }

  getFileExtension(fullFileName: string): string {
    let dotIndex = fullFileName.lastIndexOf('.');
    if (dotIndex < 0) {
      return '';
    } else {
      return fullFileName.substring(dotIndex + 1, fullFileName.length);
    }
  }

  private dateWithoutTime(date: Date): Date {
    const _date = date.setHours(0, 0, 0, 0).valueOf();

    return new Date(_date);
  }

  private fillApprovalPlanDates(): Observable<any> {
    if (this.loopCounter < this.document.approval.approvalCycle.agreed.length - 1) {
      let nextAgreed = this.document.approval.approvalCycle.agreed[this.loopCounter + 1];
      return this.fillApprovalPlanDate(nextAgreed);
    }
    return of('');
  }

  private fillApprovalPlanDate(agreed: MemoApprovalListItem): Observable<any> {
    if (agreed.approvalPlanDate) {
      return of('');
    }
    const duration = agreed.approvalTerm &&  agreed.approvalTerm.duration ?
      `P${agreed.approvalTerm.duration}D` : agreed.approvalTime;
    if (!duration) {
      return of('');
    }

    return this.orderNsiService.addDuration(new Date(), duration).pipe(
      tap(result => {
        const date = new Date(result);
        agreed.approvalPlanDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 0, 0);
      })
    );
  }

  hasAgreed(): boolean {
    return this.document.approval
      && this.document.approval.approvalCycle
      && this.document.approval.approvalCycle.agreed
      && _.some(this.document.approval.approvalCycle.agreed, i => i.approvalTypeCode === 'agreed');
  }

  makeMemoDs(certEx: ICertificateInfoEx): MemoDs {
    let result = new MemoDs();
    result.dsLastName = certEx.lastName;
    result.dsName = certEx.firstName;
    result.dsPosition = certEx.position;
    result.dsCN = certEx.serialNumber;
    result.dsFrom = moment(certEx.from, 'DD-MM-YYYY').format('YYYY-MM-DD');
    result.dsTo = moment(certEx.till, 'DD-MM-YYYY').format('YYYY-MM-DD');
    return result;
  }

  makeMemoApprovalListItemSignature(certEx: ICertificateInfoEx): MemoApprovalListItemSignature {
    let result = new MemoApprovalListItemSignature();
    result.agreedDsLastName = certEx.lastName;
    result.agreedDsName = certEx.firstName;
    result.agreedDsPosition = certEx.position;
    result.agreedDsCN = certEx.serialNumber;
    result.agreedDsFrom = moment(certEx.from, 'DD-MM-YYYY').format('YYYY-MM-DD');
    result.agreedDsTo = moment(certEx.till, 'DD-MM-YYYY').format('YYYY-MM-DD');
    return result;
  }
}
