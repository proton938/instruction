import * as angular from 'angular';
import * as _ from "lodash";
import {ToastrService} from 'ngx-toastr';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {NsiResourceService, UserBean} from '@reinform-cdp/nsi-resource';
import {StateService} from '@uirouter/core';
import {PlatformConfig} from '@reinform-cdp/core';
import {EditMemoModel} from '../../../../models/memo/EditMemoModel';
import {AlertService, LoadingStatus} from '@reinform-cdp/widgets';
import {ActiveTaskService, ActivityResourceService, ITask,IProcessDefinition} from "@reinform-cdp/bpm-components";
import {MemoDocument} from '../../../../models/memo-document/MemoDocument';
import {MemoDictsModel} from '../../../../models/memo/MemoDictsModel';
import {MemoRestService} from "../../../../services/memo-rest.service";
import {MemoActivitiService} from '../../../../services/memo-activiti.service';
import {MemoDictsService} from '../../../../services/memo-dicts.service';
import {MemoDocumentService} from '../../../../services/memo-document.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {catchError, debounceTime, distinctUntilChanged, map, mergeMap, switchMap, tap} from "rxjs/internal/operators";
import {concat, forkJoin, from, Observable, of, Subject, throwError} from 'rxjs/index';
import {FileType} from '../../../../models/instruction/FileType';
import {CdpReporterResourceService} from '@reinform-cdp/reporter-resource';
import {MemoKind} from '../../../../models/memo/MemoKind';
import {NSIEmailGroup} from '../../../../models/nsi/NSIEmailGroups';
import {OrderNsiService} from '../../../../services/order-nsi.service';
import {MemoHeadingTopic} from '../../../../models/memo/MemoHeadingTopic';
import {MemoTrestActivityKind} from '../../../../models/memo/MemoTrestActivityKind';
import {ApprovalType} from '../../../../models/instruction/ApprovalType';
import {HelperService} from '../../../../services/helper.service';
import {
  MemoApproval, MemoApprovalHistoryList,
  MemoApprovalListItem, MemoApprovalTerm
} from '../../../../models/memo-document/MemoApproval';
import {NSIMemoAppDuration} from '../../../../models/nsi/NSIMemoAppDuration';
import { FileResourceService } from "@reinform-cdp/file-resource";
import { FileService } from '../../../../services/file.service';
import {SessionStorage} from '@reinform-cdp/security';

@Component({
  selector: 'mggt-rework-memo',
  templateUrl: './rework-memo.component.html',
  styleUrls: ['./rework-memo.component.scss']
})
export class ReworkMemoComponent implements OnInit, OnDestroy {
  document: MemoDocument;
  copyDocument: MemoDocument;
  dicts: MemoDictsModel;
  task: ITask;

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  model: EditMemoModel;

  refusedApprovalItem: MemoApprovalListItem;

  emailGroups: NSIEmailGroup[];
  selectedEmailGroups: NSIEmailGroup[];
  memoKinds: MemoKind[];
  memoHeadingTopics: MemoHeadingTopic[];
  memoTrestActivityKinds: MemoTrestActivityKind[];
  privacies: any[];
  appDurations: NSIMemoAppDuration[];
  listCollapsed = false;
  memoKindName = null;
  headingTopicName = null;
  activityKindName = null;
  recipientGroupName = null;
  privacy = null;
  approvers: UserBean[];
  approversSign: UserBean[];
  approvalTypes: ApprovalType[];
  approvalCycleNum: number;

  relateId: any[];

  relatedMemos: MemoDocument[];

  relatedDocuments$: Observable<any[]>;
  relatedDocumentsLoading = false;
  relatedDocumentsInput$ = new Subject<string>();
  expand: any = {
    relatedDocs: false,
    linkedElements: false
  };
  linkedControls: any = {};

  isShow: any = {
    docInfo: false
  };

  filePathForWord:string;
  showButton: boolean = true;

  @BlockUI('reworkMemo') blockUI: NgBlockUI;

  constructor(
    public $state: StateService,
    private activityResourceService: ActivityResourceService,
    private alertService: AlertService,
    private activeTaskService: ActiveTaskService,
    private activityRestService: ActivityResourceService,
    private orderNsiService: OrderNsiService,
    private memoDictsService: MemoDictsService,
    private memoActivityService: MemoActivitiService,
    public platformConfig: PlatformConfig,
    private memoDocumentService: MemoDocumentService,
    private reporterResourceService: CdpReporterResourceService,
    private toastr: ToastrService,
    private nsiResourceService: NsiResourceService,
    private memoRestService: MemoRestService,
    private fileResourceService: FileResourceService,
    public helper: HelperService,
    private session: SessionStorage,
    private fileService:FileService) {
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.loadDictionaries();
      }),
      mergeMap(() => {
        return this.memoDictsService.getDicts();
      }),
      mergeMap((response) => {
        this.dicts = response;
        return this.memoActivityService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.memoRestService.get(resp);
      }),
      mergeMap((response) => {
        this.document = new MemoDocument();
        this.document.build(response.document);
        this.copyDocument = angular.copy(this.document);

        this.approvalCycleNum = this.document.approvalHistory ? this.document.approvalHistory.approvalCycle.length + 1 : 1;

        if (!this.document.approval || _.isEmpty(this.document.approval)) {
          this.document.approval = MemoApproval.create(this.approvalCycleNum);
        }

        this.model = new EditMemoModel();
        this.model.updateModelBeforeEditing(this.document, this.dicts);

        this.updateEmailGroupsAfterExecutor();

        let latestApprovalCycle: MemoApprovalHistoryList = _.first(this.document.approvalHistory.approvalCycle
          .slice().sort((a, b) => +b.approvalCycleNum - +a.approvalCycleNum));

        this.refusedApprovalItem = latestApprovalCycle.agreed.filter((a) => (
          a.approvalResult === 'Не согласовано' || a.approvalResult === 'Не утверждено')
        )[0];

        if (!this.document.approval.approvalCycle.agreed.length) {
          latestApprovalCycle.agreed.forEach((a) => {
            if(a.approvalNum.indexOf('.')>=0)return;
            const approvalItem = new MemoApprovalListItem();
            approvalItem.approvalNum = a.approvalNum;
            approvalItem.agreedBy = a.agreedBy;
            approvalItem.approvalTime = a.approvalTime;
            approvalItem.approvalType = a.approvalType;
            approvalItem.approvalTypeCode = a.approvalTypeCode;

            this.document.approval.approvalCycle.agreed.push(approvalItem);
          });
        }

        if (this.model.headingTopic) {
          this.headingTopicName = this.model.headingTopic.code;
        }

        if (this.model.activityKind) {
          this.activityKindName = this.model.activityKind.code;
        }

        if (this.model.privacy) {
          this.privacy = this.model.privacy.code;
        }

        if(!!this.document.draftWithoutAtt.length)
          return this.fileService.delegateAccess(
            this.document.draftWithoutAtt[0].idFile,
            this.session.login(),
            this.task.formKey,"sdomemo");
        else return of(null);
      }),
      mergeMap(() => {
        return this.memoDocumentService.getRelatedMemos(this.model.relateId);
      }),
      mergeMap(response => {
        this.relateId = response;

        this.relatedDocuments$ = concat(
          of(response),
          this.relatedDocumentsInput$.pipe(
            debounceTime(100),
            distinctUntilChanged(),
            tap(() => this.relatedDocumentsLoading = true),
            switchMap(term => this.searchDocuments(term).pipe(
              catchError(() => of([])),
              tap(() => this.relatedDocumentsLoading = false)
            ))
          )
        );
        return  this.fileResourceService.getFolderinfo(this.document.folderId);
      }),
      tap(result => {
        this.filePathForWord = result.path;
        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
       }),
      catchError(error => {
        this.loadingStatus = LoadingStatus.ERROR;
        return throwError(error);
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }


  getAlt(onCondition: boolean, hintId: string, on?: boolean, condition?: boolean ) {
    if (onCondition) {
      let hint = document.getElementById(hintId);
      if (on) {
        document.addEventListener('mousemove', function (event) {
          hint.style.left = event.pageX + 10 + 'px';
          hint.style.top = event.pageY - window.pageYOffset + 10 + 'px';
        });
        hint.style.display = 'block';

      } else {
        hint.style.left = -1000 + 'px';
        hint.style.top = -1000 + 'px';
        hint.style.display = 'none';
      }
    }
  }

  cancellimitedAccess: boolean = false;
  limitedAccess() {
    if (!this.document.limitedAccess && !this.cancellimitedAccess) {
      this.alertService.confirm({
        okButtonText: 'Изменить',
        size: 'lg',
        message: 'Документ будет доступен только участникам процесса',
        type: 'warning'
      }).then(() => {
        return;
      }).catch(e => {
        this.cancellimitedAccess = true;
        this.document.limitedAccess = false;
      });
    }
    if (!this.document.limitedAccess && this.cancellimitedAccess) {
      this.cancellimitedAccess = false;
    }
  }

  searchDocuments(search: string): Observable<any[]> {
    if (search && search.length > 1) {
      return from(this.memoDocumentService.searchMemos(search)).pipe(
        map((_response: any) => {
          return _response.map((memoSearch) => ({
            ...memoSearch,
            number: memoSearch.docNumberMemo,
            documentId: memoSearch.sys_documentId,
          }));
        }), catchError(error => {
          this.toastr.error('Ошибка при получении информации о пользователях!');
          return throwError(error);
        })
      );
    } else {
      return of([]);
    }
  }

  save() {
    if (!this.model.isValid()) {
      this.toastr.warning('Не заполнены обязательные поля!');
      return;
    }

    this.helper.beforeUnload.start();
    this.blockUI.start();
    this.model.updateDocumentFromModel(this.document);

    if (this.document.approvalElectronically) {
      this.fillApprovalTerm();
    }

    this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document)
      .pipe(
        mergeMap(() => {
          return this.linkedControls.save ? this.linkedControls.save(true) : of('');
        }),
        mergeMap(() => {
          return this.memoRestService.get(this.document.documentId);
        }),
      )
      .subscribe(
        (response) => {
          this.helper.beforeUnload.stop();
          this.document = new MemoDocument();
          this.document.build(response.document);
          this.copyDocument = angular.copy(this.document);

          this.blockUI.stop();
        },
        (error) => {
          this.helper.beforeUnload.stop();
          console.log(error);
          this.blockUI.stop();
          return throwError(error);
        });
  }

  hasNotFilledApprover() {
    return this.model.approval.approvalCycle.agreed.some((a) => {
      if (!a.approvalType) {
        return true;
      }

      if (!a.agreedBy) {
        return true;
      }

      return false;
    });
  }

  sendToApproval() {
    if (!this.model.isValid()) {
      this.toastr.warning('Не заполнены обязательные поля!');
      return;
    }

    if (this.document.memoKind.code === '16' && !this.document.draftFiles.length) {
      this.toastr.warning('Не заполнены обязательные поля!');
      return;
    }

    if (this.hasNotFilledApprover()) {
      this.toastr.warning('Некорректно заполнен Лист согласования');
      return;
    }

    if (this.model.signingElectronically) {
      const hasApproval = this.model.approval.approvalCycle.agreed.some((a) => {
        return a.approvalTypeCode === 'approval';
      });

      if (!hasApproval) {
        this.toastr.warning('Некорректно заполнен Лист согласования, не указан Утвержающий');
        return;
      }
    } else {
      const hasCreator = this.model.approval.approvalCycle.agreed.some((a) => {
        return a.agreedBy.accountName === this.model.creator.accountName;
      });

      if (!hasCreator) {
        this.toastr.warning('Некорректно заполнен Лист согласования, не указан Утвержающий');
        return;
      }
    }

    this.helper.beforeUnload.start();
    this.blockUI.start();
    this.model.updateDocumentFromModel(this.document);

    this.fillApprovalTermAndPlanDates().pipe(
      mergeMap(() => {
        return this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document);
      }),
      mergeMap(() => {
        return this.linkedControls.save ? this.linkedControls.save(true) : of('');
      }),
      mergeMap(() => {
        return this._finishTask(true);
      }),
      tap(() => {
        this.helper.beforeUnload.stop();
        this.blockUI.stop();
        window.location.href = '/main/#/app/tasks';
      }),
      catchError(error => {
        this.helper.beforeUnload.stop();
        console.log(error);
        this.blockUI.stop();
        return throwError(error);
      })
    ).subscribe();
  }

  private _finishTask(isApprovalElectronically: boolean): Observable<any> {
    return from(this.activityRestService.getFormData({ taskId: this.task.id })).pipe(
      mergeMap(response => {
        return this.activityRestService.finishTask(parseInt(this.task.id), [
          { name: 'IsApprovalElectronically', value: isApprovalElectronically },
        ]);
      }),
      tap((response) => {
        this.toastr.success('Задача успешно завершена!');

        return response;
      }),
      catchError(error => {
        this.toastr.error('Ошибка при завершении задачи!');
        return throwError(error);
      })
    );
  }

  private fillApprovalTermAndPlanDates(): Observable<any> {
    if (!this.document.approvalElectronically) {
      return of('');
    }
    this.fillApprovalTerm();
    return this.fillApprovalPlanDate(this.document.approval.approvalCycle.agreed[0]);
  }

  fillApprovalTerm() {
    const defaultAppDuration = this.appDurations.find(ad => ad.Default);
    if (defaultAppDuration) {
      this.document.approval.approvalCycle.agreed.forEach(a => {
        a.approvalTerm = new MemoApprovalTerm();
        a.approvalTerm.code = defaultAppDuration.Code;
        a.approvalTerm.name = defaultAppDuration.Name;
        a.approvalTerm.duration = parseInt(defaultAppDuration.Duration);
      });
    }
  }

  private fillApprovalPlanDate(agreed: MemoApprovalListItem): Observable<any> {
    if (agreed.approvalPlanDate) {
      return of('');
    }
    const duration = agreed.approvalTerm &&  agreed.approvalTerm.duration ?
      `P${agreed.approvalTerm.duration}D` : agreed.approvalTime;
    if (!duration) {
      return of('');
    }

    return this.orderNsiService.addDuration(new Date(), duration).pipe(
      tap(result => {
        const date = new Date(result);
        agreed.approvalPlanDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 0, 0);
      })
    );
  }

  goToCard() {
    this.$state.go('app.instruction.memo-card', { id: this.document.documentId });
  }

  cancel() {
    this.alertService.confirm({
      message: 'Вы уверены, что хотите закрыть задачу без сохранения введенных данных?',
      okButtonText: 'Ок'
    })
      .then(() => {
        this.$state.go('app.instruction.showcase-memo');
      })
      .catch(() => {
        console.log('cancel');
      });
  }

  loadDictionaries(): Observable<any> {
    return forkJoin([
      this.nsiResourceService.get('EmailGroups'),
      this.nsiResourceService.get('MemoKind'),
      this.nsiResourceService.get('MemoHeadingTopic'),
      this.nsiResourceService.get('MemoTrestActivityKind'),
      this.nsiResourceService.get('mggt_memo_ApprovalType'),
      this.nsiResourceService.ldapUsers('MGGT_MEMO_APPROVE'),
      this.nsiResourceService.ldapUsers('MGGT_ELECTRONIC_SIGN'),
      this.nsiResourceService.get('mggt_privacy'),
      this.nsiResourceService.get('mggt_instruction_memo_AppDuration')
    ]).pipe(
      map((result: [NSIEmailGroup[], MemoKind[], MemoHeadingTopic[], MemoTrestActivityKind[], ApprovalType[], UserBean[], UserBean[], any[], NSIMemoAppDuration[]]) => {
        this.emailGroups = result[0].filter(emailGroup => {
          return emailGroup.sendSystem && emailGroup.sendSystem.find(sendSystem => {
            return sendSystem.code === 'SDO_INSTRUCTION';
          });
        });
        this.memoKinds = result[1];
        this.memoHeadingTopics = result[2];
        this.memoTrestActivityKinds = result[3];
        this.approvalTypes = result[4];
        this.approvers = result[5];
        this.approvers = result[5];
        this.approversSign = result[6];
        this.privacies = result[7];
        this.appDurations = result[8];

        return true;
      })
    );
  }

  memoKindChanged(mk) {
    this.model.memoKind = this.memoKinds.find(memoKind => memoKind.code === mk.code);
    this.refreshModel();
  }

  activityKindChanged(ak) {
    this.model.activityKind = this.memoTrestActivityKinds.find(activityKind => activityKind.code === ak.code);
    this.refreshModel();
  }

  privacyChanged(pr) {
    this.model.privacy = this.privacies.find(p => p.code === pr.code);
    this.refreshModel();
  }

  updateEmailGroupsAfterExecutor() {
    const executorAccounts: string[] = this.model.executor ? this.model.executor.map(user => {
      return user.accountName;
    }) : [];

    this.selectedEmailGroups = this.emailGroups.filter((eg) => {
      if (_.every(eg.users, user => (executorAccounts.indexOf(user) > -1))) {
        return true;
      }

      return false;
    });
  }

  findUser(users: UserBean[], login: string) {
    return users.find(user => {
      return user.accountName === login;
    });
  }

  recipientGroupChanged(emailGroups: NSIEmailGroup[]) {
    this.model.executor = this.model.executor || [];
    this.selectedEmailGroups = emailGroups;

    this.selectedEmailGroups.forEach((emailGroup) => {
      forkJoin(emailGroup.users.filter(login => !this.findUser(this.model.executor, login))
        .map(login => this.nsiResourceService.ldapUser(login)))
        .subscribe((users) => {
          this.model.executor = this.model.executor.concat(users);
        });
    });
  }

  headingTopicChanged(ht) {
    this.model.headingTopic = this.memoHeadingTopics.find(headingTopic => headingTopic.code === ht.code);
    this.refreshModel();
  }

  onRelateIdChange(relateIds: any[]) {
    if (relateIds && relateIds.length) {
      this.model.relateId = relateIds.map(relateId => relateId['documentId']);
    } else {
      this.model.relateId = [];
    }

    this.relateId = relateIds;
  }

  refreshModel() {
    this.model = this.model ? this.model : null;
  }

  onDraftFilesWithoutAttDelete = (fileInfo) => {
    from(this.fileResourceService.deleteFile(fileInfo.idFile)).pipe(
      mergeMap(() => {
        return this.updateMemoPart(memo => {
          memo.draftWithoutAtt = [];
        });
      })
    ).subscribe(() => {},
    (err) => {
      console.error(err);
    });
  };
  onDraftFilesDelete = (fileInfo) => {
    from(this.fileResourceService.deleteFile(fileInfo.idFile)).pipe(
      mergeMap(() => {
        return this.updateMemoPart(memo => {
          memo.draftFiles = [];
        });
      })
    ).subscribe(() => {},
    (err) => {
      console.error(err);
    });
  };
  onAttachDelete = (fileInfo) => {
    from(this.fileResourceService.deleteFile(fileInfo.idFile)).pipe(
      mergeMap(() => {
        return this.updateMemoPart(memo => {
          memo.attachment = [];
        });
      })
    ).subscribe(() => {},
    (err) => {
      console.error(err);
    });
  };

  updateMemoPart(callback?: (model: MemoDocument) => any): Observable<any> {
    let docForChanges = angular.copy(this.copyDocument);
    if (callback) {
      callback(docForChanges);
    }
    return this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, docForChanges)
      .pipe(
        tap(() => {
          callback(this.document);
          callback(this.copyDocument);
        }),
      );
  }


  createDraftFilesWithoutAtt() {
    this.blockUI.start();
    this._generateDraftFile()
      .pipe(
        tap((response) => {
          this.document.draftWithoutAtt = [FileType.create(response.versionSeriesGuid, response.fileName,
            response.fileSize.toString(), false, response.dateCreated, 'draftWithoutAtt', response.mimeType)];
          // this.blockUI.stop();
        // }),mergeMap(()=>{
        //   return this.fileService.updateAccess(this.document.draftWithoutAtt[0].idFile,
        //     this.session.login());
        }),mergeMap(()=>{
          return this.updateMemoPart(memo => {
            memo.draftWithoutAtt = this.document.draftWithoutAtt;
          });
        }),mergeMap(()=>{
          return this.activityResourceService.getProcessDefinitions({ key: 'sdord_updateAccessRules', latest: true });
        }),mergeMap((response) => {
          const processes = response.data;
          if (processes.length <= 0) {
            return throwError('Процесс sdord_updateAccessRules не найден');
          } else {
            return of(processes[0]);
          }
        }),
        mergeMap((process: IProcessDefinition) => {
          return this.activityResourceService.initProcess({
            processDefinitionId: process.id,
            variables: [{
              name: 'EntityIdVar',
              value: this.document.documentId
            }, {
              name: 'FileIdVar',
              value: this.document.draftWithoutAtt[0].idFile
            }, {
              name: 'PrincipalIdVar',
              value: this.session.login()
            }]
          });
        }),catchError(error => {
          console.log(error);
          this.toastr.error('Произошла ошибка при формировании файла');
          this.blockUI.stop();

          return throwError(error);
        }))
      .subscribe(()=>{
        this.blockUI.stop();
      });
    this.showButton = true;
  }

  createDraftFiles() {
    this.blockUI.start();
    const namePrefix = `(${this.approvalCycleNum})`;
    this._convertToPdfIfNecessary(this.document.draftWithoutAtt[0], 'draftFiles', namePrefix).pipe(
      mergeMap((response) => {
        const draftFile = response;

        if (!this.document.attachment || !this.document.attachment.length) {
          this.document.draftFiles = [draftFile];
          return of(draftFile);
        }

        return this._convertToPdfIfNecessary(this.document.attachment[0]).pipe(
          mergeMap((attachment: FileType) => {
            const request: any = {
              filenetDestination: {
                fileAttrs: [
                  { attrName: 'docEntityID', attrValues: [this.document.documentId] },
                  { attrName: 'docSourceReference', attrValues: ['UI'] }
                ],
                fileName: `${draftFile.nameFile.split('.')[0]}.pdf`,
                fileType: 'draftFiles',
                folderGuid: this.document.folderId
              },
              versionSeriesGuids: [draftFile.idFile, attachment.idFile]
            };

            return from(this.reporterResourceService.pdfConcatenateFiles(request)).pipe(
              mergeMap((response: any) => {
                return of(FileType.create(response.versionSeriesGuid, response.fileName,
                  response.fileSize.toString(), false, response.dateCreated, 'draftFiles', response.mimeType))
              })
            );
          }),
          tap((response) => {
            this.document.draftFiles = [FileType.create(response.idFile, response.nameFile,
              response.sizeFile, false, response.dateFile, 'draftFiles', response.classFile)];
          })
        );
      }),mergeMap(()=>{
        return this.updateMemoPart(memo => {
          memo.draftFiles = this.document.draftFiles;
        });
      }),
      catchError(error => {
        console.log(error);
        return throwError(error);
      }))
      .subscribe(
        () => {
          this.blockUI.stop();
        }
      );
  }

  _convertToPdfIfNecessary(file: FileType, fileType?: string, namePrefix?: string): Observable<FileType> {
    if (file.nameFile.indexOf('.pdf') !== -1) {
      return of(file);
    }

    const request: any = {
      filenetDestination: {
        fileAttrs: [
          { attrName: 'docEntityID', attrValues: [this.document.documentId] },
          { attrName: 'docSourceReference', attrValues: ['UI'] }
        ],
        fileName: `${file.nameFile.split('.')[0]}${namePrefix ? namePrefix : ''}.pdf`,
        fileType: fileType ? fileType : file.typeFile,
        folderGuid: this.document.folderId
      },
      wordVersionSeriesGuid: file.idFile
    };

    return from(this.reporterResourceService.word2pdf(request)).pipe(
      map((response) => {
        return FileType.create(response.versionSeriesGuid, response.fileName,
          response.fileSize.toString(), false, response.dateLastModified, 'draftFiles', response.mimeType);
      })
    );
  }

  _generateDraftFile(): Observable<any> {
    const templateCode = this.model.signingElectronically ? 'MemoWithSign' : 'MemoWithoutSign';
    const fileName = this.model.signingElectronically ? 'Проект внутреннего документа с ЭП' : 'Проект внутреннего документа без ЭП';

    this.model.updateDocumentFromModel(this.document);

    const request: any = {
      options: {
        jsonSourceDescr: '',
        onProcess: 'Сформировать проект СЗ',
        placeholderCode: '',
        strictCheckMode: true,
        xwpfResultDocumentDescr: `${fileName}.docx`,
        xwpfTemplateDocumentDescr: ''
      },
      jsonTxt: JSON.stringify({
        document: this.document
      }),
      rootJsonPath: '$',
      nsiTemplate: {
        templateCode,
      },
      filenetDestination: {
        fileName: `${fileName}.docx`,
        fileType: 'draftWithoutAtt',
        folderGuid: this.document.folderId,
        fileAttrs: [
          { attrName: 'docEntityID', attrValues: [this.document.documentId] },
          { attrName: 'docSourceReference', attrValues: ['UI'] },
        ]
      }
    };

    return from(this.reporterResourceService.nsi2Filenet(request));
  }

  isOfficeMemo = () => (this.document.memoKind.code === '16');

  fileAdded(key: string): void {
    this.document[key] = this.document[key].map((file: FileType) => ({
      ...file,
      dateFile: new Date(file.dateFile)
    }));
    if((key=='attachment'||key=='draftWithoutAtt'||key=='draftFiles')
      && !!this.document[key].length){
        this.updateMemoPart(memo => {
          memo[key] = this.document[key];
        });
      }
    switch(key) {
      // case 'attachment':
      //   this.clearDraftFiles();
      //   break;
      case 'draftWithoutAtt':
        // if (!this.document.draftWithoutAtt.length) { this.clearDraftFiles(); }
        // else{
        if (!!this.document.draftWithoutAtt.length) {
          from(this.activityResourceService.getProcessDefinitions({ key: 'sdord_updateAccessRules', latest: true })).pipe(
            mergeMap((response) => {
              const processes = response.data;
              if (processes.length <= 0) {
                return throwError('Процесс sdord_updateAccessRules не найден');
              } else {
                return of(processes[0]);
              }
            }),
            mergeMap((process: IProcessDefinition) => {
              return this.activityResourceService.initProcess({
                processDefinitionId: process.id,
                variables: [{
                  name: 'EntityIdVar',
                  value: this.document.documentId
                }, {
                  name: 'FileIdVar',
                  value: this.document.draftWithoutAtt[0].idFile
                }, {
                  name: 'PrincipalIdVar',
                  value: this.session.login()
                }]
              });
            })
          ).subscribe();
          // forkJoin(this.document.draftWithoutAtt.map(f=>{
          //   return this.fileService.updateAccess(f.idFile,
          //     this.session.login());
          // })).subscribe();
        }
        break;
    }
    this.showButton = true;
  }

  clearDraftFiles() {
    if (this.document.draftFiles.length) { this.document.draftFiles.length = 0; }
  }
}
