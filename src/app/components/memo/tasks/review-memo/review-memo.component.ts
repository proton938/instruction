import {NsiResourceService, UserBean} from '@reinform-cdp/nsi-resource';
import {BsModalService} from 'ngx-bootstrap/modal';
import * as angular from 'angular';
import { StateService } from '@uirouter/core';
import { ToastrService } from 'ngx-toastr';
import {Component, Input, OnInit} from '@angular/core';
import { SearchResultDocument } from '@reinform-cdp/search-resource';
import { LoadingStatus, AlertService } from '@reinform-cdp/widgets';
import { ActiveTaskService, ActivityResourceService, ITask } from '@reinform-cdp/bpm-components';
import { MemoDictsModel } from '../../../../models/memo/MemoDictsModel';
import { MemoDocument } from '../../../../models/memo-document/MemoDocument';
import { SessionStorage } from '@reinform-cdp/security';
import { MemoModalService } from '../../../../services/memo-modal.service';
import { MemoDocumentService } from '../../../../services/memo-document.service';
import { FileService } from '../../../../services/file.service';
import { OrderDocumentService } from '../../../../services/order-document.service';
import { MemoRestService } from '../../../../services/memo-rest.service';
import { MemoActivitiService } from '../../../../services/memo-activiti.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import * as _ from 'lodash';
import { Observable, from, of, throwError } from 'rxjs';
import { catchError, mergeMap, retryWhen, tap, map, filter } from 'rxjs/internal/operators';
import { DocumentUser } from '../../../../models/core/DocumentUser';
import { MemoDictsService } from '../../../../services/memo-dicts.service';
import {ExFileType} from '@reinform-cdp/widgets';
import { MemoDocumentResult } from '../../../../models/memo-document/MemoDocumentResult';
import {HelperService} from '../../../../services/helper.service';
import {DocumentStatus} from '../../../../models/core/DocumentStatus';
import {RedirectMemoModalComponent} from './redirect-memo/redirect-memo.modal';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {InstructionDocumentResult} from "../../../../models/instruction-document/InstructionDocumentResult";

@Component({
  selector: 'mggt-review-memo',
  templateUrl: './review-memo.component.html',
  styleUrls: ['./review-memo.component.scss']
})
export class ReviewMemoComponent implements OnInit {

  result: InstructionDocumentResult;

  document: MemoDocument;
  copyDocument: MemoDocument;
  lastResult: MemoDocumentResult;
  dicts: MemoDictsModel;
  task: ITask;
  files: ExFileType[];
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;

  instructions: SearchResultDocument[];
  instructionDicts: any;

  selectedTab = 0;
  hideOvers = true;

  reportContent = '';
  reportFile: ExFileType[] = [];

  relatedMemos: MemoDocument[];

  processVersion = '';
  visibleAnswerwBtn = true;
  visibleRedirectBtn = true;
  showLinkedElements = false;

  @BlockUI('reviewMemo') blockUI: NgBlockUI;

  constructor(public fileService: FileService,
    private activeTaskService: ActiveTaskService,
    private instructionDocumentService: OrderDocumentService,
    private memoRestService: MemoRestService,
    private memoDictsService: MemoDictsService,
    private memoDocumentService: MemoDocumentService,
    private memoActivityService: MemoActivitiService,
    private memoModalService: MemoModalService,
    private toastr: ToastrService,
    public $state: StateService,
    private session: SessionStorage,
    private activityRestService: ActivityResourceService,
    private nsiRestService: NsiResourceService,
    private modalService: BsModalService,
    public helper: HelperService,
    private alertService: AlertService,
    private solrMediator: SolrMediatorService) {
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.memoDictsService.getDicts();
      }),
      mergeMap(response => {
        this.dicts = response;
        return this.nsiRestService.getDictsFromCache(['InstructionStatus', 'InstructionPriority']);
      }),
      mergeMap(response => {
        this.instructionDicts = {
          statuses: response.InstructionStatus,
          priorities: response.InstructionPriority
        };

        return this.activityRestService.getProcessDefinitionsById(this.task.processDefinitionId);
      }),
      mergeMap((result: any) => {
        this.processVersion = result.releaseNum;
        this.visibleAnswerwBtn = !(parseInt(result.releaseNum, 10) < 4);

        return this.memoActivityService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.memoRestService.get(resp);
      }),
      mergeMap(response => {
        this.document = new MemoDocument();
        this.document.build(response.document);
        this.copyDocument = angular.copy(this.document);
        this.lastResult = _.last(this.document.result);

        // this.reportContent = this.document.content;

        if (_.some(this.document.executor, e => e.login === this.session.login())
          && (!this.document.statusId || this.document.statusId.code !== 'review')) {
          this.document.statusId = new DocumentStatus();
          this.document.statusId.build(_.find(this.dicts.statuses, s => s.code === 'review'));
          this.memoDocumentService
            .updateDocument(this.document.documentId, this.copyDocument, this.document).subscribe();
        }

        return this.getFiles();
      }),
      mergeMap(response => {
        this.files = response;

        /*
        if (this.files.length > 0) {
          this.reportFile = this.files;
        }
        */

        // return this.memoDocumentService.getInstructions(this.document.documentId);
        return this.solrMediator.query({
          page: 0,
          pageSize: 999,
          query: 'memoID:' + this.document.documentId + ' AND NOT confident:closed AND NOT confident:refused',
          types: [ this.solrMediator.types.instruction ]
        }).pipe(mergeMap(res => {
          return of(this.solrMediator.getDocs(res));
        }));
      }),
      mergeMap(response => {
        this.instructions = response;

        return this.solrMediator.query({
          page: 0,
          pageSize: 999,
          query: 'memoID:' + this.document.documentId + ' AND creatorLogin:' + this.session.login(),
          types: [ this.solrMediator.types.instruction ]
        }).pipe(mergeMap(res => {
          return of(this.solrMediator.getDocs(res));
        }));
      }),
      mergeMap(response => {
        this.visibleRedirectBtn = response.length === 0;

        const relatedIds = this.document.relateId && this.document.relateId.length ? this.document.relateId : [];

        return this.memoDocumentService.getRelatedMemos(relatedIds);
      }),
      tap(response => {
        this.relatedMemos = response;

        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;

      }),
      catchError(error => {
        console.log(error);
        this.loadingStatus = LoadingStatus.ERROR;
        return throwError(error);
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  getFiles(): Observable<any> {
    return Observable.create(obs => {
      this.fileService.getRealInfoAboutFilesByIds(this.document.fileId).subscribe(res => {
        obs.next(res);
      }, () => {
        obs.next([]);
      }, () => {
        obs.complete();
      })
    });
  }

  getAlt(onCondition: boolean, hintId: string, on?: boolean, condition?: boolean ) {
    if (onCondition) {
      let hint = document.getElementById(hintId);
      if (on) {
        document.addEventListener('mousemove', function (event) {
          hint.style.left = event.pageX + 10 + 'px';
          hint.style.top = event.pageY - window.pageYOffset + 10 + 'px';
        });
        hint.style.display = 'block';

      } else {
        hint.style.left = -1000 + 'px';
        hint.style.top = -1000 + 'px';
        hint.style.display = 'none';
      }
    }
  }

  saving = false;
  saveDocument() {
    this.helper.beforeUnload.start();
    this.saving = true;
    this.blockUI.start();

    const fileId = this.reportFile.map((f: any) => (f.idFile));
    const fileIdsToDelete = _.difference(this.document.fileId, fileId);
    this.document.fileId = fileId;

    this.document.content = this.reportContent;

    fileIdsToDelete.forEach((id) => {
      this.fileService.deleteFile(id);
    });

    this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document)
      .subscribe(() => {
      this.helper.beforeUnload.stop();
      this.saving = false;
      this.blockUI.stop();
      window.location.reload();
    }, (error) => {
      this.helper.beforeUnload.stop();
      console.log(error);
      this.saving = false;
      this.blockUI.stop();
    });
  }




  mail() {
    this.memoModalService.mail(this.document).subscribe(
      () => {
      },
      (error) => {
        console.log(error);
      });
  }

  finish() {
    this.alertService.confirm({
      okButtonText: 'Да',
      size: 'lg',
      message: 'Работа над служебной запиской будет завершена.',
      type: 'warning'
    }).then(() => {
      this.finishTask(false);
    }).catch(e => {

    });
  }

  reply() {
    if (this.instructions.length) {
      this.alertService.confirm({
        okButtonText: 'Да',
        size: 'lg',
        message: 'Есть невыполненное поручение по данной СЗ. Всё равно отправить ответ?',
        type: 'warning'
      }).then(() => {
        this.finishTask(true);
      }).catch(e => {

      });
    } else {
      this.finishTask(true);
    }
  }

  redirect() {
    new Observable(subscriber => {
      let ref = this.modalService.show(RedirectMemoModalComponent, { 'class': 'modal-lg' });
      let subscription = this.modalService.onHide.subscribe(() => {
        subscriber.next(ref);
        subscription.unsubscribe();
      });
    }).pipe(
      map((ref: any) => <RedirectMemoModalComponent> ref.content),
      mergeMap((component: any) => {
        if (component.redirect) {
          return new Observable<[UserBean[], string]>(subscriber => {
            this.alertService.confirm({
              message: 'Задача по рассмотрению будет перенаправлена указанным сотрудникам',
              okButtonText: 'Да',
              type: 'warning'
            }).then(() => {
              subscriber.next([component.users, component.comment]);
            }).catch(() => {
              subscriber.error('retry');
            })
          });
        } else {
          return throwError('cancel');
        }
      }),
      retryWhen(errors => {
        return errors.pipe(filter(e => e === 'retry'))
      }),
      mergeMap((res: [UserBean[], string]) =>  {
        this.helper.beforeUnload.start();
        this.blockUI.start();

        const users = res[0];
        const comment = res[1];
        const result = new MemoDocumentResult();
        result.reportExecutor = new DocumentUser();
        result.reportExecutor.login = this.session.login();
        result.reportExecutor.fioFull = this.session.fullName();
        result.reportExecutor.post = this.session.post();
        result.reportDate = new Date();
        // @ts-ignore
        result.processId = this.task.processInstanceId;
        result.reportButton = 'Перенаправил';
        result.redirectedBy = users.map(u => {
          let redirected = new DocumentUser();
          redirected.updateFromUserBean(u);
          return redirected;
        });
        result.reportContent = comment;
        this.document.result.push(result);
        return this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document).pipe(
          mergeMap(() => {
            const processVariables = [{
              'name': 'EntityIdVar',
              'value': this.document.documentId
            }, {
              'name': 'ExecutorsVar',
              'value': users.map(u => u.accountName).join(',')
            }];
            return this.memoActivityService.startProcess('sdomemo_StartMemoRedirect', this.document, processVariables);
          })
        );
      }),
      mergeMap(() => {
        return this._finishTask(false, this.session.fullName());
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.blockUI.stop();
      window.location.href = '/main/#/app/tasks';
    }, () => {
      this.blockUI.stop();
    });
  }

  finishTask(isComment: boolean) {
    this.helper.beforeUnload.start();
    this.blockUI.start();

    // if (parseInt(this.processVersion, 10) < 4) {
    //   this.memoDocumentService.updateDocumentStatus(this.document, this.dicts.statuses, 'viewed');
    // }

    if (this.reportContent.trim() === '' && isComment) {
      this.helper.beforeUnload.stop();
      this.toastr.error('Не указан комментарий!');
      this.blockUI.stop();
      return;
    }

    this.memoRestService.get(this.document.documentId).subscribe(
      (response) => {
        this.document.build(response.document);
        this.memoDocumentService
          .updateDocument(this.document.documentId, this.copyDocument, this.document)
          .pipe(
            mergeMap(() => {
              const result = new MemoDocumentResult();
              const reportExecutor = new DocumentUser();
              reportExecutor.login = this.session.login();
              reportExecutor.fioFull = this.session.fullName();
              reportExecutor.post = this.session.post();

              result.reportContent = this.reportContent;
              result.reportExecutor = reportExecutor;
              result.reportDate = new Date();
              // @ts-ignore
              result.processId = this.task.processInstanceId;
              result.reportButton = isComment ? 'Ответил' : 'Ознакомлен';
              result.reportFileId = this.reportFile.map((f) => (f.idFile));

              this.document.result.push(result);

              return this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document);
            }),
            mergeMap(() => {
              return this._finishTask(isComment, this.session.fullName());
            })
          ).subscribe(
            () => {
              this.helper.beforeUnload.stop();
              window.location.href = '/main/#/app/tasks';
            },
            (error) => {
              this.helper.beforeUnload.stop();
              this.blockUI.stop();
              console.log(error);
              this.loadingStatus = LoadingStatus.ERROR;
            });
      },
      (error) => {
        this.blockUI.stop();
        console.log(error);
        this.loadingStatus = LoadingStatus.ERROR;
      }
    );
  }

  private _finishTask(isReply, fio): Observable<any> {
    return from(this.activityRestService.getFormData({ taskId: this.task.id })).pipe(
      mergeMap(response => {
        return this.activityRestService.finishTask(parseInt(this.task.id, 10), [{
          name: 'IsReplyVar',
          value: isReply
        }, {
          name: 'ReplyUserVar',
          value: fio
        }]);
      }), tap(response => {
        this.toastr.success('Задача успешно завершена!');
      }), catchError(error => {
        this.toastr.error('Ошибка при завершении задачи!');
        return throwError(error);
      })
    );
  }

  // private updateStatusInWorkIfNeeded(): Observable<any> {
  //   if (this.document.statusId.code !== 'review' && this.document.statusId.code !== 'instruction') {
  //     const copyDocument = angular.copy(this.document);
  //     this.memoDocumentService.updateDocumentStatus(this.document, this.dicts.statuses, 'review');
  //     return this.memoDocumentService.updateDocument(copyDocument.documentId, copyDocument, this.document).pipe(
  //       tap(response => {
  //         this.toastr.success('Статус поручения успешно изменен!');
  //         this.$state.reload();
  //       }), catchError(error => {
  //         this.toastr.error('Ошибка при изменении статуса поручения!');
  //         return throwError(error);
  //       })
  //     );
  //   } else {
  //     return of('');
  //   }
  // }

  isActive(index: number): boolean {
    return index === this.selectedTab;
  }

  onTabSelected(index: number) {
    if (this.selectedTab === index) {
      this.hideOvers = !this.hideOvers;
    } else {
      this.hideOvers = true;
    }
    this.selectedTab = index;
  }

}
