import {ToastrService} from "ngx-toastr";
import {BsModalRef} from "ngx-bootstrap";
import {Component, OnInit} from '@angular/core';
import {UserBean} from "@reinform-cdp/nsi-resource";

@Component({
  selector: 'redirect-memo',
  templateUrl: './redirect-memo.modal.html'
})
export class RedirectMemoModalComponent implements OnInit {
  users: UserBean[] = [];
  comment: string = '';

  redirect = false;

  constructor(private bsModalRef: BsModalRef, private toastr: ToastrService) {
  }

  ngOnInit() {
  }

  ok() {
    if (this.users.length > 0) {
      this.redirect = true;
      this.bsModalRef.hide()
    } else {
      this.toastr.warning('Не выбран адресат');
    }
  }

  cancel() {
    this.bsModalRef.hide();
  }

}
