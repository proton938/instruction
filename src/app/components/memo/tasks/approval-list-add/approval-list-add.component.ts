import * as _ from "lodash";
import { mergeMap } from "rxjs/internal/operators";
import { NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApprovalType } from '../../../../models/instruction/ApprovalType';
import { OrderNsiService } from "../../../../services/order-nsi.service";
import { AddedInstructionApprovalListItem, InstructionApprover } from "../../../../models/instruction/InstructionApprovalList";

@Component({
  selector: 'mggt-approval-list-add',
  templateUrl: './approval-list-add.component.html',
  styleUrls: ['./approval-list-add.component.scss']
})
export class ApprovalListAddComponent implements OnInit {

  @Input() agreed: AddedInstructionApprovalListItem[];
  @Output() agreedChanged: EventEmitter<any> = new EventEmitter<any>();
  @Input() except: string[];
  @Input() systemCode: string;
  @Input() dueDate: Date;
  @Input() approvalNum: string;
  @Input() alreadyAddedCounter: number;

  approversAssent: UserBean[] = [];
  approversAgreed: UserBean[] = [];
  approversAssentFiltered: UserBean[] = [];
  approversAgreedFiltered: UserBean[] = [];
  approvalTypes: ApprovalType[] = [];

  constructor(private nsiResourceService: NsiResourceService, private orderNsiService: OrderNsiService) {
  }

  ngOnInit() {
    this.nsiResourceService.get('mggt_memo_ApprovalType')
      .then((response: ApprovalType[]) => {
        this.approvalTypes = response.filter((approvalType: ApprovalType) => {
          return approvalType.approvalTypeCode === 'assent' || approvalType.approvalTypeCode === 'agreed';
        });

        const group = ['MGGT_MEMO_APPROVE'];

        return this.nsiResourceService.searchUsers({ group });
      })
      .then((approvers: UserBean[]) => {
        this.approversAssent = approvers;

        const group = ['MGGT_MEMO_APPROVE', 'MGGT_ELECTRONIC_SIGN'];

        return this.nsiResourceService.searchUsers({ group });
      })
      .then((approvers: UserBean[]) => {
        this.approversAgreed = approvers;
      })
      .then(() => {
        this.recalculateApprovers();
      });
  }

  recalculateApprovers() {
    this.approversAgreedFiltered = this.approversAgreed
      .filter((a) => (_.indexOf(this.except, a.accountName) < 0))
      .filter((a) => (_.indexOf(this.agreed.map(agreed => agreed.agreedBy ? agreed.agreedBy.accountName : ''), a.accountName) < 0))
      .map((a) => InstructionApprover.fromUserBean(a));

    this.approversAssentFiltered = this.approversAssent
      .filter((a) => (_.indexOf(this.except, a.accountName) < 0))
      .filter((a) => (_.indexOf(this.agreed.map(agreed => agreed.agreedBy ? agreed.agreedBy.accountName : ''), a.accountName) < 0))
      .map((a) => InstructionApprover.fromUserBean(a));
  }

  setNumeration() {
    _.each(this.agreed, (item, index) => {
      item.approvalNum = `${this.approvalNum}.${index + this.alreadyAddedCounter + 1}`;
    });
  }

  swapApprovals(ind1: number, ind2: number) {
    const app = this.agreed[ind1];

    this.agreed[ind1] = this.agreed[ind2];
    this.agreed[ind2] = app;
    this.setNumeration();
    this.agreedChanged.emit();
  }

  delApproval(ind: number) {
    this.agreed.splice(ind, 1);
    this.setNumeration();
    this.recalculateApprovers();
    this.agreedChanged.emit();
  }

  addApproval() {
    const agreed = new AddedInstructionApprovalListItem();
    const approvalType = _.find(this.approvalTypes, (at: ApprovalType) => {
      if (!this.systemCode) {
        return at.approvalTypeCode === ApprovalType.assent;
      }
      return at.approvalTypeCode === ApprovalType.agreed;
    });

    agreed.approvalType = approvalType.approvalType;
    agreed.approvalTypeCode = approvalType.approvalTypeCode;

    if (this.dueDate) {
      this.orderNsiService.toLocalDateTime(this.dueDate).pipe(
        mergeMap(value => {
          return this.orderNsiService.calcDuration(value);
        }),
        mergeMap(value => {
          return this.orderNsiService.divideTimeBy(value, this.agreed.length + 1);
        }),
        mergeMap(approvalTime => {
          this.agreed.push(agreed);
          this.agreed.forEach(agree => {
            agree.approvalTime = approvalTime;
          });
          this.setNumeration();
          const now = new Date();
          return this.orderNsiService.addDuration(now, approvalTime);
        })
      ).subscribe((approvalPlanDate) => {
        const date = new Date(approvalPlanDate);
        this.agreed.forEach(agree => {
          agree.approvalPlanDate = date;
        });
      });
    } else {
      this.agreed.push(agreed);
      this.setNumeration();
    }

    this.agreedChanged.emit();
  }

  approvalTypeChanged(ind: number) {
    const approvalType = _.find(this.approvalTypes, (at: ApprovalType) => {
      return at.approvalTypeCode === this.agreed[ind].approvalTypeCode;
    });

    if (approvalType) {
      this.agreed[ind].approvalType = approvalType.approvalType;


      let validationList;

      if (approvalType.approvalTypeCode === 'assent') {
        validationList = this.approversAssentFiltered;
      } else if (approvalType.approvalTypeCode === 'agreed') {
        validationList = this.approversAgreedFiltered;
      }

      if (this.agreed[ind].agreedBy) {
        const exists = validationList.find((a) => a.accountName === this.agreed[ind].agreedBy.accountName);

        if (!exists) {
          this.agreed[ind].agreedBy = null;
        }
      }

    } else {
      this.agreed[ind].approvalType = null;
      this.agreed[ind].agreedBy = null;
    }

    this.recalculateApprovers();
    this.setNumeration();
    this.agreedChanged.emit();
  }
}
