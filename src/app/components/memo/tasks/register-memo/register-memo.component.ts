import * as angular from 'angular';
import { StateService } from '@uirouter/core';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SearchResultDocument } from '@reinform-cdp/search-resource';
import { LoadingStatus } from '@reinform-cdp/widgets';
import { ActiveTaskService, ActivityResourceService, ITask } from '@reinform-cdp/bpm-components';
import { MemoDictsModel } from '../../../../models/memo/MemoDictsModel';
import { MemoDocument } from '../../../../models/memo-document/MemoDocument';
import { SessionStorage } from '@reinform-cdp/security';
import { MemoDocumentService } from '../../../../services/memo-document.service';
import { FileService } from '../../../../services/file.service';
import { OrderDocumentService } from '../../../../services/order-document.service';
import { MemoRestService } from '../../../../services/memo-rest.service';
import { MemoActivitiService } from '../../../../services/memo-activiti.service';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { catchError, mergeMap, tap } from 'rxjs/internal/operators';
import { Observable, from, of, throwError } from 'rxjs';
import { DocumentUser } from '../../../../models/core/DocumentUser';
import { DocumentComment } from '../../../../models/core/DocumentComment';
import { MemoDictsService } from '../../../../services/memo-dicts.service';
import { MemoDocumentReview } from '../../../../models/memo-document/MemoDocumentReview';
import { FileType } from '../../../../models/instruction/FileType';
import {HelperService} from '../../../../services/helper.service';

@Component({
  selector: 'mggt-register-memo',
  templateUrl: './register-memo.component.html',
  styleUrls: ['./register-memo.component.scss']
})
export class RegisterMemoComponent implements OnInit, OnDestroy {

  document: MemoDocument;
  copyDocument: MemoDocument;
  dicts: MemoDictsModel;
  task: ITask;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;

  draftFiles: FileType[] = [];

  instructions: SearchResultDocument[];
  instructionDicts: any;
  comment = '';
  selectedTab = 0;
  hideOvers = true;

  alreadyHasComment = false;

  @BlockUI('reviewMemo') blockUI: NgBlockUI;

  constructor(public fileService: FileService,
    private activeTaskService: ActiveTaskService,
    private instructionDocumentService: OrderDocumentService,
    private memoRestService: MemoRestService,
    private memoDictsService: MemoDictsService,
    private memoDocumentService: MemoDocumentService,
    private memoActivityService: MemoActivitiService,
    private toastr: ToastrService,
    public $state: StateService,
    private session: SessionStorage,
    private activityRestService: ActivityResourceService,
    private nsiRestService: NsiResourceService,
    public helper: HelperService) {
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.memoDictsService.getDicts();
      }),
      mergeMap(response => {
        this.dicts = response;
        return this.nsiRestService.getDictsFromCache(['InstructionStatus', 'InstructionPriority']);
      }), mergeMap(response => {
        this.instructionDicts = {
          statuses: response.InstructionStatus,
          priorities: response.InstructionPriority
        };
        return this.memoActivityService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.memoRestService.get(resp);
      }),
      tap(response => {
        this.document = new MemoDocument();
        this.document.build(response.document);

        this.copyDocument = angular.copy(this.document);

        const currentUser = this.instructionDocumentService.getCurrentUser();

        if (this.document.review) {
          this.document.review.some((review) => {
            if (review.reviewBy.login === currentUser.accountName) {
              this.alreadyHasComment = true;
              this.comment = review.reviewNote;

              return true;
            }

            return false;
          });
        }

        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      }),
      catchError(error => {
        console.log(error);
        this.loadingStatus = LoadingStatus.ERROR;
        return throwError(error);
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  updateReview() {
    const comment = this.comment.trim();
    const currentUser = this.instructionDocumentService.getCurrentUser();
    const documentCurrentUser = new DocumentUser();
    documentCurrentUser.login = currentUser.accountName;
    documentCurrentUser.fioFull = currentUser.displayName;
    documentCurrentUser.post = currentUser.post;

    const newReview = new MemoDocumentReview();
    newReview.reviewFactDate = new Date();
    newReview.reviewBy = documentCurrentUser;

    if (this.alreadyHasComment) {
      this.document.review.forEach((review) => {
        if (review.reviewBy.login === currentUser.accountName) {
          review.reviewNote = comment;
        }
      });
    } else if (comment) {
      const newReview = new MemoDocumentReview();
      newReview.reviewFactDate = new Date();
      newReview.reviewNote = this.comment;
      newReview.reviewBy = documentCurrentUser;

      this.document.review.push(newReview);
    }

    if (!this.document.draftFiles.length) {
      this.document.draftFiles = this.draftFiles.map((draftFile) => {
        draftFile.dateFile = new Date(draftFile.dateFile);
        return draftFile;
      });
    }
  }

  cancel() {
    window.location.href = '/main/#/app/tasks';
  }

  save() {
    this.helper.beforeUnload.start();
    this.blockUI.start();
    this.updateReview();

    this.memoDocumentService
      .updateDocument(this.document.documentId, this.copyDocument, this.document)
      .subscribe(() => {
        this.helper.beforeUnload.stop();
      }, (error) => {
        this.helper.beforeUnload.stop();
        this.blockUI.stop();
      });
  }

  sign() {
    this.helper.beforeUnload.start();
    this.blockUI.start();
    this.updateReview();
    this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document).pipe(
      mergeMap(response => {
        return this._finishTask();
      }),
      tap(() => {
        this.memoActivityService.startProcess('sdomemo_ReviewMemo', this.document);
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      window.location.href = '/main/#/app/tasks';
    }, (error) => {
      this.helper.beforeUnload.stop();
      console.log(error);
      this.blockUI.stop();
    });

  }

  private _finishTask(): Observable<any> {
    return from(this.activityRestService.getFormData({ taskId: this.task.id })).pipe(
      mergeMap(response => {
        return this.activityRestService.finishTask(parseInt(this.task.id));
      }), tap(response => {
        this.toastr.success('Задача успешно завершена!');
      }), catchError(error => {
        this.toastr.error('Ошибка при завершении задачи!');
        return throwError(error);
      })
    );
  }
}
