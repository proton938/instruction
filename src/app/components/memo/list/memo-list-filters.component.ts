import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsLocaleService } from "ngx-bootstrap/datepicker";
import { UserBean } from '@reinform-cdp/nsi-resource';
import { val } from '@uirouter/core';
import { formatDate } from "@angular/common";

@Component({
    selector: 'memo-list-filters',
    templateUrl: './memo-list-filters.component.html',
    styleUrls: ['./memo-list-filters.component.scss']
})
export class MemoListFiltersComponent implements OnInit {
    @Input() filter: any;
    @Input() dicts: any;
    @Output() filterChanged: EventEmitter<any> = new EventEmitter();
    @Output() filterReset: EventEmitter<any> = new EventEmitter();

    isExtendedSearch: boolean = false;

    constructor() {
    }
    test = '';

    ngOnInit() {
    }

    onFilterChange() {
        this.filterChanged.emit(this.filter);
    }

    changeLdapField(user: UserBean, field: string) {
        this.filter[field] = user;
        this.onFilterChange();
    }

    changeCheckbox(value: any) {
        this.filter.privateInst = value;
        this.onFilterChange();
    }

    changeDateField(value: any, field: string, direction: string) {
        if (value) {
            const date = new Date(value);
            this.filter[field] = this.filter[field] || {};
            this.filter[field][direction] = formatDate(date, 'yyyy-MM-dd', 'en');
            this.onFilterChange();
        }
    }

    changeTextField(value: any, field: string) {
        console.log(value);
        this.filter[field] = value;
        this.onFilterChange();
    }

    clear() {
        this.filterReset.emit();
    }

    toggleExtendSearch() {
        this.isExtendedSearch = !this.isExtendedSearch;
    }
}
