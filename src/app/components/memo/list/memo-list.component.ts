import {Component, Input, OnInit} from '@angular/core';
import {
  FacetedSearchResult, QuerySearchData, SearchExtData, SearchExtDataItem
} from '@reinform-cdp/search-resource';
import {from, Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {NsiResourceService, UserBean} from '@reinform-cdp/nsi-resource';
import {ShowcaseSorting} from '../../../models/sorting/ShowcaseSorting';
import {ShowcaseMemoSortingBuilder} from '../../../models/sorting/ShowcaseMemoSortingBuilder';
import {SolrMediatorService} from '../../../services/solr-mediator.service';

@Component({
  selector: 'memo-list',
  templateUrl: './memo-list.component.html',
  styleUrls: ['./memo-list.component.scss']
})
export class MemoListComponent implements OnInit {
  @Input() filter: any;
  @Input() dicts: any;
  @Input() preFilter: any;
  @Input() filterChanged: Subject<any>;

  isLoading: boolean;
  searchRequest: any;
  searchResult: FacetedSearchResult;
  currentPage: number = 1;
  searchQuery: string = '';
  sortings: ShowcaseSorting[] = [];

  constructor(private solrMediator: SolrMediatorService,
              private toastr: ToastrService) {
    this.isLoading = true;
  }

  ngOnInit() {

    let sortingBuilder = new ShowcaseMemoSortingBuilder();
    this.sortings = sortingBuilder.build();

    this.filterChanged.subscribe(() => {
      this.onFilterChange();
    });
    this.searchRequest = {
      common: '',
      page: this.currentPage - 1,
      pageSize: 10,
      sort: this.sortingsToString(),
      types: [ this.solrMediator.types.memo ]
    };
    this.search();
  }

  search() {
    this.isLoading = true;
    const req: QuerySearchData = this.searchRequest;
    req.query = this.buildFilter();
    req.sort = this.sortingsToString();
    this.solrMediator.query(req).subscribe(resp => {
      this.searchResult = resp;
      this.searchResult.docs = this.searchResult.docs || [];
      this.searchResult.docs.map(d => {
        const status = this.dicts.statuses.find(s => s.name === d['statusMemo']);
        d['statusColor'] = status ? status.color : 'default';
      });
      this.isLoading = false;
    }, err => {
      this.toastr.error(err && err.data ? err.data.exception : 'Произошла ошибка!');
      console.log(err);
      this.isLoading = false;
    });
  }

  buildFilter(): string {
    let result = '';
    if (this.preFilter) {
      result += `${this.preFilter}`
    }
    if (!this.filter.isEmpty()) {
      let _filter = this.filter.toString();
      result += result ? ' AND ' : '';
      result += `(${_filter})`;
    }
    return result;
  }

  commonSearch(common: string) {
    this.searchRequest.common = common;
    this.searchRequest.page = 0;
    this.search();
  }

  // prepareFilter(params?): SearchExtDataItem[] {
  //   let result: SearchExtDataItem[] = [];
  //   if (this.filter.statusMemo && this.filter.statusMemo.length) {
  //     result.push(new SearchExtDataItem('statusMemo', this.filter.statusMemo.map(i => i.name)));
  //   }
  //   if (this.filter.creatorMemo && this.filter.creatorMemo.name) {
  //     result.push(new SearchExtDataItem('creatorLogMemo', this.filter.creatorMemo.name));
  //   }
  //   if (this.filter.executorMemo && this.filter.executorMemo.name) {
  //     result.push(new SearchExtDataItem('executorLogMemo', this.filter.executorMemo.name));
  //   }
  //   if (this.filter.contentMemo) {
  //     result.push(new SearchExtDataItem('contentMemo', this.filter.contentMemo));
  //   }
  //   if (this.filter.docNumberMemo) {
  //     result.push(new SearchExtDataItem('docNumberMemo', this.filter.docNumberMemo));
  //   }
  //   if (this.filter.docDateMemo && !this.isDateRangeEmpty(this.filter.docDateMemo)) {
  //     result.push(new SearchExtDataItem('docDateMemo', this.filter.docDateMemo));
  //   }
  //
  //   return result;
  // }

  changeDateRangeField = function () {
    this.onFilterChange();
  }.bind(this);

  isDateRangeEmpty(obj: any): boolean {
    return obj.from === null && obj.start === null;
  }

  changePage() {
    this.searchRequest.page = this.currentPage - 1;
    this.search();
  }

  onFilterChange() {
    // this.searchRequest.fields = this.prepareFilter();
    this.search();
  }

  changeLdapField(user: UserBean, field: string) {
    this.filter[field] = user;
    this.onFilterChange();
  }

  sortingChanged(s: ShowcaseSorting) {
    this.sortings.forEach(_s => {
      if (s !== _s) {
        _s.value = 'none';
      } else {
        _s.value = (_s.value === 'none') ? 'desc' : ((_s.value === 'desc') ? 'asc' : 'desc');
      }
    });
    this.search();
  }

  sortingsToString(): string {
    let result = '';

    this.sortings.forEach(s => {
      if (s.value !== 'none') {
        result = `${s.code} ${s.value}`;
      }
    });

    return result;
  }
}
