import * as _ from "lodash";
import { NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource"
import { SessionStorage } from "@reinform-cdp/security";
import { StateService } from '@uirouter/core';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import {
  FacetedSearchResult, QuerySearchData, SearchExtData, SolarResourceService, SearchResultDocument
} from '@reinform-cdp/search-resource';
import { from } from 'rxjs/index';
import { ToastrService } from 'ngx-toastr';
import { Subject } from "rxjs/Rx";
import { MemoRestService } from "./../../../../../services/memo-rest.service";
import { MEMOFilter } from "./../../../../../models/memo/MemoFilter";


@Component({
  selector: 'mggt-mobile-memo-list',
  templateUrl: './mobile-memo-list.component.html',
  styleUrls: ['./mobile-memo-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MobileMemoListComponent implements OnInit {
  @Input() filter: MEMOFilter;
  @Input() overdue: { value: boolean };
  @Input() dicts: any;
  @Input() preFilter: any;
  @Input() filterChanged: Subject<any>;
  isLoading: boolean;
  searchRequest: SearchExtData;
  searchResult: FacetedSearchResult;
  currentPage: number = 0;
  selectedSorting: any;
  sortings: any[] = [];
  isDocsLoaded: boolean = false;
  filtersCount: number = 0;
  appendBlock: boolean = false;
  extSearchCollapsed = true;

  cancelation;
  authors: UserBean[] = [];
  assistants: UserBean[] = [];
  executors: UserBean[] = [];
  coExecutors: UserBean[] = [];
  forInformationLogins: UserBean[] = [];

  constructor(public $state: StateService, public solarResourceService: SolarResourceService, private memoRestService: MemoRestService, private session: SessionStorage, public toastr: ToastrService, private nsiResourceService: NsiResourceService, ) {
  }

  ngOnInit() {
    this.filterChanged.subscribe(() => {
      this.onFilterChange();
    });
    this.sortings = [{
      code: 'docDateMemo',
      direction: 'desc',
      name: 'Сначала новые',
      id: 1,
    }, {
      code: 'docDateMemo',
      direction: 'asc',
      name: 'Сначала старые',
      id: 2,
    }, {
      code: 'docDateMemo',
      direction: 'desc',
      name: 'За текущее число',
      id: 3,
    }];
    this.selectedSorting = this.sortings[0];
    this.searchRequest = new SearchExtData(null, this.currentPage, 20, this.sortingsToString());
    this.searchRequest.types = ['SDO_INSTRUCTION_MEMO'];
    this.search();
  }

  getLabelClass(statusCode: string) {
    if (statusCode === 'viewed' || statusCode === 'registered') {
      return 'success';
    }
    if (statusCode === 'send' || statusCode === 'review' || statusCode === 'instruction') {
      return 'primary';
    }
    if (statusCode === 'approval') {
      return 'info';
    }
  }

  mobileChangeSorting(sort: any): void {

    if (this.selectedSorting.id == sort.id) {
      return;
    }
    this.selectedSorting = sort;
    this.search();
  }

  mobileLoadMore() {
    if (this.isDocsLoaded) {
      return;
    }
    this.appendBlock = true;
    let searchRequest = _.clone(this.searchRequest);
    searchRequest.page = (this.searchResult.docs.length / this.searchResult.pageSize);
    from(this.memoRestService.search(searchRequest)).subscribe(response => {
      let loadedDocs = <SearchResultDocument[]>response.docs;
      this.searchResult.docs = this.searchResult.docs.concat(loadedDocs);
      this.isDocsLoaded = this.searchResult.docs.length === this.searchResult.numFound;
      this.searchResult.numFound = response.numFound;
      this.searchResult.pageSize = response.pageSize;
      this.appendBlock = false;
    }, error => {
      console.log(error);
      this.appendBlock = false;
    });
  }

  mobileFilterChange() {
    this.filtersCount = this.filter.count();
    this.onFilterChange();
  }

  mobileClearFilters() {
    this.filter.clear();
    this.mobileFilterChange();
  }

  solicitorSearch(data: { search: string, users: UserBean[] }) {
    if (data.search && data.search.length > 2) {
      if (this.cancelation) {
        clearTimeout(this.cancelation);
      }
      this.cancelation = setTimeout(() => {
        this.nsiResourceService.searchUsers({ fio: data.search }).then(_response => {
          data.users.length = 0;
          _.forEach(_response, r => {
            data.users.push(r);
          });
        }).catch(error => {
          this.toastr.error('Ошибка при получении информации из справочника!');
        });
      }, 500);
    }
  }

  search() {
    this.isLoading = true;
    const req: QuerySearchData = this.searchRequest;
    if (!req.common) {
      delete req.common;
    }
    req.query = this.buildFilter();
    if (!req.query) {
      delete req.query;
    }
    req.sort = this.sortingsToString();
    from(this.memoRestService.search(req)).subscribe(resp => {
      this.searchResult = resp;
      this.searchResult.docs = this.searchResult.docs || [];
      this.isLoading = false;
    }, err => {
      this.toastr.error(err && err.data ? err.data.exception : 'Произошла ошибка!');
      console.log(err);
      this.isLoading = false;
    });
  }

  commonSearch(common: string) {
    this.searchRequest.common = common;
    this.searchRequest.page = 0;
    this.search();
  }

  buildFilter(): string {
    let result = '';
    if (this.preFilter) {
      result += this.preFilter;
    }
    if (!this.filter.isEmpty()) {
      result += (result) ? " AND " : "";
      result += this.filter.toString()
    }
    return result;
  }

  changeDateRangeField = function () {
    this.onFilterChange();
  }.bind(this);

  isDateRangeEmpty(obj: any): boolean {
    return obj.from === null && obj.start === null;
  }

  changePage() {
    this.searchRequest.page = this.currentPage - 1;
    this.search();
  }

  onFilterChange() {
    this.searchRequest.page = 0;
    this.search();
  }

  changeLdapField(user: UserBean, field: string) {
    this.filter[field] = user;
    this.onFilterChange();
  }

  sortingsToString(): string {
    return `${this.selectedSorting.code} ${this.selectedSorting.direction}`
  }
}
