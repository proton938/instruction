import { StateService, Transition } from '@uirouter/core';
import { ToastrService } from 'ngx-toastr';
import { Component, ViewEncapsulation, ViewChild } from '@angular/core'
import { AuthorizationService, SessionStorage } from "@reinform-cdp/security";
import { SolarResourceService } from "@reinform-cdp/search-resource";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import { Subject, from } from 'rxjs';
import * as _ from 'lodash';
import { LoadingStatus } from "@reinform-cdp/widgets";
import { MobileMemoListComponent } from './mobile-memo-list/mobile-memo-list.component';
import { MEMOFilter } from './../../../../models/memo/MemoFilter';


let tabs = [
  {
    code: 'memo',
    name: 'Внутренние документы',
    show: true,
    showcaseCode: 'SDO_MEMO',
    filter: (user: string): string => ''
  },
  {
    code: 'memo-by-user',
    name: 'Направленные мной',
    show: true,
    showcaseCode: 'SDO_MEMO_BY_USER',
    filter: (user: string): string => `((userCreatorRecipientMemo:*${user}*))`
  },
  {
    code: 'memo-for-user',
    name: 'Направленные мне',
    show: true,
    showcaseCode: 'SDO_MEMO_FOR_USER',
    filter: (user: string): string =>`((executorLogMemo:"${user}"))`
  },
  {
    code: 'memo-viewed',
    name: 'Рассмотренные',
    show: true,
    showcaseCode: 'SDO_MEMO_VIEWED',
    filter: (user: string): string =>`(statusMemo:"Рассмотрена") AND ((userShowcaseMemo:*${user}*))`
  },
  {
    code: 'memo-draft',
    name: 'Проекты мои',
    show: true,
    showcaseCode: 'SDO_MEMO_DRAFT',
    filter: (user: string): string =>`(statusMemo:"Проект") AND  ((userShowcaseMemo:*${user}*))`
  }
];

@Component({
  selector: 'mggt-memo-showcase-mobile',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShowcaseMemoMobileComponent {
  @ViewChild(MobileMemoListComponent)
  memos: MobileMemoListComponent;
  hideOvers: boolean = true;
  loading: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  activeTab: string;
  searchQuery: string = '';

  tabs: any[];
  dicts: any = {
    statuses: [],
    themes: [],
    priorities: [],
    difficulties: [],
    executors: [],
    types: [],
    _themes: []
  };
  filter: MEMOFilter = new MEMOFilter();
  overdue = { value: false };
  preFilter: any;
  updateList: Subject<any> = new Subject();
  creatingReport: boolean = false;

  constructor(public $state: StateService,
    private solarResourceService: SolarResourceService,
    private toastr: ToastrService,
    private nsiRestService: NsiResourceService,
    private session: SessionStorage,
    private authorizationService: AuthorizationService,
    private transition: Transition) {
  }

  ngOnInit() {
    this.filter.clear();
    this.tabs = tabs.filter(i => i.show);
    this.activeTab = this.transition.params()['tab'] || 'memo';
    from(this.nsiRestService.getDictsFromCache([
      'mggt_instruction_memo_status',
      'MemoKind',
      'MemoHeadingTopic',
      'MemoTrestActivityKind',
      'mggt_privacy'
    ])).subscribe(response => {
      this.dicts = {
        statuses: _.orderBy(response.mggt_instruction_memo_status.filter((i: any) => i.code !== 'draft'), ['name'], ['asc']),
        memoKindes: _.orderBy(response.MemoKind, ['name'], ['asc']),
        memoHeadingTopics: _.orderBy(response.MemoHeadingTopic, ['name'], ['asc']),
        memoTrestActivityKindes: _.orderBy(response.MemoTrestActivityKind, ['name'], ['asc']),
        privacies: _.orderBy(response.mggt_privacy, ['name'], ['asc']),
      };
      this.makePreFilters();
      this.loading = LoadingStatus.SUCCESS;
      this.success = true;
    }, err => {
      this.toastr.error(err.data.exception);
      this.loading = LoadingStatus.ERROR;
      console.log(err);
    });
  }

  filterChange(filter: any) {
    this.filter = filter;
    this.updateList.next();
  }

  searchChanged() {
    if (this.memos) {
      this.memos.commonSearch(this.searchQuery);
    }
  }

  makePreFilters() {
    this.preFilter = {};
    this.tabs.forEach(tab => {
      this.preFilter[tab.code] = tab.filter(this.session.login());
    })
  }

  clear() {
    this.filter.clear();
    if (this.memos) {
      setTimeout(() => {
        this.updateList.next()
      }, 200);
    }
  }

  isActive(index: string): boolean {
    return index === this.activeTab;
  }

  activate(index: string): boolean {
    if (this.isActive(index)) {
      this.hideOvers = !this.hideOvers;
      return false;
    }
    this.hideOvers = true;
    if (this.activeTab === index) {
      return false;
    } else {
      this.$state.go('app.instruction.showcase-memo', { tab: index });
      this.activeTab = index;
      return true;
    }
  }

  loseFocus() {
    setTimeout(() => {
      console.log('showcase loseFocus');
      let element = document.activeElement;
      if (element && element instanceof HTMLInputElement) {
        element.blur();
      }
    })
  }
}
