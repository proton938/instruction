import { Component, OnInit, ViewChild } from '@angular/core';
import { SessionStorage } from "@reinform-cdp/security";
import { FacetedSearchResult, SolarResourceService, SearchExtData, SearchExtDataItem } from '@reinform-cdp/search-resource';
import { from } from 'rxjs/index';
import { ToastrService } from 'ngx-toastr';
import { NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import * as _ from "lodash";
import { Observable, Subject, concat, of } from "rxjs";
import { distinctUntilChanged, debounceTime, switchMap, tap, catchError } from 'rxjs/operators'
import { MemoListComponent } from '../list/memo-list.component';
import { MEMOFilter } from '../../../models/memo/MemoFilter';
import { ShowcaseMEMOTabBuilder } from '../../../models/tabs/ShowcaseMEMOTabBuilder';
import { InstructionFilterService } from '../../../services/instruction-filter.service';

@Component({
  selector: 'memo-showcase',
  templateUrl: './memo-showcase.component.html',
  styleUrls: ['./memo-showcase.component.scss']
})
export class MemoShowcaseComponent implements OnInit {

  @ViewChild(MemoListComponent)
  memo: MemoListComponent;

  isLoading: boolean;
  activeTab: string = 'memoAll';
  searchQuery: string = '';
  tabs: any[] = [];
  dicts: any = {
    statuses: [],
    executors: []
  };
  filter: MEMOFilter = new MEMOFilter();
  preFilter: any;
  updateList: Subject<any> = new Subject();


  constructor(private solarResourceService: SolarResourceService,
    private toastr: ToastrService,
    private nsiRestService: NsiResourceService,
    private session: SessionStorage,
    private filterService:InstructionFilterService) {
    this.isLoading = true;
  }

  ngOnInit() {
    this.filterService.clearAssignmentFilter();
    this.filter.clear();

    from(this.nsiRestService.getDictsFromCache([
      'MemoStatus',
      'InstructionExecutors'
    ])).subscribe(response => {
      this.dicts = {
        statuses: _.orderBy(response.MemoStatus.filter((i: any) => i.code !== 'draft'), ['name'], ['asc']),
        executors: response.InstructionExecutors
      };
      let tabBuilder = new ShowcaseMEMOTabBuilder(this.dicts.executors, this.session);
      this.tabs = tabBuilder.build();
      this.makePreFilters();
      
      this.isLoading = false;
    }, err => {
      this.toastr.error(err.data.exception);
      this.isLoading = false;
      console.log(err);
    });
  }

  filterChange(filter: any) {
    this.filter = filter;
    this.updateList.next();
  }

  searchChanged() {
    if (this.memo) {
      this.memo.commonSearch(this.searchQuery);
    }
  }

  makePreFilters() {
    this.preFilter = {};
    this.tabs.forEach(tab => {
      this.preFilter[tab.alias] = tab.filter;
    })
  }

  clear() {
    this.filter.clear();
    setTimeout(() => this.updateList.next(), 200);
  }

  isActive(index: string): boolean {
    return index === this.activeTab;
  }

  activate(index: string): boolean {
    if (this.activeTab === index) {
      return false;
    } else {
      this.activeTab = index;
      return true;
    }
  }
}
