import {Component, Input} from '@angular/core';
import * as _ from 'lodash';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {HelperService} from '../../../../services/helper.service';
import {from, Observable} from 'rxjs';
import {of} from 'rxjs';
import {ShowcaseField, ShowcaseSettings, ShowcaseTableColumn} from '../../../../models/showcase/ShowcaseSettings';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'mggt-memo-showcase-desktop',
  templateUrl: './desktop.component.html',
  styleUrls: [
    './desktop.component.scss'
  ]
})
export class ShowcaseMemoDesktopComponent {

  limitedAccessValue: boolean = false;

  model: any;
  searchResult;
  searchResults;
  advancedSearchExpand = false;
  firstLoading = true;
  permission: string[] = [];
  login: string = '';
  fields: ShowcaseField[] = [
    new ShowcaseField({
      code: 'statusMemo',
      name: 'Статус',
      type: 'dictionary',
      dictionary: 'mggt_instruction_memo_status',
      dictionaryCode: 'name',
      dictionaryText: 'name',
      sortable: true
    }),
    new ShowcaseField({
      code: 'kindMemo',
      name: 'Вид документа',
      type: 'dictionary',
      dictionary: 'MemoKind',
      dictionaryCode: 'name',
      dictionaryText: 'name',
      sortable: true
    }),
    new ShowcaseField({
      code: 'creatorMemo',
      name: 'Кто подписал',
      type: 'user'
    }),
    new ShowcaseField({
      code: 'executorMemo',
      name: 'Кому',
      type: 'user'
    }),
    new ShowcaseField({
      code: 'recipientMemo',
      name: 'Исполнитель',
      type: 'user'
    }),
    new ShowcaseField({
      code: 'contentMemo',
      name: 'Содержание',
      type: 'text'
    }),
    new ShowcaseField({
      code: 'descriptionMemo',
      name: 'Текст',
      type: 'text'
    }),
    new ShowcaseField({
      code: 'headingTopicMemo',
      name: 'Тема рубрики',
      type: 'dictionary',
      dictionary: 'MemoHeadingTopic',
      dictionaryCode: 'name',
      dictionaryText: 'name'
    }),
    new ShowcaseField({
      code: 'activityKindMemo',
      name: 'Вид деятельности Треста',
      type: 'dictionary',
      dictionary: 'MemoTrestActivityKind',
      dictionaryCode: 'name',
      dictionaryText: 'name'
    }),
    new ShowcaseField({
      code: 'privacyMemo',
      name: 'Конфиденциальность',
      type: 'dictionary',
      dictionary: 'mggt_privacy',
      dictionaryCode: 'name',
      dictionaryText: 'name'
    }),
    new ShowcaseField({
      code: 'assistantMemo',
      name: 'Карточку создал',
      type: 'user'
    }),
    new ShowcaseField({
      code: 'docDateMemo',
      name: 'Дата служебной записки',
      type: 'date',
      sortable: true
    }),
    new ShowcaseField({
      code: 'docNumberMemo',
      name: 'Номер',
      type: 'text',
      sortable: true
    }),
    new ShowcaseField({
      code: 'relateDocNum',
      name: 'Связанные документы. СДО.',
      type: 'text'
    }),
    new ShowcaseField({
      code: 'linkDocNum',
      name: 'Связанные документы. Внешние.',
      type: 'text'
    }),
    new ShowcaseField({
      code: 'limitedAccess',
      name: 'Ограниченный доступ',
      type: 'boolean'
    })
  ];
  settings: any = {
    loading: true,
    documentCode: 'SDO_INSTRUCTION_MEMO',
    changeNumberOfElementsPerPage: true,
    settings: {
      fields: this.fields,
      tableFilter: true,
      filters: [
        {
          code: 'statusMemo',
          name: 'Статус',
          multiple: true,
          placeholder: 'Статус...'
        },
        {
          code: 'kindMemo',
          name: 'Вид документа',
          multiple: true,
          placeholder: 'Вид документа...'
        },
        {
          code: 'creatorMemo',
          name: 'Кто подписал',
          multiple: false,
          placeholder: 'Для начала поиска введите не менее трех символов'
        },
        {
          code: 'executorMemo',
          name: 'Кому',
          multiple: false,
          placeholder: 'Для начала поиска введите не менее трех символов'
        },
        {
          code: 'recipientMemo',
          name: 'Исполнитель',
          multiple: false,
          placeholder: 'Для начала поиска введите не менее трех символов'
        },
        {
          code: 'contentMemo',
          name: 'Содержание'
        },
        {
          code: 'descriptionMemo',
          name: 'Текст'
        },
        {
          code: 'headingTopicMemo',
          name: 'Тема рубрики',
          multiple: true
        },
        {
          code: 'activityKindMemo',
          name: 'Вид деятельности Треста',
          multiple: true
        },
        {
          code: 'privacyMemo',
          name: 'Конфиденциальность',
          multiple: true
        },
        {
          code: 'assistantMemo',
          name: 'Карточку создал',
          multiple: false
        },
        {
          code: 'docDateMemo',
          name: 'Дата служебной записки'
        },
        {
          code: 'docNumberMemo',
          name: 'Номер'
        },
        {
          code: 'relateDocNum',
          name: 'Связанные документы. СДО'
        },
        {
          code: 'linkDocNum',
          name: 'Связанные документы. Внешние'
        },
        {
          code: 'limitedAccess',
          name: 'Ограниченный доступ'
        }
      ],
      tableColumns: [
        new ShowcaseTableColumn({
          code: 'statusMemo',
          name: 'Статус',
          filterable: true
        }),
        new ShowcaseTableColumn({
          code: 'kindMemo',
          name: 'Вид документа',
          filterable: true
        }),
        new ShowcaseTableColumn({
          code: 'docNumberMemo',
          name: 'Номер',
          filterable: true
        }),
        new ShowcaseTableColumn({
          code: 'docDateMemo',
          name: 'Дата',
          filterable: true
        }),
        new ShowcaseTableColumn({
          code: 'creatorMemo',
          name: 'Кто подписал',
          filterable: true
        }),
        new ShowcaseTableColumn({
          code: 'executorMemo',
          name: 'Кому',
          filterable: true
        }),
        new ShowcaseTableColumn({
          code: 'recipientMemo',
          name: 'Исполнитель',
          filterable: true
        }),
        new ShowcaseTableColumn({
          code: 'contentMemo',
          name: 'Содержание',
          filterable: true
        })
      ],
      numberOfElementPerPage: 20,
      placeholderForCommonSearch: 'Номер, дата, статус'
    }
  };

  constructor(private solrMediator: SolrMediatorService,
              private helper: HelperService,
              private nsi: NsiResourceService,
              private session: SessionStorage) {}

  ngOnInit(): void {
    this.firstLoading = true;
    if (this.settings) {
      this.model = new ShowcaseSettings(this.settings);
      this.updateDicts().subscribe(() => {
        this.model.clearFilters();
        this.search().subscribe(response => {
          this.firstLoading = false;
        }, error => {
          this.firstLoading = false;
        });
      }, error => {
        this.firstLoading = false;
      });
    }
  }

  updateDicts(): Observable<void> {
    return Observable.create(obs => {
      const dictFields = this.model.settings.fields.filter(i => i.type === 'dictionary');
      const dictList = _.compact(_.uniq(<string[]>dictFields.map(i => i.dictionary)));
      (dictList.length ? from(this.nsi.getDictsFromCache(dictList)) : of({})).subscribe(res => {
        this.model.dicts = res;
        dictFields.forEach(i => i.dict = this.model.dicts[i.dictionary]);
        obs.next('');
        obs.complete();
      }, error => {
        this.helper.error(error);
        obs.error(error);
        obs.complete();
      });
    });
  }

  commonSearchChanged() {
    this.model.pagination.currentPage = 1;
    this.updateData();
  }

  sortingChanged() {
    this.updateData();
  }

  changeFilters() {
    this.model.pagination.currentPage = 1;
    this.updateData();
  }

  updateData() {
    this._updateData();
  }

  _updateData() {
    this.model.loading = true;
    this.search().subscribe(response => {
      this.model.loading = false;
      // this.model.saveToLocalStorage(this.localStorageService);
    }, error => {
      this.helper.error(error)
    });
  }

  _pageChanged() {
    this._updateData();
  }

  _itemsPerPageChanged() {
    this.model.pagination.currentPage = 1;
    this._pageChanged();
  }

  private search(): Observable<void> {
    this.model.selectedFiltersNumber = this.model.calculateSelectedFilters();
    return Observable.create(observer => {
      const queryParams = this.model.toQueryRequestParams();
      queryParams.query = queryParams.query
        ? this.getPreQuery() + ' AND (' + queryParams.query + ')'
        : this.getPreQuery();
      this.solrMediator.query(queryParams).subscribe(searchResult => {
          if (searchResult.docs) {
            searchResult.docs.forEach(d => {
              this.model.fillInSearchResultItemInfoAboutDict(d);
              this.model.correctSearchResultData(d);
            });
          }
          this.searchResult = searchResult;
          let bufferArray: any = [];
          if (this.limitedAccessValue) {
            for (let i=0; i<this.searchResult.docs.length; i++) {
              if (this.searchResult.docs[i]['limitedAccess'] === true) {
                bufferArray.push(this.searchResult.docs[i]);
              }
            }
            this.searchResult.docs = bufferArray;
            this.searchResult.numFound = this.searchResult.docs.length % this.searchResult.pageSize + 1;
          }
          this.model.pagination.totalItems = this.searchResult.numFound;
          this.model.pagination.itemsPerPage = this.searchResult.pageSize;
          this.model.pagination.currentPage = this.searchResult.start / this.searchResult.pageSize + 1;
          observer.next();
          observer.complete();

        }, error => {
          observer.error(error);
          observer.complete();
        }
      );
    });
  }

  private getPreQuery(): string {
    let userQuery = () => {
      const login = this.session.login();
      const fields: string[] = [
        'assistantLogMemo', 'creatorLogMemo', 'executorLogMemo',
        'userShowcaseMemo', 'redirectedLogMemo', 'executorLogInstrMemo'
      ];
      return fields.length ? fields.join(':' + login + ' OR ') + ':' + login : '';
    };
    let r = '(*:* AND !limitedAccess:true)';
    r += ' OR (limitedAccess:true AND (' + userQuery() + '))';
    return '*:* AND (' + r + ')';
  }

}
