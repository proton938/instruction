import { Component, OnInit, OnDestroy } from '@angular/core';
import { IShowcaseBuilderConfig } from '@reinform-cdp/showcase-builder';
import { InstructionFilterService } from '../../../services/instruction-filter.service';

@Component({
  selector: 'memo-mka-showcase',
  templateUrl: './memo-mka-showcase.component.html'
})
export class MemoMkaShowcaseComponent implements OnInit, OnDestroy {
  config: IShowcaseBuilderConfig;
  title: string;
  constructor(private filterService:InstructionFilterService) { }

  ngOnInit(): void {
    this.filterService.clearAssignmentFilter();
    const urlParams = !!window.location.href.split('?')[1] ? window.location.href.split('?')[1].split('&') : null;
    const filters = [];
    if (!!urlParams) {
      urlParams.forEach((item) => {
        const key = item.split('=')[0];
        const value = new URLSearchParams(item).get(key);
        if (key == 'showcaseName') {
          this.title = value;
        } else if (value.indexOf(',') > 0) {
          filters.push({ code: key, value: value.split(',') });
        } else if (value === 'true' || value === 'false') {
          filters.push({ code: key, value: value == 'true' });
        } else {
          filters.push({ code: key, value: value });
        }
      });
    }

    console.log('showcase init');
    this.config = {
      system: 'SDO',
      subsystem: 'SDO_INSTRUCTION',
      document: 'SDO_INSTRUCTION_MEMO-MKA',
      showcase: 'OASI_MEMO-MKA',
      filters: filters
    };
  }

  ngOnDestroy() {
    console.log('showcase destroy');
  }
}
