import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { BsModalRef } from "ngx-bootstrap";
import { Component, OnInit, Input } from '@angular/core';
import { UserBean } from "@reinform-cdp/nsi-resource";
import { MemoRestService } from "../../../../../services/memo-rest.service";
import { SessionStorage } from "@reinform-cdp/security";
import { catchError, tap } from "rxjs/internal/operators";
import { throwError } from "rxjs/index";
import { MemoDocument } from "../../../../../models/memo-document/MemoDocument";

@Component({
  selector: 'instruction-modal-content',
  templateUrl: './send-mail.modal.html'
})
export class SendMailModalComponent implements OnInit {
  @Input() document: MemoDocument;

  users: UserBean[] = [];
  sended: boolean = false;

  submit = false;

  constructor(private memoRestService: MemoRestService, private bsModalRef: BsModalRef,
    private toastr: ToastrService, private session: SessionStorage) {
  }

  ngOnInit() {
  }

  ok() {
    if (this.isValid()) {
      let logins: string[] = _.map(this.users, u => { return u.accountName; });
      this.sended = true;
      setTimeout(response => {
        this.memoRestService.notify(logins, `${window.location.origin}/sdo/instruction/#/app/instruction/memo/card/${this.document.documentId}`, this.session.login()).pipe(
          tap(response => {
            this.toastr.success('Уведомление успешно отправлено!');
            this.submit = true;
            this.bsModalRef.hide();
          }), catchError(error => {
            console.log(error);
            this.sended = false;
            this.toastr.error('Ошибка при отправке уведомления!');
            return throwError(error);
          })
        ).subscribe();
      }, 1500);
    } else {
      this.toastr.warning('Не заполнены обязательные поля!');
    }
  }

  cancel() {
    this.bsModalRef.hide();
  }

  isValid(): boolean {
    return this.users.length > 0;
  }

}
