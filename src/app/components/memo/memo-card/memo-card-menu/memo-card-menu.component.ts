import * as _ from "lodash";
import {StateService} from '@uirouter/core';
import {Component, Input, OnInit, Inject} from '@angular/core';
import {InstructionDictsModel} from "../../../../models/instruction/InstructionDictsModel";
import {IInfoAboutProcessInit} from "@reinform-cdp/bpm-components";
import {MemoDictsModel} from "../../../../models/memo/MemoDictsModel";
import {SearchResultDocument} from "@reinform-cdp/search-resource";
import {MemoDocument} from "../../../../models/memo-document/MemoDocument";
import {FileService} from "../../../../services/file.service";
import {MemoDocumentService} from "../../../../services/memo-document.service";
import {AlertService} from "@reinform-cdp/widgets";
import {SessionStorage, AuthorizationService} from "@reinform-cdp/security";
import {OrderActivitiService} from "../../../../services/order-activiti.service";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {catchError, mergeMap, tap} from "rxjs/internal/operators";
import {Observable} from "rxjs/Rx";
import {EMPTY, from, throwError} from "rxjs/index";
import {MemoModalService} from "../../../../services/memo-modal.service";

@Component({
  selector: 'mggt-memo-card-menu',
  templateUrl: './memo-card-menu.component.html',
  styleUrls: ['./memo-card-menu.component.scss']
})
export class MemoCardMenuComponent implements OnInit {

  @Input() document: MemoDocument;
  @Input() dicts: MemoDictsModel;
  @Input() initProcess: IInfoAboutProcessInit;
  @Input() instructionDicts: InstructionDictsModel;
  @Input() memoType: string;
  showEdit: boolean;
  showCopy: boolean;
  showDelete: boolean;
  showInstruction: boolean;
  @Input() instructions: SearchResultDocument[];
  @Input() sdoLinksCount: number;

  @BlockUI('cardMemo') blockUI: NgBlockUI;

  constructor(private fileService: FileService,
              private memoModalService: MemoModalService,
              private memoDocumentService: MemoDocumentService,
              public $state: StateService,
              @Inject('$localStorage') public $localStorage: any,
              private instructionActivityService: OrderActivitiService,
              private alertService: AlertService,
              private session: SessionStorage,
              private authorizationService: AuthorizationService) {
  }

  ngOnInit() {
    this.showEdit = this.isShowEditBtn();
    this.showCopy = this.isShowCopyBtn();
    this.showDelete = this.isShowDeleteBtn();
    this.showInstruction = false;
  }

  isShowEditBtn(): boolean {
    return this.memoType === 'memo' && this.authorizationService.check('INSTRUCTION_MEMO_EDIT');
  }

  isShowCopyBtn(): boolean {
    if (this.$localStorage.isMobile || this.memoType === 'memo') {
      return false;
    }

    const login = this.session.login().trim();
    return (this.document.creator.login === login) || (this.document.executor.some((e) => (e.login === login))) || this.isAssistantviewEx();
  }

  isShowDeleteBtn(): boolean {
    if (this.authorizationService.check('INSTRUCTION_MEMO_DELETE')) {
      return true;
    }
    if (this.authorizationService.check('INSTRUCTION_MEMO_DELETE_BUTTON')) {
      let statusCode = this.document.statusId.code;
      const login = this.session.login().trim();
      return (statusCode === 'initial' || statusCode === 'approval') &&
        (this.document.creator.login === login || this.document.recipient.login === login || this.document.assistant.login === login);
    }
    return false;
  }

  isShowInstructionBtn(): boolean {
    const login = this.session.login().trim();
    return ((this.document.executor.some((e) => (e.login === login))) || this.isAssistantviewEx2());
  }

  editMemo() {
    this.$state.go('app.instruction.memo-edit', { field: 'id', id: this.document.documentId, backUrl: window.location.href });
  }

  isAssistantview(): boolean {
    let result = false;
    let login = this.session.login().trim();
    this.dicts.executors.forEach(e => {
      if (e.creator === this.document.creator.login) {
        if (e.assistantview && e.assistantview.length) {
          e.assistantview.forEach(a => {
            if (a === login) {
              result = true;
            }
          })
        }
      }
    });

    return result;
  }

  isAssistantviewEx(): boolean {
    let result = false;
    let login = this.session.login().trim();
    this.dicts.executors.forEach(e => {
      if (e.creator === this.document.creator.login || (this.document.executor.some((e) => (e.login === login)))) {
        if (e.assistantview && e.assistantview.length) {
          e.assistantview.forEach(a => {
            if (a === login) {
              result = true;
            }
          })
        }
      }
    });

    return result;
  }


  isAssistantviewEx2(): boolean {
    let result = false;
    let login = this.session.login().trim();
    this.dicts.executors.forEach(e => {
      if ((this.document.executor.some((e) => (e.login === login)))) {
        if (e.assistantview && e.assistantview.length) {
          e.assistantview.forEach(a => {
            if (a === login) {
              result = true;
            }
          });
        }
      }
    });

    return result;
  }

  delete() {
    if (this.instructions.length > 0) {
      this.alertService.message({
        message: 'Удалите поручения, созданные по данной служебной записке',
        type: 'warning',
        size: 'md'
      });
    } else {
      let message = this.sdoLinksCount > 0 ? 'В документе есть связанные элементы. Удалить документ?' :
        'Вы действительно хотите удалить служебную записку?';
      from(this.alertService.confirm({
        message: message,
        type: 'warning',
        size: 'md',
        okButtonText: 'Удалить'
      })).pipe(
        mergeMap(response => {
          this.blockUI.start();
          return this.deleteOneMemo(this.document.documentId, this.document.number)
        }), tap(() => {
          this.blockUI.stop();
          this.$state.go('app.instruction.showcase-memo');
        }), catchError(error => {
          this.blockUI.stop();
          return EMPTY;
        })
      ).subscribe();
    }
  }

  mail() {
    this.memoModalService.mail(this.document).pipe(
      catchError(error => {
        console.log(error);
        return throwError(error);
      })
    ).subscribe();
  }

  private deleteOneMemo(id: string, number: string): Observable<any> {
    return this.memoDocumentService.deleteDocument(id, number).pipe(
      mergeMap(() => {
        return this.instructionActivityService.getProcessHistoryById(id);
      }), mergeMap(response => {
        let openedProcesses: string[] = this.instructionActivityService.getOpenedProcesses(response);
        return this.instructionActivityService.deleteProcesses(openedProcesses);
      })
    );
  }

}
