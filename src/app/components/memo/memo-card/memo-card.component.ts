import * as angular from 'angular';
import { PlatformConfig } from '@reinform-cdp/core';
import { ICdpBpmProcessManagerConfig } from '@reinform-cdp/bpm-components';
import * as _ from 'lodash';
import { ToastrService } from "ngx-toastr";
import { Transition } from '@uirouter/core';
import { BsModalService } from 'ngx-bootstrap';
import { Component, OnInit, Inject } from '@angular/core';
import { SearchResultDocument } from '@reinform-cdp/search-resource';
import { FileResourceService } from '@reinform-cdp/file-resource';
import { IInfoAboutProcessInit } from '@reinform-cdp/bpm-components';
import { MemoDictsModel } from '../../../models/memo/MemoDictsModel';
import { MemoDocument } from '../../../models/memo-document/MemoDocument';
import { SessionStorage, AuthorizationService } from '@reinform-cdp/security';
import { MemoDocumentService } from '../../../services/memo-document.service';
import { MemoDictsService } from '../../../services/memo-dicts.service';
import { FileService } from '../../../services/file.service';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { MemoActivitiService } from '../../../services/memo-activiti.service';
import { MemoRestService } from '../../../services/memo-rest.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { catchError, mergeMap, tap } from 'rxjs/internal/operators';
import { throwError, Observable, from, forkJoin, of } from 'rxjs/index';
import { LoadingStatus, ExFileType } from '@reinform-cdp/widgets';
import { HistoryModel } from '../../../models/documents-log/HistoryModel';
import { MemoDocumentHistoryService } from '../../../services/memo-document-history.service';
import { MemoBreadcrumbsService } from '../../../services/memo-breadcrumbs.service';
import { AttachFileModalComponent } from '../../commons/attach-file-modal/attach-file-modal.component';
import { InstructionApproval } from '../../../models/instruction/InstructionApproval';
import {LinkRestService} from '../../../services/link-rest.service';

@Component({
  selector: 'mggt-memo-card',
  templateUrl: './memo-card.component.html',
  styleUrls: ['./memo-card.component.scss']
})
export class MemoCardComponent implements OnInit {

  commentExpanded = true;
  resultExpanded = true;

  memoType: string;
  documentType: string;
  showcaseState: string;
  showcaseTitle: string;
  document: MemoDocument;
  copyDocument: MemoDocument;
  relatedDocuments: MemoDocument[] = [];
  dicts: MemoDictsModel;
  instructionDicts: any;
  sdoLinksCount: number = 0;
  init: IInfoAboutProcessInit;
  files: ExFileType[] = [];
  copyFiles: ExFileType[];
  instructions: SearchResultDocument[];
  subTasks: SearchResultDocument[];
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  insideOrder: SearchResultDocument[];
  success = false;
  log: HistoryModel;

  processManagerConfig: ICdpBpmProcessManagerConfig;
  selectedTab = 0;
  hideOvers = true;

  showJsonEditor: boolean;
  showDocumentLog: boolean;
  isFullDescription = false;
  showApprovalCycle = false;

  resultFiles: { [key: string] : ExFileType } = {};

  // attachedFiles: any[] = [];
  // draftFiles: any[] = [];

  showFiles = false;

  @BlockUI('cardMemo') blockUI: NgBlockUI;

  constructor(private fileService: FileService,
    private nsiRestService: NsiResourceService,
    private memoDictsService: MemoDictsService,
    private breadcrumbsService: MemoBreadcrumbsService,
    private memoDocumentService: MemoDocumentService,
    @Inject('$localStorage') public $localStorage: any,
    private memoActivityService: MemoActivitiService,
    private transition: Transition,
    private modalService: BsModalService,
    private toastr: ToastrService,
    private memoRestService: MemoRestService,
    private fileRestService: FileResourceService,
    private session: SessionStorage,
    private authorizationService: AuthorizationService,
    private memoDocumentHistoryService: MemoDocumentHistoryService,
    private linkRestService: LinkRestService,
    private platformConfig: PlatformConfig) {
  }

  ngOnInit() {
    const id = this.transition.params()['id'];
    this.memoType = this.transition.params()['memoType'] || 'memo';
    this.documentType = this.transition.params()['documentType'] || 'MEMO';
    this.showcaseState = this.transition.params()['showcaseState'] || 'app.instruction.showcase-memo';
    this.showcaseTitle = this.transition.params()['showcaseTitle'] || 'Служебные записки';

    this.processManagerConfig = {
      sysName: this.platformConfig.systemCode.toLowerCase(),
      linkVarValue: id
    };
    this.showJsonEditor = this.authorizationService.check('INSTRUCTION_MEMO_CARD_JSON_HISTORY') && this.memoType !== 'memomka';
    this.showDocumentLog = this.authorizationService.check('INSTRUCTION_MEMO_CARD_JSON_HISTORY') && this.memoType !== 'memomka';
    const currentUserLogin = this.session.login();
    const usersWithPermissions = [];

    this.breadcrumbsService.memoCard(this.showcaseState, this.showcaseTitle);
    _.defer(() => {
      this.memoDictsService.getDicts().pipe(
        mergeMap(response => {
          this.dicts = response;
          return this.nsiRestService.getDictsFromCache(['InstructionStatus', 'InstructionPriority']);
        }),
        mergeMap(response => {
          this.instructionDicts = {
            statuses: response.InstructionStatus,
            priorities: response.InstructionPriority
          };

          return this.memoRestService.get(id, this.memoType);
        }),
        mergeMap(response => {
          this.document = new MemoDocument();
          this.document.build(angular.copy(response.document));
          this.copyDocument = angular.copy(this.document);

          if (this.memoType === 'memo' && this.document.privacy && this.document.privacy.code) {
            usersWithPermissions.push(this.document.creator.login);

            if (this.document.assistant) {
              usersWithPermissions.push(this.document.assistant.login);
            }

            if (this.document.recipient) {
              usersWithPermissions.push(this.document.recipient.login);
            }

            if (this.document.executor && this.document.executor.length) {
              this.document.executor.forEach((u) => {
                usersWithPermissions.push(u.login);
              });
            }

            if (usersWithPermissions.indexOf(currentUserLogin) === -1) {
              return this.memoRestService.getDelegates(currentUserLogin).pipe(
                mergeMap((delegates) => {
                  console.log(4);
                  delegates.forEach((delegate) => {
                    if (
                      delegate.delegationRule.toUser === currentUserLogin &&
                      usersWithPermissions.indexOf(delegate.delegationRule.fromUser) !== -1
                    ) {
                      this.showFiles = true;
                    }
                  });

                  return this.memoActivityService.getProcessHistory(this.document);
                })
              );
            } else {
              this.showFiles = true;
            }
          } else {
            this.showFiles = true;
          }

          return this.memoActivityService.getProcessHistory(this.document);
        }),
        mergeMap((response) => {
          if (!this.showFiles) {
            this.removeFilesByPrivacyPolicy();
          }

          this.init = response;

          return this.fileService.getRealInfoAboutFilesByIds(this.document.fileId);
        }),
        mergeMap(response => {
          this.files = response;
          this.copyFiles = angular.copy(this.files);

          return this.getFilesFromFolder(this.document.folderId);
        }),
        mergeMap(response => {

          this.document.result.forEach(r => {
            if (r.reportFileId) {
              this.fileService.getInfoAboutFilesByIds(r.reportFileId).forEach(f => this.resultFiles[f.idFile] = f);
            }
            if (r.acceptFileId) {
              this.fileService.getInfoAboutFilesByIds(r.acceptFileId).forEach(f => this.resultFiles[f.idFile] = f);
            }
          });

          if (this.document.relateId && this.document.relateId.length) {
            const requests = [];
            this.document.relateId.forEach(relateId => {
              requests.push(this.memoRestService.get(relateId, this.memoType));
            });
            return forkJoin(requests);
          }
          return of([]);
        }),
        mergeMap(responses => {
          if (responses && responses.length) {
            responses.forEach(response => {
              const relatedDocument = new MemoDocument();
              relatedDocument.build(angular.copy(response.document));
              this.relatedDocuments.push(relatedDocument);
            });
          }

          return this.memoDocumentService.getInstructions(this.document.documentId, this.memoType);
        }),
        mergeMap(response => {
          this.instructions = response;
          console.log('this.instructions', this.instructions);
          return this.memoDocumentService.getSubTasks(_.map(this.instructions, i => {
            return i.documentId;
          }));
        }),
        mergeMap(response => {
          this.subTasks = response;

          return this.memoDocumentHistoryService.updateInfoHistory(this.document.documentId, this.documentType);
        }),
        mergeMap(response => {
          return this.memoDocumentService.getInsideInstructions(this.document.documentId, this.documentType);
        }),
        mergeMap(response => {
          this.insideOrder = response;
          return this.memoActivityService.getHistoricalProcesses(this.document.documentId);
        }),
        mergeMap(response => {
          this.showApprovalCycle = response.data.some((p) => (p.processDefinitionId.indexOf('sdomemo_ApproveMemoProject_prc') !== -1));
          this.log = this.memoDocumentHistoryService.getModel();
          this.loadingStatus = LoadingStatus.SUCCESS;
          this.success = true;
          return this.linkRestService.findSdoLinks(this.document.documentId);
        }),
        tap(response => {
          this.sdoLinksCount = response.length;
          this.loadingStatus = LoadingStatus.SUCCESS;
          this.success = true;
        }),
        catchError(error => {
          this.loadingStatus = LoadingStatus.ERROR;
          return throwError(error);
        })
      ).subscribe();
    });
  }

  getAlt(onCondition: boolean, hintId: string, on?: boolean, condition?: boolean ) {
    if (onCondition) {
      let hint = document.getElementById(hintId);
      if (on) {
        document.addEventListener('mousemove', function (event) {
          hint.style.left = event.pageX + 10 + 'px';
          hint.style.top = event.pageY - window.pageYOffset + 10 + 'px';
        });
        hint.style.display = 'block';

      } else {
        hint.style.left = -1000 + 'px';
        hint.style.top = -1000 + 'px';
        hint.style.display = 'none';
      }
    }
  }


  getFilesFromFolder(folderId: string): Observable<any> {
    return Observable.create(obs => {
      this.fileService
        .updateInfoAboutFilesFromFolder(folderId)
        .subscribe(res => {
          obs.next(res);
        }, error => {
          obs.next([]);
        }, () => {
          obs.complete();
        })
    });
  }

  get isDeletableFile(): boolean {
    return _.some(this.session.groups(), i => i === 'MGGT_MEMO_ADD_SCAN') && this.memoType !== 'memomka' && !this.$localStorage.isMobile;
  }

  onDeleteScan(file: ExFileType): void {
    this.showAttachScanModal();
  }

  showAttachScanModal(): void {
    this.modalService.show(AttachFileModalComponent, {
      class: 'modal-lg',
      keyboard: false,
      backdrop: 'static',
      initialState: {
        folderId: this.document.folderId,
        saveCallback: (file: ExFileType) => {
          this.deleteFilesIfExist(this.files).pipe(
            mergeMap(() => {
              this.document.fileId = [file.idFile];
              this.files = [file];
              this.copyFiles = angular.copy(this.files);
              return this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document);
            }),
            mergeMap(() => {
              return this.memoRestService.sendToDocumentsDB(this.document.documentId);
            })
          ).subscribe(
            (data) => {
            },
            (error) => {
              console.log(error);
            }
          );
        },
        cancelCallback: (file: ExFileType) => {
          this.files = angular.copy(this.copyFiles);

          if (file) {
            this.deleteFilesIfExist([file]).subscribe();
          }
        }
      }
    });
  }

  private deleteFile(file: ExFileType): Observable<any> {
    return from(this.fileRestService.deleteFile(file.idFile, this.platformConfig.systemCode)).pipe(
      catchError(error => {
        console.error(error);
        this.toastr.error(`Произошла ошибка при удалении файла ${file.nameFile}.`);
        return throwError(error);
      })
    );
  }

  private deleteFilesIfExist(files: ExFileType[]): Observable<any> {
    if (files && files.length) {
      return forkJoin(files.map(f => this.deleteFile(f)));
    } else {
      return of({});
    }
  }

  removeFilesByPrivacyPolicy() {
    this.document.fileId = [];

    if (this.document.result && this.document.result.length) {
      this.document.result.forEach((result) => {
        result.reportFileId = [];
        result.acceptFileId = [];
      });
    }

    if (this.document.approval && this.document.approval.approvalCycle) {
      this.document.approval.approvalCycle.agreed.forEach((agreed) => {
        agreed.fileApproval = [];
      });
    }

    if (this.document.approvalHistory && this.document.approvalHistory.approvalCycle) {
      this.document.approvalHistory.approvalCycle.forEach((cycle) => {
        cycle.approvalCycleFile = [];
        cycle.agreed.forEach((agreed) => {
          agreed.fileApproval = [];
        });
      });
    }

    this.document.attachedFile = [];
    this.document.draftWithoutAtt = [];
    this.document.attachment = [];
    this.document.draftFiles = [];
    this.document.folderId = null;
    this.document.IdDraftFilesRegInfo = null;
  }

  showInstructionsTab(): boolean {
    let result = false;
    const login = this.session.login().trim();

    this.dicts.executors.forEach(e => {
      if (this.document.executor.some((ex) => (ex.login === e.creator))) {
        if (e.assistantview && e.assistantview.length) {
          e.assistantview.forEach(a => {
            if (a === login) {
              result = true;
            }
          });
        }
      }
    });

    return ((this.document.executor.some((ex) => (ex.login === this.session.login()))) || result);
  }

  toggleDescription(): void {
    this.isFullDescription = !this.isFullDescription;
  }

  getFiles(files): any[] {
    return this.fileService.getInfoAboutFilesByIds(files);
  }

  isActive(index: number): boolean {
    return index === this.selectedTab;
  }

  onTabSelected(index: number) {
    if (this.selectedTab === index) {
      this.hideOvers = !this.hideOvers;
    } else {
      this.hideOvers = true;
    }
    this.selectedTab = index;
  }

  linkedElementsSaved(num: number) {
    if (num > 0) {
      this.memoRestService.sendToDocumentsDB(this.document.documentId).subscribe(() => {});
    }
  }
}
