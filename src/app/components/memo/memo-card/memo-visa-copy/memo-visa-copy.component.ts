import {Component, Input, OnInit} from '@angular/core';


@Component({
  selector: 'mggt-memo-visa-copy',
  templateUrl: './memo-visa-copy.component.html',
  styleUrls: ['./memo-visa-copy.component.scss']
})
export class MemoVisaCopyComponent implements OnInit {

  @Input() document: any;

  constructor() {
  }

  ngOnInit() {
  }
}
