import * as angular from "angular";
import {StateService} from '@uirouter/core';
import {Component, Input, OnInit} from '@angular/core';
import {MemoDocument} from "../../../../models/memo-document/MemoDocument";
import {SessionStorage} from "@reinform-cdp/security";
import {MemoDocumentService} from "../../../../services/memo-document.service";
import {AlertService} from "@reinform-cdp/widgets";
import {OrderModalService} from "../../../../services/order-modal.service";
import {catchError, mergeMap, tap} from "rxjs/internal/operators";
import {EMPTY, from} from "rxjs/index";

@Component({
  selector: 'mggt-memo-comments',
  templateUrl: './memo-comments.component.html',
  styleUrls: ['./memo-comments.component.scss']
})
export class MemoCommentsComponent implements OnInit {

  @Input() document: MemoDocument;
  copyDocument: MemoDocument;
  @Input() edit: boolean;

  constructor(private session: SessionStorage,
              private $state: StateService,
              private memoDocumentService: MemoDocumentService,
              private instructionModalService: OrderModalService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.copyDocument = angular.copy(this.document);
  }

  add() {
    this.instructionModalService.addComment().pipe(
      mergeMap(response => {
        this.copyDocument.comment.push(response);
        return this.memoDocumentService.updateDocument(this.document.documentId, this.document, this.copyDocument);
      }), tap(response => {
        this.$state.reload();
      }), catchError(error => {
        return EMPTY;
      })
    ).subscribe();
  }

  remove(index: number) {
    from(this.alertService.confirm({
      okButtonText: 'Ок',
      message: 'Вы действительно хотите удалить данный комментарий?',
      type: 'warning',
      size: 'md'
    })).pipe(
      mergeMap(response => {
        this.copyDocument.comment.splice(this.copyDocument.comment.length - 1 - index, 1);
        return this.memoDocumentService.updateDocument(this.document.documentId, this.document, this.copyDocument);
      }), tap(response => {
        this.$state.reload();
      }), catchError(error => {
        return EMPTY;
      })
    ).subscribe();
  }

}
