import * as angular from "angular";
import {compare} from "fast-json-patch";
import {ToastrService} from "ngx-toastr";
import {StateService} from '@uirouter/core';
import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {catchError, tap} from "rxjs/internal/operators";
import {throwError} from "rxjs/index";
import {MemoRestService} from "../../../../services/memo-rest.service";
import * as JSONEditor from 'jsoneditor';

@Component({
  selector: 'mggt-memo-json-editor',
  templateUrl: './memo-json-editor.component.html',
  styleUrls: ['./memo-json-editor.component.scss']
})
export class MemoJsonEditorComponent implements OnInit, OnDestroy {

  @Input() id: string;
  @Input() memoType: string;
  container: any;
  options: any;
  editor: any;
  document;
  documentEdited;
  loadingStatus: string = "LOADING";
  success: boolean = false;

  @BlockUI('editDocument') blockUI: NgBlockUI;

  constructor(private memoRestService: MemoRestService,
              private $state: StateService, private toastr: ToastrService) {
  }

  ngOnInit() {
    this.memoRestService.get(this.id, this.memoType).pipe(
      tap(response => {
        this.document = response.document;
        this.documentEdited = angular.copy(this.document);
        this.loadingStatus = "SUCCESS";
        this.success = true;
        setTimeout(() => {
          this.container = document.getElementById("jsoneditor");
          this.options = {};
          this.editor = new JSONEditor(this.container, this.options);
          this.editor.set(this.documentEdited);
        }, 100);
      }), catchError(error => {
        console.log(error);
        this.loadingStatus = "ERROR";
        return throwError(error);
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.blockUI.reset();
  }

  save() {
    this.blockUI.start();
    this.documentEdited = this.editor.get();
    let diff = compare({"document": this.document}, {"document": this.documentEdited});
    if (diff.length > 0) {
      this.memoRestService.patch(this.id, <any>JSON.stringify(diff)).pipe(
        tap(response => {
          this.toastr.success("Документ успешно сохранен!");
          this.blockUI.stop();
          this.$state.reload();
        }), catchError(error => {
          this.toastr.error("Ошибка при сохранении документа!");
          this.blockUI.stop();
          return throwError(error);
        })
      ).subscribe();
    } else {
      this.toastr.warning("В документе изменений нет!");
      this.blockUI.stop();
    }
  }


}
