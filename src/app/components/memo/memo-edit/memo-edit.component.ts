import {formatDate} from "@angular/common";
import {catchError, debounceTime, distinctUntilChanged, map, mergeMap, switchMap, tap} from "rxjs/internal/operators";
import {SearchResultDocument, SolarResourceService} from '@reinform-cdp/search-resource';
import {StateService, Transition} from '@uirouter/core';
import * as angular from 'angular';
import {PlatformConfig} from '@reinform-cdp/core';
import * as _ from 'lodash';
import {ToastrService} from "ngx-toastr";
import {BsModalService} from 'ngx-bootstrap';
import {Component, Inject, OnInit} from '@angular/core';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {MemoDocument} from '../../../models/memo-document/MemoDocument';
import {AuthorizationService, SessionStorage} from '@reinform-cdp/security';
import {MemoDocumentService} from '../../../services/memo-document.service';
import {MemoDictsService} from '../../../services/memo-dicts.service';
import {FileService} from '../../../services/file.service';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {MemoActivitiService} from '../../../services/memo-activiti.service';
import {MemoRestService} from '../../../services/memo-rest.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {concat, forkJoin, from, Observable, of, Subject, throwError} from 'rxjs/index';
import { AlertService, LoadingStatus} from '@reinform-cdp/widgets';
import {MemoDocumentHistoryService} from '../../../services/memo-document-history.service';
import {MemoBreadcrumbsService} from '../../../services/memo-breadcrumbs.service';
import {MemoDictsModel} from "../../../models/memo/MemoDictsModel";
import {EditMemoModel} from "../../../models/memo/EditMemoModel";
import {MemoHeadingTopic} from "../../../models/memo/MemoHeadingTopic";
import {MemoTrestActivityKind} from "../../../models/memo/MemoTrestActivityKind";
import {ActivityResourceService} from '@reinform-cdp/bpm-components';

@Component({
  selector: 'mggt-memo-edit',
  templateUrl: './memo-edit.component.html',
  styleUrls: ['./memo-edit.component.css']
})
export class MemoEditComponent implements OnInit {

  document: MemoDocument;
  copyDocument: MemoDocument;
  model: EditMemoModel;

  dicts: MemoDictsModel;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;

  isFullDescription = false;

  canEditReportTerm = true;

  memoHeadingTopics: MemoHeadingTopic[];
  memoTrestActivityKinds: MemoTrestActivityKind[];

  relateId: RelatedDocument[] = [];
  copyRelateId: RelatedDocument[] = [];

  relatedDocuments$: Observable<SearchResultDocument[]>;
  relatedDocumentsLoading = false;
  relatedDocumentsInput$ = new Subject<string>();

  @BlockUI('editMemo') blockUI: NgBlockUI;

  constructor(private fileService: FileService,
              private nsiRestService: NsiResourceService,
              private memoDictsService: MemoDictsService,
              private breadcrumbsService: MemoBreadcrumbsService,
              private memoDocumentService: MemoDocumentService,
              public $state: StateService,
              @Inject('$localStorage') public $localStorage: any,
              private memoActivityService: MemoActivitiService,
              private transition: Transition,
              private modalService: BsModalService,
              private toastr: ToastrService,
              private memoRestService: MemoRestService,
              private activityResourceService: ActivityResourceService,
              private fileRestService: FileResourceService,
              private session: SessionStorage,
              private authorizationService: AuthorizationService,
              private memoDocumentHistoryService: MemoDocumentHistoryService,
              private solarRestService: SolarResourceService,
              private platformConfig: PlatformConfig,
              private alertService: AlertService) {
  }

  ngOnInit() {
    const id = this.transition.params()['id'];

    this.relatedDocuments$ = concat(
      of([]),
      this.relatedDocumentsInput$.pipe(
        debounceTime(100),
        distinctUntilChanged(),
        tap(() => this.relatedDocumentsLoading = true),
        switchMap(term => this.searchDocuments(term).pipe(
          catchError(() => of([])),
          tap(() => this.relatedDocumentsLoading = false)
        ))
      )
    );

    this.breadcrumbsService.memoCard('app.instruction.showcase-memo', 'Служебные записки');
    _.defer(() => {
      this.memoDictsService.getDicts().pipe(
        mergeMap(response => {
          this.dicts = response;
          return this.loadDictionaries();
        }),
        mergeMap(() => {
          return this.memoRestService.get(id, 'memo');
        }),
        mergeMap(response => {
          this.document = new MemoDocument();
          this.document.build(angular.copy(response.document));
          this.copyDocument = angular.copy(this.document);

          this.model = new EditMemoModel();
          this.model.updateModelBeforeEditing(this.document, this.dicts);

          return this.memoDocumentService.getRelatedMemos(this.model.relateId);
        }),
        mergeMap((response: MemoDocument[]) => {
          this.relateId = response.map(d => new RelatedDocument(d.number, d.beginDate, d.memoKind.name, d.documentId));
          this.copyRelateId = angular.copy(this.relateId);
          return this.activityResourceService.queryTasks({
            active: true,
            processInstanceVariables: [{
              name: "EntityIdVar",
              value: this.document.documentId,
              operation: "equals",
              type: "string"
            }]
          })
        }),
        tap(tasksStatus => {
          this.canEditReportTerm = _.some(tasksStatus.data, x => {
            return /^(ApproveMemoProject_frm|ScanSignedMemo_frm|memoScan)?$/.test(x.formKey);
          });
        })
      ).subscribe(() => {
        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      }, () => {
        this.loadingStatus = LoadingStatus.ERROR;
      });
    });
  }

  getAlt(onCondition: boolean, hintId: string, on?: boolean, condition?: boolean ) {
    if (onCondition) {
      let hint = document.getElementById(hintId);
      if (on) {
        document.addEventListener('mousemove', function (event) {
          hint.style.left = event.pageX + 10 + 'px';
          hint.style.top = event.pageY - window.pageYOffset + 10 + 'px';
        });
        hint.style.display = 'block';

      } else {
        hint.style.left = -1000 + 'px';
        hint.style.top = -1000 + 'px';
        hint.style.display = 'none';
      }
    }
  }

  cancellimitedAccess: boolean = false;
  limitedAccess() {
    if (!this.document.limitedAccess && !this.cancellimitedAccess) {
      this.alertService.confirm({
        okButtonText: 'Изменить',
        size: 'lg',
        message: 'Документ будет доступен только участникам процесса',
        type: 'warning'
      }).then(() => {
        return;
      }).catch(e => {
        this.cancellimitedAccess = true;
        this.document.limitedAccess = false;
      });
    }
    if (!this.document.limitedAccess && this.cancellimitedAccess) {
      this.cancellimitedAccess = false;
    }
  }

  searchDocuments(search: string): Observable<RelatedDocument[]> {
    if (search && search.length > 1) {
      return from(this.memoDocumentService.searchMemos(search)).pipe(
        map((response: any[]) => {
          return response.map(d => new RelatedDocument(d.docNumberMemo, new Date(d.docDateMemo), d.kindMemo, d.sys_documentId));
        }),
        catchError(error => {
          this.toastr.error('Ошибка при получении информации о документах!');
          return throwError(error);
        })
      );
    } else {
      return of([]);
    }
  }

  loadDictionaries(): Observable<any> {
    return forkJoin([
      this.nsiRestService.get('MemoHeadingTopic'),
      this.nsiRestService.get('MemoTrestActivityKind')
    ]).pipe(
      map((result: [MemoHeadingTopic[], MemoTrestActivityKind[]]) => {
        this.memoHeadingTopics = result[0];
        this.memoTrestActivityKinds = result[1];
        return true;
      })
    );
  }

  headingTopicChanged(ht) {
    if (ht) {
      this.model.headingTopic.name = this.memoHeadingTopics.find(i => i.code === ht.code).name;
    } else {
      this.model.headingTopic.name = null;
    }
  }

  activityKindChanged(ak) {
    if (ak) {
      this.model.activityKind.name = this.memoTrestActivityKinds.find(i => i.code === ak.code).name;
    } else {
      this.model.activityKind.name = null;
    }
  }

  onRelateIdChange(relateIds: RelatedDocument[]) {
    if (relateIds && relateIds.length) {
      this.model.relateId = relateIds.map(relateId => relateId.documentId);
    } else {
      this.model.relateId = [];
    }

    this.relateId = relateIds;
  }

  addFile(key: string) {
    this.document[key] = this.document[key].map((file) => ({
      ...file,
      dateFile: new Date(file.dateFile)
    }));
  }

  isOfficeMemo = () => (this.document.memoKind.code === '16');

  toggleDescription(): void {
    this.isFullDescription = !this.isFullDescription;
  }

  save() {
    if (!this.model.isValid()) {
      this.toastr.warning('Не заполнены обязательные поля!');
      return;
    }
    this.blockUI.start();
    this.model.updateDocumentFromModel(this.document, true);

    return this.memoDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document).pipe(
      mergeMap(() => {
        let copyRelatedIds = this.copyRelateId.map(doc => doc.documentId).join(',');
        let relatedIds = this.relateId.map(doc => doc.documentId).join(',');
        if (this.copyDocument.headingTopic.code !== this.document.headingTopic.code ||
          this.copyDocument.activityKind.code !== this.document.activityKind.code ||
          copyRelatedIds !== relatedIds ||
          this.copyDocument.note !== this.document.note) {
          return this.memoRestService.sendToDocumentsDB(this.document.documentId);
        }
        return of({});
      })
    ).subscribe(() => {
      this.blockUI.stop();
      this.$state.go('app.instruction.memo-card', {
        field: 'id',
        id: this.document.documentId,
        backUrl: window.location.href
      });
    });
  }

  cancel() {
    this.$state.go('app.instruction.memo-card', {
      field: 'id',
      id: this.document.documentId,
      backUrl: window.location.href
    });
  }

}

export class RelatedDocument {
  number: string;
  beginDate: Date;
  memoKind: string;
  documentId: string;
  title: string;

  constructor(number: string, beginDate: Date, memoKind: string, documentId: string) {
    this.number = number;
    this.beginDate = beginDate;
    this.memoKind = memoKind;
    this.documentId = documentId;
    this.title = `${number} от ${formatDate(beginDate, 'dd.MM.yyyy', 'en')} ${memoKind ? memoKind : ''}`;
  }

}
