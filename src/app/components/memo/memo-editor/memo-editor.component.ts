import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {UserBean} from "@reinform-cdp/nsi-resource";
import {MemoDictsModel} from "../../../models/memo/MemoDictsModel";
import {EditMemoModel} from "../../../models/memo/EditMemoModel";
import {FileService} from "../../../services/file.service";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {CdpReporterResourceService, CdpReporterPreviewService} from "@reinform-cdp/reporter-resource";
import {forkJoin, Observable, from, of, throwError} from 'rxjs';
import {catchError, map, mergeMap, tap} from 'rxjs/operators';
import {NSIEmailGroup} from "../../../models/nsi/NSIEmailGroups";
import {MemoKind} from "../../../models/memo/MemoKind";
import {MemoHeadingTopic} from "../../../models/memo/MemoHeadingTopic";
import {MemoTrestActivityKind} from "../../../models/memo/MemoTrestActivityKind";
import {ApprovalType} from '../../../models/instruction/ApprovalType';
import {MemoDocument} from '../../../models/memo-document/MemoDocument';
import {MemoDocumentService} from '../../../services/memo-document.service';
import * as angular from 'angular';
import {FileType} from '../../../models/instruction/FileType';

@Component({
  selector: 'mggt-memo-editor',
  templateUrl: './memo-editor.component.html',
  styleUrls: ['./memo-editor.component.scss']
})
export class MemoEditorComponent implements OnInit {

  @Input() model: EditMemoModel;
  @Input() dicts: MemoDictsModel;
  @Input() edit: boolean;
  @Input() document: MemoDocument;

  emailGroups: NSIEmailGroup[];
  memoKinds: MemoKind[];
  memoHeadingTopics: MemoHeadingTopic[];
  memoTrestActivityKinds: MemoTrestActivityKind[];
  listCollapsed = false;
  memoKindName = null;
  headingTopicName = null;
  activityKindName = null;
  recipientGroupName = null;
  approvers: UserBean[];
  approvalTypes: ApprovalType[];
  attachedFile: any[] = [];
  attachment: any[] = [];
  draftWithoutAtt: any[] = [];
  draftFiles: any[] = [];

  loadingStatus: LoadingStatus;
  success = false;

  constructor(
    public fileService: FileService,
    private nsiResourceService: NsiResourceService,
    private memoDocumentService: MemoDocumentService,
    private reporterResourceService: CdpReporterResourceService
  ) { }

  ngOnInit() {
    this.loadingStatus = LoadingStatus.LOADING;
    this.loadDictionaries().subscribe((result: any) => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, error => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  loadDictionaries(): Observable<any> {
    return forkJoin([
      this.nsiResourceService.get('EmailGroups'),
      this.nsiResourceService.get('MemoKind'),
      this.nsiResourceService.get('MemoHeadingTopic'),
      this.nsiResourceService.get('MemoTrestActivityKind'),
      this.nsiResourceService.get('MemoAproveType'),
      this.nsiResourceService.ldapUsers('MGGT_MEMO_ALL')
    ]).pipe(
      map((result: [NSIEmailGroup[], MemoKind[], MemoHeadingTopic[], MemoTrestActivityKind[], ApprovalType[], UserBean[]]) => {
        this.emailGroups = result[0].filter(emailGroup => {
          return emailGroup.sendSystem && emailGroup.sendSystem.find(sendSystem => {
            return sendSystem.code === 'SDO_INSTRUCTION';
          });
        });
        this.memoKinds = result[1];
        this.memoHeadingTopics = result[2];
        this.memoTrestActivityKinds = result[3];
        this.approvalTypes = result[4];
        this.approvers = result[5];
        return true;
      })
    );
  }

  memoKindChanged(mk) {
    this.model.memoKind = this.memoKinds.find(memoKind => memoKind.code === mk.code);
    this.refreshModel();
  }

  activityKindChanged(ak) {
    this.model.activityKind = this.memoTrestActivityKinds.find(activityKind => activityKind.code === ak.code);
    this.refreshModel();
  }

  recipientGroupChanged(rg) {
    this.model.recipientGroup = this.emailGroups.find(recipientGroup => recipientGroup.code === rg.code);
    this.refreshModel();
  }

  headingTopicChanged(ht) {
    this.model.headingTopic = this.memoHeadingTopics.find(headingTopic => headingTopic.code === ht.code);
    this.refreshModel();
  }

  refreshModel() {
    this.model = this.model ? this.model : null;
  }

  addFile(type: string) {
    const files = this[type].map(el => {
      return {file: el, fileType: type};
    });
    this.model.files = [...this.model.files, ...files];
  }

  createDraftFilesWithoutAtt() {
    this._generateDraftFile().pipe(tap(response => {
      this.draftWithoutAtt = [FileType.create(response.versionSeriesGuid, response.fileName,
        response.fileSize.toString(), false, response.dateCreated, 'draftWithoutAtt', response.mimeType)];
    }), catchError(error => {
      console.log(error);
      return throwError(error);
    })).subscribe();
  }

  createDraftFiles() {
    this._checkIfDraftFileWithoutAttExist().pipe(
      mergeMap((response) => {
        if (this.draftWithoutAtt.length === 0) {
          this.draftWithoutAtt = [FileType.create(response.versionSeriesGuid, response.fileName,
            response.fileSize.toString(), false, response.dateCreated, 'draftWithoutAtt', response.mimeType)];
        }
        return this._convertToPdfIfNecessary(this.draftWithoutAtt[0], 'draftFiles');
      }),
      mergeMap((response) => {
        let draftFile = response;
        if (!this.document.attachment) {
          this.draftFiles = [draftFile];
          return of(draftFile);
        }
        else {
          return this._convertToPdfIfNecessary(this.attachment[0]).pipe(
            mergeMap((res) => {
              let request: any = {
                filenetDestination: {
                  fileAttrs: [
                    {attrName: 'docEntityID', attrValues: [this.document.documentId]},
                    {attrName: 'docSourceReference', attrValues: ['UI']}
                  ],
                  fileName: draftFile.nameFile.split('.')[0] + '.pdf',
                  fileType: 'draftFiles',
                  folderGuid: this.document.folderId,
                },
                versionSeriesGuids: [draftFile.idFile, this.document.attachment[0].idFile]
              };
              return this.reporterResourceService.pdfConcatenateFiles(request);
            }),
            tap((response) => {
              this.draftFiles = [FileType.create(response.versionSeriesGuid, response.fileName,
                response.fileSize.toString(), false, response.dateCreated, 'draftFiles', response.mimeType)];
            })
          );
        }
      }),
      catchError(error => {
        console.log(error);
        return throwError(error);
    })).subscribe();
  }

  _convertToPdfIfNecessary(file: FileType, fileType?: string): Observable<FileType> {
    if (file.nameFile.indexOf('.pdf') !== -1) {
      return of(file);
    }
    else {
      let request: any = {
        filenetDestination: {
          fileAttrs: [
            {attrName: 'docEntityID', attrValues: [this.document.documentId]},
            {attrName: 'docSourceReference', attrValues: ['UI']}
          ],
          fileName: file.nameFile.split('.')[0] + '.pdf',
          fileType: fileType ? fileType : file.typeFile,
          folderGuid: this.document.folderId
        },
        wordVersionSeriesGuid: file.idFile
      };
      return from(this.reporterResourceService.word2pdf(request)).pipe(
        map((response) => {
          return FileType.create(response.versionSeriesGuid, response.fileName,
            response.fileSize.toString(), false, response.dateCreated, 'draftFiles', response.mimeType);
        })
      );
    }
  }

  _checkIfDraftFileWithoutAttExist(): Observable<any> {
    return this.draftWithoutAtt.length ? of([]) : this._generateDraftFile();
  }

  _generateDraftFile(): Observable<any> {
    return this._saveDocument('initial').pipe(
      mergeMap(response => {
        const request: any = {
          options: {
            jsonSourceDescr: '',
            onProcess: 'Сформировать проект СЗ',
            placeholderCode: '',
            strictCheckMode: true,
            xwpfResultDocumentDescr: 'Проект СЗ.docx',
            xwpfTemplateDocumentDescr: ''
          },
          jsonTxt: JSON.stringify({ document: this.document }),
          rootJsonPath: '$',
          nsiTemplate: {
            templateCode: 'projectMemoSign'
          },
          filenetDestination: {
            fileName: 'Проект СЗ.docx',
            fileType: 'draftWithoutAtt',
            folderGuid: this.document.folderId,
            fileAttrs: [
              { attrName: 'docEntityID', attrValues: [this.document.documentId] },
              { attrName: 'docSourceReference', attrValues: ['UI'] },
            ]
          }
        };
        return this.reporterResourceService.nsi2Filenet(request);
      })
    );
  }

  _saveDocument(status: string): Observable<void> {
    const FILE_TYPES = ['attachedFile', 'attachment', 'draftWithoutAtt', 'draftFiles'];

    this.model.updateDocumentFromModel(this.document);
    this.document.beginDate = this.dateWithoutTime(new Date());
    this.memoDocumentService.updateDocumentStatus(this.document, this.dicts.statuses, status);

    return this.memoDocumentService.createDocument(this.document).pipe(
      mergeMap(response => {
        return this.fileService.saveFilesWithFileType(this.document.documentId, this.document.folderId, this.model.files);
      }), mergeMap(response => {
        if (response.length > 0) {
          let document = angular.copy(this.document);
          this.document.fileId = response;
          return this.fileService.updateInfoAboutFilesFromFolder(this.document.folderId).pipe((
            mergeMap(response => {
              let files = [];
              response.files.forEach(el => {
                if (FILE_TYPES.indexOf(el.fileType) !== -1) {
                  const file = FileType.create(el.versionSeriesGuid, el.fileName,
                    el.fileSize.toString(), false, el.dateCreated, el.fileType, el.mimeType);
                  const fileName = file.nameFile;
                  this.document[el.fileType] = this.document[el.fileType] ? this.document[el.fileType] : [];
                  const hasFile = this.document[el.fileType].find((f) => f.idFile === file.idFile);
                  if (!hasFile) {
                    this.document[el.fileType].push(file);
                  }
                  files = this.model.files.filter((f) => f.file.name !== fileName);
                }
              });
              this.model.files = files;
              return this.memoDocumentService.updateDocument(this.document.documentId, document, this.document);
            })
          ));
        }
        else {
          return of([]);
        }
      }), catchError(error => {
        console.log(error);
        return throwError(error);
      })
    );
  }

  private dateWithoutTime(date: Date): Date {
    let _date = date.setHours(0, 0, 0, 0).valueOf();
    return new Date(_date);
  }
}
