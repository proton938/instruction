import * as angular from "angular";
import {ToastrService} from "ngx-toastr";
import {Component, Inject, OnInit} from '@angular/core';
import {StateService} from '@uirouter/core';
import {EditMemoModel} from "../../../models/memo/EditMemoModel";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {MemoDocument} from "../../../models/memo-document/MemoDocument";
import {MemoDictsModel} from "../../../models/memo/MemoDictsModel";
import {FileService} from "../../../services/file.service";
import {MemoActivitiService} from "../../../services/memo-activiti.service";
import {MemoDictsService} from "../../../services/memo-dicts.service";
import {MemoRestService} from "../../../services/memo-rest.service";
import {MemoDocumentService} from "../../../services/memo-document.service";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {catchError, mergeMap, tap} from "rxjs/internal/operators";
import {Observable, throwError, of} from 'rxjs/index';
import {MemoBreadcrumbsService} from "../../../services/memo-breadcrumbs.service";
import {FileType} from '../../../models/instruction/FileType';
import {CdpReporterResourceService, CdpReporterPreviewService} from "@reinform-cdp/reporter-resource";

@Component({
  selector: 'mggt-create-memo',
  templateUrl: './create-memo.component.html',
  styleUrls: ['./create-memo.component.scss']
})
export class CreateMemoComponent implements OnInit {

  dicts: MemoDictsModel;
  document: MemoDocument;
  prevDocument: MemoDocument;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;
  model: EditMemoModel;

  @BlockUI('newMemo') blockUI: NgBlockUI;

  constructor(public $state: StateService,
              private memoRestService: MemoRestService,
              private breadcrumbsService: MemoBreadcrumbsService,
              private memoDictsService: MemoDictsService,
              private memoActivityService: MemoActivitiService,
              private fileService: FileService,
              private memoDocumentService: MemoDocumentService,
              private reporterResourceService: CdpReporterResourceService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.memoDictsService.getDicts().pipe(
      tap(response => {
        this.dicts = response;
        this.model = new EditMemoModel();
        this.model.assistant = this.memoDocumentService.getCurrentUser();
        this.document = new MemoDocument();
        this.prevDocument = new MemoDocument();
        this.breadcrumbsService.newMemo();
        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      }), catchError(error => {
        this.loadingStatus = LoadingStatus.ERROR;
        return throwError(error);
      })
    ).subscribe();
  }

  create() {
    if (this.model.isValid()) {
      this.blockUI.start();

      this._saveDocument('send').pipe(
        mergeMap(() => {
          const processVariables = [{
            'name': 'EntityIdVar',
            'value': this.document.documentId
          }, {
            'name': 'ResponsibleExecutor_VAR',
            'value': this.document.executor.map((ex) => (ex.login)).join(',')
          }, {
            'name': 'OwnerExecutorVar',
            'value': this.document.assistant.login
          }];
          return this.memoActivityService.startProcess('sdomemo_StartMemo_prc', this.document, processVariables);
        }), tap(response => {
          this.blockUI.stop();
          this.goToCard();
        }), catchError(error => {
          console.log(error);
          this.blockUI.stop();
          return throwError(error);
        })
      ).subscribe();

    } else {
      this.toastr.warning('Не заполнены обязательные поля!');
    }
  }

  sendToApproval() {
    if (this.model.isValid()) {
      const hasApprovalWithSign = this.model.approval.approvalCycle.agreed.find((agreed) => agreed.approvalTypeCode === 'withSign');
      const processName = hasApprovalWithSign ? 'sdomemo_ApproveMemoProject_prc' : 'sdomemo_ApproveMemoProjectNoESig';
      this._saveDocument('send').pipe(
        mergeMap(() => {
          let processVariables = [{
            'name': 'EntityIdVar',
            'value': this.document.documentId
          }, {
            'name': 'ResponsibleExecutor',
            'value': this.document.executor.map((ex) => (ex.login)).join(',')
          }, {
            'name': 'OwnerExecutorVar',
            'value': this.document.assistant.login
          }, {
            'name': 'CreatorVar',
            'value': this.document.creator.login
          }];

          if (hasApprovalWithSign) {
            processVariables.push({'name': 'IsApprovalNeeded', 'value': 'true'});
          }

          return this.memoActivityService.startProcess(processName, this.document, processVariables);
        }), tap(response => {
          this.blockUI.stop();
          this.goToCard();
        }), catchError(error => {
          console.log(error);
          this.blockUI.stop();
          return throwError(error);
        })
      ).subscribe();
    } else {
      this.toastr.warning('Не заполнены обязательные поля!');
    }
  }

  save() {
    if (this.model.isValid()) {
      this._saveDocument('initial').pipe(
        tap(response => {
          this.goToCard();
          this.blockUI.stop();
        }), catchError(error => {
          console.log(error);
          this.blockUI.stop();
          return throwError(error);
        })
      ).subscribe();
    } else {
      this.toastr.warning('Не заполнены обязательные поля!');
    }
  }

  _resolveDocumentState(): Observable<any> {
    if (this.document.documentId) {
      return of([]);
    }
    else {
      return this.memoDocumentService.createDocument(this.document);
    }
  }

  _saveDocument(status: string): Observable<any> {
    const FILE_TYPES = ['attachedFile', 'attachment', 'draftWithoutAtt', 'draftFiles'];

    this.blockUI.start();
    this.model.updateDocumentFromModel(this.document);
    this.document.beginDate = this.document.beginDate ? this.document.beginDate : this.dateWithoutTime(new Date());
    this.memoDocumentService.updateDocumentStatus(this.document, this.dicts.statuses, status);

    return this._resolveDocumentState().pipe(
      mergeMap(response => {
        return this.fileService.saveFilesWithFileType(this.document.documentId, this.document.folderId, this.model.files);
      }), mergeMap(response => {
        if (response.length > 0) {
          let document = angular.copy(this.document);
          this.document.fileId = response;
          return this.fileService.updateInfoAboutFilesFromFolder(this.document.folderId).pipe((
            mergeMap(response => {
              response.files.forEach(el => {
                if (FILE_TYPES.indexOf(el.fileType) !== -1) {
                  const file = FileType.create(el.versionSeriesGuid, el.fileName,
                    el.fileSize.toString(), false, el.dateCreated, el.fileType, el.mimeType);
                  this.document[el.fileType] = this.document[el.fileType] ? this.document[el.fileType] : [];
                  const hasFile = this.document[el.fileType].find((f) => f.idFile === file.idFile);
                  if (!hasFile) {
                    this.document[el.fileType].push(file);
                  }
                }
              });
              return this.memoDocumentService.updateDocument(this.document.documentId, document, this.document);
            })
          ));
        }
        else {
          return of([]);
        }
      }), catchError(error => {
        console.log(error);
        this.blockUI.stop();
        return throwError(error);
      })
    );
  }

  goToCard() {
    this.$state.go('app.instruction.memo-card', {id: this.document.documentId});
  }

  private dateWithoutTime(date: Date): Date {
    let _date = date.setHours(0, 0, 0, 0).valueOf();
    return new Date(_date);
  }

}
