import {Component, Input, OnInit} from '@angular/core';
import {InstructionDictsModel} from "../../../models/instruction/InstructionDictsModel";
import * as _ from 'lodash';
import {HelperService} from '../../../services/helper.service';
import {InstructionDocumentNumberWeek} from "../../../models/instruction-document/InstructionDocumentNumberWeek";
import {
  InstructionDocumentPeriodMonth,
  InstructionDocumentPeriodWeekday,
  InstructionDocumentRepeated
} from '../../../models/instruction-document/InstructionDocumentRepeated';

@Component({
  selector: 'mggt-order-repeat-editor',
  templateUrl: './order-repeat-editor.component.html',
  styleUrls: ['./order-repeat-editor.component.scss']
})
export class OrderRepeatEditorComponent implements OnInit {

  @Input() model: InstructionDocumentRepeated;
  @Input() create: boolean;
  @Input() edit: boolean;
  @Input() dicts: InstructionDictsModel;
  @Input() planDate: Date;

  weekNumbers = [];

  d: any = {};

  constructor(public helper: HelperService) {}

  ngOnInit() {
    this.initDicts();
  }

  initDicts() {
    this.d.weekNumbers = this.dicts.weekNumbers.map(i => {
      const item: InstructionDocumentNumberWeek = new InstructionDocumentNumberWeek();
      item.build(<any>i);
      return item;
    });
    this.d.weekdays = this.dicts.weekdays.map(rw => {
      const weekday = new InstructionDocumentPeriodWeekday();
      weekday.updateFromNsi(rw);
      return weekday;
    });
    this.d.months = this.dicts.months.map(rw => {
      const month = new InstructionDocumentPeriodMonth();
      month.updateFromNsi(rw);
      return month;
    });

  }

  repeatPeriodChange() {
    this.model.periodicity = null;
    this.model.RepeatWorkingDay = null;
    this.model.weekday = [];
    this.model.Month = [];
    this.model.dayMonth = null;
    this.model.numberWeek = null;
  }

  repeatPeriodEndDateChange(planDate: Date) {
    if (this.model.dateEndPeriodRepeat && this.model.dateEndPeriodRepeat.getHours() === 0
      && this.model.dateEndPeriodRepeat.getMinutes() === 0 && this.model.dateEndPeriodRepeat.getMinutes() === 0) {
      this.model.dateEndPeriodRepeat.setHours(23, 59, 59, 0);
    }

    if (planDate && this.planDate && this.planDate > planDate) {
      this.model.dateEndPeriodRepeat = this.planDate;
    }
  }

  getMinMaxResult(value: string = '', min?: number, max?: number): any {
    if (value && min && _.toNumber(value) < min) { return _.toString(min); }
    if (value && max && _.toNumber(value) > max) { return _.toString(max); }
    return value;
  }

  dayNumberPipe(value: string = ''): any {
    return this.getMinMaxResult(value, 1, 31);
  }

  fromOneAndMore(value: string = ''): any {
    return this.getMinMaxResult(value, 1);
  }

  isMandatory(name: string): boolean {
    let r = false;
    switch(name) {
      case 'repeatWeekNumber':
        r = !this.model.dayMonth
          // && (!!this.model.numberWeek || !!this.model.weekday.length)
          && (!this.model.numberWeek || !this.model.weekday.length);
        break;
      case 'dayMonth':
        r = !this.model.numberWeek && !this.model.weekday.length;
        break;
    }
    return r;
  }

}
