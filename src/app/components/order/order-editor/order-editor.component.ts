import {Component, OnInit, Input} from '@angular/core';
import * as _ from 'lodash';
import {copy} from 'angular';
import { catchError, tap } from 'rxjs/internal/operators';
import { EMPTY, from } from 'rxjs/index';
import { InstructionDictsModel } from '../../../models/instruction/InstructionDictsModel';
import { NsiResourceService, UserBean } from '@reinform-cdp/nsi-resource'
import { OrderModalService } from '../../../services/order-modal.service';
import { FileService } from '../../../services/file.service';
import { InstructionExecutor } from '../../../models/instruction/InstructionExecutor';
import {CoExecutor, CoExecutorUser} from '../../../models/instruction/CoExecutorModel';
import { OrderDocumentService } from '../../../services/order-document.service';
import {CdpBpmHistoryResourceService} from '@reinform-cdp/bpm-components';
import {HelperService} from '../../../services/helper.service';
import {InstructionDocument} from '../../../models/instruction-document/InstructionDocument';
import {DocumentUser} from '../../../models/core/DocumentUser';
import {InstructionDocumentForInformation} from '../../../models/instruction-document/InstructionDocumentForInformation';
import {InstructionDocumentSubject} from '../../../models/instruction-document/InstructionDocumentSubject';
import {sevenDaysInMs} from '../../../consts';
import {InstructionDocumentTheme} from '../../../models/instruction-document/InstructionDocumentTheme';
import {InstructionDocumentPriority} from '../../../models/instruction-document/InstructionDocumentPriority';

@Component({
  selector: 'mggt-order-editor',
  templateUrl: './order-editor.component.html',
  styleUrls: ['./order-editor.component.scss']
})
export class OrderEditorComponent implements OnInit {

  @Input() model: InstructionDocument;
  @Input() dicts: InstructionDictsModel;
  @Input() edit: boolean;
  @Input() extData: any = {};

  coExecutors: UserBean[] = [];
  forInformationUsers: UserBean[] = [];

  executor: UserBean;
  executors: UserBean[] = [];

  editRepeat: boolean = false;
  numberRepeat: number = null;
  isShowRepeat: boolean = false;

  d: any = {};

  constructor(private nsiResourceService: NsiResourceService,
              public fileService: FileService,
              private instructionModalService: OrderModalService,
              private orderDocumentService: OrderDocumentService,
              private bpmHistoryService: CdpBpmHistoryResourceService,
              private helper: HelperService) { }

  ngOnInit() {
    this.transformDicts();
    if (!this.extData.minDate) {
      this.extData.minDate = new Date((new Date).valueOf() - sevenDaysInMs);
    }
    this.numberRepeat = this.getRepeatIndex();
    const prepareUsers = (item: any) => {
      const coEx: any = copy(item);
      coEx.accountName = item.reviewBy.login;
      coEx.displayName = item.reviewBy.fioFull;
      coEx.post = item.reviewBy.post;
      return coEx;
    };
    this.dicts._themes = _.orderBy(this.dicts._themes, ['name'], ['asc']);
    this.coExecutors = this.getCoExecutors().map(prepareUsers);
    this.forInformationUsers = this.getForInformations().map(prepareUsers);
    this.executor = this.model.instruction.executor
      ? this.model.instruction.executor.toUserBean()
      : null;
    this.extData.priority = this.model.instruction.priority ? this.model.instruction.priority.code : '';
    this.extData.difficulty = this.model.instruction.difficulty ? this.model.instruction.difficulty.code : '';

    this.editRepeat = this.model.instruction.repeated.planDateRepeat.length === 1
      && !this.model.instruction.repeated.planDateRepeat[0].reportAccepted;

    if (this.model.documentId) {
      this.bpmHistoryService.listOfHistoricProcessInstances({
        variables: [
          { name: 'EntityIdVar', operation: 'equals', type: 'string',
            value: this.model.documentId }
        ],
        size: 1000000,
        includeProcessVariables: true
      }).subscribe(res => {
        if (res && res.data) {
          this.isShowRepeat = !_.some(res.data, i => {
            const pdIdPart = i.processDefinitionId.split(':');
            return pdIdPart[0] === 'sdoassign_issueAssignment_prc' && +pdIdPart[1] <= 7;
          });
        } else {
          this.isShowRepeat = true;
        }
      })
    } else {
      this.isShowRepeat = true;
    }
  }

  transformDicts() {
    this.d.subjects = this.dicts._themes.map(i => {
      const r = new InstructionDocumentSubject();
      r.build(i);
      return r;
    });
    this.d.themes = this.dicts.themes.map(i => {
      const r = new InstructionDocumentTheme();
      r.build(i);
      return r;
    });
    this.d.priorities = this.dicts.priorities.map(i => {
      const r = new InstructionDocumentPriority();
      r.build(i);
      return r;
    });
    this.d.difficulties = this.dicts.difficulties.map(i => {
      const r = new InstructionDocumentPriority();
      r.build(i);
      return r;
    });
    if (this.model.instruction.creator) {
      this.executors = _.find(this.dicts.creators, i => i.creator.accountName === this.model.instruction.creator.login).executor;
    }
  }

  // Update "information.coExecutor" in model when field is changed
  onCoExecutorChange() {
    const newCoExecutors: CoExecutor[] = [];
    const repeatIndex = this.model.documentId ? this.getRepeatIndex() : 1;
    this.model.instruction.coExecutor = this.model.instruction.coExecutor || [];
    this.coExecutors.forEach((coExecutor: any) => {
      const coEx = new CoExecutor(coExecutor);
      if (coEx.isEmptyObject()) {
        coEx.reviewBy = new CoExecutorUser(coExecutor);
        coEx.numberRepeat = this.numberRepeat;
      }
      if (!this.model.documentId) { coEx.numberRepeat = 1; }
      newCoExecutors.push(coEx);
    });
    this.model.instruction.coExecutor = _.concat([],
      repeatIndex ? this.model.instruction.coExecutor.filter(i => i.numberRepeat !== repeatIndex) : [],
      newCoExecutors);
  }

  // Update "forInformation" in model when field is changed
  onForInformationChange() {
    const newForInformation: InstructionDocumentForInformation[] = [];
    const repeatIndex = this.model.documentId ? this.getRepeatIndex() : 1;
    this.model.forInformation = this.model.forInformation || [];
    this.forInformationUsers.forEach((fiUser: any) => {
      const fi = new InstructionDocumentForInformation();
      fi.build(fiUser);
      if (!fi.reviewBy.login) { fi.updateFromUserBean(fiUser) }
      fi.numberRepeat = fiUser.numberRepeat || this.numberRepeat;
      if (!this.model.documentId) { fi.numberRepeat = 1; }
      newForInformation.push(fi);
    });
    this.model.forInformation = _.concat([],
      repeatIndex ? this.model.forInformation.filter(i => i.numberRepeat !== repeatIndex) : [],
      newForInformation);
  }

  // create new theme with modal-window
  createTheme() {
    this.instructionModalService.createNewTheme().pipe(
      tap(response => {
        let newTheme = response;
        from(this.nsiResourceService.getDicts(['InstructionTheme'])).pipe(
          tap(response => {
            this.dicts._themes = _.orderBy(response.InstructionTheme, ['name'], ['asc']);
            this.nsiResourceService.updateDictFromCache('InstructionTheme', response.InstructionTheme);
            this.model.instruction.subject = new InstructionDocumentSubject();
            this.model.instruction.subject.build(this.dicts._themes.find(th => th.name === newTheme));
          }),
          catchError(error => {
            return EMPTY;
          })
        ).subscribe();
      }),
      catchError(error => {
        return EMPTY;
      })
    ).subscribe();
  }

  // Update "instruction.create" in model and executor list when field is changed
  creatorChanged(creator: InstructionExecutor) {
    if (creator) {
      this.model.instruction.creator = new DocumentUser();
      this.model.instruction.creator.updateFromUserBean(creator.creator);
      this.executors = creator.executor;
    } else {
      this.model.instruction.creator = null;
      this.executors = [];
    }
  }

  // Update "instruction.executor" in model when field is changed
  executorChanged() {
    this.model.instruction.executor = null;
    if (this.executor) {
      this.model.instruction.executor = new DocumentUser();
      this.model.instruction.executor.updateFromUserBean(this.executor);
    }
  }

  // Update "planDate" when field "beginDate" is changed
  beginDateChange(beginDate: Date) {
    if (beginDate && this.model.instruction.planDate && this.model.instruction.planDate < beginDate) {
      this.model.instruction.planDate = null;
    }
  }

  // Transform "planDate" when field is changed
  planDateChange(planDate: Date) {
    if (this.model.instruction.planDate && this.model.instruction.planDate.getHours() === 0
      && this.model.instruction.planDate.getMinutes() === 0 && this.model.instruction.planDate.getMinutes() === 0) {
      this.model.instruction.planDate.setHours(17, 0, 0, 0);
    }
    this.orderDocumentService.planDate = planDate;
  }

  exFilesChanged() {
    this.fileService.checkFiles2Rename();
  }

  getRepeatIndex(): number {
    return this.helper.getField('instruction.repeated.currentNumberRepeat', this.model);
  }

  getCoExecutors(): any[] {
    return (this.helper.getField('instruction.coExecutor', this.model) || [])
      .filter(i => _.isNumber(this.getRepeatIndex()) ? i.numberRepeat === this.getRepeatIndex() : true) ;
  }

  getForInformations(): any[] {
    return (this.helper.getField('forInformation', this.model) || [])
      .filter(i => _.isNumber(this.getRepeatIndex()) ? i.numberRepeat === this.getRepeatIndex() : true) ;
  }

  isDisabled(fieldName: string) {
    let r = false;
    switch(fieldName) {
      case 'executor':
        const dictCreator = this.model.instruction.creator
          ? _.find(this.dicts.creators, i => i.creator.accountName === this.model.instruction.creator.login)
          : null;
        r = !(dictCreator && dictCreator.executor && dictCreator.executor.length > 0) || this.edit;
        break;
    }
    return r;
  }

  priorityChanged() {
    this.model.instruction.priority = _.find(this.d.priorities, i => i.code === this.extData.priority);
  }

  difficultyChanged() {
    this.model.instruction.difficulty = _.find(this.d.difficulties, i => i.code === this.extData.difficulty);
  }
}
