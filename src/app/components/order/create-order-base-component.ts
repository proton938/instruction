import * as _ from 'lodash';
import * as angular from 'angular';
import { Observable, of } from 'rxjs';
import { mergeMap, tap } from 'rxjs/internal/operators';
import { InstructionDictsModel } from '../../models/instruction/InstructionDictsModel';
import { InstructionDocument } from '../../models/instruction-document/InstructionDocument';
import { InstructionRestService } from '../../services/instruction-rest.service';
import { OrderDictsService } from '../../services/order-dicts.service';
import { OrderActivitiService } from '../../services/order-activiti.service';
import { FileService } from '../../services/file.service';
import { OrderDocumentService } from '../../services/order-document.service';
import {DocumentUser} from "../../models/core/DocumentUser";
import {HelperService} from "../../services/helper.service";
import {InstructionDocumentDifficulty} from '../../models/instruction-document/InstructionDocumentDifficulty';
import {InstructionDocumentPriority} from '../../models/instruction-document/InstructionDocumentPriority';

export class CreateOrderBaseComponent {

  extData: any = {};
  public dicts: InstructionDictsModel;
  public document: InstructionDocument;
  public model: InstructionDocument;

  constructor(public instructionRestService: InstructionRestService,
              public instructionDictsService: OrderDictsService,
              public instructionActivityService: OrderActivitiService,
              public fileService: FileService,
              public documentService: OrderDocumentService,
              public helper: HelperService) {
  }

  init(): Observable<any> {
    return this.instructionDictsService.getDicts(undefined, false).pipe(
      tap(response => {
        this.dicts = response;
        this.document = new InstructionDocument();
        this.updateInfoAboutSource();
        this.initModel();
      })
    );
  }

  initModel() {
    this.model = angular.copy(this.document);
    this.model.instruction.beginDate = new Date();
    this.model.instruction.assistant = new DocumentUser();
    this.model.instruction.assistant.updateFromUserBean(this.documentService.getCurrentUser());
    if (this.dicts.creators.length > 0) {
      this.model.instruction.creator = new DocumentUser();
      this.model.instruction.creator.updateFromUserBean(this.dicts.creators[0].creator);
    }
    this.model.instruction.priority = new InstructionDocumentPriority();
    this.model.instruction.priority.build(_.find(this.dicts.priorities, i => i.code === 'normal'));
    this.model.instruction.difficulty = new InstructionDocumentDifficulty();
    this.model.instruction.difficulty.build(_.find(this.dicts.difficulties, i => i.code === 'normal'));
  }

  updateInfoAboutSource() {}

  prepareData() {
    this.document = this.model;
    this.documentService.updateDocumentStatus(this.document, this.dicts.statuses, 'assign');
    this.documentService.beforeSave(this.document);
  }

  save(): Observable<any> {
    this.prepareData();
    return this.documentService.createDocument(this.document).pipe(
      mergeMap(() => {
        return this.fileService.syncExFiles(this.document.documentId, this.document.folderId, r => {});
      }),
      mergeMap(response => {
        let document = angular.copy(this.document);
        this.document.instruction.fileId = response;
        return this.documentService.updateDocument(this.document.documentId, document, this.document);
      }),
      mergeMap(() => {
        return this.instructionActivityService.startProcess('sdoassign_issueAssignment_prc', this.document);
      }),
      mergeMap(() => {
        return this.instructionActivityService.startReviewProcess('sdoassign_startTakeIntoConsideration', this.document);
      }),
      mergeMap(() => {
        return this.instructionActivityService.startCoExecutionProcess('sdoassign_startTakeIntoCoexecution', this.document);
      }),
      mergeMap(() => {
        return this.saveAdditionInformation();
      }),
      mergeMap(() => {
        return this.documentService.sendMessage(this.dicts.executors, this.document);
      }),
      mergeMap(() => {
        return this.afterSave();
      }),
      tap(() => {
        this.goBack();
      })
    );
  }

  saveAdditionInformation(): Observable<any> {
    return of('');
  }

  afterSave(): Observable<any> {
    return of('');
  }

  goBack() {}

}
