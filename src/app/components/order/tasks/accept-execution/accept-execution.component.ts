import * as _ from 'lodash';
import { SessionStorage } from '@reinform-cdp/security';
import { Component, OnInit } from '@angular/core';
import * as angular from 'angular';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { StateService, Transition } from '@uirouter/core';
import { catchError, mergeMap, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { SearchResultDocument } from '@reinform-cdp/search-resource';
import { ActiveTaskService, ActivityResourceService, ITask } from '@reinform-cdp/bpm-components';
import { LoadingStatus, ExFileType } from '@reinform-cdp/widgets';

import { OrderDictsService } from '../../../../services/order-dicts.service';
import { OrderActivitiService } from '../../../../services/order-activiti.service';
import { InstructionDictsModel } from '../../../../models/instruction/InstructionDictsModel';
import { InstructionDocument } from '../../../../models/instruction-document/InstructionDocument';
import { InstructionDocumentResult } from '../../../../models/instruction-document/InstructionDocumentResult';
import { InstructionRestService } from '../../../../services/instruction-rest.service';
import { OrderDocumentService } from '../../../../services/order-document.service';
import { MemoDocument } from '../../../../models/memo-document/MemoDocument';
import { FileService } from './../../../../services/file.service';
import {CoExecutor, CoExecutorUser} from './../../../../models/instruction/CoExecutorModel';
import { IFileInFolderInfo } from '@reinform-cdp/file-resource';
import {HelperService} from '../../../../services/helper.service';
import {Observable, from, of, throwError} from 'rxjs';
import {SolrMediatorService} from "../../../../services/solr-mediator.service";
import {LinkRestService} from "../../../../services/link-rest.service";

@Component({
  selector: 'mggt-accept-execution',
  templateUrl: './accept-execution.component.html',
  styleUrls: ['./accept-execution.component.scss']
})
export class AcceptExecutionComponent implements OnInit {

  rd: any[] = [];
  rdExpand = true;
  mainOrderExpanded = true;
  params: any;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  task: ITask;
  dicts: InstructionDictsModel;
  document: InstructionDocument;
  copyDocument: InstructionDocument;
  recreatedDocument: InstructionDocument;
  subTasks: SearchResultDocument[];
  cadastralChecked: number;
  cadastralAuthor: string;
  result: InstructionDocumentResult;
  memo: MemoDocument;
  selectedTab = 0;
  meeting: SearchResultDocument[];
  meetingQuestion: SearchResultDocument[] | any[];
  reviewNote: string = '';
  currentUser: CoExecutorUser = new CoExecutorUser();
  delegationRules: any[] = [];
  files: ExFileType[];

  instructionMkaExpanded = true;
  orderMkaExpanded = true;
  expressMeetingMkaExpanded = true;
  meetingMkaExpanded = true;
  protocolMkaExpanded = true;
  memoMKAExpanded: boolean = true;
  memoExpanded = true;
  instructionMka: any = null;
  orderMka: any = null;
  expressMeetingMka: any = null;
  meetingMka: any = null;
  protocolMka: any = null;
  memoMKA: MemoDocument;
  meetingTypeExpressMeetingFull: string;
  meetingTypeMeetingFull: string;
  hideOvers = true;
  exFiles: ExFileType[] = [];

  parentDocument: InstructionDocument;

  taskOwnerLogin: string = '';
  id: string;

  numberRepeat: number;
  coExecutorValue: any = [];
  forInformationValue: any = [];

  @BlockUI('TakeIntoConsideration') blockUI: NgBlockUI;

  constructor(public $state: StateService,
    public instructionDocumentService: OrderDocumentService,
    private transition: Transition,
    private activeTaskService: ActiveTaskService,
    private instructionDictsService: OrderDictsService,
    private instructionActivityService: OrderActivitiService,
    private instructionRestService: InstructionRestService,
    private toastr: ToastrService,
    private session: SessionStorage,
    private activityRestService: ActivityResourceService,
    public fileService: FileService,
    private solrMediator: SolrMediatorService,
    public helper: HelperService,
    private linkService: LinkRestService) {
    this.fileService.ex.filesToUpload = []
  }

  ngOnInit() {
    this.params = angular.copy(this.$state.params);
    let user = this.instructionDocumentService.getCurrentUser();
    this.currentUser.login = user.accountName;
    this.currentUser.fioFull = user.displayName;

    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        this.updateTaskOwner();
        return this.instructionDictsService.getDicts([
          'InstructionStatus',
          'mggt_meeting_MeetingTypes',
          'InstructionPriority',
        ]);
      }),
      mergeMap(response => {
        this.dicts = response;
        return this.instructionDictsService.getDelegate(this.session.login());
      }),
      mergeMap(response => {
        this.delegationRules = response;
        return this.instructionActivityService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        this.id = resp;
        this.linkService.findFromSolr(this.id, [this.solrMediator.types.order]).subscribe(res => {
          this.rd = res;
        }, error => {
          console.log(error);
        });
        return this.instructionRestService.get(resp);
      }),
      mergeMap(response => {
        this.document = new InstructionDocument();
        this.document.build(response.document);

        if (this.document.result && this.document.result.length === 0) {
          this.document.result.push(new InstructionDocumentResult());
        }
        else {
          let lastResult = this.document.result[this.document.result.length - 1];
          if (lastResult.reportAcceptor.login) {
            this.document.result.push(new InstructionDocumentResult());
          }
        }
        this.numberRepeat = this.getRepeatedIndex();
        this.coExecutorValue = this.getCoExecutors(true);
        this.forInformationValue = this.getForInfo(true);

        this.result = this.document.result[this.document.result.length - 1];
        return this.instructionDocumentService.getParentDocument(this.document.parentId)
      }),
      mergeMap(response => {
        this.parentDocument = response;
        return this.fileService.updateInfoAboutFilesFromFolder(this.document.folderId);
      }),
      mergeMap(response  => {
        return this.instructionDocumentService.getSubTasks(this.document.documentId);
      }),
      mergeMap(response => {
        this.subTasks = response;
        this.files = this.fileService.getInfoAboutFilesByIds(this.document.instruction.fileId);
        this.fileService.updateInfoAboutFilesByIds([].concat
          .apply([], _.compact(this.document.instruction.coExecutor
            .filter(i => i.reviewBy && i.reviewBy.login === this.taskOwnerLogin)
            .map(c => c.fileId))
          )
        );

        return this.instructionDocumentService.getMemo(this.document.instruction.memoId);
      }),
      mergeMap(response => {
        this.memo = response;
        return this.instructionDocumentService.getMemoMKA(this.document.instruction.memoMKAID);
      }),
      mergeMap((response) => {
        this.memoMKA = response;
        if (this.document.instruction.InstructionMKAID) {
          return this.getInstructionMka();
        }
        if (this.document.instruction.OrderID) {
          return this.getOrderMka();
        }
        if (this.document.instruction.AgendaID) {
          return this.getMeetingMka();
        }
        if (this.document.instruction.ProtocolID) {
          return this.getProtocolMka();
        } else {
          return of([]);
        }
      })
    ).subscribe(() => {
      this.document.instruction.coExecutor = this.document.instruction.coExecutor || [];
      let coExecutor = this.getCoExecutor(this.session.login());
      if (!coExecutor) {
        coExecutor = new CoExecutor();
        coExecutor.reviewBy = this.currentUser;
        this.document.instruction.coExecutor.push(coExecutor);
      }

      if (coExecutor.fileId && coExecutor.fileId.length) {
        this.fileService.updateInfoAboutFilesByIds(coExecutor.fileId);
        this.exFiles = this.fileService.ex.files.slice();
      }
      this.reviewNote = coExecutor.reviewNote;
      this.success = true;
      this.loadingStatus = LoadingStatus.SUCCESS;
    }, error => {
      console.log(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  updateTaskOwner() {
    const login = this.session.login();
    if (!!this.task && !!this.task.owner) {
      this.taskOwnerLogin = this.task.owner; // Владелец делегированной задачи
    } else if (!!this.task && !!this.task.assignee && this.task.assignee !== login) {
      this.taskOwnerLogin = this.task.assignee; // Задача делегирована, но только взята в работу (не перезагружена)
    } else this.taskOwnerLogin = login;
  }

  exFilesChanged() {
    // this.fileService.ex.filesToUpload = this.exFiles.filter(i => !i.idFile);
    this.fileService.checkFiles2Rename();
  }

  deleteExFile(params: any) {
    if (params && params.fileInfo) {
      let index = this.exFiles.findIndex(f => f === params.fileInfo);
      this.exFiles.splice(index, 1);
      this.exFilesChanged();
    }
  }

  saveDocument() {
    this.blockUI.start();

    this.instructionRestService.get(this.document.documentId).pipe(
      mergeMap(response => {
        this.document.build(response.document);
        this.copyDocument = angular.copy(this.document);
        return this.fileService.syncExFiles(this.document.documentId, this.document.folderId, ids => {
          let login = this.session.login();
          let coExecutor = this.getCoExecutor(login);
          if (coExecutor) {
            coExecutor.fileId = _.filter(coExecutor.fileId, id => !_.find(ids, i => i === id));
          }
        });
      }),
      mergeMap((response) => {
        let login = this.session.login();
        let coExecutor = this.getCoExecutor(login);
        if (!coExecutor) {
          this.toastr.error('Не удалось определить блок соисполнения для пользователя ' + login);
          return;
        }
        Array.prototype.push.apply(coExecutor.fileId, response);
        coExecutor.factReviewBy = this.currentUser;
        //coExecutor.reviewFactDate = new Date();
        coExecutor.reviewNote = this.reviewNote;

        return this.instructionDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document)
      })
    ).subscribe(response => {
      this.blockUI.stop();
      //window.location.reload();
      window.location.href = '/main/#/app/tasks';
    }, error => {
      console.log(error);
      this.blockUI.stop();
    });
  }

  finishTask() {
    this.blockUI.start();
    this.instructionRestService.get(this.document.documentId).pipe(
      mergeMap(response => {
        this.document.build(response.document);
        this.copyDocument = angular.copy(this.document);
        return this.fileService.syncExFiles(this.document.documentId, this.document.folderId, ids => {
          let login = this.session.login();
          let coExecutor = this.getCoExecutor(login);
          if (coExecutor) {
            coExecutor.fileId = _.filter(coExecutor.fileId, id => !_.find(ids, i => i === id));
          }
        });
      }),
      mergeMap((response) => {
        let login = this.session.login();
        let coExecutor = this.getCoExecutor(login);
        if (!coExecutor) {
          this.toastr.error('Не удалось определить блок соисполнения для пользователя ' + login);
          return throwError('Отсутствует запись о соисполнителе с bpmTaskId  ' + this.task.id);
        }
        Array.prototype.push.apply(coExecutor.fileId, response);
        coExecutor.factReviewBy = this.currentUser;
        coExecutor.reviewFactDate = new Date();
        coExecutor.reviewNote = this.reviewNote;
        return this.instructionDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document)
      }), mergeMap(response => {
        return this.activityRestService.finishTask(parseInt(this.task.id));
      }), tap(response => {
        this.toastr.success('Задача успешно завершена!');
        this.blockUI.stop();
        window.location.href = '/main/#/app/tasks';
      }), catchError(error => {
        this.toastr.error('Ошибка при завершении задачи!');
        console.log(error);
        this.blockUI.stop();
        return throwError(error);
      })
    ).subscribe(response => {
      window.location.href = '/main/#/app/tasks';
    }, error => {
      console.log(error);
      this.blockUI.stop();
    });
  }

  getCoExecutor(login: string): CoExecutor {
    const getCoExByLogin = lgn => {
      const isRepeated = this.helper.getField('instruction.repeated.repeatedSign', this.document);
      const items = this.document.instruction.coExecutor.filter(r => r.reviewBy.login === lgn);
      const itemsWithoutTaskId = items.filter(r => !r.bpmTaskId);
      const itemsByTaskId = items.filter(r =>  r.bpmTaskId === this.task.id);
      if (!isRepeated) { return items[0] || null; }
      if (itemsByTaskId.length) { // Успешно нашелся по bpmTaskId
        return itemsByTaskId[0];
      } else if (itemsWithoutTaskId.length > 0) { // Не нашелся по taskId, ищем по логину с незаполненным bpmTaskId
        itemsWithoutTaskId.forEach(i => i.bpmTaskId = this.task.id);
        return items[0];
      }
      return null;
    };
    let taskOwnerLogin: string;
    // Определяем реального владельца задачи
    if (!!this.task && !!this.task.owner) {
      taskOwnerLogin = this.task.owner; // Владелец делегированной задачи
    } else if (!!this.task && !!this.task.assignee && this.task.assignee !== this.session.login()) {
      taskOwnerLogin = this.task.assignee; // Задача делегирована, но только взята в работу (не перезагружена)
    } else taskOwnerLogin = login;

    if (taskOwnerLogin !== login) {
      // section if task is delegated
      const today = Date.now();
      let delegationRule: any = _.find(this.delegationRules, (rule: any) => {
        return rule.delegationRule.fromUser === taskOwnerLogin && rule.delegationRule.toUser === login
          && rule.delegationRule.startDate < today && rule.delegationRule.endDate > today;
      });
      if (delegationRule) {
        return getCoExByLogin(delegationRule.delegationRule.fromUser);
      }
    } else {
      return getCoExByLogin(login);
    }
  }

  getInstructionMka(): any {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.InstructionMKAID], type: this.solrMediator.types.instruction }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res)[0])),
      mergeMap(docs => {
        if (docs.length) {
          this.instructionMka = docs[0];
          this.instructionMka.beginDate = this.instructionMka.beginDate
            ? this.instructionMka.beginDate.slice(0, -9)
            : this.instructionMka.beginDate;
        }
        return of([]);
      })
    );
  }


  getOrderMka(): any {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.OrderID], type: this.solrMediator.types.order }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length) { this.orderMka = docs[0]; }
        return of([]);
      })
    );
  }

  getMeetingMka(): any {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.AgendaID], type: this.solrMediator.types.agenda }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length) {
          const meeting = docs[0];
          const isExpress = this.dicts.meetingTypes
            .find(val => val.meetingType === meeting['meetingType'])['Express'];
          if (isExpress) {
            this.expressMeetingMka = meeting;
            this.meetingTypeExpressMeetingFull = this.dicts.meetingTypes
              .find(val => val.meetingType === this.expressMeetingMka['meetingType']).meetingFullName;
          } else {
            this.meetingMka = meeting;
            this.meetingTypeMeetingFull = this.dicts.meetingTypes
              .find(val => val.meetingType === this.meetingMka['meetingType']).meetingFullName;
          }
        }
        return of([]);
      })
    );
  }

  getProtocolMka(): any {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.ProtocolID], type: this.solrMediator.types.protocol }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length) { this.protocolMka = docs[0]; }
        return of([]);
      })
    );
  }

  isActive(index: number): boolean {
    return index === this.selectedTab;
  }

  onTabSelected(index: number) {
    if (this.selectedTab === index) {
      this.hideOvers = !this.hideOvers;
    } else {
      this.hideOvers = true;
    }
    this.selectedTab = index;
  }

  isRepeatedSign(): boolean {
    return !!this.document.instruction && !!this.document.instruction.repeated && this.document.instruction.repeated.repeatedSign;
  }

  getRepeatedIndex(): number {
    const item = _.first(this.getCoExecutors()) || _.first(this.getForInfo());
    return item ? item.numberRepeat : null;
  }

  getPlanDate(): Date {
    return this.isRepeatedSign()
      ? (_.find(this.document.instruction.repeated.planDateRepeat, p => {
        return p.numberRepeat === this.getRepeatedIndex();
      }) || {planDate: null}).planDate
      : this.document.instruction.planDate;

  }

  getCoExecutors(current?: boolean): any[] {
    return (this.document.instruction && this.document.instruction.coExecutor ? this.document.instruction.coExecutor : [])
      .filter(i => current
        ? (this.isRepeatedSign() ? i.numberRepeat === this.numberRepeat : true)
        : (this.isRepeatedSign() ? i.bpmTaskId === this.task.id : true));
  }

  getForInfo(current?: boolean): any[] {
    return (this.document.forInformation || [])
      .filter(i => current
        ? (this.isRepeatedSign() ? i.numberRepeat === this.numberRepeat : true)
        : (this.isRepeatedSign() ? i.bpmTaskId === this.task.id : true));
  }

  isShowFile(id: string): boolean {
      return _.some([].concat.apply([], this.coExecutorValue.map(i => i.fileId)), i => i === id);
  }

}
