import * as _ from 'lodash';
import * as angular from 'angular';
import {SessionStorage} from '@reinform-cdp/security';
import {StateService, Transition} from '@uirouter/core';
import {ToastrService} from 'ngx-toastr';
import {ActiveTaskService, ActivityResourceService, ITask} from '@reinform-cdp/bpm-components';
import {ExFileType, LoadingStatus} from '@reinform-cdp/widgets';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MeetingQuestionProtocolPair} from '../../../../models/instruction/MeetingQuestionProtocolPair';
import {MemoDocument} from '../../../../models/memo-document/MemoDocument';
import {InstructionDocument} from '../../../../models/instruction-document/InstructionDocument';
import {InstructionDocumentResult} from '../../../../models/instruction-document/InstructionDocumentResult';
import {InstructionDictsModel} from '../../../../models/instruction/InstructionDictsModel';
import {SearchResultDocument} from '@reinform-cdp/search-resource';
import {FileService} from '../../../../services/file.service';
import {OrderDictsService} from '../../../../services/order-dicts.service';
import {OrderDocumentService} from '../../../../services/order-document.service';
import {OrderActivitiService} from '../../../../services/order-activiti.service';
import {OrderModalService} from '../../../../services/order-modal.service';
import {InstructionRestService} from '../../../../services/instruction-rest.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {catchError, mergeMap, tap} from 'rxjs/internal/operators';
import {DocumentUser} from '../../../../models/core/DocumentUser';
import {from, Observable, of, throwError} from 'rxjs';
import {HelperService} from '../../../../services/helper.service';
import {SolrMediatorService} from "../../../../services/solr-mediator.service";
import {LinkRestService} from "../../../../services/link-rest.service";

@Component({
  selector: 'mggt-issue-assignment',
  templateUrl: './issue-assignment.component.html',
  styleUrls: ['./issue-assignment.component.scss']
})
export class IssueAssignmentComponent implements OnInit, OnDestroy {

  rd: any[] = [];
  rdExpand = true;
  memoExpanded = true;
  memoMKAExpanded = true;
  instructionMkaExpanded = true;
  orderMkaExpanded = true;
  expressMeetingMkaExpanded = true;
  protocolMkaExpanded = true;

  document: InstructionDocument;
  copyDocument: InstructionDocument;
  dicts: InstructionDictsModel;
  task: ITask;
  files: ExFileType[];
  exFiles: ExFileType[] = [];
  cadastralChecked: number;
  result: InstructionDocumentResult;
  subTasks: SearchResultDocument[];
  params: any;
  recreatedDocument: InstructionDocument;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  memo: MemoDocument;
  memoMKA: MemoDocument;
  meeting: SearchResultDocument[];
  //TODO: remove ' | any[]'
  meetingQuestion: SearchResultDocument[] | any[];
  pairs: MeetingQuestionProtocolPair[] = [];
  selectedTab = 0;
  hideOvers = true;
  questions: SearchResultDocument[];

  instructionMka: any = null;
  orderMka: any = null;
  expressMeetingMka: any = null;
  meetingMka: any = null;
  protocolMka: any = null;
  meetingTypeExpressMeetingFull: string;
  meetingTypeMeetingFull: string;

  parentDocument: InstructionDocument;
  id: string;

  @BlockUI('issueAssignment') blockUI: NgBlockUI;

  constructor(public fileService: FileService,
              private activeTaskService: ActiveTaskService,
              private instructionRestService: InstructionRestService,
              private instructionDictsService: OrderDictsService,
              public instructionDocumentService: OrderDocumentService,
              private instructionActivityService: OrderActivitiService,
              private instructionModalService: OrderModalService,
              private toastr: ToastrService,
              public $state: StateService,
              private transition: Transition,
              private session: SessionStorage,
              private activityRestService: ActivityResourceService,
              private solrMediator: SolrMediatorService,
              public helper: HelperService,
              private linkService: LinkRestService) {
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.params = angular.copy(this.$state.params);
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.instructionDictsService.getDicts([
          'InstructionStatus',
          'mggt_meeting_MeetingTypes',
          'InstructionPriority',
         ]);
      }),
      mergeMap(response => {
        this.dicts = response;
        return this.instructionActivityService.getEntityIdVar(this.task);
      }), mergeMap(resp => {
        this.id = resp;
        this.linkService.findFromSolr(this.id, [this.solrMediator.types.order]).subscribe(res => {
          this.rd = res;
        }, error => {
          console.log(error);
        });
        return this.instructionRestService.get(resp).pipe(
          mergeMap(response => {
            this.document = new InstructionDocument();
            this.document.build(response.document);
            return this.instructionDocumentService.getParentDocument(this.document.parentId).pipe(
              mergeMap(response => {
                this.parentDocument = response;
                return this.instructionDocumentService.getSubTasks(this.document.documentId);
              }),
              mergeMap(response => {
                this.subTasks = response;
                this.copyDocument = angular.copy(this.document);
                if (this.document.instruction.repeated.repeatedSign) {
                  this.result = _.chain(this.document.result).filter(res => !res.acceptStatus).first().value();
                  if (!this.result) {
                    this.result = new InstructionDocumentResult();
                    this.document.result.push(this.result);
                  }
                } else {
                  if (this.task.formKey === 'IssueAssignment') {
                    if (this.document.result && this.document.result.length === 0) {
                      this.document.result.push(new InstructionDocumentResult());
                    }
                    this.result = this.document.result[this.document.result.length - 1];
                  } else {
                    const lastResult = this.document.result[this.document.result.length - 1];
                    if (lastResult.reportAcceptor.login) {
                      this.document.result.push(new InstructionDocumentResult());
                    }
                    this.result = this.document.result[this.document.result.length - 1];
                  }
                }

                return this.updateStatusInWorkIfNeeded();
              }), mergeMap(response => {
                return this.fileService.updateInfoAboutFilesFromFolder(this.document.folderId);
              }), mergeMap(response => {
                this.files = this.fileService.getInfoAboutFilesByIds(this.document.instruction.fileId);
                this.fileService.updateInfoAboutFilesByIds(this.result.reportFileId);
                this.exFiles = this.fileService.ex.files.slice();
                return this.instructionDocumentService.getRecreatedDocument(this.document.previousId);
              }), mergeMap(response => {
                this.recreatedDocument = response;
                return this.instructionDocumentService.getQuestions(this.document);
              }), mergeMap(response => {
                this.questions = response;
                this.pairs = this.instructionDocumentService.buildPair(this.questions, this.dicts.meetingTypes);
                return this.instructionDocumentService.getMemo(this.document.instruction.memoId);
              }),
              mergeMap(response => {
                this.memo = response;
                return this.instructionDocumentService.getMemoMKA(this.document.instruction.memoMKAID);
              }),
              mergeMap(response => {
                this.memoMKA = response;
                return this.instructionDocumentService.getMeeting(this.document.instruction.expressMeetingID);
              }),
              mergeMap(response => {
                this.meeting = response;
                return this.instructionDocumentService.getMeetingQuestion(this.document.instruction.questionId);
              }), mergeMap(response => {
                this.meetingQuestion = response;
                if (this.document.instruction.InstructionMKAID) {
                  return this.getInstructionMka();
                }
                if (this.document.instruction.OrderID) {
                  return this.getOrderMka();
                }
                if (this.document.instruction.AgendaID) {
                  return this.getMeetingMka();
                }
                if (this.document.instruction.ProtocolID) {
                  return this.getProtocolMka();
                } else {
                  return of([]);
                }
              }));
          })
        );
      })
    ).subscribe(() => {
      this.success = true;
      this.loadingStatus = LoadingStatus.SUCCESS;
    }, error => {
      console.log(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  exFilesChanged() {
    // this.fileService.ex.filesToUpload = this.exFiles.filter(i => !i.idFile);
    this.fileService.checkFiles2Rename();
  }

  deleteExFile(params: any) {
    if (params && params.fileInfo) {
      let index = this.exFiles.findIndex(f => f === params.fileInfo);
      this.exFiles.splice(index, 1);
      this.exFilesChanged();
    }
  }

  saveDocument() {
    this.helper.beforeUnload.start();
    this.blockUI.start();
    this.result.reportExecutor = this.updateReportExecutor();
    this.result.reportDate = new Date();
    this.fileService.syncExFiles(this.document.documentId, this.document.folderId, ids => {
      this.result.reportFileId = _.filter(this.result.reportFileId, id => !_.find(ids, i => i === id));
    }).pipe(
      mergeMap(response => {
        Array.prototype.push.apply(this.result.reportFileId, response);
        if (this.document.instruction.cadastral && this.document.instruction.cadastral.author) {
          this.document.instruction.cadastral.checked = this.cadastralChecked;
        }
        return this.instructionDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document);
      })
    ).subscribe(response => {
      this.helper.beforeUnload.stop();
      window.location.href = '/main/#/app/tasks';
    }, error => {
      this.helper.beforeUnload.stop();
      console.log(error);
      this.blockUI.stop();
    });
  }

  finishTask() {
    this.helper.beforeUnload.start();
    if (this.isValid()) {
      this.blockUI.start();
      this.result.reportExecutor = this.updateReportExecutor();
      this.result.reportDate = new Date();
      this.fileService.syncExFiles(this.document.documentId, this.document.folderId, ids => {
        this.result.reportFileId = _.filter(this.result.reportFileId, id => !_.find(ids, i => i === id));
      }).pipe(
        mergeMap(response => {
          Array.prototype.push.apply(this.result.reportFileId, response);
          this.instructionDocumentService.updateDocumentStatus(this.document, this.dicts.statuses, 'check');
          if (this.document.instruction.cadastral && this.document.instruction.cadastral.author) {
            this.document.instruction.cadastral.checked = this.cadastralChecked;
          }
          return this.instructionDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document);
        }), mergeMap(response => {
          return this._finishTask();
        })
      ).subscribe(response => {
        this.helper.beforeUnload.stop();
        window.location.href = '/main/#/app/tasks';
      }, error => {
        this.helper.beforeUnload.stop();
        console.log(error);
        this.blockUI.stop();
      });
    } else {
      this.toastr.warning('Не заполнены обязательные поля!');
    }
  }

  private _finishTask(): Observable<any> {
    return from(this.activityRestService.getFormData({taskId: this.task.id})).pipe(
      mergeMap(response => {
        const hasDecisionVar = _.find(response.formProperties, v => {
          return v.id === 'DecisionVar';
        });
        const vars = [{name: 'Author_VAR', value: this.document.instruction.creator.login}];
        if (hasDecisionVar) {
          vars.push({name: 'DecisionVar', value: 'Positive'});
        }
        return from(this.activityRestService.finishTask(parseInt(this.task.id), vars));
      }), tap(response => {
        this.toastr.success('Задача успешно завершена!');
      }), catchError(error => {
        this.toastr.error('Ошибка при завершении задачи!');
        return throwError(error);
      })
    );
  }

  private updateStatusInWorkIfNeeded(): Observable<any> {
    if (this.document.statusId.code !== 'inwork') {
      const copyDocument = angular.copy(this.document);
      this.instructionDocumentService.updateDocumentStatus(this.document, this.dicts.statuses, 'inwork');
      return this.instructionDocumentService.updateDocument(copyDocument.documentId, copyDocument, this.document).pipe(
        tap(response => {
          this.toastr.success('Статус поручения успешно изменен!');
          //window.location.reload();
        }), catchError(error => {
          this.toastr.error('Ошибка при изменении статуса поручения!');
          return throwError(error);
        })
      );
    } else {
      return of('');
    }
  }

  private isValid(): boolean {
    const isCadastral = !!(this.document.instruction.cadastral && this.document.instruction.cadastral.author);
    const hasReportContent = (this.result.reportContent.trim() !== '');
    const hasReportCadastralChecked = this.cadastralChecked !== null && (this.cadastralChecked >= 0);
    const filesCount = this.fileService.ex.files.length + this.fileService.ex.filesToUpload.length
    + this.fileService.ex.filesToUpload.length;
    const hasFiles = !this.document.filesRequired || filesCount > 0;

    return hasReportContent && hasFiles && (!isCadastral || hasReportCadastralChecked);
  }

  private updateReportExecutor(): DocumentUser {
    const user = new DocumentUser();
    user.login = this.session.login();
    user.fioFull = this.session.fullName();
    user.post = this.session.post();
    return user;
  }

  copy() {
    if (this.document.parentId) {
      this.instructionModalService.copy().subscribe(result => {
        this.$state.go('app.instruction.instruction-copy', {
          copiedId: this.document.documentId,
          asSubTask: (result === 'subTask')
        });
      });
    } else {
      this.$state.go('app.instruction.instruction-copy', {copiedId: this.document.documentId, asSubTask: false});
    }
  }

  isActive(index: number): boolean {
    return index === this.selectedTab;
  }

  onTabSelected(index: number) {
    if (this.selectedTab === index) {
      this.hideOvers = !this.hideOvers;
    } else {
      this.hideOvers = true;
    }
    this.selectedTab = index;
  }

  getInstructionMka() {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.InstructionMKAID], type: this.solrMediator.types.instruction }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length) {
          this.instructionMka = docs[0];
          this.instructionMka.beginDate = this.instructionMka.beginDate
            ? this.instructionMka.beginDate.slice(0, -9)
            : this.instructionMka.beginDate;
        }
        return of([]);
      })
    );
  }

  getOrderMka() {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.OrderID], type: this.solrMediator.types.order }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length) { this.orderMka = docs[0]; }
        return of([]);
      })
    );
  }

  getMeetingMka() {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.AgendaID], type: this.solrMediator.types.agenda }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length) {
          const meeting = docs[0];
          const isExpress = this.dicts.meetingTypes
            .find(val => val.meetingType === meeting['meetingType'])['Express'];
          if (isExpress) {
            this.expressMeetingMka = meeting;
            this.meetingTypeExpressMeetingFull = this.dicts.meetingTypes
              .find(val => val.meetingType === this.expressMeetingMka['meetingType']).meetingFullName;
          } else {
            this.meetingMka = meeting;
            this.meetingTypeMeetingFull = this.dicts.meetingTypes
              .find(val => val.meetingType === this.meetingMka['meetingType']).meetingFullName;
          }
        }
        return of([]);
      })
    );
  }

  getProtocolMka() {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.ProtocolID], type: this.solrMediator.types.protocol }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length) { this.protocolMka = docs[0]; }
        return of([]);
      })
    );
  }

  getRepeatIndex(): number {
    return this.helper.getField('instruction.repeated.currentNumberRepeat', this.document);
  }

  getCoExecutors(): any[] {
    return (this.helper.getField('instruction.coExecutor', this.document) || [])
      .filter(i => _.isNumber(this.getRepeatIndex()) ? i.numberRepeat === this.getRepeatIndex() : true);
  }

  getForInfo(): any[] {
    return (this.helper.getField('forInformation', this.document) || [])
      .filter(i => _.isNumber(this.getRepeatIndex()) ? i.numberRepeat === this.getRepeatIndex() : true);
  }

  /*isShowFile(id: string): boolean {
    return _.some([].concat.apply([], this.getCoExecutors().map(i => i.fileId)), i => i === id);
  }*/

}
