import * as angular from 'angular';
import * as _ from 'lodash';
import { StateService, Transition } from '@uirouter/core';
import { ActiveTaskService, ITask, ActivityResourceService } from '@reinform-cdp/bpm-components';
import { LoadingStatus } from '@reinform-cdp/widgets';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { OrderActivitiService } from '../../../../services/order-activiti.service';
import { InstructionRestService } from '../../../../services/instruction-rest.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { catchError, map, mergeMap, tap } from 'rxjs/internal/operators';
import { EMPTY, from, throwError, of } from 'rxjs/index';
import { Observable } from 'rxjs/Rx';
import { InstructionDocument } from '../../../../models/instruction-document/InstructionDocument';
import { InstructionDocumentReview } from '../../../../models/instruction-document/InstructionDocumentReview';
import { OrderDocumentService } from '../../../../services/order-document.service';
import { DocumentUser } from '../../../../models/core/DocumentUser';
import { ToastrService } from 'ngx-toastr';
import { OrderModalService } from './../../../../services/order-modal.service';
import { OrderDictsService } from '../../../../services/order-dicts.service';
import { InstructionDictsModel } from '../../../../models/instruction/InstructionDictsModel';
import { SearchResultDocument } from '@reinform-cdp/search-resource';
import {HelperService} from '../../../../services/helper.service';


@Component({
  selector: 'mggt-review-mka-doc',
  templateUrl: './review-mka-doc.component.html'
})
export class ReviewMKADocComponent implements OnInit, OnDestroy {

  document: InstructionDocument;
  reviewBy: InstructionDocumentReview[];
  task: ITask;
  docType: string;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  copyDocument: InstructionDocument;
  reviewNote = '';
  currentUser: DocumentUser = new DocumentUser();
  listEmpl: any;
  processId: string;
  sentForReviewExpanded = false;
  internalInstructionsExpanded = false;
  dicts: InstructionDictsModel;
  insideOrder: SearchResultDocument[] = [];

  @BlockUI('reviewMKADoc') blockUI: NgBlockUI;

  constructor(
    private toastr: ToastrService,
    private activityRestService: ActivityResourceService,
    private activeTaskService: ActiveTaskService,
    private instructionRestService: InstructionRestService,
    public instructionDocumentService: OrderDocumentService,
    private instructionActivityService: OrderActivitiService,
    public $state: StateService,
    public transition: Transition,
    private instructionModalService: OrderModalService,
    private instructionDictsService: OrderDictsService,
    public helper: HelperService
  ) {
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    const user = this.instructionDocumentService.getCurrentUser();
    this.currentUser.login = user.accountName;
    this.currentUser.fioFull = user.displayName;
    this.currentUser.post = user.post;


    this.instructionDictsService.getDicts().pipe(
      mergeMap(response => {
        this.dicts = response;
        return this.activeTaskService.getTask();
      }),
      mergeMap(task => {
        this.task = task;
        return this.instructionActivityService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.instructionRestService.get(resp);
      }),
      mergeMap(response => {
        this.document = new InstructionDocument();
        this.document.build(response.document);
        this.copyDocument = angular.copy(this.document);
        return this.instructionDocumentService.getInsideInstructions(this.document.documentId)
          .pipe(
            tap(response => {
              this.insideOrder = response || [];
            })
          );
      }),
      mergeMap(() => {
        const review: InstructionDocumentReview = _.find(this.document.review, f => f.reviewBy && f.reviewBy.login === this.currentUser.login);
        if (review && !review.reviewPlanDate) {
          review.reviewPlanDate = this.task.dueDate;
          return this.instructionDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document).pipe(
            mergeMap(() => {
              return this.instructionRestService.get(this.document.documentId);
            }),
            tap(response => {
              this.document = new InstructionDocument();
              this.document.build(response.document);
            })
          );
        }
        return of(this.document);
      })
    ).subscribe(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, error => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  creteOrder() {
    this.$state.go('app.instruction.instruction-new', { field: 'InstructionMKAID', id: this.document.documentId, backUrl: window.location.href });
  }

  addEmpl() {
    this.helper.beforeUnload.start();
    this.instructionModalService.addEmployee().pipe(
      mergeMap(response => {
        this.listEmpl = response;
        return this.getProcessId('sdoassignmka_reviewMkaDocumentHand');
      }), mergeMap(response => {
        this.processId = response.data[0].id;
        return this.instructionRestService.get(this.document.documentId);
      }), mergeMap(response => {

        this.document.build(response.document);
        this.copyDocument = angular.copy(this.document);
        if (this.listEmpl.length > 0) {
          this.listEmpl.forEach(l => {
            const reviewBy = new DocumentUser();
            reviewBy.login = l.accountName;
            reviewBy.post = l.post;
            reviewBy.fioFull = l.displayName;

            const newReview = new InstructionDocumentReview();
            newReview.sentReviewBy = this.currentUser;
            newReview.reviewBy = reviewBy;
            this.document.review.push(newReview);

            this.activityRestService.initProcess({
              processDefinitionId: this.processId,
              variables: [
                { 'name': 'EntityIdVar', 'value': this.document.documentId },
                { 'name': 'sdoUserVar', 'value': l.accountName }
              ]
            });
          });
        }
        return this.instructionDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document);
      }), catchError(error => {
        console.log(error);
        this.blockUI.stop();
        return throwError(error);
      }), catchError(error => {
        return EMPTY;
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
    }, error => {
      this.helper.beforeUnload.stop();
    });
  }


  finishTask() {
    this.helper.beforeUnload.start();
    this.blockUI.start();
    this.instructionRestService.get(this.document.documentId).pipe(
      mergeMap(response => {
        this.document.build(response.document);
        this.copyDocument = new InstructionDocument();
        this.copyDocument = angular.copy(this.document);

        let review: InstructionDocumentReview = this.document.review.find(r => r.reviewBy.login === this.currentUser.login && !r.factReviewBy.login);
        if (review) {
          review.reviewFactDate = new Date();
          review.reviewNote = this.reviewNote;
          review.factReviewBy = this.currentUser;
        } else {
          review = new InstructionDocumentReview();
          review.reviewFactDate = new Date();
          review.reviewNote = this.reviewNote;
          review.factReviewBy = this.currentUser;
          this.document.review.push(review);
        }
        return this.instructionDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document);
      }), mergeMap(response => {
        return this.activityRestService.finishTask(parseInt(this.task.id));
      }), tap(response => {
        this.toastr.success('Задача успешно завершена!');
        this.blockUI.stop();
        window.location.href = '/main/#/app/tasks';
      }), catchError(error => {
        this.toastr.error('Ошибка при завершении задачи!');
        console.log(error);
        this.blockUI.stop();
        return throwError(error);
      })
    ).subscribe(response => {
      this.helper.beforeUnload.stop();
    }, error => {
      this.helper.beforeUnload.stop();
      console.log(error);
      this.blockUI.stop();
    });
  }

  getProcessId(name: string): Observable<any> {
    return from(this.activityRestService.getProcessDefinitions({ key: name, latest: true })).pipe(
      map(id => {
        return id;
      })
    );
  }
}
