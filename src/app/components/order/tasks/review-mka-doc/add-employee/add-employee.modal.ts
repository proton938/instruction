import {ToastrService} from "ngx-toastr";
import {BsModalService, BsModalRef} from "ngx-bootstrap/modal";
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'add-employee-modal',
  templateUrl: './add-employee.modal.html'
})
export class AddEmployeeModalComponent implements OnInit {

  theme: string = '';
  submit: boolean;
  employee: any;
  group: string = 'MGGT_MEMO_MKA';

  constructor(private bsModalRef: BsModalRef) {
  }

  ngOnInit() {
    this.submit = false;
  }

  create() {
    this.submit = true;
    this.bsModalRef.hide();
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

}
