import * as _ from 'lodash';
import * as angular from 'angular';
import {formatDate} from '@angular/common';
import {compare} from 'fast-json-patch';
import {ToastrService} from 'ngx-toastr';
import {StateService} from '@uirouter/core';
import {Component, OnInit} from '@angular/core';
import {SearchResultDocument} from '@reinform-cdp/search-resource';
import {MeetingQuestionProtocolPair} from '../../../../models/instruction/MeetingQuestionProtocolPair';
import {MemoDocument} from '../../../../models/memo-document/MemoDocument';
import {LoadingStatus, AlertService, ExFileType} from '@reinform-cdp/widgets';
import {InstructionDocument} from '../../../../models/instruction-document/InstructionDocument';
import {InstructionDocumentResult} from '../../../../models/instruction-document/InstructionDocumentResult';
import {
  ActiveTaskService, ActivityResourceService, IActivityFormProperty, IInfoAboutProcessInit,
  ITask
} from '@reinform-cdp/bpm-components';
import {IFileInFolderInfo} from '@reinform-cdp/file-resource';
import {InstructionDictsModel} from '../../../../models/instruction/InstructionDictsModel';
import {FileService} from '../../../../services/file.service';
import {SessionStorage} from '@reinform-cdp/security';
import {OrderModalService} from '../../../../services/order-modal.service';
import {OrderActivitiService} from '../../../../services/order-activiti.service';
import {OrderDocumentService} from '../../../../services/order-document.service';
import {OrderDictsService} from '../../../../services/order-dicts.service';
import {InstructionRestService} from '../../../../services/instruction-rest.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {catchError, mergeMap, tap} from 'rxjs/internal/operators';
import {NSIPzzDeveloper} from '../../../../models/nsi/NSIPzzDeveloper';
import {DocumentUser} from '../../../../models/core/DocumentUser';
import {Observable} from 'rxjs/Rx';
import {forkJoin, from, of, throwError} from 'rxjs/index';
import {MemoDocumentResult} from '../../../../models/memo-document/MemoDocumentResult';
import {MemoDocumentService} from '../../../../services/memo-document.service';
import {MemoRestService} from '../../../../services/memo-rest.service';
import {DocumentStatus} from '../../../../models/core/DocumentStatus';
import {BsModalService} from 'ngx-bootstrap/modal';
import {CloseMemoConfirmModalComponent} from './close-memo-confirm-modal.component';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {LinkRestService} from '../../../../services/link-rest.service';
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-accept-issue-assignment',
  templateUrl: './accept-issue-assignment.component.html'
})
export class AcceptIssueAssignmentComponent implements OnInit {

  rd: any[] = [];
  rdExpand = true;
  meetingExpanded = true;
  admMeetingExpanded = true;
  memoMKAExpanded = true;
  memoExpanded = true;
  mainOrderExpanded = true;
  instructionMkaExpanded = true;
  orderMkaExpanded = true;
  expressMeetingMkaExpanded = true;
  protocolMkaExpanded = true;

  document: InstructionDocument;
  copyDocument: InstructionDocument;
  dicts: InstructionDictsModel;
  task: ITask;
  files: ExFileType[];
  init: IInfoAboutProcessInit;
  result: InstructionDocumentResult;
  recreatedDocument: InstructionDocument;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  memo: MemoDocument;
  memoMKA: MemoDocument;
  meeting: SearchResultDocument[];
  //TODO: remove ' | any[]'
  meetingQuestion: SearchResultDocument[] | any[];
  pairs: MeetingQuestionProtocolPair[] = [];
  questions: SearchResultDocument[];

  instructionMka: any = null;
  orderMka: any = null;
  expressMeetingMka: any = null;
  meetingMka: any = null;
  protocolMka: any = null;
  meetingTypeExpressMeetingFull: string;
  meetingTypeMeetingFull: string;

  parentDocument: InstructionDocument;
  memoInstructions: any[] = [];
  id: string;

  @BlockUI('acceptIssueAssignment') blockUI: NgBlockUI;

  constructor(public fileService: FileService,
              private activeTaskService: ActiveTaskService,
              private instructionRestService: InstructionRestService,
              private instructionDictsService: OrderDictsService,
              public instructionDocumentService: OrderDocumentService,
              private instructionActivityService: OrderActivitiService,
              private toastr: ToastrService,
              private instructionModalService: OrderModalService,
              public $state: StateService,
              private modalService: BsModalService,
              private session: SessionStorage,
              private activityRestService: ActivityResourceService,
              private solrMediator: SolrMediatorService,
              private alertService: AlertService,
              private memoDocumentService: MemoDocumentService,
              private helper: HelperService,
              private linkService: LinkRestService) {
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.instructionDictsService.getDicts([
          'InstructionStatus',
          'mggt_meeting_MeetingTypes'
        ]);
      }),
      mergeMap(response => {
        this.dicts = response;
        return this.instructionActivityService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        this.id = resp;
        this.linkService.findFromSolr(this.id, [this.solrMediator.types.order]).subscribe(res => {
          this.rd = res;
        }, error => {
          console.log(error);
        });
        return this.instructionRestService.get(resp).pipe(
          mergeMap(response => {
            this.document = new InstructionDocument();
            this.document.build(response.document);
            this.copyDocument = angular.copy(this.document);
            return this.instructionDocumentService.getParentDocument(this.document.parentId).pipe(
              mergeMap(response => {
                this.parentDocument = response;
                return this.instructionActivityService.getProcessHistory(this.document)
              }),
              mergeMap(response => {
                this.init = response;
               /* if (this.document.instruction.cadastral && this.document.instruction.cadastral.author) {
                  this.cadastralAuthor = _.find(this.dicts.pzzDevelopers, (d: NSIPzzDeveloper) => {
                    return d.code === this.document.instruction.cadastral.author
                  }).name;
                }*/
                if (this.document.instruction.repeated.repeatedSign) {
                  this.result = _.chain(this.document.result).filter(res => !res.acceptStatus).first().value();
                  if (!this.result) {
                    this.result = new InstructionDocumentResult();
                    this.document.result.push(this.result);
                  }
                } else {
                  this.result = this.document.result[this.document.result.length - 1];
                }
                return this.fileService.updateInfoAboutFilesFromFolder(this.document.folderId);
              })
            );
          }),
          mergeMap(() => {
            if (this.document.instruction && this.document.instruction.memoId) {
              return this.solrMediator.query({
                page: 0,
                pageSize: 999,
                types: [this.solrMediator.types.instruction],
                query: 'memoID:' + this.document.instruction.memoId + ' AND NOT statusCode:closed',
                sort: 'beginDate desc'
              });
            } else {
              return of({docs: []});
            }
          }),
          mergeMap(res => {
            if (res && res.docs) this.memoInstructions = res.docs;
            this.files = this.fileService.getInfoAboutFilesByIds(this.document.instruction.fileId);
            return this.instructionDocumentService.getRecreatedDocument(this.document.previousId);
          }),
          mergeMap(response => {
            this.recreatedDocument = response;
            return this.instructionDocumentService.getQuestions(this.document);
          }),
          mergeMap(response => {
            this.questions = response;
            this.pairs = this.instructionDocumentService.buildPair(this.questions, this.dicts.meetingTypes);
            return this.instructionDocumentService.getMemo(this.document.instruction.memoId);
          }),
          mergeMap(response => {
            this.memo = response;
            return this.instructionDocumentService.getMemoMKA(this.document.instruction.memoMKAID);
          }),
          mergeMap(response => {
            this.memoMKA = response;
            return this.instructionDocumentService.getMeeting(this.document.instruction.expressMeetingID);
          }),
          mergeMap(response => {
            this.meeting = response;
            return this.instructionDocumentService.getMeetingQuestion(this.document.instruction.questionId);
          }),
          mergeMap(response => {
            this.meetingQuestion = response;
            if (this.document.instruction.InstructionMKAID) {
              return this.getInstructionMka();
            }
            if (this.document.instruction.OrderID) {
              return this.getOrderMka();
            }
            if (this.document.instruction.AgendaID) {
              return this.getMeetingMka();
            }
            if (this.document.instruction.ProtocolID) {
              return this.getProtocolMka();
            }
            else {
              return of([]);
            }
          })
        );
      })
    ).subscribe(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.checkAndSetTaskAccepted();
      this.success = true;
    }, error => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  checkAndSetTaskAccepted() {
    if (!_.isBoolean(this.document.instruction.taskAccepted)) {
      this.document.instruction.taskAccepted = true;
      const diff: any[] = compare({document: this.copyDocument.min()}, {document: this.document.min()});
      this.instructionRestService.patch(this.document.documentId, <any>diff).subscribe(() => {
        this.copyDocument = angular.copy(this.document);
      });
    }
  }

  copy() {
    if (this.document.parentId) {
      this.instructionModalService.copy().subscribe(result => {
        this.$state.go('app.instruction.instruction-copy', {
          copiedId: this.document.documentId,
          asSubTask: (result === 'subTask')
        });
      });
    } else {
      this.$state.go('app.instruction.instruction-copy', {copiedId: this.document.documentId, asSubTask: false});
    }
  }

  accept() {
    if (this.memoInstructions.length == 1 && this.memoInstructions[0].documentId === this.document.documentId) {
      this.alertCloseMemo();
    } else {
      this.finishTask(true);
    }
  }

  finishTaskObservable(result: boolean): Observable<any> {
    return Observable.create(obs => {
      if (this.isValid(result)) {
        this.result.acceptStatus = (result) ? 'Принято' : 'Отклонено';
        if (result && !this.document.instruction.repeated.repeatedSign) {
          this.document.instruction.factDate = new Date();
        }
        let status;
        if (this.document.instruction.repeated.repeatedSign && result) {
          status = 'inwork';
        } else if (!this.document.instruction.repeated.repeatedSign){
          status = (result) ? 'closed' : 'inwork';
        }
        if (status) {
          this.instructionDocumentService.updateDocumentStatus(this.document, this.dicts.statuses, status);
        }
        this.result.reportAcceptor = this.updateReportAcceptor();
        this.result.acceptDate = new Date();
        if (this.document.instruction.repeated.repeatedSign) {
          const {planDateRepeat} = this.document.instruction.repeated;
          if (planDateRepeat.length === 1 && !planDateRepeat[0].reportAccepted) {
            planDateRepeat[0].planDate = this.document.instruction.planDate;
            planDateRepeat[0].numberRepeat = 1;
          }
        }
        this.fileService.syncExFiles(this.document.documentId, this.document.folderId, ids => {
          this.result.acceptFileId = _.filter(this.result.acceptFileId, id => !_.find(ids, i => i === id));
        }).pipe(
          mergeMap(response => {
            this.result.acceptFileId = response;
            this.instructionDocumentService.updatePrimaryDate(this.copyDocument, this.document);
            // this.instructionDocumentService.updatePrimaryDateChangedHistory(this.copyDocument, this.document);
            return this.instructionDocumentService.updateDocument(this.document.documentId, this.copyDocument, this.document);
          }), mergeMap(response => {
            return this.updateProcesses(this.copyDocument, this.document);
          }), mergeMap(response => {
            return this.updateTasks(this.copyDocument, this.document);
          }), mergeMap(response => {
            return this._finishTask(result);
          })
        ).subscribe(response => {
          obs.next(response);
          obs.complete();
          // window.location.href = '/main/#/app/tasks';
        }, error => {
          // console.log(error);
          this.blockUI.stop();
          obs.error(error);
          obs.complete();
        });
      } else {
        obs.error('При отказе поле с комментарием обязательно для заполнения!');
        obs.complete();
      }
    });
  }

  finishTask(result: boolean) {
    this.blockUI.start();
    this.finishTaskObservable(result).subscribe(res => {
      this.blockUI.stop();
      window.location.href = '/main/#/app/tasks';
    }, error => {
      this.toastr.error(error);
      this.blockUI.stop();
    });
  }

  cancel() {
    this.instructionModalService.cancel().subscribe((reason: any) => {
      if (reason.status != 'cancel') {
        this.instructionModalService.reCreate(reason, this.document.documentId).subscribe((reason: any) => {
          this.instructionDocumentService.getDeletedOrCanceledCandidates(this.document).pipe(
            mergeMap(documents => {
              let observables = _.map(documents, d => {
                return this.instructionDocumentService.cancelOrder(reason.reason, d, reason.comment, this.dicts);
              });
              return forkJoin(observables);
            })
          ).subscribe(response => {
            window.location.href = '/main/#/app/tasks';
          }, error => {
            this.toastr.error('Ошибка при снятии поручения!');
          });
        })
      } else {
        this.instructionDocumentService.getDeletedOrCanceledCandidates(this.document).pipe(
          mergeMap(documents => {
            let observables = _.map(documents, d => {
              return this.instructionDocumentService.cancelOrder(reason.reason, d, reason.comment, this.dicts);
            });
            return forkJoin(observables);
          })
        ).subscribe(response => {
          this.toastr.success('Поручение снято!');
          window.location.href = '/main/#/app/tasks';
        }, error => {
          this.toastr.error('Ошибка при снятии поручения!');
        });
      }
    }, error => {
    });
  }

  planDateMinGuard() {
    this.toastr.warning('Срок исполнения не может быть меньше даты поручения!');
  }

  planDateChange(planDate: Date) {
    if (this.document.instruction.planDate  && this.document.instruction.planDate.getHours() === 0 && this.document.instruction.planDate.getMinutes() === 0 && this.document.instruction.planDate.getMinutes() === 0) {
      const now = new Date();
      this.document.instruction.planDate.setHours(now.getHours(), now.getMinutes(), now.getSeconds(), now.getMilliseconds());
    }
  }

  private _finishTask(result: boolean): Observable<any> {
    return from(this.activityRestService.getFormData({taskId: this.task.id})).pipe(
      mergeMap(response => {
        let vars = (this.isAssignmentAccepted(response.formProperties)) ?
          [{name: 'AssignmentAccepted', value: result}] :
          [{name: 'DecisionVar', value: (result) ? 'Positive' : 'Negative'}];
        return this.activityRestService.finishTask(parseInt(this.task.id), vars);
      }), tap(response => {
        this.toastr.success('Задача успешно завершена!');
      }), catchError(error => {
        this.toastr.error('Ошибка при завершении задачи!');
        return throwError(error);
      })
    );
  }

  isAssignmentAccepted(properties: IActivityFormProperty[]): boolean {
    return _.find(properties, v => {
      return v.id === 'AssignmentAccepted';
    }) !== undefined;
  }

  getInstructionMka() {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.InstructionMKAID], type: this.solrMediator.types.instruction }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length) {
          this.instructionMka = docs[0];
          this.instructionMka.beginDate = this.instructionMka.beginDate
            ? this.instructionMka.beginDate.slice(0, -9)
            : this.instructionMka.beginDate;
        }
        return of([]);
      })
    );
  }

  getOrderMka() {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.OrderID], type: this.solrMediator.types.order }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length) { this.orderMka = docs[0]; }
        return of([]);
      })
    );
  }

  getMeetingMka() {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.AgendaID], type: this.solrMediator.types.instruction }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length) {
          const meeting = docs[0];
          const isExpress = this.dicts.meetingTypes
            .find(val => val.meetingType === meeting['meetingType'])['Express'];
          if (isExpress) {
            this.expressMeetingMka = meeting;
            this.meetingTypeExpressMeetingFull = this.dicts.meetingTypes
              .find(val => val.meetingType === this.expressMeetingMka['meetingType']).meetingFullName;
          }
          else {
            this.meetingMka = meeting;
            this.meetingTypeMeetingFull = this.dicts.meetingTypes
              .find(val => val.meetingType === this.meetingMka['meetingType']).meetingFullName;
          }
        }
        return of([]);
      })
    );
  }

  getProtocolMka() {
    return this.solrMediator.getByIds([
      { ids: [this.document.instruction.ProtocolID], type: this.solrMediator.types.protocol }
    ]).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length) { this.protocolMka = docs[0]; }
        return of([]);
      })
    );
  }

  private isValid(result: boolean): boolean {
    return (result || (!result && this.result.acceptContent.trim() !== ''));
  }

  private updateReportAcceptor(): DocumentUser {
    let user = new DocumentUser();
    user.login = this.session.login();
    user.fioFull = this.session.fullName();
    user.post = this.session.post();
    return user;
  }

  private updateProcesses(left: InstructionDocument, right: InstructionDocument): Observable<any> {
    let executor = this.instructionDocumentService.getExecutorLoginForUpdateBPM(left, right);
    let planDate = this.instructionDocumentService.getPlanDateForUpdateBPM(left, right);
    let processIds = this.instructionActivityService.getOpenedProcesses(this.init);
    return this.instructionActivityService.changeProcesses(processIds, executor, planDate);
  }

  private updateTasks(left: InstructionDocument, right: InstructionDocument): Observable<any> {
    let executor = this.instructionDocumentService.getExecutorLoginForUpdateBPM(left, right);
    let planDate = this.instructionDocumentService.getPlanDateForUpdateBPM(left, right);
    let taskIds = _.map(this.instructionActivityService.getOpenedTasks(this.init), id => {
      return parseInt(id);
    });
    return this.instructionActivityService.changeTasks(taskIds, executor, planDate);
  }

  private alertCloseMemo() {
    const options = {
      initialState: {
        docNumber: this.memo.number,
        docDate: formatDate(this.memo.beginDate, 'dd.MM.yyyy', 'en')
      },
      'class': 'modal-md'
    };
    const ref = this.modalService.show(CloseMemoConfirmModalComponent, options);
    let subscr = this.modalService.onHide.subscribe(
      () => {
        let component = <CloseMemoConfirmModalComponent> ref.content;
        if (component.submit) {
          this.blockUI.start();
          this.finishTaskObservable(true).subscribe(() => {
            this.finishReviewMemoTasks();
          });
        } else if (!component.cancelled) {
          this.finishTask(true);
        }
        subscr.unsubscribe();
      }
    );
  }

  finishReviewMemoTasks() {
    from(this.activityRestService.getTasksHistory({
      processVariables: [ {
        name: 'EntityIdVar',
        value: this.document.instruction.memoId,
        operation: 'equals'
      } ]
    })).pipe(
      mergeMap(res => {
        const task = res && res.data ? _.find(res.data, i => i.formKey === 'ReviewMemo' &&
          i.assignee === this.session.login() && !i.endTime) : null;
        if (task) {
          let copyMemo = angular.copy(this.memo);
          const result = new MemoDocumentResult();
          const reportExecutor = new DocumentUser();
          reportExecutor.login = this.session.login();
          reportExecutor.fioFull = this.session.fullName();
          reportExecutor.post = this.session.post();
          result.reportExecutor = reportExecutor;
          result.reportDate = new Date();
          result.processId = task.processInstanceId;
          result.reportButton = 'Ознакомлен';
          result.reportContent = this.result.acceptContent;
          this.memo.result.push(result);
          return this.memoDocumentService.updateDocument(this.memo.documentId, copyMemo, this.memo).pipe(
            mergeMap(() => this._finishReviewMemoTask(task.id, false, this.session.fullName()))
          );
        } else {
          return of('');
        }
      })
    ).subscribe(res => {
      window.location.href = '/main/#/app/tasks';
      this.blockUI.stop();
    }, error => {
      console.log(error);
      this.blockUI.stop();
    });
  }

  private _finishReviewMemoTask(taskId, isReply, fio): Observable<any> {
    // return from(this.activityRestService.getFormData({ taskId: this.task.id })).pipe(
    //   mergeMap(() => {
    return from(this.activityRestService.finishTask(parseInt(taskId, 10), [
      { name: 'IsReplyVar',   value: isReply },
      { name: 'ReplyUserVar', value: fio }
    ]));
    //   }), tap(() => {
    //     this.toastr.success('Задача успешно завершена!');
    //   }), catchError(error => {
    //     this.toastr.error('Ошибка при завершении задачи!');
    //     return throwError(error);
    //   })
    // );
  }

  exFilesChanged() {
    // this.fileService.ex.filesToUpload = this.exFiles.filter(i => !i.idFile);
    this.fileService.checkFiles2Rename();
  }

  getRepeatIndex(): number {
    return this.helper.getField('instruction.repeated.currentNumberRepeat', this.document);
  }

  getCoExecutors(): any[] {
    return (this.helper.getField('instruction.coExecutor', this.document) || [])
      .filter(i => _.isNumber(this.getRepeatIndex()) ? i.numberRepeat === this.getRepeatIndex() : true) ;
  }

  getForInfo(): any[] {
    return (this.helper.getField('forInformation', this.document) || [])
      .filter(i => _.isNumber(this.getRepeatIndex()) ? i.numberRepeat === this.getRepeatIndex() : true) ;
  }

}
