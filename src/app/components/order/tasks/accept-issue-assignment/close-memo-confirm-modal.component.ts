import {BsModalRef} from "ngx-bootstrap";
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'close-memo-confirm-modal',
  templateUrl: './close-memo-confirm-modal.component.html'
})
export class CloseMemoConfirmModalComponent implements OnInit {

  docNumber: string;
  docDate: string;
  submit: boolean = false;
  cancelled: boolean = false;

  constructor(private bsModalRef: BsModalRef) {
  }

  ngOnInit() {
  }

  ok() {
    this.submit = true;
    this.bsModalRef.hide();
  }

  no() {
    this.bsModalRef.hide();
  }

  cancel() {
    this.cancelled = true;
    this.bsModalRef.hide();
  }

}
