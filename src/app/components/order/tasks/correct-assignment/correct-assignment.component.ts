import {StateService, Transition} from '@uirouter/core';
import {ToastrService} from 'ngx-toastr';
import {Component} from '@angular/core';
import {IssueAssignmentComponent} from '../issue-assignment/issue-assignment.component';
import {ActiveTaskService, ActivityResourceService} from '@reinform-cdp/bpm-components';
import {OrderActivitiService} from '../../../../services/order-activiti.service';
import {InstructionRestService} from '../../../../services/instruction-rest.service';
import {OrderModalService} from '../../../../services/order-modal.service';
import {SessionStorage} from '@reinform-cdp/security';
import {OrderDocumentService} from '../../../../services/order-document.service';
import {FileService} from '../../../../services/file.service';
import {OrderDictsService} from '../../../../services/order-dicts.service';
import {HelperService} from '../../../../services/helper.service';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {  ExFileType } from '@reinform-cdp/widgets';
import {LinkRestService} from "../../../../services/link-rest.service";

@Component({
  selector: 'mggt-correct-assignment',
  templateUrl: './correct-assignment.component.html',
  styleUrls: ['./correct-assignment.component.scss']
})
export class CorrectAssignmentComponent extends IssueAssignmentComponent {
  selectedTab = 0;
  hideOvers = true;
  exFiles: ExFileType[] = [];
  constructor(fileService: FileService,
              activeTaskService: ActiveTaskService,
              instructionRestService: InstructionRestService,
              instructionDictsService: OrderDictsService,
              instructionDocumentService: OrderDocumentService,
              instructionActivityService: OrderActivitiService,
              instructionModalService: OrderModalService,
              toastr: ToastrService,
              $state: StateService,
              transition: Transition,
              session: SessionStorage,
              activityRestService: ActivityResourceService,
              solrMediator: SolrMediatorService,
              helper: HelperService,
              linkService: LinkRestService) {
    super(fileService, activeTaskService, instructionRestService, instructionDictsService, instructionDocumentService, instructionActivityService,
      instructionModalService, toastr, $state, transition, session, activityRestService, solrMediator, helper, linkService);
  }

  isActive(index: number): boolean {
    return index === this.selectedTab;
  }

  onTabSelected(index: number) {
    if (this.selectedTab === index) {
      this.hideOvers = !this.hideOvers;
    } else {
      this.hideOvers = true;
    }
    this.selectedTab = index;
  }
}
