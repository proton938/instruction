import {Transition} from '@uirouter/core';
import {Component, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {OrderActivitiService} from '../../../services/order-activiti.service';
import {InstructionRestService} from '../../../services/instruction-rest.service';
import {OrderDocumentService} from '../../../services/order-document.service';
import {FileService} from '../../../services/file.service';
import {OrderDictsService} from '../../../services/order-dicts.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {mergeMap, tap} from 'rxjs/internal/operators';
import {OrderBreadcrumbsService} from '../../../services/order-breadcrumbs.service';
import {AlertService} from '@reinform-cdp/widgets';
import {HelperService} from '../../../services/helper.service';
import {CreateOrderBaseComponent} from '../create-order-base-component';
import {ITask} from '@reinform-cdp/bpm-components';
import {Observable, of} from 'rxjs';
import {OrderNsiService} from '../../../services/order-nsi.service';
import {LinkRestService} from '../../../services/link-rest.service';

@Component({
  selector: 'mggt-create-instruction-from-order',
  templateUrl: './create-from-order.component.html'
})
export class CreateInstructionFromOrderComponent extends CreateOrderBaseComponent implements OnInit {

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;

  orderId: string;
  taskId: string;
  task: ITask;

  @BlockUI('newOrder') blockUI: NgBlockUI;

  constructor(instructionRestService: InstructionRestService,
              instructionDictsService: OrderDictsService,
              instructionActivityService: OrderActivitiService,
              fileService: FileService,
              documentService: OrderDocumentService,
              helper: HelperService,
              private alertService: AlertService,
              private breadcrumbsService: OrderBreadcrumbsService,
              private transition: Transition,
              private orderNsi: OrderNsiService,
              private linkService: LinkRestService) {
    super(instructionRestService, instructionDictsService, instructionActivityService, fileService, documentService, helper);
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    this.orderId = this.transition.params()['orderId'];
    this.taskId = this.transition.params()['taskId'];

    this.init().pipe(
      mergeMap(() => this.getTaskInfo()),
      mergeMap(() => this.orderNsi.getPlanWorkDate(3))
    ).subscribe(planDate => {
      this.breadcrumbsService.newOrder();
      this.model.instruction.planDate = new Date(planDate);
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  afterSave(): Observable<any> {
    return this.linkService.create({
      id2: this.document.documentId,
      id1: this.orderId,
      type1: 'SDO_ORDER_ORDER',
      type2: 'SDO_INSTRUCTION_INSTRUCTION',
      linkKind: {},
      subsystem1: 'SDO_ORDER',
      subsystem2: 'SDO_INSTRUCTION'
    });
  }

  create() {
    if (this.documentService.isValid(this.model)) {
      if (!this.isValidPlanDate()) {
        this.alertService.message({
          message: 'Время исполнения поручения не может быть меньше текущего',
          type: 'warning',
          size: 'md'
        });
        return;
      }
      this.blockUI.start();
      this.save().subscribe(() => {
        this.blockUI.stop();
      }, error => {
        console.log(error);
        this.helper.error(error);
        this.blockUI.stop();
      });
    } else {
      this.helper.warning(this.documentService.errorMsg || 'Не заполнены обязательные поля!');
    }
  }

  goBack() {
    let url = '';
    if (this.task) {
      const prefix = this.task.processDefinitionId.split('_')[0];
      url = `/sdo/order/#/app/execution/sdord/${this.taskId}/${prefix}${this.task.formKey}?systemCode=SDO`;
    } else {
      url = '/sdo/order/#/app/order/document/' + this.orderId;
    }
    window.open(url, '_self');
  }

  isValidPlanDate() {
    let currentDate = (new Date()).getTime();
    let selectedDate = (new Date(this.model.instruction.planDate)).getTime();
    return selectedDate > currentDate;
  }

  getTaskInfo(): Observable<any> {
    return this.taskId ? this.instructionActivityService.getTask(this.taskId).pipe(tap((taskRes: ITask) => {
      this.task = taskRes;
    })) : of('');
  }

}
