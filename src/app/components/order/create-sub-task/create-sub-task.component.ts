import * as _ from 'lodash';
import * as angular from 'angular';
import { StateService, Transition } from '@uirouter/core';
import { Component, OnInit } from '@angular/core';
import { LoadingStatus } from '@reinform-cdp/widgets';
import { InstructionDocument } from '../../../models/instruction-document/InstructionDocument';
import { InstructionDictsModel } from '../../../models/instruction/InstructionDictsModel';
import { FileService } from '../../../services/file.service';
import { OrderDictsService } from '../../../services/order-dicts.service';
import { OrderActivitiService } from '../../../services/order-activiti.service';
import { OrderDocumentService } from '../../../services/order-document.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { mergeMap, tap } from 'rxjs/internal/operators';
import {AlertService} from '@reinform-cdp/widgets';
import { OrderBreadcrumbsService } from '../../../services/order-breadcrumbs.service';
import {DocumentUser} from '../../../models/core/DocumentUser';
import {InstructionDocumentPriority} from '../../../models/instruction-document/InstructionDocumentPriority';
import {InstructionDocumentDifficulty} from '../../../models/instruction-document/InstructionDocumentDifficulty';
import {HelperService} from '../../../services/helper.service';

@Component({
  selector: 'mggt-create-sub-task',
  templateUrl: './create-sub-task.component.html'
})
export class CreateSubTaskComponent implements OnInit {

  extData: any = {};
  dicts: InstructionDictsModel;
  document: InstructionDocument;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;
  model: InstructionDocument;
  parentDocument: InstructionDocument;

  @BlockUI('newOrder') blockUI: NgBlockUI;

  constructor(private breadcrumbsService: OrderBreadcrumbsService,
              private instructionDictsService: OrderDictsService,
              private instructionActivityService: OrderActivitiService,
              private fileService: FileService,
              private documentService: OrderDocumentService,
              private $state: StateService,
              private alertService: AlertService,
              private transition: Transition,
              private helper: HelperService) {
    this.fileService.ex.filesToUpload = [];
    this.fileService.ex.files = [];
  }


  ngOnInit() {
    console.info('PARAMS: ', this.transition.params());
    this.instructionDictsService.getDicts(undefined, false).pipe(
      mergeMap(response => {
        this.dicts = response;
        return this.documentService.getParentDocument(this.transition.params()['parentId']);
      }),
      tap(response => {
        this.parentDocument = response;
        this.document = new InstructionDocument();
        this.document.parentId = this.parentDocument.documentId;
        this.document.instruction.subject = this.parentDocument.instruction.subject;
        this.initModel();
        this.breadcrumbsService.newOrder();
        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      })
    ).subscribe(() => {

    }, error => {
      this.loadingStatus = LoadingStatus.ERROR;
      console.log(error);
    });
  }

  initModel() {
    this.model = angular.copy(this.document);
    this.model.instruction.beginDate = new Date();
    this.model.instruction.assistant = new DocumentUser();
    this.model.instruction.assistant.updateFromUserBean(this.documentService.getCurrentUser());
    if (this.dicts.creators.length > 0) {
      this.model.instruction.creator = new DocumentUser();
      this.model.instruction.creator.updateFromUserBean(this.dicts.creators[0].creator);
    }
    this.model.instruction.priority = new InstructionDocumentPriority();
    this.model.instruction.priority.build(_.find(this.dicts.priorities, i => i.code === 'normal'));
    this.model.instruction.difficulty = new InstructionDocumentDifficulty();
    this.model.instruction.difficulty.build(_.find(this.dicts.difficulties, i => i.code === 'normal'));
  }

  prepareData() {
    this.document = this.model;
    this.documentService.updateDocumentStatus(this.document, this.dicts.statuses, 'assign');
    this.documentService.beforeSave(this.document);
  }

  create() {
    if (this.documentService.isValid(this.model)) {
      if (!this.isValidPlanDate()) {
        this.alertService.message({
          message: 'Время исполнения поручения не может быть меньше текущего',
          type: 'warning',
          size: 'md'
        });
        return;
      }
      this.blockUI.start();
      this.prepareData();
      this.documentService.createDocument(this.document).pipe(
        mergeMap(() => {
          return this.fileService.syncExFiles(this.document.documentId, this.document.folderId, r => {});
        }),
        mergeMap(response => {
          let document = angular.copy(this.document);
          this.document.instruction.fileId = response;
          return this.documentService.updateDocument(this.document.documentId, document, this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startProcess('sdoassign_issueAssignment_prc', this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startReviewProcess('sdoassign_startTakeIntoConsideration', this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startCoExecutionProcess('sdoassign_startTakeIntoCoexecution', this.document);
        }),
        mergeMap(() => {
          return this.documentService.sendMessage(this.dicts.executors, this.document);
        }),
        tap(() => {
          this.goBack();
          this.blockUI.stop();
        })
      ).subscribe(() => {

      }, error => {
        console.log(error);
        this.blockUI.stop();
        this.helper.error(error);
      });
    } else {
      this.helper.warning(this.documentService.errorMsg || 'Не заполнены обязательные поля!');
    }
  }

  goBack() {
    if (this.transition.params()['from'] === 'document') {
      this.$state.go('app.instruction.instruction-card', {
        id: this.document.documentId || this.parentDocument.documentId
      });
    } else {
      let processPrefix = this.transition.params()['processPrefix'];
      let taskId = this.transition.params()['taskId'];
      let formKey = this.transition.params()['formKey'];
      let systemCode = this.transition.params()['systemCode'];
      let url = `/sdo/instruction/#/app/execution/${processPrefix}/${taskId}/${formKey}?systemCode=${systemCode}`;
      window.open(url, '_self');
    }
  }

  isValidPlanDate() {
    let currentDate = (new Date()).getTime();
    let selectedDate = (new Date(this.model.instruction.planDate)).getTime();
    return selectedDate > currentDate;
  }
}
