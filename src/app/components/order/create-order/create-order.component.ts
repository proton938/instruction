import { Component, OnInit, OnDestroy } from '@angular/core';
import { StateService, Transition } from '@uirouter/core';
import { InstructionDictsModel } from '../../../models/instruction/InstructionDictsModel';
import { InstructionDocument } from '../../../models/instruction-document/InstructionDocument';
import { LoadingStatus } from '@reinform-cdp/widgets';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { OrderDictsService } from '../../../services/order-dicts.service';
import { OrderDocumentService } from '../../../services/order-document.service';
import { RouterStateWrapperService } from '@reinform-cdp/skeleton';
import { AlertService } from '@reinform-cdp/widgets';
import { catchError, mergeMap, tap } from 'rxjs/internal/operators';
import { throwError, of } from 'rxjs/index';
import * as angular from 'angular';
import { FileService } from '../../../services/file.service';
import { OrderActivitiService } from '../../../services/order-activiti.service';
import { OrderBreadcrumbsService } from '../../../services/order-breadcrumbs.service';
import * as _ from 'lodash';
import {copy} from 'angular';
import {OrderNsiService} from '../../../services/order-nsi.service';
import {InstructionDocumentDifficulty} from '../../../models/instruction-document/InstructionDocumentDifficulty';
import {InstructionDocumentPriority} from '../../../models/instruction-document/InstructionDocumentPriority';
import {DocumentUser} from '../../../models/core/DocumentUser';

@Component({
  selector: 'mggt-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss']
})
export class CreateOrderComponent implements OnInit, OnDestroy {

  extData: any = {};
  dicts: InstructionDictsModel;
  document: InstructionDocument = new InstructionDocument();
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;
  model: InstructionDocument;

  @BlockUI('newOrder') blockUI: NgBlockUI;

  constructor(private instructionDictsService: OrderDictsService,
              private instructionActivityService: OrderActivitiService,
              public $state: StateService,
              public transition: Transition,
              private documentService: OrderDocumentService,
              private breadcrumbsService: OrderBreadcrumbsService,
              private fileService: FileService,
              private routerStateWrapper: RouterStateWrapperService,
              private toastr: ToastrService,
              private alertService: AlertService,
              private orderNsi: OrderNsiService) {
    this.fileService.ex.filesToUpload = [];
    this.fileService.ex.files = [];
  }

  ngOnInit() {
    this.instructionDictsService.getDicts(undefined, false).subscribe(response => {
      this.dicts = response;
      this.model = copy(this.document);
      this.model.instruction.beginDate = new Date();
      this.model.instruction.assistant = new DocumentUser();
      this.model.instruction.assistant.updateFromUserBean(this.documentService.getCurrentUser());
      if (this.dicts.creators.length > 0) {
        this.model.instruction.creator = new DocumentUser();
        this.model.instruction.creator.updateFromUserBean(this.dicts.creators[0].creator);
      } else {
        this.loadingStatus = LoadingStatus.ERROR;
      }
      this.document = new InstructionDocument();
      this.breadcrumbsService.newOrder();
      this.model.instruction.difficulty = new InstructionDocumentDifficulty();
      this.model.instruction.difficulty.build(_.find(this.dicts.difficulties, d => d.code === 'normal'));

      // #MGGT-2121
      this.model.instruction.priority = new InstructionDocumentPriority();
      this.model.instruction.priority.build(_.find(this.dicts.priorities, d => d.code === 'normal'));
      this.orderNsi.getPlanWorkDate(3).subscribe(res => {
        this.model.instruction.planDate = new Date(res);
      });

      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, error => {
      this.loadingStatus = LoadingStatus.ERROR;
      this.success = false;
    });

    if (localStorage.getItem('linkCreateOrder') !== null) {
      let url = `/sdo/instruction/#/app/instruction/instruction/new`;
      window.open(url, '_self');
      localStorage.setItem('reloadCreateOrder', 'reload');
      localStorage.removeItem('linkCreateOrder');
    }
    if (localStorage.getItem('reloadCreateOrder') !== null) {
      localStorage.removeItem('reloadCreateOrder');
      location.reload();
    }
  }

  ngOnDestroy(): void {
    localStorage.setItem('reloadCreateOrder', 'reload');
  }

  prepareData() {
    this.document = this.model;
    this.documentService.updateDocumentStatus(this.document, this.dicts.statuses, 'draft');
    if (this.document.forInformation) {
      this.document.forInformation.forEach(i => i.numberRepeat = 1);
    }
    this.documentService.beforeSave(this.document);
  }

  create() {
    if (this.documentService.planDate !== null && this.documentService.repeatPeriodEndDate !== null) {
      if (this.documentService.planDate > this.documentService.repeatPeriodEndDate) {
        this.toastr.error('Дата окончания не может быть меньше срока исполнения поручения!');
        return;
      }
    }
    if (this.documentService.isValid(this.model)) {
      if (!this.isValidPlanDate()) {
        this.alertService.message({
          message: 'Время исполнения поручения не может быть меньше текущего',
          type: 'warning',
          size: 'md'
        });
        return;
      }
      this.blockUI.start();
      this.prepareData();
      this.documentService.createDocument(this.document).pipe(
        mergeMap(() => {
          return this.fileService.syncExFiles(this.document.documentId, this.document.folderId, ids => {
            this.document.instruction.fileId = _.filter(this.document.instruction.fileId, id => !_.find(ids, i => i === id));
          })
        }),
        mergeMap(response => {
          let document = angular.copy(this.document);
          this.document.instruction.fileId = response;
          return this.documentService.updateDocumentFiles(this.document.documentId, document, this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startProcess('sdoassign_issueAssignment_prc', this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startReviewProcess('sdoassign_startTakeIntoConsideration', this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startCoExecutionProcess('sdoassign_startTakeIntoCoexecution', this.document);
        }),
        mergeMap(() => {
          let document = angular.copy(this.document);
          this.documentService.updateDocumentStatus(this.document, this.dicts.statuses, 'assign');
          return this.documentService.updateDocument(this.document.documentId, document, this.document);
        }),
        mergeMap(() => {
          return this.documentService.sendMessage(this.dicts.executors, this.document);
        }),
        tap(() => {
          this.blockUI.stop();
          this.goBack();
        }),
        catchError(error => {
          console.log(error);
          this.blockUI.stop();
          return throwError(error);
        })
      ).subscribe()
    } else {
      this.toastr.warning(this.documentService.errorMsg || 'Не заполнены обязательные поля!');
    }
  }

  isValidPlanDate() {
    let currentDate = (new Date()).getTime();
    let selectedDate = (new Date(this.model.instruction.planDate)).getTime();
    return selectedDate > currentDate;
  }

  goBack() {
    const backUrl = this.transition.params()['backUrl'];
    let prevState = this.routerStateWrapper.getPrevState();
    if (backUrl) {
      window.location.href = backUrl;
    } else if (prevState.name) {
      this.$state.go(prevState.name, prevState.params);
    } else {
      window.location.href = '/main/#/app/tasks';
    }
  }

}
