import {Transition} from '@uirouter/core';
import {Component, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {OrderActivitiService} from '../../../services/order-activiti.service';
import {InstructionRestService} from '../../../services/instruction-rest.service';
import {OrderDocumentService} from '../../../services/order-document.service';
import {FileService} from '../../../services/file.service';
import {OrderDictsService} from '../../../services/order-dicts.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {OrderBreadcrumbsService} from '../../../services/order-breadcrumbs.service';
import {AlertService} from '@reinform-cdp/widgets';
import {HelperService} from '../../../services/helper.service';
import {CreateOrderBaseComponent} from '../create-order-base-component';

@Component({
  selector: 'mggt-create-order-from-meeting',
  templateUrl: './create-order-from-meeting.component.html'
})
export class CreateOrderFromMeetingComponent extends CreateOrderBaseComponent implements OnInit {

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;

  @BlockUI('newOrder') blockUI: NgBlockUI;

  constructor(instructionRestService: InstructionRestService,
              instructionDictsService: OrderDictsService,
              instructionActivityService: OrderActivitiService,
              fileService: FileService,
              documentService: OrderDocumentService,
              helper: HelperService,
              private alertService: AlertService,
              private breadcrumbsService: OrderBreadcrumbsService,
              private transition: Transition) {
    super(instructionRestService, instructionDictsService, instructionActivityService, fileService, documentService, helper);
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    this.init().subscribe(() => {
      this.breadcrumbsService.newOrder();
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  updateInfoAboutSource() {
    const params = this.transition.params();
    this.document.instruction.questionId = params['questionID'] || '';
    this.document.instruction.questionPrimaryID = params['questionPrimaryID'] || '';
    this.document.instruction.expressMeetingID = params['expressMeetingID'] || '';
  }

  create() {
    if (this.documentService.isValid(this.model)) {
      if (!this.isValidPlanDate()) {
        this.alertService.message({
          message: 'Время исполнения поручения не может быть меньше текущего',
          type: 'warning',
          size: 'md'
        });
        return;
      }
      this.blockUI.start();
      this.save().subscribe(() => {
        this.blockUI.stop();
      }, error => {
        console.log(error);
        this.helper.error(error);
        this.blockUI.stop();
      });
    } else {
      this.helper.warning(this.documentService.errorMsg || 'Не заполнены обязательные поля!');
    }
  }

  goBack() {
    let url = '';
    if (this.transition.params()['expressMeetingID']) {
      url = `/sdo/meeting/#/app/meeting/agenda/${this.transition.params()['expressMeetingID']}`;
    } else {
      url = `/sdo/meeting/#/app/meeting/question/${this.transition.params()['questionID']}`;
    }
    window.open(url, '_self');
  }

  isValidPlanDate() {
    let currentDate = (new Date()).getTime();
    let selectedDate = (new Date(this.model.instruction.planDate)).getTime();
    return selectedDate > currentDate;
  }

}
