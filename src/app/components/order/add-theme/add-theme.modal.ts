import {ToastrService} from 'ngx-toastr';
import {Component, OnInit} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {AlertService} from "@reinform-cdp/widgets";
import {catchError, map, mergeMap, tap} from "rxjs/internal/operators";
import {Observable} from "rxjs/Rx";
import {from, of, throwError} from "rxjs/index";
import {NSIInstructionTheme} from "../../../models/nsi/NSIInstructionTheme";
import {INsiSearchParams} from "@reinform-cdp/nsi-resource/dist/models/INsiSearchParams";

@Component({
  selector: 'instruction-modal-content',
  templateUrl: './add-theme.modal.html'
})
export class AddThemeModalComponent implements OnInit {

  theme: string = '';
  submit: boolean;

  constructor(private bsModalRef: BsModalRef, private toastr: ToastrService,
              private nsiRestService: NsiResourceService, private alertService: AlertService) {
  }

  ngOnInit() {
    this.submit = false;
  }

  addTheme(theme: string, themes: NSIInstructionTheme[]): Observable<string> {
    let findInExistingThemes = themes.find((th: any) => {
      return th.name === theme && (th.deleted === 0);
    });
    let findInDeletedThemes: any = themes.find((th: any) => {
      return th.name === theme.trim() && (th.deleted === 1);
    });
    let codes = themes.map(t => {
      return t.code;
    });
    let numbers: number[] = [];
    codes.forEach(c => {
      try {
        let number = parseInt(c.substr(4));
        numbers.push(number || 0);
      } catch (error) {

      }
    });

    let max = Math.max(...numbers);
    if (findInExistingThemes) {
      this.toastr.error('Указанная тема уже существует!');
      return of('');
    } else {
      if (findInDeletedThemes) {
        return from(this.alertService.confirm({
          okButtonText: "Восстановить тему",
          type: "warning",
          size: "md",
          windowClass: 'add-theme',
          message: "Указанная тема уже существует в статусе удаленной. Восстановить ее?"
        })).pipe(
          mergeMap(() => {
            return from(this.nsiRestService.addNewItemToDict({
              "ValueDict": {
                "NickDict": "InstructionTheme",
                "Element": {
                  "ElementId": findInDeletedThemes.id,
                  "Deleted": 0,
                  "SortValue": findInDeletedThemes.sortValue,
                  "ElementParent": null,
                  "Values": [{
                    "NickAttr": "code",
                    "Deleted": 0,
                    "Value": findInDeletedThemes.code
                  }, {
                    "NickAttr": "name",
                    "Deleted": 0,
                    "Value": findInDeletedThemes.name
                  }]
                }
              }
            }))
          }), map(response => {
            this.toastr.success('Указанная тема успешно востановлена!');
            return 'created';
          }), catchError(error => {
            this.toastr.error('Ошибка при восстановлении темы!');
            return throwError(error);
          })
        );
      } else {
        return from(this.nsiRestService.addNewItemToDict({
          "ValueDict": {
            "NickDict": "InstructionTheme",
            "Element": {
              "Deleted": 0,
              "SortValue": `order${max + 1}`,
              "ElementParent": null,
              "Values": [{
                "NickAttr": "code",
                "Deleted": 0,
                "Value": `соde${max + 1}`
              }, {
                "NickAttr": "name",
                "Deleted": 0,
                "Value": theme.trim()
              }]
            }
          }
        })).pipe(
          map(response => {
            this.toastr.success('Указанная тема успешно добавлена в словарь!');
            return 'created';
          }), catchError(error => {
            this.toastr.error('Ошибка при добавлении темы!');
            return throwError(error);
          })
        );
      }
    }
  }

  create() {
    if (this.theme.trim() === '') {
      this.toastr.warning(`Поле 'Тема' обязательно для заполнения!`);
    } else {
      from(this.nsiRestService.get('InstructionTheme', <INsiSearchParams>{deleted: true})).pipe(
        mergeMap(response => {
          let themes: NSIInstructionTheme[] = response;
          return this.addTheme(this.theme, themes);
        }),
        tap(response => {
          if (response === 'created') {
            this.submit = true;
            this.bsModalRef.hide();
          }
        })
      ).subscribe();
    }
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

}
