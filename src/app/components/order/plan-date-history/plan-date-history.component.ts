import { Component, Input, OnInit } from '@angular/core';
import { InstructionDocument } from "../../../models/instruction-document/InstructionDocument";
import { formatDate } from "@angular/common";
import { has } from 'lodash';

export class IPlanDateHistoryModel {
  date: Date;
  tooltipText: string;
}

@Component({
  selector: 'mggt-plan-date-history',
  templateUrl: './plan-date-history.component.html'
})
export class PlanDateHistoryComponent implements OnInit {

  @Input() document: InstructionDocument;
  items: IPlanDateHistoryModel[] = [];
  show: boolean;

  constructor() { }

  ngOnInit() {
    let dateTimeToString = (date) => {
      return formatDate(date, 'dd.MM.yyyy HH:mm:ss', 'en');
    };

    let dateToString = (date) => {
      return formatDate(date, 'dd.MM.yyyy', 'en');
    };

    this.document.instruction.planDateHistory.forEach((h, k) => {
      let newDate = this.document.instruction.planDate;

      if (k > 0 && has(this.document.instruction.planDateHistory, k + 1)) {
        newDate = this.document.instruction.planDateHistory[k + 1].planDateOld;
      }

      this.items.push({
        date: h.planDateOld,
        tooltipText: `c ${dateToString(h.planDateOld)} на ${dateToString(newDate)} (${h.user.fioFull}, ${dateTimeToString(h.changeDate)})`
      });
    });

    this.show = this.document.instruction.planDateHistory.length > 0;
  }

}
