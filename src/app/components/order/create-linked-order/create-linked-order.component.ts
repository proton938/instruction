import * as angular from 'angular';
import {StateService, Transition} from '@uirouter/core';
import {Component, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {InstructionDocument} from '../../../models/instruction-document/InstructionDocument';
import {InstructionDictsModel} from '../../../models/instruction/InstructionDictsModel';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {OrderDictsService} from '../../../services/order-dicts.service';
import {OrderActivitiService} from '../../../services/order-activiti.service';
import {FileService} from '../../../services/file.service';
import {OrderDocumentService} from '../../../services/order-document.service';
import {RouterStateWrapperService} from '@reinform-cdp/skeleton';
import {mergeMap} from 'rxjs/internal/operators';
import {AlertService} from '@reinform-cdp/widgets';
import {OrderBreadcrumbsService} from '../../../services/order-breadcrumbs.service';
import * as _ from 'lodash';
import {DocumentUser} from '../../../models/core/DocumentUser';
import {HelperService} from '../../../services/helper.service';

@Component({
  selector: 'mggt-create-linked-order',
  templateUrl: './create-linked-order.component.html'
})
export class CreateLinkedOrderComponent implements OnInit {

  extData: any = {};
  dicts: InstructionDictsModel;
  document: InstructionDocument;
  parentDocument: InstructionDocument;
  copiedDocument: InstructionDocument;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;
  model: InstructionDocument;

  @BlockUI('newOrder') blockUI: NgBlockUI;

  constructor(private instructionDictsService: OrderDictsService,
              private breadcrumbsService: OrderBreadcrumbsService,
              private instructionActivityService: OrderActivitiService,
              private fileService: FileService,
              private documentService: OrderDocumentService,
              private $state: StateService,
              private transition: Transition,
              private helper: HelperService,
              private alertService: AlertService,
              private routerStateWrapper: RouterStateWrapperService,) {
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    this.instructionDictsService.getDicts(undefined, false).pipe(
      mergeMap(response => {
        this.dicts = response;
        return this.documentService.getRecreatedDocument(this.transition.params()['linkedId']);
      }),
      mergeMap(response => {
        this.copiedDocument = response;
        this.document = new InstructionDocument();
        this.copyFields(this.copiedDocument);
        this.initModel();
        return this.fileService.getFilesFromRecreatedDocument((this.copiedDocument) ? this.copiedDocument.folderId : null,
          (this.copiedDocument) ? this.copiedDocument.instruction.fileId : []);
      }),
      mergeMap(response => {
        this.fileService.ex.filesToUpload = response;
        return this.documentService.getParentDocument(this.document.parentId);
      })
    ).subscribe(response => {
      this.parentDocument = response;
      this.breadcrumbsService.newOrder();
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, error => {
      this.loadingStatus = LoadingStatus.ERROR;
      this.helper.error(error);
    });
  }

  initModel() {
    this.model = angular.copy(this.document);
    this.model.instruction.beginDate = new Date();
    this.model.instruction.assistant = new DocumentUser();
    this.model.instruction.assistant.updateFromUserBean(this.documentService.getCurrentUser());
    if (this.dicts.creators.length > 0) {
      this.model.instruction.creator = new DocumentUser();
      this.model.instruction.creator.updateFromUserBean(this.dicts.creators[0].creator);
    }
  }

  private copyFields(document: InstructionDocument) {
    if (this.transition.params()['asSubTask'] === 'true') {
      this.document.parentId = document.parentId;
    }
    let user = this.documentService.getCurrentUser();
    this.document.instruction.assistant.updateFromUserBean(user);
    this.document.instruction.theme = document.instruction.theme;
    this.document.instruction.subject = document.instruction.subject;
    this.document.instruction.cadastral = document.instruction.cadastral;
    this.document.instruction.content = document.instruction.content;
    this.document.instruction.description = document.instruction.description;
    this.document.instruction.priority = document.instruction.priority;
    this.document.instruction.difficulty = document.instruction.difficulty;
    this.document.linkedId = document.documentId;
  }

  prepareData() {
    this.document = this.model;
    this.documentService.updateDocumentStatus(this.document, this.dicts.statuses, 'assign');
    this.documentService.beforeSave(this.document);
  }

  create() {
    if (this.documentService.isValid(this.model)) {
      if (!this.isValidPlanDate()) {
        this.alertService.message({
          message: 'Время исполнения поручения не может быть меньше текущего',
          type: 'warning',
          size: 'md'
        });
        return;
      }
      this.blockUI.start();
      this.prepareData();
      this.documentService.createDocument(this.document).pipe(
        mergeMap(() => {
          return this.fileService.syncExFiles(this.document.documentId, this.document.folderId, ids => {
            this.document.instruction.fileId = _.filter(this.document.instruction.fileId, id => !_.find(ids, i => i === id));
          })
        }),
        mergeMap(response => {
          let document = angular.copy(this.document);
          this.document.instruction.fileId = response;
          return this.documentService.updateDocument(this.document.documentId, document, this.document);
        }),
        mergeMap(() => {
            return this.instructionActivityService.startProcess('sdoassign_issueAssignment_prc', this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startReviewProcess('sdoassign_startTakeIntoConsideration', this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startCoExecutionProcess('sdoassign_startTakeIntoCoexecution', this.document);
        }),
        mergeMap(() => {
          return this.documentService.sendMessage(this.dicts.executors, this.document);
        })
      ).subscribe(() => {
        this.goBack();
        this.blockUI.stop();
      }, error => {
        console.log(error);
        this.helper.error(error);
        this.blockUI.stop();
      });
    } else {
      this.helper.warning(this.documentService.errorMsg || 'Не заполнены обязательные поля!');
    }
  }

  goBack() {
    let prevState = this.routerStateWrapper.getPrevState();
    if (prevState.name) {
      window.location.href = this.$state.href(prevState.name, prevState.params);
    } else {
      this.$state.go('app.instruction.instruction-card', {
        id: this.document.documentId || this.copiedDocument.documentId
      });
    }
  }

  isValidPlanDate() {
    let currentDate = (new Date()).getTime();
    let selectedDate = (new Date(this.model.instruction.planDate)).getTime();
    return selectedDate > currentDate;
  }

}
