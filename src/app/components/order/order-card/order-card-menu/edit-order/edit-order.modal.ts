import {BsModalRef} from 'ngx-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {Component, OnInit} from '@angular/core';
import * as _ from 'lodash';
import {copy} from 'angular';
import {MemoDocument} from '../../../../../models/memo-document/MemoDocument';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {InstructionDocument} from '../../../../../models/instruction-document/InstructionDocument';
import {InstructionDictsModel} from '../../../../../models/instruction/InstructionDictsModel';
import {OrderDictsService} from '../../../../../services/order-dicts.service';
import {catchError, mergeMap, tap} from 'rxjs/internal/operators';
import {throwError} from 'rxjs';
import {InstructionRestService} from '../../../../../services/instruction-rest.service';
import {OrderDocumentService} from '../../../../../services/order-document.service';
import {FileService} from '../../../../../services/file.service';
import {AlertService} from '@reinform-cdp/widgets';
import {sevenDaysInMs} from '../../../../../consts';

@Component({
  selector: 'instruction-modal-content',
  templateUrl: './edit-order.modal.html'
})
export class EditOrderModalComponent implements OnInit {

  id: string;
  extData: any = {};
  dicts: InstructionDictsModel;
  document: InstructionDocument;
  model: InstructionDocument;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;
  minDate: Date;
  memo: MemoDocument;

  submit: boolean = false;

  constructor(private instructionDictsService: OrderDictsService,
              private instructionRestService: InstructionRestService,
              private instructionDocumentService: OrderDocumentService,
              private toastr: ToastrService,
              private alertService: AlertService,
              private fileService: FileService,
              private bsModalRef: BsModalRef) {
    this.fileService.ex.filesToUpload = []
  }

  ngOnInit() {
    this.instructionDictsService.getDicts().pipe(
      mergeMap(response => {
        this.dicts = response;
        return this.instructionRestService.get(this.id);
      }), mergeMap(response => {
        let doc = new InstructionDocument();
        doc.build(response.document);
        this.document = doc;
        this.model = copy(this.document);
        this.calculateMinDate();
        this.fileService.updateInfoAboutFilesByIds(this.document.instruction.fileId);
        return this.instructionDocumentService.getMemo(this.document.instruction.memoId);
      }), tap(response => {
        this.memo = response;
        if (this.document.instruction.memoId) {
          this.extData.memoContent = this.memo.content;
          this.extData.memoView = this.document.instruction.memoView;
        }
        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      }),
      catchError(error => {
        this.loadingStatus = LoadingStatus.ERROR;
        return throwError(error);
      })
    ).subscribe();
  }

  ok() {
    if (this.instructionDocumentService.planDate !== null && this.instructionDocumentService.repeatPeriodEndDate !== null) {
      if (this.instructionDocumentService.planDate > this.instructionDocumentService.repeatPeriodEndDate) {
        this.toastr.error('Дата окончания не может быть меньше срока исполнения поручения!');
        return;
      }
    }

    if (this.instructionDocumentService.isValid(this.model)) {
      if (!this.isValidPlanDate()) {
        this.alertService.message({
          message: 'Время исполнения поручения не может быть меньше текущего',
          type: 'warning',
          size: 'md',
          windowClass: 'zindex'
        });
        return;
      }
      this.document = this.model;
      this.submit = true;
      this.bsModalRef.hide();
    } else {
      this.toastr.warning(this.instructionDocumentService.errorMsg);
    }
  }

  calculateMinDate() {
    let currentDate = (new Date()).setHours(0, 0, 0, 0).valueOf();
    const currentBeginDate = _.clone(this.document.instruction.beginDate);
    let beginDate = currentBeginDate.setHours(0, 0, 0, 0).valueOf();

    this.extData.minDate = (currentDate - beginDate > sevenDaysInMs)
      ? currentBeginDate
      : new Date(currentDate - sevenDaysInMs);
  }

  cancel() {
    this.bsModalRef.hide();
  }

  isValidPlanDate() {
    let currentDate = (new Date()).getTime();
    let selectedDate = (new Date(this.model.instruction.planDate)).getTime();
    return selectedDate > currentDate;
  }

}
