import {BsModalRef} from "ngx-bootstrap";
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'instruction-modal-content',
  templateUrl: './cancel-order.modal.html'
})
export class CancelOrderModalComponent implements OnInit {

  result: string = 'Отменено';
  comment: string = '';
  status: string;
  submit: boolean = false;

  constructor(private bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

  ok(status: string) {
    this.status = status;
    this.submit = true;
    this.bsModalRef.hide();
  }

  cancel() {
    this.bsModalRef.hide();
  }

}
