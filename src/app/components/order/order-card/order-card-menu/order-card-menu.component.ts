import {ToastrService} from "ngx-toastr";
import {StateService} from '@uirouter/core';
import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {IInfoAboutProcessInit} from "@reinform-cdp/bpm-components";
import {InstructionDictsModel} from "../../../../models/instruction/InstructionDictsModel";
import {InstructionDocument} from "../../../../models/instruction-document/InstructionDocument";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {OrderDocumentService} from "../../../../services/order-document.service";
import {OrderActivitiService} from "../../../../services/order-activiti.service";
import {Observable} from "rxjs/Rx";
import {catchError, mergeMap, tap} from "rxjs/internal/operators";
import {FileService} from "../../../../services/file.service";
import {OrderModalService} from "../../../../services/order-modal.service";
import {EMPTY, forkJoin} from "rxjs/index";
import * as angular from "angular";
import * as _ from "lodash";
import { InstructionRestService } from "../../../../services/instruction-rest.service";
import {LinkRestService} from "../../../../services/link-rest.service";

@Component({
  selector: 'mggt-order-card-menu',
  templateUrl: './order-card-menu.component.html'
})
export class OrderCardMenuComponent implements OnInit {

  @Input() document: InstructionDocument;
  @Input() dicts: InstructionDictsModel;
  @Input() initProcess: IInfoAboutProcessInit;

  @Output() onSave: EventEmitter<any> = new EventEmitter<any>();
  showSubTask: boolean;
  showCancel: boolean;
  showEdit: boolean;
  showCopyOrder: boolean;
  showLinked: boolean;

  @BlockUI('cardOrder') blockUI: NgBlockUI;

  constructor(public $state: StateService,
              private instructionDocumentService: OrderDocumentService,
              private instructionActivityService: OrderActivitiService,
              private instructionModalService: OrderModalService,
              private fileService: FileService,
              private toastr: ToastrService,
              private instructionRestService: InstructionRestService,
              private linkService: LinkRestService) {
  }

  ngOnInit() {
    this.showSubTask = this.instructionDocumentService.showSubTask(this.document, this.dicts.executors);
    this.showCancel = this.instructionDocumentService.showCancel(this.document);
    this.showEdit = this.instructionDocumentService.showEdit(this.document);
    this.showCopyOrder = this.instructionDocumentService.showCopyOrder(this.document, this.dicts.executors);
    this.showLinked = this.instructionDocumentService.showLinked(this.document, this.dicts.executors);
  }

  cancel() {
    this.instructionModalService.cancel().pipe(
      mergeMap((reason: any) => {
        if (reason.status != 'cancel') {
          return this.instructionModalService.reCreate(reason, this.document.documentId).pipe(
            mergeMap((r: any) => {
              return this.linkService.cloneLinks(this.document.documentId, r.documentId, ['SDO_ORDER_ORDER']);
            }), mergeMap(() => {
              return this.instructionDocumentService.getDeletedOrCanceledCandidates(this.document);
            }), mergeMap(documents => {
              let observables = _.map(documents, d => {
                return this.instructionDocumentService.cancelOrder(reason.reason, d, reason.comment, this.dicts);
              });
              return forkJoin(observables);
            }), tap(response => {
              this.$state.go('app.instruction.instruction-card', {id: this.document.documentId});
            })
          );
        } else {
          return this.instructionDocumentService.getDeletedOrCanceledCandidates(this.document).pipe(
            mergeMap(documents => {
              let observables = _.map(documents, d => {
                return this.instructionDocumentService.cancelOrder(reason.reason, d, reason.comment, this.dicts);
              });
              return forkJoin(observables);
            }), tap(response => {
              this.toastr.success('Поручение снято!');
            })
          );
        }
      })
    ).subscribe(() => {
    }, error => {
      if (error !== 'canceled') {
        this.toastr.error('Ошибка при снятии поручения!');
      }
    });
  }

  edit() {
    this.instructionModalService.edit(this.document.documentId).pipe(
      mergeMap(editedDocument => {
        this.blockUI.start();
        this.instructionDocumentService.updatePrimaryDate(this.document, editedDocument);
        // this.instructionDocumentService.updatePrimaryDateChangedHistory(this.document, editedDocument);
        return this.fileService.syncExFiles(this.document.documentId, this.document.folderId, ids => {
          editedDocument.instruction.fileId = _.filter(editedDocument.instruction.fileId, id => !_.find(ids, i => i === id));
        }).pipe(
          mergeMap(response => {
            Array.prototype.push.apply(editedDocument.instruction.fileId, response);
            return this.updateProcesses(this.document, editedDocument);
          }),
          mergeMap(response => {
            return this.updateTasks(this.document, editedDocument);
          }),
          mergeMap(response => {
            return this.instructionDocumentService.updateDocument(this.document.documentId, this.document, editedDocument);
          }),
          tap(response => {
            this.blockUI.stop();
            this.onSave.emit(true);
            // window.location.reload();
          }), catchError(error => {
            console.log(error);
            this.blockUI.stop();
            window.location.reload();
            return EMPTY;
          })
        )
      })
    ).subscribe()
  }

  editNew() {
    this.instructionModalService.editNew(this.document.documentId).pipe(
      mergeMap(editedDocument => {
        this.blockUI.start();
        this.instructionDocumentService.beforeSave(editedDocument);
        this.instructionDocumentService.updatePrimaryDate(this.document, editedDocument);
        // this.instructionDocumentService.updatePrimaryDateChangedHistory(this.document, editedDocument);
        return this.fileService.syncExFiles(this.document.documentId, this.document.folderId, ids => {
          editedDocument.instruction.fileId = _.filter(editedDocument.instruction.fileId, id => !_.find(ids, i => i === id));
        }).pipe(
          mergeMap(response => {
            Array.prototype.push.apply(editedDocument.instruction.fileId, response);
            return this.updateProcesses(this.document, editedDocument);
          }),
          mergeMap(response => {
            return this.updateTasks(this.document, editedDocument);
          }),
          mergeMap(response => {
            return this.instructionDocumentService.updateDocument(this.document.documentId, this.document, editedDocument);
          }),
          tap(response => {
            this.blockUI.stop();
            this.onSave.emit(true);
            // window.location.reload();
          }), catchError(error => {
            console.log(error);
            this.blockUI.stop();
            window.location.reload();
            return EMPTY;
          })
        )
      })
    ).subscribe()
  }



  delete() {
    this.instructionModalService.delete().pipe(
      mergeMap(response => {
        this.blockUI.start();
        return this.instructionDocumentService.getDeletedOrCanceledCandidates(this.document).pipe(
          mergeMap(response => {
            let observables = _.map(response, r => {
              return this.deleteOneInstruction(r.documentId, r.instruction.number, r.folderId);
            });
            return forkJoin(observables);
          }), tap(response => {
            this.$state.go('app.instruction.showcase-assignments');
          }), catchError(error => {
            console.log(error);
            this.blockUI.stop();
            this.$state.reload();
            return EMPTY;
          })
        );
      })
    ).subscribe();
  }

  copy() {
    if (this.document.parentId) {
      this.instructionModalService.copy().pipe(
        tap(result => {
          this.$state.go('app.instruction.instruction-copy', {
            copiedId: this.document.documentId,
            asSubTask: (result === 'subTask')
          });
        })
      ).subscribe();
    } else {
      this.$state.go('app.instruction.instruction-copy', {copiedId: this.document.documentId, asSubTask: false});
    }
  }

  private deleteOneInstruction(id: string, number: string, folderId: string): Observable<any> {
    return this.instructionDocumentService.deleteDocument(id, number).pipe(
      mergeMap(response => {
        return this.instructionDocumentService.removeLinkedId(id);
      }), mergeMap(response => {
        return this.instructionActivityService.getProcessHistoryById(id);
      }), mergeMap(response => {
        let openedProcesses: string[] = this.instructionActivityService.getOpenedProcesses(response);
        return this.instructionActivityService.deleteProcesses(openedProcesses);
      })
    );
  }

  private deleteFolder(folderId: string): Observable<void> {
    return Observable.create(obs => {
      this.fileService.deleteFolder(folderId).subscribe(res => {
        obs.next(res);
      }, error => {
        obs.next(error);
      }, () => {
        obs.complete();
      })
    });
  }

  private updateProcesses(left: InstructionDocument, right: InstructionDocument): Observable<any> {
    let executor = this.instructionDocumentService.getExecutorLoginForUpdateBPM(left, right);
    let planDate = this.instructionDocumentService.getPlanDateForUpdateBPM(left, right);
    let processIds = this.instructionActivityService.getOpenedProcesses(this.initProcess);
    return this.instructionActivityService.changeProcesses(processIds, executor, planDate);
  }

  private updateTasks(left: InstructionDocument, right: InstructionDocument): Observable<any> {
    let executor = this.instructionDocumentService.getExecutorLoginForUpdateBPM(left, right);
    let planDate = this.instructionDocumentService.getPlanDateForUpdateBPM(left, right);
    let taskIds = _.map(this.instructionActivityService.getOpenedTasks(this.initProcess), id => {
      return parseInt(id);
    });
    return this.instructionActivityService.changeTasks(taskIds, executor, planDate);
  }

  private _cancel(reason: string, document: InstructionDocument, comment: string): Observable<any> {
    let documentCopy = angular.copy(document);
    let status = (reason === 'Отменено') ? 'refused' : 'notExecuted';
    this.instructionDocumentService.updateDocumentStatus(document, this.dicts.statuses, status);
    document.instruction.factDate = new Date();
    document.removeBase = comment;
    return this.instructionDocumentService.updateDocument(document.documentId, documentCopy, document).pipe(
      mergeMap(response => {
        return this.instructionActivityService.getProcessHistoryById(document.documentId);
      }), mergeMap(response => {
        let openedProcesses: string[] = this.instructionActivityService.getOpenedProcesses(response);
        return this.instructionActivityService.deleteProcesses(openedProcesses);
      }), mergeMap(response => {
        return this.instructionDocumentService.sendNotify(document.documentId);
      })
    );
  }

}
