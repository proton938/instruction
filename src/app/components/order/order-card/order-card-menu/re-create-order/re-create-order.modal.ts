import {BsModalRef} from 'ngx-bootstrap';
import {Component, OnInit} from '@angular/core';
import {InstructionDocument} from '../../../../../models/instruction-document/InstructionDocument';
import * as _ from 'lodash';
import * as angular from 'angular';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {InstructionDictsModel} from '../../../../../models/instruction/InstructionDictsModel';
import {FileService} from '../../../../../services/file.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {OrderDictsService} from '../../../../../services/order-dicts.service';
import {OrderDocumentService} from '../../../../../services/order-document.service';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {mergeMap} from 'rxjs/internal/operators';
import {from, throwError} from 'rxjs';
import {AlertService} from '@reinform-cdp/widgets';
import {OrderActivitiService} from '../../../../../services/order-activiti.service';
import {OrderBreadcrumbsService} from '../../../../../services/order-breadcrumbs.service';
import {LinkRestService} from '../../../../../services/link-rest.service';
import {DocumentUser} from '../../../../../models/core/DocumentUser';
import {HelperService} from '../../../../../services/helper.service';

@Component({
  selector: 'instruction-modal-content',
  templateUrl: './re-create-order.modal.html'
})
export class ReCreateOrderModalComponent implements OnInit {

  documentId: string;
  extData: any = {};
  dicts: InstructionDictsModel;
  document: InstructionDocument;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  model: InstructionDocument;
  parentDocument: InstructionDocument;
  recreatedDocument: InstructionDocument;
  orders: any[] = [];

  submit: boolean = false;

  @BlockUI('newOrder') blockUI: NgBlockUI;

  constructor(private instructionDictsService: OrderDictsService,
              private fileService: FileService,
              private documentService: OrderDocumentService,
              private bsModalRef: BsModalRef,
              private helper: HelperService,
              private breadcrumbsService: OrderBreadcrumbsService,
              private fileHttpService: FileResourceService,
              private instructionActivityService: OrderActivitiService,
              private alertService: AlertService,
              private linkService: LinkRestService) {
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    this.linkService.findFromSolr(this.documentId, ['SDO_ORDER_ORDER']).subscribe(res => {
      if (res && res.length) { this.orders = res || []; }
    });
    this.instructionDictsService.getDicts().pipe(
      mergeMap(response => {
        this.dicts = response;
        return this.documentService.getRecreatedDocument(this.documentId);
      }),
      mergeMap(response => {
        this.recreatedDocument = response;
        this.document = new InstructionDocument();
        this.copyFields(this.recreatedDocument);
        this.initModel();
        //Получаем информацию о файла для отображения и которые надо будет скопировать
        return from(this.fileHttpService.getFullFolderInfo(this.recreatedDocument.folderId, false));
      }),
      mergeMap(response => {
        this.fileService.ex.files = response.files.map(f => this.fileService.convertToExFile(f));
        return this.documentService.getParentDocument(this.document.parentId);
      })
    ).subscribe(response => {
      this.parentDocument = response;
      this.breadcrumbsService.newOrder();
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, error => {
      this.loadingStatus = LoadingStatus.ERROR;
      this.helper.error(error);
    });
  }

  initModel() {
    this.model = angular.copy(this.document);
    this.model.instruction.beginDate = new Date();
    this.model.instruction.assistant = new DocumentUser();
    this.model.instruction.assistant.updateFromUserBean(this.documentService.getCurrentUser());
    if (this.dicts.creators.length > 0) {
      this.model.instruction.creator = new DocumentUser();
      this.model.instruction.creator.updateFromUserBean(this.dicts.creators[0].creator);
    }
  }

  private copyFields(document: InstructionDocument) {
    this.document.parentId = document.parentId;
    this.document.instruction.creator = document.instruction.creator;
    let user = this.documentService.getCurrentUser();
    this.document.instruction.assistant.updateFromUserBean(user);
    this.document.instruction.theme = document.instruction.theme;
    this.document.instruction.subject = document.instruction.subject;
    this.document.instruction.cadastral = document.instruction.cadastral;
    this.document.instruction.content = document.instruction.content;
    this.document.instruction.description = document.instruction.description;
    this.document.instruction.priority = document.instruction.priority;
    this.document.instruction.difficulty = document.instruction.difficulty;
    this.document.instruction.private = document.instruction.private;
    this.document.instruction.coExecutor = document.instruction.coExecutor;
    this.document.instruction.questionId = document.instruction.questionId;
    this.document.instruction.questionPrimaryID = document.instruction.questionPrimaryID;
    this.document.instruction.expressMeetingID = document.instruction.expressMeetingID;
    this.document.previousId = document.documentId;
  }

  prepareData() {
    this.document = this.model;
    this.documentService.updateDocumentStatus(this.document, this.dicts.statuses, 'assign');
    this.documentService.beforeSave(this.document);
  }

  create() {
    let ids = [];
    if (this.documentService.isValid(this.model)) {
      if (!this.isValidPlanDate()) {
        this.alertService.message({
          message: 'Время исполнения поручения не может быть меньше текущего',
          type: 'warning',
          size: 'md',
          windowClass: 'zindex'
        });
        return;
      }
      this.blockUI.start();
      this.prepareData();
      this.documentService.createDocument(this.document).pipe(
        mergeMap(() => {
          //Копирование файлов из родительского поручения
          let files = _.map(this.fileService.ex.files, f => f.idFile);
          return this.fileHttpService.copyFiles(files, this.document.folderId)
        }),
        mergeMap(response => {
          ids = _.map(response, f => {
            return f.versionSeriesGuid;
          });
          //Сохранение загруженных файлов
          return this.fileService.syncExFiles(this.document.documentId, this.document.folderId, _ => {}).pipe(
            mergeMap(response => {
              let document = angular.copy(this.document);
              //Сохранение списка файлов в документ
              this.document.instruction.fileId = ids.concat(response);
              return this.documentService.updateDocument(this.document.documentId, document, this.document);
            })
          );
        }),
        mergeMap(() => {
          return this.instructionActivityService.startProcess('sdoassign_issueAssignment_prc', this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startReviewProcess('sdoassign_startTakeIntoConsideration', this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startCoExecutionProcess('sdoassign_startTakeIntoCoexecution', this.document);
        }),
        mergeMap(() => {
          return this.documentService.sendMessage(this.dicts.executors, this.document);
        })
      ).subscribe(() => {
        this.blockUI.stop();
        this.submit = true;
        this.bsModalRef.hide();
      }, error => {
        console.log(error);
        this.blockUI.stop();
        return throwError(error);
      });
    } else {
      this.helper.warning(this.documentService.errorMsg || 'Не заполнены обязательные поля!');
    }
  }

  goBack() {
    this.bsModalRef.hide();
  }

  isValidPlanDate() {
    let currentDate = (new Date()).getTime();
    let selectedDate = (new Date(this.model.instruction.planDate)).getTime();
    return selectedDate > currentDate;
  }

}
