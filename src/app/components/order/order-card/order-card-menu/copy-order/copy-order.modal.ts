import {BsModalRef} from "ngx-bootstrap";
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'instruction-modal-content',
  templateUrl: './copy-order.modal.html'
})
export class CopyOrderModalComponent implements OnInit {

  status: string;
  submit: boolean;

  constructor(private bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

  ok(status: string) {
    this.status = status;
    this.submit = true;
    this.bsModalRef.hide();
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

}
