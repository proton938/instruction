import {ExFileType} from '@reinform-cdp/widgets';
import { PlatformConfig } from '@reinform-cdp/core';
import { ICdpBpmProcessManagerConfig } from '@reinform-cdp/bpm-components';
import { StateService, Transition } from '@uirouter/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SearchResultDocument } from '@reinform-cdp/search-resource';
import { IFileInFolderInfo } from '@reinform-cdp/file-resource';
import { InstructionDocument } from '../../../../models/instruction-document/InstructionDocument';
import { IInfoAboutProcessInit } from '@reinform-cdp/bpm-components';
import { MeetingQuestionProtocolPair } from '../../../../models/instruction/MeetingQuestionProtocolPair';
import { LoadingStatus } from '@reinform-cdp/widgets';
import { InstructionDictsModel } from '../../../../models/instruction/InstructionDictsModel';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { OrderDictsService } from '../../../../services/order-dicts.service';
import { OrderDocumentService } from '../../../../services/order-document.service';
import { OrderActivitiService } from '../../../../services/order-activiti.service';
import { FileService } from '../../../../services/file.service';
import { InstructionRestService } from '../../../../services/instruction-rest.service';
import { catchError, mergeMap, tap } from 'rxjs/internal/operators';
import { Observable, throwError, of } from 'rxjs';
import { MemoDocument } from '../../../../models/memo-document/MemoDocument';
import { HistoryModel } from '../../../../models/documents-log/HistoryModel';
import { OrderDocumentHistoryService } from '../../../../services/order-document-history.service';
import { OrderBreadcrumbsService } from '../../../../services/order-breadcrumbs.service';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {InstructionDocumentRepeated} from '../../../../models/instruction-document/InstructionDocumentRepeated';
import { formatDate } from '@angular/common';
import {LinkRestService} from "../../../../services/link-rest.service";
import * as _ from 'lodash';
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-order-desktop-card',
  templateUrl: './desktop.component.html',
  styleUrls: ['./desktop.component.scss']
})
export class OrderDesktopCardComponent implements OnInit, OnDestroy {

  rd: any[] = [];
  rdExpand: boolean = true;
  parentDocumentExpanded: boolean = true;
  meetingExpanded: boolean = true;
  admMeetingExpanded: boolean = true;
  memoExpanded: boolean = true;
  memoMKAExpanded: boolean = true;
  instructionExpanded: boolean = true;
  commentExpanded: boolean = true;
  resultExpanded: boolean = true;
  instructionMkaExpanded: boolean = true;
  orderMkaExpanded: boolean = true;
  agendaMkaExpanded: boolean = true;
  protocolMkaExpanded: boolean = true;
  expressMeetingMkaExpanded: boolean = true;
  isInstructionMKA: boolean = false;

  document: InstructionDocument;
  instructionMka: any;
  orderMka: any;
  agendaMka: any;
  protocolMka: any;
  expressMeetingMka: any;
  dicts: InstructionDictsModel;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;
  pairs: MeetingQuestionProtocolPair[] = [];
  init: IInfoAboutProcessInit;
  log: HistoryModel;
  subTasks: SearchResultDocument[];
  insideOrder: SearchResultDocument[];
  linked: SearchResultDocument[];
  questions: SearchResultDocument[];
  parentDocument: InstructionDocument;
  previousDocument: SearchResultDocument;
  files: ExFileType[];
  memo: MemoDocument;
  memoMKA: MemoDocument;
  meeting: SearchResultDocument[];
  //TODO: remove ' | any[]'
  meetingQuestion: SearchResultDocument[] | any[];
  meetingTypeAgendaFull: string;
  meetingTypeExpressMeetingFull: string;

  showLinked: boolean = false;

  processManagerConfig: ICdpBpmProcessManagerConfig;

  selectedTab = 0;
  @BlockUI('cardOrder') blockUI: NgBlockUI;

  constructor(public $state: StateService,
              private instructionDictsService: OrderDictsService,
              public instructionDocumentService: OrderDocumentService,
              private instructionActivityService: OrderActivitiService,
              private fileService: FileService,
              private instructionRestService: InstructionRestService,
              private instructionDocumentHistoryService: OrderDocumentHistoryService,
              private breadcrumbsService: OrderBreadcrumbsService,
              private transition: Transition,
              private platformConfig: PlatformConfig,
              private solrMediator: SolrMediatorService,
              private linkService: LinkRestService,
              private helper: HelperService) {}

  ngOnInit() {
    if (localStorage.getItem('linkCardOrder') !== null) {
      let url = `/sdo/instruction/#/app/instruction/instruction/card/` + localStorage.getItem('linkCardOrder');
      window.open(url, '_self');
      localStorage.setItem('reloadCardOrder', 'reload');
      localStorage.removeItem('linkCardOrder');
    }
    if (localStorage.getItem('reloadCardOrder') !== null) {
      localStorage.removeItem('reloadCardOrder');
      location.reload();
    }

    let id = this.transition.params()['id'];

    this.processManagerConfig = {
      sysName: this.platformConfig.systemCode.toLowerCase(),
      linkVarValue: id
    };

    this.linkService.findFromSolr(id, [this.solrMediator.types.order]).subscribe(res => {
      this.rd = res;
    }, error => {
      console.log(error);
    });

    this.reload();
  }

  ngOnDestroy(): void {
    localStorage.setItem('reloadCardOrder', 'reload');
  }

  reload(params?: any) {
    let id = this.transition.params()['id'];

    this.instructionDictsService.getDicts([
      'InstructionExecutors',
      'mggt_meeting_MeetingTypes'//
  ]).subscribe(response => {
      this.dicts = response;
      this.instructionRestService.get(id).subscribe(response => {
        this.document = new InstructionDocument();
        this.document.build(response.document);
        this.instructionActivityService.getProcessHistory(this.document).pipe(
          mergeMap(response => {
            this.init = response;
            return this.getFilesFromFolder(this.document.folderId);
          }), mergeMap(response => {
            this.files = this.fileService.getInfoAboutFilesByIds(this.document.instruction.fileId);
            return this.instructionDocumentHistoryService.updateInfoHistory(this.document.documentId);
          }), mergeMap(response => {
            this.log = this.instructionDocumentHistoryService.getModel();
            return this.instructionDocumentService.getSubTasks(this.document.documentId);
          }), mergeMap(response => {
            this.subTasks = response;
            return this.instructionDocumentService.getLinked(this.document);
          }), mergeMap(response => {
            this.linked = response;
            return this.instructionDocumentService.getQuestions(this.document);
          }), mergeMap(response => {
            this.questions = response;
            this.pairs = this.instructionDocumentService.buildPair(this.questions, this.dicts.meetingTypes);
            return this.instructionDocumentService.getParentDocument(this.document.parentId);
          }),
          mergeMap(response => {
            this.parentDocument = response;
            return this.document.receivedFromMKA.FromMKA ? this.instructionDocumentService.getMemoMKA(this.document.instruction.memoId) : this.instructionDocumentService.getMemo(this.document.instruction.memoId);
          }),
          mergeMap(response => {
            this.memo = response;
            return this.instructionDocumentService.getMemoMKA(this.document.instruction.memoMKAID);
          }),
          mergeMap(response => {
            this.memoMKA = response;
            return this.instructionDocumentService.getMeeting(this.document.instruction.expressMeetingID);
          }), mergeMap(response => {
            this.meeting = response;
            return this.instructionDocumentService.getInsideInstructions(this.document.documentId);
          }), mergeMap(response => {
            this.insideOrder = response;
            return this.instructionDocumentService.getMeetingQuestion(this.document.instruction.questionId);
          }), tap(response => {
            this.meetingQuestion = response;
            this.getPreviousDocument();
            this.showLinked = this.instructionDocumentService.showLinked(this.document, this.dicts.executors);
            if (this.document.receivedFromMKA.FromMKA) {
              this.isInstructionMKA = true;
            }
            else {
              this.getInstructionMka();
              this.getOrderMka();
              this.getAgendaMka();
              this.getProtocolMka();
              this.getExpressMeetingMka();
            }
            this.breadcrumbsService.orderCard(this.isInstructionMKA);
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.success = true;
            this.blockUI.stop();

          }), catchError(error => {
            this.loadingStatus = LoadingStatus.ERROR;

            return throwError(error);
          })
        ).subscribe();
      }, error => {
        this.loadingStatus = LoadingStatus.ERROR;
      });
    }, error => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  getFilesFromFolder(folderId: string): Observable<any> {
    return Observable.create(obs => {
      this.fileService
        .updateInfoAboutFilesFromFolder(folderId)
        .subscribe(res => {
          obs.next(res);
        }, error => {
          obs.next([]);
        }, () => {
          obs.complete();
        })
    });
  }


  getPreviousDocument() {
    if (this.document.previousId) {
      this.solrMediator.getByIds([
        { ids: [this.document.previousId], type: this.solrMediator.types.instruction }
      ]).subscribe(res => {
        const docs = this.solrMediator.getDocs(res);
        if (docs.length) {
          this.previousDocument = <SearchResultDocument>docs[0];
        }
      });
    }
  }

  getInstructionMka() {
    if (this.document.instruction.InstructionMKAID) {
      this.solrMediator.getByIds([
        { ids: [this.document.instruction.InstructionMKAID], type: this.solrMediator.types.instruction }
      ]).subscribe(res => {
        const docs = this.solrMediator.getDocs(res[0]);
        if (docs.length) {
          this.instructionMka = docs[0];
          this.instructionMka.beginDate = this.instructionMka.beginDate
            ? this.instructionMka.beginDate.slice(0, -9)
            : this.instructionMka.beginDate;
        }
      });
    }
  }

  getOrderMka() {
    if (this.document.instruction.OrderID) {
      this.solrMediator.getByIds([
        { ids: [this.document.instruction.OrderID], type: this.solrMediator.types.order }
      ]).subscribe(res => {
        const docs = this.solrMediator.getDocs(res[0]);
        if (docs.length) {
          this.orderMka = docs[0];
        }
      });
    }
  }

  getAgendaMka() {
    if (this.document.instruction.AgendaID) {
      this.solrMediator.getByIds([
        { ids: [this.document.instruction.AgendaID], type: this.solrMediator.types.agenda }
      ]).subscribe(res => {
        const docs = this.solrMediator.getDocs(res[0]);
        if (docs.length) {
          const isExpress = this.dicts.meetingTypes
            .find(val => val.meetingType === docs[0]['meetingType'])['Express'];
          if (!isExpress) {
            this.agendaMka = docs[0];
            this.meetingTypeAgendaFull = this.dicts.meetingTypes
              .find(val => val.meetingType === this.agendaMka.meetingType).meetingFullName;
          }
        }
      });
    }
  }

  getProtocolMka() {
    if (this.document.instruction.ProtocolID) {
      this.solrMediator.getByIds([
        { ids: [this.document.instruction.ProtocolID], type: this.solrMediator.types.protocol }
      ]).subscribe(res => {
        const docs = this.solrMediator.getDocs(res[0]);
        if (docs.length) {
          this.protocolMka = docs[0];
        }
      });
    }
  }

  getExpressMeetingMka() {
    if (this.document.instruction.AgendaID) {
      this.solrMediator.getByIds([
        { ids: [this.document.instruction.AgendaID], type: this.solrMediator.types.agenda }
      ]).subscribe(res => {
        const docs = this.solrMediator.getDocs(res[0]);
        if (docs.length) {
          const isExpress = this.dicts.meetingTypes
            .find(val => val.meetingType === docs[0]['meetingType'])['Express'];
          if (isExpress) {
            this.expressMeetingMka = docs[0];
            this.meetingTypeExpressMeetingFull = this.dicts.meetingTypes
              .find(val => val.meetingType === this.expressMeetingMka.meetingType).meetingFullName;
          }
        }
      });
    }
  }

  getRepeatPeriod(repeated: InstructionDocumentRepeated) {
    let r = '';
    const periodicity = repeated.periodicity;
    const weekday = repeated.weekday.length ? repeated.weekday.map(w => w.name).join(', ') : null;
    switch (repeated.periodRepeat.code) {
      case 'daily':
        r = `каждый ${periodicity} день` + (repeated.RepeatWorkingDay ? ', только рабочие дни' : '');
        break;
      case 'weekly':
        r = `каждую ${periodicity} неделю` + (weekday ? `; ${weekday}` : '');
        break;
      case 'monthly':
        r = `каждый ${periodicity} месяц` + (repeated.dayMonth ? `; ${repeated.dayMonth} числа` : '')
          + (weekday && repeated.numberWeek
            ? `; ${repeated.numberWeek.name} ${weekday}`.toLowerCase()
            : '');
        break;
      case 'yearly':
        const month = repeated.Month.length ? repeated.Month.map(w => w.name).join(', ') : null;
        r = `каждый ${periodicity} год`
          + (month ? `; ${month}` : '')
          + (repeated.dayMonth ? `; ${repeated.dayMonth} числа` : '')
          + (weekday && repeated.numberWeek
            ? `; ${repeated.numberWeek.name} ${weekday}`.toLowerCase()
            : '');
        break;
      default:
        r = '';
    }
    if (r && repeated.dateEndPeriodRepeat) {
      r += ' до ' + formatDate(repeated.dateEndPeriodRepeat, 'dd.MM.yyyy', 'en');
    }
    return r;
  }

  getRepeatIndex(): number {
    return this.helper.getField('instruction.repeated.currentNumberRepeat', this.document);
  }

  getCoExecutors(): any[] {
    return (this.helper.getField('instruction.coExecutor', this.document) || [])
      .filter(i => _.isNumber(this.getRepeatIndex()) ? i.numberRepeat === this.getRepeatIndex() : true);
  }

  getForInformation(): any[] {
    return (this.document.forInformation || [])
      .filter(i => _.isNumber(this.getRepeatIndex()) ? i.numberRepeat === this.getRepeatIndex() : true);
  }
}
