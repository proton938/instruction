import {Component, Input, OnInit} from '@angular/core';
import {IInstructionResultModel} from "../../../../models/instruction/IInstructionResultModel";
import {InstructionDocument} from "../../../../models/instruction-document/InstructionDocument";
import {FileService} from "../../../../services/file.service";
import * as angular from "angular"

@Component({
  selector: 'mggt-order-results',
  templateUrl: './order-results.component.html',
  styleUrls: ['./order-results.component.scss']
})
export class OrderResultsComponent implements OnInit {

  @Input() document: InstructionDocument;
  results: IInstructionResultModel[];

  constructor(private fileService: FileService) {
  }

  ngOnInit() {
    /*let filtered = angular.copy(this.document.result).filter(r => {
      return r.reportExecutor.login;
    });*/
    let filtered = angular.copy(this.document.result);
    this.results = filtered.map(r => {
      return {
        reportFiles: this.fileService.getInfoAboutFilesByIds(r.reportFileId),
        reportDate: r.reportDate,
        executorFIOFull: r.reportExecutor.fioFull,
        reportContent: r.reportContent,
        acceptDate: r.acceptDate,
        acceptStatus: r.acceptStatus,
        acceptorFIOFull: r.reportAcceptor.fioFull,
        acceptContent: r.acceptContent,
        acceptFiles: this.fileService.getInfoAboutFilesByIds(r.acceptFileId),
      };
    });
  }

}
