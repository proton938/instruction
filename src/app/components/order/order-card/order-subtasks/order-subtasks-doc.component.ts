import {Component, Input, OnInit} from '@angular/core';
import {InstructionDictsModel} from "../../../../models/instruction/InstructionDictsModel";
import {SearchResultDocument} from "@reinform-cdp/search-resource";

@Component({
  selector: 'mggt-order-subtasks-doc',
  templateUrl: './order-subtasks-doc.component.html',
  styleUrls: ['./order-subtasks.component.scss']
})
export class OrderSubtasksDocComponent implements OnInit {

  @Input() subTasks: SearchResultDocument[] | any[];
  @Input() dicts: InstructionDictsModel;

  statuses: { [key: string]: string } = {};
  priorities: { [key: string]: string } = {};

  constructor() {
  }

  ngOnInit() {
    this.dicts.statuses.forEach(st => this.statuses[st.name] = st.color);
    this.dicts.priorities.forEach(pr => this.priorities[pr.name] = pr.color);
  }

}
