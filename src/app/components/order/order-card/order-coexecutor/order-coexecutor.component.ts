import * as _ from 'lodash';
import {Component, Inject, Input, OnInit} from '@angular/core';
import {ExFileType} from '@reinform-cdp/widgets';
import {FileService} from '../../../../services/file.service';
import {CoExecutor} from '../../../../models/instruction/CoExecutorModel';
import {formatDate} from '@angular/common';

@Component({
  selector: 'mggt-order-coexecutor',
  templateUrl: './order-coexecutor.component.html'
})
export class OrderCoexecutorComponent implements OnInit {

  @Input() document;

  coExecutor: CoExecutor[] = [];
  coExecutorGroups: any[] = [];
  files: _.Dictionary<ExFileType[]> = {};

  constructor(private fileService: FileService,
              @Inject('$localStorage') public $localStorage: any) {}

  ngOnInit() {
    this.coExecutor = this.document.instruction.coExecutor || [];
    this.coExecutor.forEach(coEx => {
      this.files[coEx.reviewBy.login + (this.isRepeated ? coEx.numberRepeat : '')] = coEx.fileId
        ? this.fileService.getInfoAboutFilesByIds(coEx.fileId)
        : [];
    });
    if (this.isRepeated) { this.coExecutorGroups = this.getRepeatGroups() }
  }

  getRepeatGroups() {
    return this.document.instruction.repeated.planDateRepeat.map(p => {
      return {
        title: formatDate(p.planDate, 'dd.MM.yyyy', 'en'),
        coExecutor: this.coExecutor.filter(i => i.numberRepeat === p.numberRepeat)
      };
    });
  }

  get isRepeated(): boolean {
    return !!this.document && !!this.document.instruction.repeated && !!this.document.instruction.repeated
      && this.document.instruction.repeated.repeatedSign;
  }

}
