import {Component, Inject, Input, OnInit} from '@angular/core';
import {formatDate} from '@angular/common';

@Component({
  selector: 'mggt-order-for-information',
  templateUrl: './order-for-information.component.html'
})
export class OrderForInformationComponent implements OnInit {

  @Input() document;

  forInformGroups: any[] = [];

  constructor(@Inject('$localStorage') public $localStorage: any) { }

  ngOnInit() {
    this.forInformGroups = this.getRepeatGroups();
  }

  getRepeatGroups() {
    return this.document.instruction.repeated.planDateRepeat.map(p => {
      return {
        title: formatDate(p.planDate, 'dd.MM.yyyy', 'en'),
        forInformation: this.document.forInformation.filter(i => i.numberRepeat === p.numberRepeat)
      };
    });
  }

  get isRepeated(): boolean {
    return !!this.document && !!this.document.instruction.repeated && !!this.document.instruction.repeated
      && this.document.instruction.repeated.repeatedSign;
  }


}
