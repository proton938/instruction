import {ToastrService} from "ngx-toastr";
import {BsModalRef} from "ngx-bootstrap";
import {Component, OnInit} from '@angular/core';
import {DocumentComment} from "../../../../models/core/DocumentComment";
import {DocumentUser} from "../../../../models/core/DocumentUser";
import {OrderDocumentService} from "../../../../services/order-document.service";

@Component({
  selector: 'instruction-modal-content',
  templateUrl: './add-comment.modal.html'
})
export class AddCommentModalComponent implements OnInit {

  text: string = '';
  newComment: DocumentComment;
  submit: boolean;

  constructor(private bsModalRef: BsModalRef, private instructionDocumentService: OrderDocumentService, private toastr: ToastrService) {
  }

  ngOnInit() {
  }

  ok() {
    if (this.text.trim() !== '') {
      this.newComment = new DocumentComment();
      this.newComment.commentDate = new Date();
      this.newComment.commentText = this.text;
      let currentUser = this.instructionDocumentService.getCurrentUser();
      this.newComment.commentUser = new DocumentUser();
      this.newComment.commentUser.login = currentUser.accountName;
      this.newComment.commentUser.fioFull = currentUser.displayName;
      this.newComment.commentUser.post = currentUser.post;
      this.submit = true;
      this.bsModalRef.hide();
    } else {
      this.toastr.warning('Не заполнены обязательные поля!');
    }
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

}
