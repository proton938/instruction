import {Component, Input, OnInit} from '@angular/core';
import {InstructionDocument} from "../../../../models/instruction-document/InstructionDocument";
import {DocumentComment} from "../../../../models/core/DocumentComment";
import {SessionStorage} from "@reinform-cdp/security";
import {AlertService} from "@reinform-cdp/widgets";
import {OrderDocumentService} from "../../../../services/order-document.service";
import {OrderModalService} from "../../../../services/order-modal.service";
import {StateService} from '@uirouter/core';
import {catchError, mergeMap, tap} from "rxjs/internal/operators";
import {EMPTY} from "rxjs/index";
import * as angular from "angular"

@Component({
  selector: 'mggt-order-comments',
  templateUrl: './order-comments.component.html'
})
export class OrderCommentsComponent implements OnInit {

  @Input() document: InstructionDocument;
  copyDocument: InstructionDocument;
  showAddBtn: boolean;

  constructor(private $state: StateService, private instructionDocumentService: OrderDocumentService, private alertService: AlertService,
              private instructionModalService: OrderModalService, private session: SessionStorage) {
  }

  ngOnInit() {
    this.copyDocument = angular.copy(this.document);
    this.showAddBtn = this._showAddBtn();
  }

  private _showAddBtn(): boolean {
    return ((this.document.instruction.creator.login === this.session.login() ||
      this.document.instruction.assistant.login === this.session.login() ||
      this.document.instruction.executor.login === this.session.login()) &&
      this.document.instruction.factDate === null &&
      (this.document.statusId.code === 'inwork' || this.document.statusId.code === 'check' || this.document.statusId.code === 'assign'));
  }

  showRemoveBtn(comment: DocumentComment): boolean {
    return (comment.commentUser.login === this.session.login())
  }

  add() {
    this.instructionModalService.addComment().pipe(
      mergeMap(response => {
        this.copyDocument.comment.push(response);
        return this.instructionDocumentService.updateDocument(this.document.documentId, this.document, this.copyDocument);
      }), tap(response => {
        window.location.reload();
      }), catchError(error => {
        return EMPTY;
      })
    ).subscribe();
  }

  remove(index: number) {
    this.alertService.confirm({
      okButtonText: 'Ок',
      message: 'Вы действительно хотите удалить данный комментарий?',
      type: 'warning',
      size: 'md'
    }).then(response => {
      this.copyDocument.comment.splice(index, 1);
      return this.instructionDocumentService.updateDocument(this.document.documentId, this.document, this.copyDocument).subscribe();
    }).then(response => {
      window.location.reload();
    }).catch(() => {
    });
  }

}
