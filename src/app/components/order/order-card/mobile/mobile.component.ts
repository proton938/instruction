import { PlatformConfig } from '@reinform-cdp/core';
import {StateService, Transition} from '@uirouter/core';
import {Component, OnInit} from '@angular/core';
import {OrderDesktopCardComponent} from '../desktop/desktop.component';
import {OrderDictsService} from '../../../../services/order-dicts.service';
import {FileService} from '../../../../services/file.service';
import {OrderActivitiService} from '../../../../services/order-activiti.service';
import {OrderDocumentService} from '../../../../services/order-document.service';
import {InstructionRestService} from '../../../../services/instruction-rest.service';
import {OrderDocumentHistoryService} from '../../../../services/order-document-history.service';
import {OrderBreadcrumbsService} from '../../../../services/order-breadcrumbs.service';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {LinkRestService} from "../../../../services/link-rest.service";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-order-mobile-card',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.scss']
})
export class OrderMobileCardComponent extends OrderDesktopCardComponent implements OnInit {

  // Mobile start section
  selectedTab = 0;
  hideOvers = true;

  constructor($state: StateService,
              instructionDictsService: OrderDictsService,
              instructionDocumentService: OrderDocumentService,
              instructionActivityService: OrderActivitiService,
              fileService: FileService,
              instructionRestService: InstructionRestService,
              instructionDocumentHistoryService: OrderDocumentHistoryService,
              breadcrumbsService: OrderBreadcrumbsService,
              transition: Transition,
              platformConfig: PlatformConfig,
              solrMediator: SolrMediatorService,
              linkService: LinkRestService,
              helper: HelperService) {
    super($state, instructionDictsService, instructionDocumentService, instructionActivityService,
      fileService, instructionRestService, instructionDocumentHistoryService, breadcrumbsService,
      transition, platformConfig, solrMediator, linkService, helper);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  isActive(index: number): boolean {
    return index === this.selectedTab;
  }

  onTabSelected(index: number) {
    if (this.selectedTab === index) {
      this.hideOvers = !this.hideOvers;
    } else {
      this.hideOvers = true;
    }
    this.selectedTab = index;
  }

}
