import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'mggt-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.scss']
})
export class OrderCardComponent implements OnInit {

  constructor(@Inject('$localStorage') public $localStorage: any) {
  }

  ngOnInit() {
  }

}
