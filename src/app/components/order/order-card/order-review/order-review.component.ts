import * as angular from "angular";
import {Component, Input, OnInit} from '@angular/core';
import {DocumentUser} from "../../../../models/core/DocumentUser";

@Component({
  selector: 'mggt-order-review',
  templateUrl: './order-review.component.html'
})
export class OrderReviewComponent implements OnInit {

  @Input() document;

  reviewBy:DocumentUser[] = [];
  factReviewBy:DocumentUser[] = [];

  constructor() {
  }

  ngOnInit() {
    this.reviewBy = angular.copy(this.document.review).filter(f =>{
      return f.reviewBy && f.reviewBy.fioFull;
    });
    this.factReviewBy = angular.copy(this.document.review).filter(f => {
      return f.factReviewBy && f.factReviewBy.fioFull;
    });
  }


}
