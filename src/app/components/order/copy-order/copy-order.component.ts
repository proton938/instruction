import * as _ from 'lodash';
import * as angular from 'angular';
import {StateService, Transition} from '@uirouter/core';
import {ToastrService} from 'ngx-toastr';
import {Component, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {InstructionDocument} from '../../../models/instruction-document/InstructionDocument';
import {InstructionDictsModel} from '../../../models/instruction/InstructionDictsModel';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {OrderDictsService} from '../../../services/order-dicts.service';
import {catchError, mergeMap, tap} from 'rxjs/internal/operators';
import {OrderDocumentService} from '../../../services/order-document.service';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {AlertService} from '@reinform-cdp/widgets';
import {forkJoin, from, of, throwError} from 'rxjs';
import {FileService} from '../../../services/file.service';
import {RouterStateWrapperService} from '@reinform-cdp/skeleton';
import {OrderActivitiService} from '../../../services/order-activiti.service';
import {OrderBreadcrumbsService} from '../../../services/order-breadcrumbs.service';
import {LinkRestService} from '../../../services/link-rest.service';
import {DocumentUser} from '../../../models/core/DocumentUser';

@Component({
  selector: 'mggt-copy-order',
  templateUrl: './copy-order.component.html'
})
export class CopyOrderComponent implements OnInit {

  extData: any = {};
  dicts: InstructionDictsModel;
  document: InstructionDocument;
  parentDocument: InstructionDocument;
  copiedDocument: InstructionDocument;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;
  model: InstructionDocument;
  orders: any[] = [];

  @BlockUI('newOrder') blockUI: NgBlockUI;

  constructor(public $state: StateService,
              private routerStateWrapper: RouterStateWrapperService,
              private instructionDictsService: OrderDictsService,
              private documentService: OrderDocumentService,
              private fileHttpService: FileResourceService,
              private fileService: FileService,
              private breadcrumbsService: OrderBreadcrumbsService,
              private instructionActivityService: OrderActivitiService,
              private alertService: AlertService,
              private toastr: ToastrService,
              public transition: Transition,
              private linkService: LinkRestService) {
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    let copiedId = this.transition.params()['copiedId'];
    this.linkService.findFromSolr(copiedId, ['SDO_ORDER_ORDER']).subscribe(res => {
      if (res && res.length) { this.orders = res || []; }
    });
    this.instructionDictsService.getDicts(undefined, false).pipe(
      mergeMap(response => {
        this.dicts = response;
        return this.documentService.getRecreatedDocument(copiedId);
      }),
      mergeMap(response => {
        this.copiedDocument = response;
        this.document = new InstructionDocument();
        this.copyFields(this.copiedDocument);
        this.initModel();
        return from(this.fileHttpService.getFullFolderInfo(this.copiedDocument.folderId, false));
      }),
      mergeMap(response => {
        this.fileService.ex.files = response.files.filter(f => {
          return this.copiedDocument.instruction.fileId
            && _.some(this.copiedDocument.instruction.fileId, x => x === f.versionSeriesGuid)})
          .map(f => this.fileService.convertToExFile(f));
        return this.documentService.getParentDocument(this.document.parentId);
      }),
      tap(response => {
        this.parentDocument = response;
        this.breadcrumbsService.newOrder();
        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      }), catchError(error => {
        this.loadingStatus = LoadingStatus.ERROR;
        return throwError(error);
      })
    ).subscribe();
  }

  initModel() {
    this.model = angular.copy(this.document);
    this.model.instruction.beginDate = new Date();
    this.model.instruction.assistant = new DocumentUser();
    this.model.instruction.assistant.updateFromUserBean(this.documentService.getCurrentUser());
    if (this.dicts.creators.length > 0) {
      this.model.instruction.creator = new DocumentUser();
      this.model.instruction.creator.updateFromUserBean(this.dicts.creators[0].creator);
    }
  }

  private copyFields(document: InstructionDocument) {
    if (this.transition.params()['asSubTask'] === 'true') {
      this.document.parentId = document.parentId;
    }
    let user = this.documentService.getCurrentUser();
    this.document.instruction.assistant.updateFromUserBean(user);
    this.document.instruction.theme = document.instruction.theme;
    this.document.instruction.subject = document.instruction.subject;
    this.document.instruction.cadastral = document.instruction.cadastral;
    this.document.instruction.content = document.instruction.content;
    this.document.instruction.description = document.instruction.description;
    this.document.instruction.priority = document.instruction.priority;
    this.document.instruction.difficulty = document.instruction.difficulty;
  }

  prepareData() {
    this.document = this.model;
    this.documentService.updateDocumentStatus(this.document, this.dicts.statuses, 'assign');
    this.documentService.beforeSave(this.document);
  }

  create() {
    let ids = [];
    if (this.documentService.isValid(this.model)) {
      if (!this.isValidPlanDate()) {
        this.alertService.message({
          message: 'Время исполнения поручения не может быть меньше текущего',
          type: 'warning',
          size: 'md'
        });
        return;
      }
      this.blockUI.start();
      this.prepareData();
      this.documentService.createDocument(this.document).pipe(
        mergeMap(() => {
          //Копирование файлов из родительского поручения
          let files = _.map(this.fileService.ex.files, f => f.idFile);
          return this.fileHttpService.copyFiles(files, this.document.folderId);
        }),
        mergeMap(response => {
          ids = _.map(response, f => f.versionSeriesGuid);
          //Сохранение загруженных файлов
          return this.fileService.syncExFiles(this.document.documentId, this.document.folderId, _ => {}).pipe(
            mergeMap(response => {
              let document = angular.copy(this.document);
              //Сохранение списка файлов в документ
              this.document.instruction.fileId = ids.concat(response);
              const idWithParams = `${this.document.documentId}?sendNotificationOnAttachedFilesChange=false`;

              return this.documentService.updateDocument(idWithParams, document, this.document);
            })
          );
        }),
        mergeMap(() => {
          return this.instructionActivityService.startProcess('sdoassign_issueAssignment_prc', this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startReviewProcess('sdoassign_startTakeIntoConsideration', this.document);
        }),
        mergeMap(() => {
          return this.instructionActivityService.startCoExecutionProcess('sdoassign_startTakeIntoCoexecution', this.document);
        }),
        mergeMap(() => {
          return this.orders.length ? forkJoin(this.orders.map(o => {
            return this.linkService.create({
              id1: o.sys_documentId,
              id2: this.document.documentId,
              type1: 'SDO_ORDER_ORDER',
              type2: 'SDO_INSTRUCTION_INSTRUCTION',
              linkKind: {},
              subsystem1: 'SDO_ORDER',
              subsystem2: 'SDO_INSTRUCTION'
            });
          })) : of([]);
        }),
        mergeMap(() => {
          return this.documentService.sendMessage(this.dicts.executors, this.document);
        }),
        tap(() => {
          this.goToCard();
          this.blockUI.stop();
        }), catchError(error => {
          console.log(error);
          this.blockUI.stop();
          return throwError(error);
        })
      ).subscribe();
    } else {
      this.toastr.warning(this.documentService.errorMsg || 'Не заполнены обязательные поля!');
    }
  }

  goBack() {
    this.routerStateWrapper.getPrevState().goTo('app.instruction.instruction-card', {
      id: this.transition.params()['copiedId']
    });
  }

  private goToCard() {
    this.$state.go('app.instruction.instruction-card', {id: this.document.documentId});
  }

  isValidPlanDate() {
    let currentDate = (new Date()).getTime();
    let selectedDate = (new Date(this.model.instruction.planDate)).getTime();
    return selectedDate > currentDate;
  }

}
