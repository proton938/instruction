import * as angular from 'angular';
import {Transition, StateService} from '@uirouter/core';
import {Component, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {MemoDocument} from '../../../models/memo-document/MemoDocument';
import {MemoDictsModel} from '../../../models/memo/MemoDictsModel';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {MemoDocumentService} from '../../../services/memo-document.service';
import {MemoRestService} from '../../../services/memo-rest.service';
import {OrderDocumentService} from '../../../services/order-document.service';
import {FileService} from '../../../services/file.service';
import {MemoDictsService} from '../../../services/memo-dicts.service';
import {OrderActivitiService} from '../../../services/order-activiti.service';
import {OrderDictsService} from '../../../services/order-dicts.service';
import {InstructionRestService} from '../../../services/instruction-rest.service';
import {mergeMap} from 'rxjs/internal/operators';
import {OrderBreadcrumbsService} from '../../../services/order-breadcrumbs.service';
import {MemoMkaRestService} from '../../../services/memo-mka-rest.service';
import {AlertService} from '@reinform-cdp/widgets';
import {OrderNsiService} from '../../../services/order-nsi.service';
import {HelperService} from '../../../services/helper.service';
import {CreateOrderBaseComponent} from '../create-order-base-component';

@Component({
  selector: 'mggt-create-order-from-memo',
  templateUrl: './create-order-from-memo.component.html'
})
export class CreateOrderFromMemoComponent extends CreateOrderBaseComponent implements OnInit {

  memoDicts: MemoDictsModel;
  memo: MemoDocument;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;

  @BlockUI('newOrder') blockUI: NgBlockUI;

  constructor(instructionRestService: InstructionRestService,
              instructionDictsService: OrderDictsService,
              instructionActivityService: OrderActivitiService,
              fileService: FileService,
              documentService: OrderDocumentService,
              helper: HelperService,
              private breadcrumbsService: OrderBreadcrumbsService,
              private memoDictsService: MemoDictsService,
              private transition: Transition,
              private memoRestService: MemoRestService,
              private memoMkaResService: MemoMkaRestService,
              private memoDocumentService: MemoDocumentService,
              private alertService: AlertService,
              private $state: StateService,
              private orderNsi: OrderNsiService) {
    super(instructionRestService, instructionDictsService, instructionActivityService, fileService, documentService, helper);
    this.fileService.ex.filesToUpload = [];
  }

  ngOnInit() {
    this.init().pipe(
      mergeMap(() => {
        return this.memoDictsService.getDicts();
      }),
      mergeMap(dicts => {
        this.memoDicts = dicts;
        if (this.transition.params()['memoType'] === 'memo') {
          return this.memoRestService.get(this.transition.params()['memoId']);
        } else {
          return this.memoMkaResService.get(this.transition.params()['memoId']);
        }
      }),
      mergeMap(response => {
        this.memo = new MemoDocument();
        this.memo.build(response.document);
        this.model.instruction.memoId = this.memo.documentId;
        this.extData.memoContent = this.memo.content;
        this.extData.memoView = true;
        return this.orderNsi.getPlanWorkDate(3);
      })
    ).subscribe(res => {
      this.model.instruction.planDate = new Date(res);
      this.breadcrumbsService.newOrder();

      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  updateInfoAboutSource() {
    this.document.instruction.memoId = (this.transition.params()['memoId']) ? this.transition.params()['memoId'] : '';
  }

  create() {
    if (this.documentService.isValid(this.model)) {
      if (!this.isValidPlanDate()) {
        this.alertService.message({
          message: 'Время исполнения поручения не может быть меньше текущего',
          type: 'warning',
          size: 'md'
        });
        return;
      }
      this.blockUI.start();
      this.save().subscribe(() => {
        this.blockUI.stop();
      }, error => {
        console.log(error);
        this.helper.error(error);
        this.blockUI.stop();
      });
    } else {
      this.helper.warning(this.documentService.errorMsg || 'Не заполнены обязательные поля!');
    }
  }

  saveAdditionInformation() {
    let copyMemo = angular.copy(this.memo);
    this.memoDocumentService.updateDocumentStatus(this.memo, this.memoDicts.statuses, 'instruction');
    return this.memoDocumentService.updateDocument(this.memo.documentId, copyMemo, this.memo);
  }

  goBack() {
    (<any>window).location.href = '/sdo/instruction/' + this.$state.href(this.transition.from().name, this.transition.params('from'));
  }

  isValidPlanDate() {
    let currentDate = (new Date()).getTime();
    let selectedDate = (new Date(this.model.instruction.planDate)).getTime();
    return selectedDate > currentDate;
  }

}
