import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsLogComponent } from './documents-log.component';

describe('DocumentsLogComponent', () => {
  let component: DocumentsLogComponent;
  let fixture: ComponentFixture<DocumentsLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentsLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
