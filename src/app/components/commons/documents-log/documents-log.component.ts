import {Component, Input, OnInit} from '@angular/core';
import {HistoryModel} from "../../../models/documents-log/HistoryModel";
import {HistoryLogSorting} from "../../../models/documents-log/HistoryLogSorting";

@Component({
  selector: 'mggt-documents-log',
  templateUrl: './documents-log.component.html',
  styleUrls: ['./documents-log.component.scss']
})
export class DocumentsLogComponent implements OnInit {

  @Input() model: HistoryModel;
  operations: { name: string, value: string }[] = [
    {name: 'Изменено', value: 'replace'},
    {name: 'Добавлено', value: 'add'},
    {name: 'Удалено', value: 'remove'}
  ];

  constructor() {
  }

  ngOnInit() {
  }

  changeOrder(sorting: HistoryLogSorting) {
    HistoryLogSorting.updateSortings(sorting.name, this.model.sortings);
    if (!this.model.filter.isEmpty()) {
      this.model.logs = this.model.filter.run(this.model.allLogs);
    }
    this.model.logs = HistoryLogSorting.run(this.model.logs, this.model.sortings);
  }

  changeFilter() {
    this.model.logs = this.model.filter.run(this.model.allLogs);
    this.model.logs = HistoryLogSorting.run(this.model.logs, this.model.sortings);
  }

  changeFilterText() {
    setTimeout(() => {
      this.model.logs = this.model.filter.run(this.model.allLogs);
      this.model.logs = HistoryLogSorting.run(this.model.logs, this.model.sortings);
    }, 2000);
  }

}
