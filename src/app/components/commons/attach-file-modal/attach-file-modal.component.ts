import {Component, Input} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {ExFileType} from '@reinform-cdp/widgets';

@Component({
  selector: 'attach-file-modal',
  templateUrl: './attach-file-modal.component.html'
})
export class AttachFileModalComponent {
  @Input() title: string = 'Загрузка файла';
  @Input() okBtnText: string = 'Сохранить';
  @Input() folderId: string = '';
  @Input() fileType: string = 'default';
  @Input() fileTitle: string = 'Файл';
  @Input() extPattern: string = 'pdf';
  @Input() saveCallback: (file: ExFileType) => void;
  @Input() cancelCallback: (file: ExFileType) => void;

  files: ExFileType[] = [];

  constructor(private bsModalRef: BsModalRef,
              private fileService: FileResourceService) {}

  onRemoveFile(file) {
    this.fileService.deleteFile(file.idFile);
  }

  save() {
    if (this.saveCallback) this.saveCallback(this.getFile());
    this.bsModalRef.hide();
  }

  cancel() {
    if (this.cancelCallback) this.cancelCallback(this.getFile());
    this.bsModalRef.hide();
  }

  getFile(): ExFileType {
    if (!this.files || !this.files.length) {
      return null;
    }
    return this.files[0];
  }
}
