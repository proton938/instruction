import {Component, OnInit, Input} from '@angular/core';
import {InstructionApprovalHistory} from "../../../models/instruction/InstructionApprovalList";

@Component({
  selector: 'mggt-approval-list-history',
  templateUrl: './approval-list-history.component.html',
  styleUrls: ['./approval-list-history.component.css']
})
export class ApprovalListHistoryComponent implements OnInit {

  @Input() approvalHistory: InstructionApprovalHistory;
  cycleShow: { [key: string]: boolean } = {};

  constructor() {
  }

  ngOnInit() {
  }

}
