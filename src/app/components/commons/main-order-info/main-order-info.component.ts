import {Component, Input, OnInit} from '@angular/core';
import {InstructionDocument} from "../../../models/instruction-document/InstructionDocument";

@Component({
  selector: 'mggt-main-order-info',
  templateUrl: './main-order-info.component.html'
})
export class MainOrderInfoComponent implements OnInit {

  @Input() mainOrder: InstructionDocument;

  constructor() {
  }

  ngOnInit() {
  }

}
