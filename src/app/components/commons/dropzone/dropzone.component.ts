import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {DropzoneConfigInterface, DropzoneDirective} from "ngx-dropzone-wrapper";

@Component({
  selector: 'mggt-dropzone',
  templateUrl: './dropzone.component.html',
  styleUrls: ['./dropzone.component.scss']
})
export class DropzoneComponent implements OnInit {

  @ViewChild(DropzoneDirective) dropzone: DropzoneDirective;
  dropzoneConfig: DropzoneConfigInterface = {
    url: '/filestore/v1/files/file',
    autoProcessQueue: false,
    paramName: 'file',
    dictDefaultMessage: 'Загрузить файл',
    headers: {
      'Accept': 'text/plain'
    }
  };

  @Output() addedFile: EventEmitter<File> = new EventEmitter<File>();

  constructor() {
  }

  ngOnInit() {
  }

  add(file: File) {
    this.dropzone.dropzone().removeFile(file);
    this.addedFile.emit(file);
  }

}
