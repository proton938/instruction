import {Component, forwardRef, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CheckboxComponent),
    multi: true
  }]
})
export class CheckboxComponent implements ControlValueAccessor, OnChanges {
  @Input() color = 'green';
  @Input() trueValue = true;
  @Input() falseValue = false;

  value;
  disabled = false;
  onChange: (value) => {};
  onTouched: () => {};

  ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      if (propName === 'falseValue') {
        if (this.value !== this.trueValue) {
          this.value = this.falseValue;
          if (this.onChange) {
            this.onChange(this.value);
          }
        }
      } else if (propName === 'trueValue') {
        if (this.value !== this.falseValue) {
          this.value = this.trueValue;
          if (this.onChange) {
            this.onChange(this.value);
          }
        }
      }
    }
  }

  click() {
    if (!this.disabled) {
      this.value = (this.value === this.trueValue) ? this.falseValue : this.trueValue;
      if (this.onChange) {
        this.onChange(this.value);
      }
    }
  }

  writeValue(value): void {
    this.value = value;
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
