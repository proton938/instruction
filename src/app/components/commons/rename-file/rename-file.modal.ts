import {BsModalRef} from "ngx-bootstrap";
import {Component, OnInit} from '@angular/core';
import * as moment_ from "moment";

const moment = moment_;
@Component({
  selector: 'instruction-modal-content',
  templateUrl: './rename-file.modal.html'
})
export class RenameFileModalComponent implements OnInit {

  oldName: string;
  newName: string = '';
  extension: string;
  submit: boolean = false;

  constructor(private bsModalRef: BsModalRef) {
  }

  ngOnInit() {
    let arr = this.oldName.split('.');
    this.newName = arr[0] + '(1)';
    this.extension = arr[1];
  }

  rename() {
    this.submit = true;
    this.bsModalRef.hide();
  }

  cancel() {
    this.bsModalRef.hide();
  }

}
