import * as _ from "lodash";
import {UserBean} from "@reinform-cdp/nsi-resource";
import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {EditMemoModel} from '../../../models/memo/EditMemoModel';
import {User} from "../../../models/instruction/User";
import {MemoApproval, MemoApprovalListItem, MemoApprover} from '../../../models/memo-document/MemoApproval';
import {ApprovalType} from '../../../models/instruction/ApprovalType';

export class ApprovalListEditDicts {
  approvalTypes: ApprovalType[];
  approvers: UserBean[];
}

@Component({
  selector: 'mggt-approval-list-edit',
  templateUrl: './approval-list-edit.component.html',
  styleUrls: ['./approval-list-edit.component.scss']
})
export class ApprovalListEditComponent implements OnInit, OnChanges {

  @Input() model: EditMemoModel;
  @Input() signingElectronically: boolean;
  @Input() approvalTypes: ApprovalType[];
  @Input() approvers: UserBean[];
  @Input() approversSign: UserBean[];
  @Input() executive: UserBean;

  approvalApprovalType: ApprovalType;

  assentApprovers: MemoApprover[];
  assentApproversNotUsed: MemoApprover[];
  agreedApprovers: MemoApprover[];
  agreedApproversNotUsed: MemoApprover[];

  users: { [key: string]: User } = {};
  fixedApproval: MemoApprovalListItem = null;

  constructor() {
  }

  ngOnInit() {
    this.model.approval = this.model.approval || MemoApproval.create(1);
    let cycle = this.model.approval.approvalCycle;
    if (!cycle.approvalCycleDate) {
      cycle.approvalCycleDate = new Date().toISOString();
    }
    if (cycle.approvalCycleNum) {
      cycle.approvalCycleNum = '1';
    }
    if (!cycle.agreed) {
      cycle.agreed = [];
    }
    cycle.agreed.forEach((a) => {
      a.agreedBy = MemoApprover.processInstructionApprover(a.agreedBy);
    });

    this.approvers.forEach((userBean) => {
      this.users[userBean.accountName] = User.fromUserBean(userBean);
    });

    this.approvalApprovalType = this.approvalTypes.find(at => at.approvalTypeCode === 'approval');
    this.approvalTypes = this.approvalTypes.filter(at => at.approvalTypeCode !== 'approval');
    this.assentApprovers = this.approvers.map(MemoApprover.fromUserBean);
    this.agreedApprovers = _.intersectionBy(this.approvers, this.approversSign, (a) => (a.accountName)).map(MemoApprover.fromUserBean);

    this.executivesChanged();

    this.calcNotUsed();
    this.setNumerationAndSort();
  }

  ngOnChanges(changes: SimpleChanges) {
    let executiveChange = changes['executive'];
    if (executiveChange && !executiveChange.firstChange) {
      this.executivesChanged();
    }
    let signingElectronicallyChange = changes['signingElectronically'];
    if (signingElectronicallyChange && !signingElectronicallyChange.firstChange) {
      if (signingElectronicallyChange.currentValue) {
        const hasApproval = this.model.approval.approvalCycle.agreed.some((a) => {
          return a.approvalTypeCode === 'approval';
        });

        if (!hasApproval) {
          const agreed = new MemoApprovalListItem();
          if (this.executive) {
            agreed.agreedBy = MemoApprover.fromUserBean(this.executive);
          }
          agreed.approvalType = this.approvalApprovalType.approvalType;
          agreed.approvalTypeCode = this.approvalApprovalType.approvalTypeCode;

          this.personSetTime(agreed);

          this.model.approval.approvalCycle.agreed = this.model.approval.approvalCycle.agreed.filter((a) => (
            !a.agreedBy || !this.executive || a.agreedBy.accountName !== this.executive.accountName
          ));
          this.model.approval.approvalCycle.agreed.push(agreed);
        }
      } else {
        this.model.approval.approvalCycle.agreed = this.model.approval.approvalCycle.agreed.filter((a) => (a.approvalTypeCode !== 'approval'));
      }
    }
    this.calcNotUsed();
    this.setNumerationAndSort();
  }

  addApproval() {
    const agreed = new MemoApprovalListItem();
    agreed.approvalNum = (this.model.approval.approvalCycle.agreed.length + 1).toString();

    this.model.approval.approvalCycle.agreed.push(agreed);
    this.setNumerationAndSort();
  }

  executivesChanged() {
    if (this.executive) {
      this.users[this.executive.accountName] = User.fromUserBean(this.executive);
    }

    if (this.signingElectronically) {
      if (this.executive) {
        this.model.approval.approvalCycle.agreed = this.model.approval.approvalCycle.agreed.filter(a => {
          return a.approvalTypeCode !== 'approval' && !(a.agreedBy && a.agreedBy.accountName === this.executive.accountName)
        });

        const agreed = new MemoApprovalListItem();
        agreed.agreedBy = MemoApprover.fromUserBean(this.executive);
        agreed.approvalType = this.approvalApprovalType.approvalType;
        agreed.approvalTypeCode = this.approvalApprovalType.approvalTypeCode;
        this.personSetTime(agreed);

        this.model.approval.approvalCycle.agreed.push(agreed);
      } else {
        this.model.approval.approvalCycle.agreed.find(a => a.approvalTypeCode === 'approval').agreedBy = null;
      }
    }
  }

  calcNotUsed() {
    if (!this.model.approval || !this.model.approval.approvalCycle.agreed || !this.assentApprovers || !this.agreedApprovers) {
      return;
    }
    let agreed = this.model.approval.approvalCycle.agreed;
    const usedAccounts = agreed.filter((a) => (!!a.agreedBy)).map((a) => (a.agreedBy.accountName));
    this.assentApproversNotUsed = this.assentApprovers.filter((a) => (usedAccounts.indexOf(a.accountName) === -1));
    this.agreedApproversNotUsed = this.agreedApprovers.filter((a) => (usedAccounts.indexOf(a.accountName) === -1));
    if (this.executive) {
      let executivesNum = agreed.filter(a => a.agreedBy && a.agreedBy.accountName === this.executive.accountName).length;
      if (executivesNum < 2 && this.assentApproversNotUsed.findIndex(a => a.accountName === this.executive.accountName) < 0) {
        this.assentApproversNotUsed.push(MemoApprover.fromUserBean(this.executive));
      }
      if (executivesNum < 2 && this.agreedApproversNotUsed.findIndex(a => a.accountName === this.executive.accountName) < 0) {
        this.agreedApproversNotUsed.push(MemoApprover.fromUserBean(this.executive));
      }
    }
  }

  approverChanged(agreed: MemoApprovalListItem) {
    agreed.agreedBy = _.clone(agreed.agreedBy);
    this.personSetTime(agreed);
    this.calcNotUsed();
    this.setNumerationAndSort();
  }

  personSetTime(agreed: MemoApprovalListItem) {
    if (!agreed.agreedBy) {
      return;
    }

    const user = this.users[agreed.agreedBy.accountName];
    const department = user ? user.department : null;
    let approvalType = [...this.approvalTypes, this.approvalApprovalType].find(at => at.approvalTypeCode === agreed.approvalTypeCode && at.department === department);

    if (!approvalType) {
      approvalType = [...this.approvalTypes, this.approvalApprovalType].find(at => at.approvalTypeCode === agreed.approvalTypeCode && !at.department);
    }

    agreed.approvalTime = approvalType && approvalType.approvalDuration;
  }

  approvalTypeChanged(ind: number) {
    if (!this.model.approval.approvalCycle.agreed[ind].approvalTypeCode) {
      this.model.approval.approvalCycle.agreed[ind].approvalType = null;
      this.model.approval.approvalCycle.agreed[ind].approvalTypeCode = null;
      return;
    }

    const approvalType = _.find(this.approvalTypes, (at: any) => {
      return at.approvalTypeCode === this.model.approval.approvalCycle.agreed[ind].approvalTypeCode;
    });

    this.model.approval.approvalCycle.agreed[ind].approvalType = approvalType.approvalType;
    this.model.approval.approvalCycle.agreed[ind].approvalTypeCode = approvalType.approvalTypeCode;

    let validationList;

    if (approvalType.approvalTypeCode === 'assent') {
      validationList = this.assentApprovers;
    } else if (approvalType.approvalTypeCode === 'agreed') {
      validationList = this.agreedApprovers;
    }

    if (this.model.approval.approvalCycle.agreed[ind].agreedBy) {
      const exists = validationList.find((a) => a.accountName === this.model.approval.approvalCycle.agreed[ind].agreedBy.accountName);

      if (!exists) {
        this.model.approval.approvalCycle.agreed[ind].agreedBy = null;
      }
    }
  }

  setNumerationAndSort() {
    if (!this.model.approval || !this.model.approval.approvalCycle || !this.model.approval.approvalCycle.agreed) {
      return;
    }
    let agreed = this.model.approval.approvalCycle.agreed;
    let lastApproval: MemoApprovalListItem = null, rest: MemoApprovalListItem[];
    if (this.signingElectronically) {
      if (this.executive && this.approvalApprovalType) {
        lastApproval = agreed.find(a => a.agreedBy && a.agreedBy.accountName === this.executive.accountName &&
          a.approvalTypeCode === this.approvalApprovalType.approvalTypeCode);
      }
    } else {
      if (this.executive) {
        lastApproval = _.findLast(agreed, (a: MemoApprovalListItem) => a.agreedBy && a.agreedBy.accountName === this.executive.accountName);
      }
    }
    rest = agreed.filter(a => a !== lastApproval);
    _.each(rest, (item, index) => {
      item.approvalNum = '' + (index + 1);
    });
    if (lastApproval) {
      lastApproval.approvalNum = '' + (rest.length + 1);
    }

    this.model.approval.approvalCycle.agreed = agreed.sort((a, b) => {
      return +a.approvalNum > +b.approvalNum ? 1 : -1;
    });
    this.fixedApproval = lastApproval;
  }

  canSwap(ind1: number, ind2: number) {
    let agreed = this.model.approval.approvalCycle.agreed;
    return ind2 >= 0 && ind2 <= agreed.length - 1 && agreed[ind2] !== this.fixedApproval;
  }

  swapApprovals(ind1: number, ind2: number) {
    const agreed = this.model.approval.approvalCycle.agreed;
    const approver = agreed[ind1];
    agreed[ind1] = agreed[ind2];
    agreed[ind2] = approver;

    this.setNumerationAndSort();
  }

  delApproval(ind: number) {
    this.model.approval.approvalCycle.agreed.splice(ind, 1);
    this.calcNotUsed();
    this.setNumerationAndSort();
  }

}
