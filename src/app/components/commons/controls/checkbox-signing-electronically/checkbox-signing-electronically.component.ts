import {Component, OnInit, Input} from '@angular/core';
import {AlertService} from '@reinform-cdp/widgets';
import {MemoDocument} from '../../../../models/memo-document/MemoDocument';
import {EditMemoModel} from '../../../../models/memo/EditMemoModel';

@Component({
  selector: 'checkbox-signing-electronically',
  templateUrl: 'checkbox-signing-electronically.component.html'
})

export class CheckboxSigningElectronicallyComponent implements OnInit {
  isShow = true;
  isReady = false;
  oldValue = null;

  @Input() document: MemoDocument;
  @Input() model: EditMemoModel;
  @Input() name: string = 'signingElectronically';
  @Input() disabled: boolean;

  constructor(private alertService: AlertService) {}

  ngOnInit() {

  }

  change() {
    if (this.isReady) {
      this.isReady = false;
      const files = [...this.document.draftWithoutAtt, ...this.document.draftFiles];
      if (files.length) {
        this.alertService.confirm({
          okButtonText: 'Изменить',
          size: 'lg',
          message: 'Вы действительно хотите изменить тип подписания? В случае его изменения сформированные ранее файлы будут удалены',
          type: 'warning'
        }).then(() => {
          this.document.draftFiles = [];
          this.document.draftWithoutAtt = [];
        }).catch(e => {
          this.isShow = false;
          this.model.signingElectronically = this.oldValue;
          setTimeout(() => this.isShow = true, 0);
        });
      }
    }
  }

  switchReady(value: boolean) {
    if (value && !this.isReady) {
      this.isReady = value;
      this.oldValue = this.model.signingElectronically;
    } else if (!value) {
      this.isReady = value;
    }
  }
}
