import {Component, ElementRef, forwardRef, Input, OnInit, ViewChild} from '@angular/core';
import * as _ from 'lodash';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Observable} from 'rxjs';


@Component({
  selector: 'select-async',
  templateUrl: './select-async.component.html',
  styleUrls: ['./select-async.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SelectAsyncComponent),
    multi: true
  }]
})
export class SelectAsyncComponent implements OnInit, ControlValueAccessor {
  @Input() multiple = false;
  @Input() color = 'orange';
  @Input() valueField: string;
  @Input() textField: string;

  value;
  disabled = false;
  onChange: (value) => {};
  onTouched: () => {};

  @ViewChild('search') el: ElementRef;
  searchValue = '';
  searchedItems = [];
  selectedItems = [];
  loading = false;

  @Input() findAsync: (searchValue: string) => Observable<any[]>;
  @Input() textLength = 3;

  searchChanged() {
    this.searchedItems.length = 0;
    if (this.searchValue.trim().length >= this.textLength) {
      this.loading = true;
      this.findAsync(this.searchValue.trim()).subscribe(response => {
        this.searchedItems = response;
        this.loading = false;
      }, error => {
        this.loading = false;
      });
    }
  }

  changeItemSelection(item) {
    if (this.multiple) {
      this.selectedItems.push(item);
    } else {
      this.selectedItems.length = 0;
      this.selectedItems.push(item);
    }
    this.updateModel();
    this.outside();
  }

  writeValue(value): void {
    this.value = value;
    this.selectedItems.length = 0;
    if (this.multiple) {
      if ((this.value !== undefined) && (this.value !== null) && this.value.length && (this.value.length > 0)) {
        if (this.valueField && this.valueField === this.textField) {
          this.value.forEach(v => {
            const o = {};
            o[this.textField] = v;
            this.selectedItems.push(o);
          });
        } else {
          this.value.forEach(v => {
            this.selectedItems.push(this.value);
          });
        }
      }
    } else {
      if ((this.value !== undefined) && (this.value !== null)) {
        if (this.valueField && this.valueField === this.textField) {
          const o = {};
          o[this.textField] = this.value;
          this.selectedItems.push(o);
        } else {
          this.selectedItems.push(this.value);
        }
      }
    }
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  outside() {
    this.searchValue = '';
  }

  ngOnInit(): void {
  }

  clickOnItem($event) {
    this.el.nativeElement.focus();
  }

  selectItem($event, item) {
    this.el.nativeElement.focus();
  }

  onKeyTab($event) {
    if (!this.disabled) {
      if (this.searchValue === '' && (this.selectedItems.length > 0)) {
        this.selectedItems[this.selectedItems.length - 1].selected = false;
        this.selectedItems.splice(-1, 1);
        this.updateModel();
      }
    }
  }

  clickOnSelectionArea($event) {
    this.el.nativeElement.focus();
  }

  updateModel() {
    if (this.multiple) {
      this.value = [];
      this.selectedItems.forEach(i => {
        this.value.push(((this.valueField) ? i[this.valueField] : i));
      });
    } else {
      if (this.selectedItems.length === 1) {
        this.value = (this.valueField) ? this.selectedItems[0][this.valueField] : this.selectedItems[0];
      } else {
        this.value = undefined;
      }
    }
    if (this.onChange) {
      this.onChange(this.value);
    }
  }

  removeItem($event, selectedItem) {
    if (this.multiple) {
      _.remove(this.selectedItems, i => {
        return i === selectedItem;
      });
    } else {
      this.selectedItems.length = 0;
    }
    this.updateModel();
    this.el.nativeElement.focus();
  }
}
