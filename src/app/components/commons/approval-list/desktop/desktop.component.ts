import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';
import { InstructionApprovalList } from '../../../../models/instruction/InstructionApprovalList';
import { ApprovalType } from '../../../../models/instruction/ApprovalType';
import { FileResourceService } from '@reinform-cdp/file-resource';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { PlatformConfig } from '@reinform-cdp/core';
import { from } from 'rxjs/index';

@Component({
  selector: 'mggt-desktop-approval-list',
  templateUrl: './desktop.component.html'
})
export class DesktopApprovalListComponent implements OnInit {

  @Input() list: InstructionApprovalList;

  approvalTypes: { [key: string]: ApprovalType };

  constructor(private fileResourceService: FileResourceService,
    private nsiResourseService: NsiResourceService,
    private platformConfig: PlatformConfig) {
  }

  ngOnInit() {
    this.list.agreed = _.sortBy(this.list.agreed, (agreed) => agreed.approvalNum);
    from(this.nsiResourseService.get('mggt_order_ApprovalType')).subscribe((approvalTypes: ApprovalType[]) => {
      this.approvalTypes = {};
      _.each(approvalTypes, at => {
        this.approvalTypes[at.approvalTypeCode] = at;
      })
    });
  }

  getFileLink(id) {
    return `/filestore/v1/files/${id}?systemCode=${this.platformConfig.systemCode}`;
  }

  getApprovalResultColor(approvalTypeCode: string, approvalResult: string) {
    if (this.approvalTypes) {
      const approvalType = this.approvalTypes[approvalTypeCode];
      if (approvalType) {
        if (approvalResult === approvalType.buttonYes) {
          return 'label-' + approvalType.buttonYesColor;
        } else if (approvalResult === approvalType.buttonNo) {
          return 'label-' + approvalType.buttonNoColor;
        }
      }
    }
  }

}
