import {Component, Inject, Input, OnInit} from '@angular/core';
import {InstructionApprovalList} from '../../../models/instruction/InstructionApprovalList';

@Component({
  selector: 'mggt-approval-list',
  templateUrl: './approval-list.component.html'
})
export class ApprovalListComponent implements OnInit {

  @Input() list: InstructionApprovalList;

  constructor(@Inject('$localStorage') public $localStorage: any) { }

  ngOnInit() {
  }

}
