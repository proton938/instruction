import {Component, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => RadioComponent),
    multi: true
  }]
})
export class RadioComponent implements ControlValueAccessor {
  @Input() color = 'green';
  @Input() value;
  _value;
  disabled = false;
  onChange: (value) => {};
  onTouched: () => {};

  click() {
    if (!this.disabled && this._value !== this.value) {
      this._value = this.value;
      if (this.onChange) {
        this.onChange(this._value);
      }
    }
  }

  writeValue(value): void {
    this._value = value;
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
