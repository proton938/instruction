import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {SessionStorage} from '@reinform-cdp/security';
import {InstructionDocument} from '../../../models/instruction-document/InstructionDocument';
import * as _ from 'lodash';
import * as angular from 'angular';
import {formatDate} from '@angular/common';
import {compare} from 'fast-json-patch';
import {MemoActivitiService} from '../../../services/memo-activiti.service';
import {InstructionRestService} from '../../../services/instruction-rest.service';
import { map, tap, mergeMap, catchError } from 'rxjs/internal/operators';
import { ActivityResourceService } from '@reinform-cdp/bpm-components';
import {Observable, from, of, EMPTY} from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { StateService } from '@uirouter/core';
import {HelperService} from '../../../services/helper.service';
import {OrderDocumentService} from '../../../services/order-document.service';

@Component({
  selector: 'mggt-btn-revoke-execution',
  templateUrl: './btn-revoke-execution.component.html'
})
export class BtnRevokeExecutionComponent implements OnInit {

  @Input() model: InstructionDocument;
  @Input() source: string;
  @Output() finish: EventEmitter<any> = new EventEmitter();

  loading: boolean = true;
  revoking: boolean;
  isShow: boolean = false;
  isDisabled: boolean = true;

  tasks: any[] = [];

  constructor(private session: SessionStorage,
              private state: StateService,
              private toastr: ToastrService,
              private activityRestService: ActivityResourceService,
              private memoActivityService: MemoActivitiService,
              private instructionRestService: InstructionRestService,
              private documentService: OrderDocumentService,
              private helper: HelperService) {}

  ngOnInit() {
    if (this.model) {
      this.isShow = !this.model.receivedFromMKA || !this.model.receivedFromMKA.FromMKA;
      this.memoActivityService.getHistoricalProcesses(this.model.documentId).subscribe(tasks => {
        if (tasks && tasks.data && tasks.data.length) { this.tasks = tasks.data; }
        this.updateDisabledStatus();
        this.loading = false;
      });
    }
  }

  revoke() {
    this.revoking = true;
    let doc = new InstructionDocument(), old, diff;
    switch(this.source) {
      case 'order':
        this.instructionRestService.get(this.model.documentId).pipe(
          mergeMap(res => {
            // set instruction.recalled = true and save document
            doc.build(res.document);
            old = angular.copy(doc);
            doc.instruction.recalled = true;
            diff = this.getDiff(old, doc);
            return this.instructionRestService.patch(this.model.documentId, <any>diff);
          }),
          mergeMap(() => {
            // finish task AcceptIssueAssignment
            old = angular.copy(doc);
            let task = _.find(this.tasks, t => t.formKey === 'AcceptIssueAssignment' && !t.endTime);
            return task ? from(this.activityRestService.finishTask(+task.id)) : of(null);
          }),
          catchError(error => {
            // if finish task method returned an error - set instruction.recalled = false and save document again
            console.log(error);
            doc.instruction.recalled = false;
            diff = this.getDiff(old, doc);
            return this.instructionRestService.patch(this.model.documentId, <any>diff);
          })
        ).subscribe(() => {
          this.deferredStateReload();
          // TODO what action do you expect?
        }, error => {
          console.log(error);
          this.toastr.error(error);
        }, () => {
          this.revoking = false;
        });
        break;
      case 'coexecutors':
        this.instructionRestService.get(this.model.documentId).pipe(
          mergeMap(res => {
            doc.build(res.document);
            if (this.isRepeated()) { // Repeated instruction
              const doc = angular.copy(this.model);
              const coEx = _.first(this.getCoExecutors().filter(i => i.reviewBy.login === this.session.login()));
              delete coEx.bpmTaskId;
              return this.documentService.updateDocument(doc.documentId, doc, this.model).pipe(
                mergeMap(() => this.getProcessDefinition('sdoassign_restartTakeIntoCoexecution')),
                mergeMap(processDefId => {
                  return this.activityRestService.initProcess({
                    processDefinitionId: processDefId,
                    variables: [
                      {name: 'EntityIdVar', value: this.model.documentId},
                      {name: 'ChangeDate', value: formatDate(new Date(), 'yyyy-MM-ddTHH:mm:ss.SSS', 'en')}
                    ]
                  })
                })
              );
            } else { // NOT repeated instruction
              return (_.some(['inwork', 'assign'], i => i === this.model.statusId.code)
                ? this.getProcessDefinition('sdoassign_TakeIntoCoexecution') : EMPTY).pipe(
                  mergeMap(processDefId => {
                    return this.activityRestService.initProcess({
                      processDefinitionId: processDefId,
                      variables: [
                        {name: 'EntityIdVar', value: this.model.documentId},
                        {name: 'AssigneeVar', value: this.session.login()}
                      ]
                    });
                  })
              );
            }
          }),
          mergeMap(() => {
            return this.instructionRestService.recall(this.model.documentId, this.session.login())
          })
        ).subscribe(res => {
          this.deferredStateReload();
        }, error => {
          this.toastr.error(error);
        }, () => {
          this.revoking = false;
          // TODO close
        });
        break;
    }
  }

  getProcessDefinition(processKey: string): Observable<string> {
    return Observable.create(obs => {
      from(this.activityRestService.getProcessDefinitions({
        key: processKey,
        latest: true
      })).subscribe(res => {
        obs.next(res && res.data ? res.data[0].id : '');
        obs.complete();
      }, error => {
        obs.error(error);
        obs.complete();
      });
    });
  }

  deferredStateReload() {
    setTimeout(() => {
      this.state.go(this.state.current, {}, {reload: true});
    }, 1000);
  }

  updateDisabledStatus() {
    let enabled = this.checkEnabled();
    this.isDisabled = !enabled;
  }

  checkEnabled(): boolean {
    let r = true;
    let instruction = this.model.instruction;
    console.log('Check btn-broke-execution is started');
    switch (this.source) {
      case 'order':
        if (r) {
          r = !!this.model.statusId && this.model.statusId.code === 'check';
          if (!r) { console.log('Order: 1-st condition is false: statusId' +
            (this.model.statusId ? '.code = ' + this.model.statusId.code + ' (expected "check")' : ' is empty')); }
        }
        if (r) {
          r = !!instruction.planDate && instruction.planDate >= new Date();
          if (!r) { console.log('Order: 2-st condition is false: ' +
            (instruction.planDate ? 'planDate < current date' : 'planDate is empty')); }
        }
        if (r) {
          r = _.isNull(instruction.taskAccepted) || _.isUndefined(instruction.taskAccepted);
          if (!r) { console.log('Order: 3-st condition is false: taskAccepted is not null'); }
        }
        if (r) {
          r = !instruction.recalled;
          if (!r) { console.log('Order: 4-st condition is false: recalled is true'); }
        }
        if (r) {
          r = !!instruction.executor && instruction.executor.login === this.session.login();
          if (!r) { console.log('Order: 5-st condition is false: executor ' +
            (instruction.executor
              ? 'executor = ' + instruction.executor.login + ' (expected ' + this.session.login() + ')'
              : ' is empty')); }
        }
        break;
      case 'coexecutors':
        if (r) {
          r = this.model.statusId && _.some(['inwork', 'assign'], i => i === this.model.statusId.code);
          if (!r) { console.log('Order: 1-st condition is false: statusId' +
            (this.model.statusId
              ? '.code = ' + this.model.statusId.code + ' (expected "inwork" or "assign")'
              : ' is empty')); }
        }
        if (r) {
          r = _.some(instruction.coExecutor, coExecutor => {
            return _.some([
              coExecutor.reviewBy ? coExecutor.reviewBy.login : '',
              coExecutor.factReviewBy ? coExecutor.factReviewBy.login : ''
            ], login => {
              return login === this.session.login();
            });
          });
          if (!r) {
            console.log('Order: 2-st condition is false: no one coExecutor ' +
              'has reviewBy or factReviewBy == current user login');
          }
        }
        if (r) {
          r = _.every(this.tasks.filter(t => {
            return this.isRepeated()
              ? t.id === this.getCurrentTaskId()
              : t.formKey === 'TakeIntoCoexecution' && t.assignee === this.session.login();
          }), t => t.endTime);
          if (!r) {
            console.log('Order: 2-st condition is false: task TakeIntoCoexecution is not finished');
          }
        }
        break;
    }
    if (r) { console.log('Check is finished: successful'); }
    return r;
  }

  isRepeated(): boolean {
    return this.helper.getField('instruction.repeated.repeatedSign', this.model);
  }

  getCurrentTaskId(): string {
    let item: any = _.first(this.getCoExecutors().filter(i => i.reviewBy.login === this.session.login()));
    return item ? item.bpmTaskId : null;
  }

  getRepeatIndex(): number {
    return this.helper.getField('instruction.repeated.currentNumberRepeat', this.model);
  }

  getCoExecutors(): any[] {
    return (this.helper.getField('instruction.coExecutor', this.model) || [])
      .filter(i => _.isNumber(this.getRepeatIndex()) ? i.numberRepeat === this.getRepeatIndex() : true);
  }

  getDiff(old: InstructionDocument, next: InstructionDocument): any[] {
    return compare({document: old.min()}, {document: next.min()});
  }

}
