import {AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {fromEvent} from "rxjs/index";
import {debounceTime, distinctUntilChanged, filter, map} from "rxjs/internal/operators";

@Component({
  selector: 'mggt-mobile-select',
  templateUrl: './mobile-select.component.html',
  encapsulation: ViewEncapsulation.None
})
export class MobileSelectComponent implements AfterViewInit {

  @Input() model: any;
  @Input() name: string;
  selection: any;
  @Input() attr: string;
  @Input() items: any[];
  @Output() onInputChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output() modelChanged: EventEmitter<any> = new EventEmitter<any>();

  constructor(private element: ElementRef) {
  }

  ngAfterViewInit() {
    let dropdown = $(this.element.nativeElement).find('.dropdown');
    dropdown.on('shown.bs.dropdown', (event) => {
      let input = $(this.element.nativeElement).find('input');
      input.focus();
      const eventStream = fromEvent(input[0], 'keyup').pipe(
        map(() => this.selection),
        filter(input => input && input.length > 2),
        debounceTime(200),
        distinctUntilChanged()
      );
      eventStream.subscribe(input => this.onInputChanged.emit({search: input, users: this.items}));
    });
  }

  mobelChanded() {
    this.onInputChanged.emit({search: this.selection, users: this.items});
  }

  select(item) {
    this.model = item;
    this.modelChanged.emit(this.model);
  }

  clear() {
    this.selection = '';
    this.model = null;
    this.items.splice(0, this.items.length);
    this.modelChanged.emit(this.model);
  }

}
