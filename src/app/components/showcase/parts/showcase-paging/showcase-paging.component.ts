import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ShowcaseSettings} from '../../../../models/showcase/ShowcaseSettings';

@Component({
  selector: 'showcase-paging',
  templateUrl: './showcase-paging.component.html',
  styleUrls: [
    './showcase-paging.component.scss'
    ]
})
export class ShowcasePagingComponent {
  @Input() model: ShowcaseSettings;
  @Output() pageChanged = new EventEmitter();
  @Output() itemsPerPageChanged = new EventEmitter();

  onPageChanged($event) {
    this.model.pagination.currentPage = $event.page;
    this.pageChanged.emit();
  }

  onItemsPerPageChanged() {
    this.itemsPerPageChanged.emit();
  }
}
