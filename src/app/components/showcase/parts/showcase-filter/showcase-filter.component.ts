import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BsLocaleService} from 'ngx-bootstrap/datepicker';
import * as _ from 'lodash';
import {from, Observable} from 'rxjs';
import {ShowcaseFilter, ShowcaseSettings} from '../../../../models/showcase/ShowcaseSettings';
import * as moment_ from 'moment';
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import { ShowcaseMemoDesktopComponent } from "../../../memo/memo-showcase/desktop/desktop.component";

const moment = moment_;

@Component({
  selector: 'showcase-filter',
  templateUrl: './showcase-filter.component.html',
  styleUrls: [
    './showcase-filter.component.scss'
  ]
})
export class ShowcaseFilterComponent {
  @Input() model: ShowcaseSettings;
  @Output() changed = new EventEmitter();

  findUsers: (string) => Observable<any[]> = searchText => {
    return from(this.nsi.searchUsers({fio: searchText}));
  };

  constructor(private localeService: BsLocaleService, private nsi: NsiResourceService, public desctop: ShowcaseMemoDesktopComponent) {}

  ngOnInit(): void {
    this.localeService.use('ru');
    this.model.settings.filters.forEach(f => {
      if (f.field.type === 'date') {
        if (this.model.selectedFilters[f.field.code]) {
          this.model.selectedFilters[f.field.code][0] = moment(this.model.selectedFilters[f.field.code][0]).toDate();
          this.model.selectedFilters[f.field.code][1] = moment(this.model.selectedFilters[f.field.code][1]).toDate();
        }
      }
    });
  }

  onBooleanFilterChange(f) {
    this.model.selectedFilters[f.field.code] = (this.model.selectedFilters[f.field.code]) ? true : null;
    this.onFilterChange();
  }

  onDateFilterChange(f) {
    if (_.isArray(this.model.selectedFilters[f.field.code]) &&  this.model.selectedFilters[f.field.code].length === 1) {
      const newDate = [];
      newDate.push(this.model.selectedFilters[f.field.code][0]);
      newDate.push(this.model.selectedFilters[f.field.code][0]);
      this.model.selectedFilters[f.field.code] = newDate;
    }
    this.onFilterChange();
  }

  onFilterChange() {
    this.model.presetFilters();
    this.changed.emit();
  }

  clearFilters() {
    this.model.clearFilters(/*this.session.utility.getStorage().getItem('ngStorage-login')*/);
    this.changed.emit();
  }

  onFilterDictionaryChange(_filter: ShowcaseFilter) {
    if (_filter.usingDictionaryLinks) {
      const dictionary = _filter.field.dictionary;
      /*this.model.settings.dictionaryLinks.forEach(l => {
        if (l.dictionary.code === _filter.field.dictionary) {
          const f = _.find(this.model.settings.fields, field => { return (field.dictionary === l.linkedDictionary.code); });
          const ff = _.find(this.model.settings.filters, _f => { return (_f.field === f); });
          this.model.selectedFilters[f.code] = (ff.multiple) ? [] : null;
          this.fillDict(_filter, ff);
        }
      });*/
    }
    this.onFilterChange();
  }

  fillDict(source: ShowcaseFilter, destination: ShowcaseFilter) {
    if (source.multiple) {
      const values = this.model.selectedFilters[source.field.code];
      if (values.length > 0) {
        destination.dict = [];
        values.forEach(value => {
          const d = _.find(source.dict, _d => {
            return _d[source.field.dictionaryCode] === value;
          });
          d[destination.field.dictionary].forEach(dd => {
            destination.dict.push(dd);
          });
        });
      } else {
        destination.dict = this.model.dicts[destination.field.dictionary];
      }
    } else {
      const value = this.model.selectedFilters[source.field.code];
      if (value) {
        const d = _.find(source.dict, _d => {
          return _d[source.field.dictionaryCode] === value;
        });
        destination.dict = d[destination.field.dictionary];
      } else {
        destination.dict = this.model.dicts[destination.field.dictionary];
      }
    }
  }

  isDisabledField(fieldCode: string): boolean {
    let loginFromStorage = this.model.settings.filters.find(filter => {
      return filter.field.code == fieldCode
    }).loginFromStorage;
    let notEmpty =  this.model.settings.filters.find(filter => {
      return filter.field.code == fieldCode
    }).notEmpty;
    return !_.isEmpty(this.model.settings.defaultFilters[fieldCode]) || loginFromStorage || notEmpty;
  }
}
