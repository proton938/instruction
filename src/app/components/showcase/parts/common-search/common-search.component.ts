import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'common-search',
  templateUrl: './common-search.component.html',
  styleUrls: [
    './common-search.component.scss'
  ]
})
export class CommonSearchComponent {
  @Input() model: any;
  @Output() changed = new EventEmitter();

  onChanged() {
    this.changed.emit();
  }
}
