import {Component, Input} from "@angular/core";

@Component({
  selector: 'showcase-search-result-table-cell',
  templateUrl: './showcase-search-result-table-cell.component.html'
})
export class ShowcaseSearchResultTableCellComponent {
  @Input() item: any;
  @Input() template: string;
  @Input() dicts: any = {};
  @Input() url: string;

  templateType: string = '';

  constructor() {}

  ngOnInit() {
    this.templateType = this.getTemplateType();
  }

  getTemplateType(): string {
    let r = '';
    if (this.template && this.template.indexOf('Date') > 0) { return 'date'; }
    switch(this.template) {
      case 'statusMemo':
        r = 'label-dictionary';
        break;
      case 'kindMemo':
        r = 'dictionary';
        break;
      default:
        r = 'text';
    }
    return r;
  }
}
