import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ShowcaseFilter, ShowcaseSettings} from '../../../../models/showcase/ShowcaseSettings';
import * as _ from 'lodash';
import {from, Observable} from 'rxjs';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';

import {ShowcaseMemoDesktopComponent} from "../../../memo/memo-showcase/desktop/desktop.component";


@Component({
  selector: 'showcase-search-result-table',
  templateUrl: './showcase-search-result-table.component.html',
  styleUrls: [
    './showcase-search-result-table.component.scss'
  ]
})
export class ShowcaseSearchResultTableComponent {
  @Input() model: ShowcaseSettings;
  @Input() items;
  @Output() sortingChanged = new EventEmitter();
  @Output() filterChanged = new EventEmitter();

  constructor(private nsi: NsiResourceService, private desctop: ShowcaseMemoDesktopComponent) {
  }

  sortChanged(column: any) {
    if (this.model.settings.tableSorting && column.field && column.field.sortable) {
      this.model.settings.tableColumns.forEach((m, i) => {
        if (m.field && m.field.sortable) {
          if (m !== column) {
            m.field.defaultSorting = 'NONE';
          } else {
            column.field.defaultSorting = (column.field.defaultSorting === 'NONE') ?
              'DESC' :
              ((column.field.defaultSorting === 'DESC') ? 'ASC' : 'DESC');
          }
        }
      });
      this.sortingChanged.emit(column);
    }
  }

  onBooleanFilterChange(f) {
    this.model.selectedFilters[f.fieldCode] = !!this.model.selectedFilters[f.fieldCode] || null;
    this.onFilterChange();
  }

  onDateFilterChange(f) {
    if (_.isArray(this.model.selectedFilters[f.field.code]) && this.model.selectedFilters[f.field.code].length === 1) {
      const newDate = [];
      newDate.push(this.model.selectedFilters[f.field.code][0]);
      newDate.push(this.model.selectedFilters[f.field.code][0]);
      this.model.selectedFilters[f.field.code] = newDate;
    }
    this.onFilterChange();
  }

  onFilterChange() {
    this.filterChanged.emit();
  }

  onFilterDictionaryChange(filter: ShowcaseFilter) {
    /*if (filter.usingDictionaryLinks) {
      let dictionary = filter.field.dictionary;
      this.model.settings.dictionaryLinks.forEach(l => {
        if (l.dictionary.code === filter.field.dictionary) {
          let f = _.find(this.model.settings.fields, field => {
            return (field.dictionary === l.linkedDictionary.code);
          });

          let ff = _.find(this.model.settings.filters, _f => {
            return (_f.field === f);
          });

          this.model.selectedFilters[f.code] = (ff.multiple) ? [] : null;
          this.fillDict(filter, ff);
        }
      });
    }*/
    this.onFilterChange();
  }

  fillDict(source: any, destination: any) {
    /*if (source.multiple) {
      let values = this.model.selectedFilters[source.field.code];
      if (values.length > 0) {
        destination.dict = [];
        values.forEach(value => {
          let d = _.find(source.dict, d => {
            return d[source.field.dictionaryCode] === value;
          });
          d[destination.field.dictionary].forEach(dd => {
            destination.dict.push(dd);
          });
        });
      } else {
        destination.dict = this.model.dicts[destination.field.dictionary];
      }
    } else {
      let value = this.model.selectedFilters[source.field.code];
      if (value) {
        let d = _.find(source.dict, d => {
          return d[source.field.dictionaryCode] === value;
        });
        destination.dict = d[destination.field.dictionary];
      } else {
        destination.dict = this.model.dicts[destination.field.dictionary];
      }
    }*/
  }

  findUsers: (string) => Observable<any[]> = searchText => {
    return from(this.nsi.searchUsers({fio: searchText}));
  };
}
