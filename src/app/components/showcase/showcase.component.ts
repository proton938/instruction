import { Component, OnInit, Inject } from '@angular/core';
import { InstructionFilterService } from '../../services/instruction-filter.service';

@Component({
  selector: 'mggt-showcase',
  templateUrl: './showcase.component.html'
})
export class ShowcaseComponent implements OnInit {
  constructor(@Inject('$localStorage') public $localStorage: any) {
  }
  ngOnInit() {
  }

}
