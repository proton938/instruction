import {StateService, Transition} from '@uirouter/core';
import {ToastrService} from 'ngx-toastr';
import {Component, ViewEncapsulation, ViewChild} from '@angular/core';
import {ShowcaseDesktopComponent} from "../desktop/desktop.component";
import {AuthorizationService, SessionStorage} from "@reinform-cdp/security";
import {SolarResourceService} from "@reinform-cdp/search-resource";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {AssignmentListMobileComponent} from '../../list/assigment-list/mobile/mobile.component';
import { InstructionFilterService } from '../../../services/instruction-filter.service';

@Component({
  selector: 'mggt-showcase-mobile',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShowcaseMobileComponent extends ShowcaseDesktopComponent {
  @ViewChild(AssignmentListMobileComponent)
  assigments: AssignmentListMobileComponent;

  hideOvers: boolean = true;

  constructor($state: StateService,
              solarResourceService: SolarResourceService,
              toastr: ToastrService,
              nsiRestService: NsiResourceService,
              session: SessionStorage,
              authorizationService: AuthorizationService,
              transition: Transition,
              filterService:InstructionFilterService) {
    super($state, solarResourceService, toastr, nsiRestService, session, authorizationService, transition,filterService);
    this.searchQuery = '';

    if (localStorage.getItem('assigment') !== null) {
      let documentId = localStorage.getItem('assigment');
      let url = `/sdo/instruction/#/app/instruction/instruction/card/${documentId}`;
      window.open(url, '_self');
      localStorage.removeItem('assigment');
    }
  }

  saveLinkForNew() {
    if (localStorage.getItem('onSaveLink') === null) {
      localStorage.setItem('newAssigment', 'new');
      localStorage.setItem('onSaveLink', 'on');
    }
    localStorage.setItem('linkCreateOrder', 'link');
  }

  activate(index: string): boolean {
    if (this.isActive(index)) {
      this.hideOvers = !this.hideOvers;
      return false;
    }

    this.hideOvers = true;
    return super.activate(index);
  }
}
