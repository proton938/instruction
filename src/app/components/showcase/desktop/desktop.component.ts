import {LoadingStatus} from "@reinform-cdp/widgets";
import {StateService, Transition} from '@uirouter/core';
import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthorizationService, SessionStorage} from "@reinform-cdp/security";
import {SolarResourceService} from '@reinform-cdp/search-resource';
import {from} from 'rxjs/index';
import {ToastrService} from 'ngx-toastr';
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import * as _ from "lodash";
import {Subject} from "rxjs";
import {AssigmentListComponent} from '../../list/assigment-list/assigment-list.component';
import {InstructionFilter} from '../../../models/instruction/InstructionFilter';
import {InstructionShowcaseTabBuilderFactory, ShowcaseInstructionTabBuilder} from '../../../models/tabs/ShowcaseInstructionTabBuilder';
import { InstructionFilterService } from "../../../services/instruction-filter.service";

@Component({
  selector: 'mggt-showcase-desktop',
  templateUrl: './desktop.component.html',
  styleUrls: ['./desktop.component.scss']
})
export class ShowcaseDesktopComponent implements OnInit {

  @ViewChild(AssigmentListComponent)
  assigments: AssigmentListComponent;

  loading: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  activeTab: string;
  searchQuery: string = '';
  tabs: any[] = [];
  dicts: any = {
    statuses: [],
    themes: [],
    priorities: [],
    difficulties: [],
    executors: [],
    types: [],
    _themes: []
  };
  filter: InstructionFilter = new InstructionFilter();
  overdue = {value: false};
  preFilter: any;
  updateList: Subject<any> = new Subject();
  creatingReport: boolean = false;
  showcaseName: string;

  constructor(public $state: StateService,
              private solarResourceService: SolarResourceService,
              private toastr: ToastrService,
              private nsiRestService: NsiResourceService,
              private session: SessionStorage,
              private authorizationService: AuthorizationService,
              private transition: Transition,
              private filterService:InstructionFilterService) {
  }

  ngOnInit() {
    this.showcaseName = this.transition.params()['tabBuilder'];

    if(this.showcaseName=='mka'){
      this.filter.clear();
      this.filterService.clearAssignmentFilter();
    }
    else {
      this.filter = this.filterService.getAssignmentFilter();
      this.searchQuery = this.filterService.getAssignmentSearch();
    }
    from(this.nsiRestService.getDictsFromCache([
      'InstructionStatus',
      'ThematicHeading',
      'InstructionPriority',
      'InstructionDifficulty',
      'InstructionExecutors',
      'InstructionTheme',
      'mggt_meeting_MeetingTypes',
    ])).subscribe(response => {
      this.dicts = {
        statuses: _.orderBy(response.InstructionStatus.filter((i: any) => i.code !== 'draft'), ['name'], ['asc']),
        themes: _.orderBy(response.ThematicHeading, ['name'], ['asc']),
        priorities: _.orderBy(response.InstructionPriority, ['name'], ['asc']),
        difficulties: _.orderBy(response.InstructionDifficulty, ['name'], ['asc']),
        executors: response.InstructionExecutors,
        _themes: _.orderBy(response.InstructionTheme, ['name'], ['asc']),
        types: response.mggt_meeting_MeetingTypes,
        acceptStatuses: [ 'Отклонено', 'Принято' ]
      };
      /*let tabBuilder = new ShowcaseInstructionTabBuilder(this.dicts.executors, this.session, this.authorizationService);
      this.tabs = tabBuilder.build();*/

      let tabBuilder: ShowcaseInstructionTabBuilder = InstructionShowcaseTabBuilderFactory.getBuilder(this.transition.params()['tabBuilder']);
      this.tabs = tabBuilder.build(this.authorizationService, this.session, this.dicts.executors);

      if (this.tabs && this.tabs.length) {
        if(this.showcaseName=='mka')
          this.activeTab = this.tabs[0].alias;
        else {
          if(!this.filterService.assignmentActiveTab)
            this.filterService.assignmentActiveTab = this.tabs[0].alias;
          this.activeTab = this.filterService.assignmentActiveTab;
        }
      }
      this.makePreFilters();
      this.loading = LoadingStatus.SUCCESS;
      this.success = true;
    }, err => {
      this.toastr.error(err.data.exception);
      this.loading = LoadingStatus.ERROR;
      console.log(err);
    });

    if (localStorage.getItem('assigment') !== null) {
      let documentId = localStorage.getItem('assigment');
      let url = `/sdo/instruction/#/app/instruction/instruction/card/${documentId}`;
      window.open(url, '_self');
      localStorage.removeItem('assigment');
    }
    if (localStorage.getItem('newAssigment') !== null) {
      let url = `/sdo/instruction/#/app/instruction/instruction/new`;
      window.open(url, '_self');
      localStorage.removeItem('newAssigment');
    }

  }

  saveLinkForNew() {
    if (localStorage.getItem('onSaveLink') === null) {
      localStorage.setItem('newAssigment', 'new');
      localStorage.setItem('onSaveLink', 'on');
    }
  }

  filterChange(filter: any) {
    this.filter = filter;
    this.updateList.next();
  }

  overdueChange(overdue: any) {
    this.overdue.value = overdue.value;
    this.updateList.next();
  }

  searchChanged() {
    if (this.assigments) {
      this.assigments.commonSearch(this.searchQuery);
    }
    if(this.showcaseName!='mka')
        this.filterService.setAssignmentSearch(this.searchQuery);
  }

  makePreFilters() {
    this.preFilter = {};
    this.tabs.forEach(tab => {
      this.preFilter[tab.alias] = tab.filter;
    })
  }

  assistantCreators(): string {
    let result = [];

    this.dicts.executors.forEach(e => {
      if (e.assistantview && e.assistantview.length && e.assistantview.length > 0) {
        e.assistantview.forEach(v => {
          if (this.session.login() === v) {
            result.push(`creatorLogin:${e.creator}`);
          }
        });
      }
    });

    return result.join(' OR ');
  }

  assistantExecutors(): string {
    let result = [];

    this.dicts.executors.forEach(e => {
      if (e.assistantview && e.assistantview.length && e.assistantview.length > 0) {
        e.assistantview.forEach(v => {
          if (this.session.login() === v) {
            result.push(`executorLogin:${e.creator}`);
          }
        });
      }
    });

    return result.join(' OR ');
  }

  clear() {
    this.filter.clear();
    if (this.assigments) {
      this.assigments.clearTableFilter();
      setTimeout(() => {
        console.log('showcase clear');
        this.updateList.next()
      }, 200);
    }
  }

  isActive(index: string): boolean {
    return index === this.activeTab;
  }

  activate(index: string): boolean {
    if (this.activeTab === index) {
      return false;
    } else {
      this.activeTab = index;
      if(this.showcaseName!='mka')
        this.filterService.assignmentActiveTab = index;
      return true;
    }
  }

  loseFocus() {
    setTimeout(() => {
      console.log('showcase loseFocus');
      let element = document.activeElement;
      if (element && element instanceof HTMLInputElement)  {
        element.blur();
      }
    })
  }

  createReport(): any {
    if (this.assigments) {
      this.creatingReport = true;
      this.assigments.createReport().then(() => {
        this.creatingReport = false;
      });
    }
  }
}
