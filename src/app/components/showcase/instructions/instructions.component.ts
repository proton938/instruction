import { Component, Inject, ViewEncapsulation } from "@angular/core";
import { BreadcrumbsService } from "@reinform-cdp/skeleton";
import { Transition } from '@uirouter/core';
import { find, first } from 'lodash';
import { InstructionFilterService } from "../../../services/instruction-filter.service";
import {SessionStorage} from "@reinform-cdp/security";

@Component({
  selector: 'mggt-showcase-instructions',
  templateUrl: './instructions.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ShowcaseInstructionsComponent {
  loading: boolean = true;
  config: any;
  tabs: any[] = [];
  activeTab: string;

  constructor(private breadcrumbsService: BreadcrumbsService,
              private transition: Transition,
              @Inject('$localStorage') public $localStorage: any,
              private filterService:InstructionFilterService,
              private session: SessionStorage) { }

  ngOnInit() {
    this.filterService.clearAssignmentFilter();
    let tabs = [
      {
        code: 'memo',
        name: 'Внутренние документы',
        show: this.session.hasPermission('INSTRUCTION_MEMO_VIEW'),
        showcaseCode: 'SDO_MEMO'
      },
      {
        code: 'memo-by-user',
        name: 'Направленные мной',
        show: true,
        showcaseCode: 'SDO_MEMO_BY_USER'
      },
      {
        code: 'memo-for-user',
        name: 'Направленные мне',
        show: true,
        showcaseCode: 'SDO_MEMO_FOR_USER'
      },
      {
        code: 'memo-viewed',
        name: 'Рассмотренные',
        show: true,
        showcaseCode: 'SDO_MEMO_VIEWED'
      },
      {
        code: 'memo-draft',
        name: 'Проекты мои',
        show: true,
        showcaseCode: 'SDO_MEMO_DRAFT'
      }
    ];
    this.tabs = tabs.filter(i => i.show);
    this.activeTab = this.transition.params()['tab'] || 'memo';
    if (!find(this.tabs, i => i.code === this.activeTab)) { this.activeTab = first(this.tabs).code; }
    this.updateBreadcrumbs();
    setTimeout(this.changeConfig.bind(this), 100);
  }

  changeConfig() {
    let tab = find(this.tabs, i => i.code === this.activeTab);
    this.config = {
      system: 'SDO',
      subsystem: 'SDO_INSTRUCTION',
      document: 'SDO_INSTRUCTION_MEMO',
      showcase: tab.showcaseCode
    };
    this.loading = false;
  }

  activate(index: string): boolean {
    if (this.activeTab === index) {
      return false;
    } else {
      this.activeTab = index;
      return true;
    }
  }

  isActive(index: string): boolean {
    return index === this.activeTab;
  }

  updateBreadcrumbs() {
    this.breadcrumbsService.setBreadcrumbsChain([{
      title: 'Распорядительные документы',
      url: null
    }]);
  }
}
