import {AppComponent} from './app.component';
import {CreateOrderComponent} from './components/order/create-order/create-order.component';
import {OrderCardComponent} from './components/order/order-card/order-card.component';
import {CopyOrderComponent} from './components/order/copy-order/copy-order.component';
import {CreateLinkedOrderComponent} from './components/order/create-linked-order/create-linked-order.component';
import {CreateSubTaskComponent} from './components/order/create-sub-task/create-sub-task.component';
import {MemoShowcaseComponent} from './components/memo/memo-showcase/memo-showcase.component';
import {MemoMkaShowcaseComponent} from './components/memo/memo-showcase/memo-mka-showcase.component';
import {CreateMemoComponent} from './components/memo/create-memo/create-memo.component';
import {MemoCardComponent} from './components/memo/memo-card/memo-card.component';
import {CopyMemoComponent} from './components/memo/copy-memo/copy-memo.component';
import {CreateOrderFromMemoComponent} from './components/order/create-order-from-memo/create-order-from-memo.component';
import {ReviewMemoComponent} from './components/memo/tasks/review-memo/review-memo.component';
import {IssueAssignmentComponent} from './components/order/tasks/issue-assignment/issue-assignment.component';
import {ReviewAssignmentComponent} from './components/order/tasks/review-assignment/review-assignment.component';
import {AcceptExecutionComponent} from './components/order/tasks/accept-execution/accept-execution.component';
import {ReviewMemoMKADocComponent} from './components/memo/tasks/review-mka-doc/review-mka-doc.component'
import {ReviewMKADocComponent} from './components/order/tasks/review-mka-doc/review-mka-doc.component'
import {CorrectAssignmentComponent} from './components/order/tasks/correct-assignment/correct-assignment.component';
import {AcceptIssueAssignmentComponent} from './components/order/tasks/accept-issue-assignment/accept-issue-assignment.component';
import {ShowcaseComponent} from './components/showcase/showcase.component';
import {CreateOrderFromMeetingComponent} from './components/order/create-order-from-meeting/create-order-from-meeting.component';
import { AcceptMemoComponent } from './components/memo/tasks/accept-memo/accept-memo.component';
import {RegisterMemoComponent} from './components/memo/tasks/register-memo/register-memo.component';
import { ApproveMemoComponent } from './components/memo/tasks/approve-memo/approve-memo.component';
import { ReworkMemoComponent } from './components/memo/tasks/rework-memo/rework-memo.component';
import {CreateOrderFromTaskComponent} from './components/order/create-order-from-task/create-order-from-task.component';
import {ShowcaseInstructionsComponent} from './components/showcase/instructions/instructions.component';
import { ScanMemoComponent } from './components/memo/tasks/scan-memo/scan-memo.component';
import { CreateMemoShortComponent } from './components/memo/create-memo-short/create-memo-short.component';
import { CreateMemoFullComponent } from './components/memo/create-memo-full/create-memo-full.component';
import {ScanSignedMemoComponent} from './components/memo/tasks/scan-signed-memo/scan-signed-memo.component';
import {PageNotFoundComponent} from '@reinform-cdp/skeleton';
import { TaskWrapperComponent} from '@reinform-cdp/bpm-components';
import {MemoEditComponent} from './components/memo/memo-edit/memo-edit.component';
import { CdpDependenciesTreeComponent } from '@reinform-cdp/widgets'
import {CreateInstructionFromOrderComponent} from './components/order/create-from-order/create-from-order.component';


export const states = [
  {
    name: 'app.versions',
    url: '/versions',
    component: CdpDependenciesTreeComponent
  },
  {
    name: '404',
    url: '/*path',
    component: PageNotFoundComponent
  },
  {
    name: 'app.execution',
    url: '/execution/:system/:taskId',
    component: TaskWrapperComponent
  },
  {
    name: 'app.instruction',
    url: '/instruction',
    component: AppComponent
  },
  {
    name: 'app.instruction.instruction-new',
    url: '/instruction/new?:from?:field?:id?:backUrl',
    component: CreateOrderComponent,
    params: {
      from: null,
      field: null,
      id: null,
      useBreadcrumbs: true,
      backUrl: null,
    }
  },
  {
    name: 'app.instruction.memo-new',
    url: '/memo/new',
    component: CreateMemoComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.instruction.memo-new-short',
    url: '/memo/new-short',
    component: CreateMemoShortComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.instruction.memo-new-full',
    url: '/memo/new-full/:id',
    component: CreateMemoFullComponent,
    params: {
      useBreadcrumbs: true,
      id: null,
    }
  },
  {
    name: 'app.instruction.instruction-copy',
    url: '/instruction/copy/:copiedId?:asSubTask',
    component: CopyOrderComponent,
    params: {
      copiedId: null,
      asSubTask: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.instruction.memo-copy',
    url: '/memo/copy/:copiedId?:asSubTask',
    component: CopyMemoComponent,
    params: {
      copiedId: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.instruction.instruction-card',
    url: '/instruction/card/:id?:action',
    component: OrderCardComponent,
    params: {
      id: null,
      action: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.instruction.memo-card',
    url: '/memo/card/:id',
    component: MemoCardComponent,
    params: {
      id: null,
      useBreadcrumbs: true,
      memoType: 'memo',
      documentType: 'MEMO',
      showcaseState: 'app.instruction.showcase-memo',
      showcaseTitle: 'Служебные записки'
    }
  },
  {
    name: 'app.instruction.memo-edit',
    url: '/memo/edit/:id',
    component: MemoEditComponent,
    params: {
      id: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.instruction.memomka-card',
    url: '/memomka/card/:id',
    component: MemoCardComponent,
    params: {
      id: null,
      useBreadcrumbs: true,
      memoType: 'memomka',
      documentType: 'MEMO-MKA',
      showcaseState: 'app.instruction.showcase-memos-mka',
      showcaseTitle: 'Служебные записки МКА'
    }
  },
  {
    name: 'app.instruction.instruction-linked',
    url: '/instruction/linked/:linkedId',
    component: CreateLinkedOrderComponent,
    params: {
      linkedId: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.instruction.instruction-subtask',
    url: '/instruction/createSubTask/:parentId?:from',
    component: CreateSubTaskComponent,
    params: {
      parentId: null,
      from: null,
      processPrefix: null,
      taskId: null,
      formKey: null,
      systemCode: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.instruction.instruction-fromMemo',
    url: '/instruction/createFromMemo/:memoId',
    component: CreateOrderFromMemoComponent,
    params: {
      memoId: null,
      memoType: 'memo',
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.instruction.instruction-fromTask',
    url: '/instruction/createFromTask/:processPrefix/:taskId/:formKey/:questionID/:questionPrimaryID?:expressMeetingID&:systemCode',
    component: CreateOrderFromTaskComponent,
    params: {
      processPrefix: null,
      taskId: null,
      formKey: null,
      questionID: null,
      questionPrimaryID: null,
      expressMeetingID: null,
      systemCode: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.instruction.instruction-fromOrder',
    url: '/instruction/createFromOrder/:orderId?:taskId',
    component: CreateInstructionFromOrderComponent
  },
  {
    name: 'app.instruction.instruction-fromMeeting',
    url: '/instruction/createFromMeeting/:questionID/:questionPrimaryID?:expressMeetingID',
    component: CreateOrderFromMeetingComponent,
    params: {
      questionID: null,
      questionPrimaryID: null,
      expressMeetingID: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.memoReviewMemo',
    url: '/memoReviewMemo',
    component: ReviewMemoComponent
  },
  {
    name: 'app.execution.sdomemoReviewMemo',
    url: '/sdomemoReviewMemo?:systemCode',
    component: ReviewMemoComponent,
    params: {
      systemCode: null
    }
  },
  {
    name: 'app.execution.memoAcceptMemoInfo',
    url: '/memoAcceptMemoInfo',
    component: AcceptMemoComponent
  },
  {
    name: 'app.execution.sdomemoAcceptMemoInfo',
    url: '/sdomemoAcceptMemoInfo?:systemCode',
    component: AcceptMemoComponent
  },
  {
    name: 'app.execution.sdomemoRegisterMemo',
    url: '/sdomemoRegisterMemo_frm',
    component: RegisterMemoComponent,
    params: {
      systemCode: null
    }
  },
  {
    name: 'app.execution.sdomemomemoCreate',
    url: '/sdomemomemoCreate',
    component: CreateMemoFullComponent,
    params: {
      systemCode: null
    }
  },
  {
    name: 'app.execution.sdomemoApproveMemoProject',
    url: '/sdomemoApproveMemoProject_frm',
    component: ApproveMemoComponent,
    params: {
      systemCode: null
    }
  },
  {
    name: 'app.execution.memoScan',
    url: '/sdomemomemoScan',
    component: ScanMemoComponent,
    params: {
      systemCode: null
    }
  },
  {
    name: 'app.execution.sdomemoReworkMemo',
    url: '/sdomemoReworkMemo_frm',
    component: ReworkMemoComponent,
    params: {
      systemCode: null
    }
  },
  {
    name: 'app.execution.sdoassignIssueAssignment',
    url: '/sdoassignIssueAssignment?:systemCode',
    component: IssueAssignmentComponent,
    params: {
      systemCode: null
    }
  },
  {
    name: 'app.execution.sdoassignTakeIntoConsideration',
    url: '/sdoassignTakeIntoConsideration',
    component: ReviewAssignmentComponent
  },
  {
    name: 'app.execution.sdoassignTakeIntoCoexecution',
    url: '/sdoassignTakeIntoCoexecution',
    component: AcceptExecutionComponent
  },
  {
    name: 'app.execution.sdoassignmkaReviewMKADoc',
    url: '/sdoassignmkaReviewMKADoc',
    component: ReviewMKADocComponent
  },
  {
    name: 'app.execution.sdomemomkaReviewMKADoc',
    url: '/sdomemomkaReviewMKADoc',
    component: ReviewMemoMKADocComponent
  },
  {
    name: 'app.execution.sdoassigncorrectAssignment',
    url: '/sdoassigncorrectAssignment',
    component: CorrectAssignmentComponent
  },
  {
    name: 'app.execution.sdoassignAcceptIssueAssignment',
    url: '/sdoassignAcceptIssueAssignment?:systemCode',
    component: AcceptIssueAssignmentComponent
  },
  {
    name: 'app.execution.ScanSignedMemo_frm',
    url: '/sdomemoScanSignedMemo_frm',
    component: ScanSignedMemoComponent
  },
  {
    name: 'app.instruction.showcase-memo',
    url: '/showcase/memo?tab',
    component: ShowcaseInstructionsComponent,
    params: {
      tab: 'memo'
    }
  },
  {
    name: 'app.instruction.showcase-assignments',
    url: '/showcase/assignments',
    component: ShowcaseComponent
  },
  {
    name: 'app.instruction.showcase-memos-mka',
    url: '/showcase/mka-memo',
    component: MemoMkaShowcaseComponent
  },
  {
    name: 'app.instruction.showcase-assignments-mka',
    url: '/showcase/mka-assignments',
    component: ShowcaseComponent,
    params: {
      activeTab: 'mka',
      tabBuilder: 'mka',
      suffix: 'mka'
    }
  }
];
