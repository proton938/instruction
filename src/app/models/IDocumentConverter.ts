export interface IDocumentConverter<R> {
  build(i: R);
  min(): R;
}
