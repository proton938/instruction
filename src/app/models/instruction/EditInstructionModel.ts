import {InstructionExecutor} from './InstructionExecutor';
import {UserBean} from '@reinform-cdp/nsi-resource';
import * as _ from 'lodash';
import {InstructionDictsModel} from "./InstructionDictsModel";
import {NSIInstructionPriority} from "../nsi/NSIInstructionPriority";
import {NSIInstructionDifficulty} from "../nsi/NSIInstructionDifficulty";
import {NSIInstructionTheme} from "../nsi/NSIInstructionTheme";
import {NSIThematicHeading} from "../nsi/NSIThematicHeading";
import {InstructionDocument} from "../instruction-document/InstructionDocument";
import {DocumentUser} from "../core/DocumentUser";
import {InstructionRestService} from "../../services/instruction-rest.service";
import {ToastrService} from "ngx-toastr";
import {Observable} from "rxjs/Rx";
import {of, throwError} from "rxjs/index";
import {sevenDaysInMs} from "../../consts";
import {InstructionDocumentForInformation} from "../instruction-document/InstructionDocumentForInformation";
import {CoExecutor} from './CoExecutorModel';
import {NSIRepeatPeriod} from '../nsi/NSIRepeatPeriod';
import {NSIWeekday} from '../nsi/NSIWeekday';
import {
  InstructionDocumentPeriodMonth,
  InstructionDocumentPeriodWeekday,
  InstructionDocumentPlanDateRepeat
} from '../instruction-document/InstructionDocumentRepeated';
import {NSIMonth} from '../nsi/NSIMonth';
import {InstructionDocumentNumberWeek} from '../instruction-document/InstructionDocumentNumberWeek';
import {InstructionDocumentTheme} from "../instruction-document/InstructionDocumentTheme";
import {InstructionDocumentSubject} from "../instruction-document/InstructionDocumentSubject";
import {InstructionDocumentPriority} from "../instruction-document/InstructionDocumentPriority";
import {InstructionDocumentDifficulty} from "../instruction-document/InstructionDocumentDifficulty";

// Depricated
export class EditInstructionModel {
  documentId: string = '';
  memoId: string = '';
  memoContent: string = '';
  memoView: boolean = false;
  creator: InstructionExecutor = null;
  beginDate: Date = new Date();
  theme: NSIThematicHeading = null;
  content: string = '';
  description: string = '';
  priority: string = null;
  difficulty: string = null;
  private: boolean = false;
  planDate: Date = null;
  executor: UserBean = null;
  coExecutor: CoExecutor[] = [];
  reviewBy: UserBean[] = [];
  cadastralAuthor: string = '';
  cadastralRequest: string = '';
  cadastralCount: number = null;
  assistant: UserBean = null;
  instructionRestService: InstructionRestService;
  toastr: ToastrService;
  _theme: NSIInstructionTheme = null;
  filesRequired: boolean = false;
  repeat: EditInstructionRepeatModel = new EditInstructionRepeatModel();

  minDate: Date = new Date((new Date).valueOf() - sevenDaysInMs);

  errorMessage: string = '';

  constructor(instructionRestService: InstructionRestService, toastr: ToastrService) {
    this.instructionRestService = instructionRestService;
    this.toastr = toastr;
  }

  isValid(): boolean {
    let isCadastral = (this.theme && this.theme.code === 'cadastral') ? (this.cadastralAuthor && this.cadastralRequest && (this.cadastralCount !== null)) : true;
    return isCadastral && this.creator && this.beginDate && this.content && this.planDate && this.executor && this.priority && this.difficulty && this.repeat.isValid();
  }

  updateDocumentFromModel(document: InstructionDocument, priority: NSIInstructionPriority[], difficulty: NSIInstructionDifficulty[]): Observable<any> {
    try {
      if (this.assistant) {
        document.instruction.assistant = new DocumentUser();
        document.instruction.assistant.updateFromUserBean(this.assistant);
      }
      if (this.creator.creator) {
        document.instruction.creator = new DocumentUser();
        document.instruction.creator.updateFromUserBean(this.creator.creator);
      }
      document.instruction.beginDate = this.beginDate || null;
      if (this.theme) {
        document.instruction.theme = new InstructionDocumentTheme();
        document.instruction.theme.updateFromNsi(this.theme);
      }
      this.copyCadastralFromModel(document);
      if (this._theme) {
        document.instruction.subject = new InstructionDocumentSubject();
        document.instruction.subject.updateFromNsi(this._theme);
      }
      document.instruction.content = this.content.trim();
      document.instruction.description = this.description.trim();
      if (this.priority) {
        document.instruction.priority = new InstructionDocumentPriority();
        document.instruction.priority.updateFromNsi(_.find(priority, (pr: NSIInstructionPriority) => pr.code === this.priority));
      }
      if (this.difficulty) {
        document.instruction.difficulty = new InstructionDocumentDifficulty();
        document.instruction.difficulty.updateFromNsi(_.find(difficulty, (dif: NSIInstructionDifficulty) => dif.code === this.difficulty));
      }
      document.instruction.private = this.private;
      document.instruction.planDate = (this.planDate) ? this.planDate : null;
      if (this.executor) {
        document.instruction.executor = new DocumentUser();
        document.instruction.executor.updateFromUserBean(this.executor);
      }
      this.updateCoExecutorsFromModel(document);
      this.updateForInformationFromModel(document);
      if (document.instruction.memoId) {
        document.instruction.memoView = this.memoView;
      }
      document.filesRequired = this.filesRequired;

      this.repeat.updateDocumentFromModel(document);

      return of('updated');
    } catch (error) {
      return throwError(error);
    }
  }

  updateModelBeforeEditing(document: InstructionDocument, dicts: InstructionDictsModel): Observable<any> {
    this.documentId = document.documentId;
    this.creator = dicts.creators.find(c => {
      return c.creator.accountName === document.instruction.creator.login;
    });
    this.assistant = new UserBean();
    if (document.instruction.assistant.login) {
      this.copyUserBeanFromOrderDocumentUser(this.assistant, document.instruction.assistant);
    }


    if (this.creator) {
      this.executor = this.creator.executor.find(c => {
        return !!document.instruction.executor && c.accountName === document.instruction.executor.login;
      });
      if (!this.executor && document.instruction.executor && document.instruction.executor.login) {
        this.executor = new UserBean();
        this.executor.accountName = document.instruction.executor.login;
        this.executor.post = document.instruction.executor.post;
        this.executor.displayName = document.instruction.executor.fioFull;
      }
    } else {
      this.creator = new InstructionExecutor();
    }
    // document.instruction.coExecutor.forEach(ce => {
    //   let user = new UserBean();
    //   this.copyUserBeanFromOrderDocumentUser(user, ce);
    //   this.coExecutor.push(user);
    // });
    this.coExecutor = document.instruction.coExecutor || [];

    document.forInformation.forEach(fi => {
      let user = new UserBean();
      this.copyUserBeanFromOrderDocumentUser(user, fi.reviewBy);
      this.reviewBy.push(user);
    });
    try {
      if (document.instruction.beginDate) {
        this.beginDate = document.instruction.beginDate;
      }
      if (document.instruction.theme && document.instruction.theme.code) {
        this.theme = dicts.themes.find(th => {
          return th.code === document.instruction.theme.code;
        });
        if (this.theme.code === 'cadastral') {
          this.copyCadastralToModel(document);
        }
      }

      if (document.instruction.subject && document.instruction.subject.code) {
        this._theme = dicts._themes.find(th => {
          return th.code === document.instruction.subject.code;
        });
      }

      this.priority = document.instruction.priority.code;
      this.difficulty = document.instruction.difficulty.code;
      if (document.instruction.content) {
        this.content = document.instruction.content;
      }
      if (document.instruction.description) {
        this.description = document.instruction.description;
      }
      if (document.instruction.planDate) {
        this.planDate = document.instruction.planDate;
      }
      if (document.instruction.private !== null) {
        this.private = document.instruction.private;
      }
      if (document.filesRequired !== null) {
        this.filesRequired = document.filesRequired;
      }

      this.repeat.updateModelBeforeEditing(document, dicts);

      return of('');
    } catch (error) {
      return throwError(error);
    }
  }

  creatorChanged() {
    this.executor = null;
  }

  private copyCadastralFromModel(document: InstructionDocument) {
    document.instruction.cadastral.author = (this.theme && this.theme.code === 'cadastral') ? this.cadastralAuthor : '';
    document.instruction.cadastral.request = (this.theme && this.theme.code === 'cadastral') ? this.cadastralRequest : '';
    document.instruction.cadastral.count = (this.theme && this.theme.code === 'cadastral') ? this.cadastralCount : null;
  }

  private copyCadastralToModel(document: InstructionDocument) {
    this.cadastralAuthor = document.instruction.cadastral.author;
    this.cadastralRequest = document.instruction.cadastral.request;
    this.cadastralCount = document.instruction.cadastral.count;
  }

  private dateWithoutTime(date: Date) {
    let _date = date.setHours(0, 0, 0, 0).valueOf();
    return new Date(_date);
  }

  private dateWithCurrentTime(date: Date) {
    const now = new Date();
    let _date = date.setHours(now.getHours(), now.getMinutes(), now.getSeconds(), now.getMilliseconds()).valueOf();
    return new Date(_date);
  }

  private copyUserBeanFromOrderDocumentUser(user: UserBean, documentUser: DocumentUser) {
    user.accountName = documentUser.login;
    user.post = documentUser.post;
    user.displayName = documentUser.fioFull;
  }

  private updateCoExecutorsFromModel(document: InstructionDocument) {
    document.instruction.coExecutor = this.coExecutor || [];
  }

  private updateForInformationFromModel(document: InstructionDocument) {
    if (this.reviewBy && this.reviewBy.length) {
      document.forInformation = _.map(this.reviewBy, e => {
        let result = new InstructionDocumentForInformation();
        result.updateFromUserBean(e);
        return result;
      });
    } else {
      document.forInformation = [];
    }
  }

  get errorMsg() {
    return this.errorMessage || this.repeat.errorMessage;
  }
}


export class EditInstructionRepeatModel {
  repeated: boolean = false;
  currentNumberRepeat: number = null;
  repeatPeriodEndDate: Date = null;
  repeatPeriod: NSIRepeatPeriod = null;
  repeatPeriodicity: number = null;
  repeatWorkingDay: boolean = false;
  repeatWeekdays: NSIWeekday[] = [];
  repeatMonth: NSIMonth[] = [];
  repeatDayOfMonth: number = null;
  repeatWeekNumber: InstructionDocumentNumberWeek = null;
  repeatPlanDates: EditInstructionRepeatPlanDateModel[] = [];

  errorMessage: string = '';

  isValid(): boolean {
    let r = true;
    if (!this.repeated) {
      return true;
    }
    if (!this.repeatPeriodicity || !this.repeatPeriod.code) {
      return false;
    }
    switch (this.repeatPeriod.code) {
      case 'weekly':
        r = this.repeatWeekdays.length > 0;
        break;
      case 'monthly':
      case 'yearly':
        r = !!this.repeatDayOfMonth || (this.repeatWeekdays.length > 0 && !!this.repeatWeekNumber);
        if (!r) { this.errorMessage = 'Необходимо определить число месяца или день недели повтора поручения'; }
        break;
      default:
        return true;
    }

    if (r && this.repeatPeriod.code === 'yearly') {
      r = this.repeatMonth.length > 0;
    }

    return r;
  }

  updateDocumentFromModel(document: InstructionDocument) {
    document.instruction.repeated.repeatedSign = this.repeated;
    document.instruction.repeated.currentNumberRepeat = this.currentNumberRepeat;
    document.instruction.repeated.dateEndPeriodRepeat = this.repeatPeriodEndDate;
    document.instruction.repeated.periodRepeat.updateFromNsi(this.repeatPeriod);
    document.instruction.repeated.periodicity = this.repeatPeriodicity;
    document.instruction.repeated.RepeatWorkingDay = this.repeatWorkingDay;
    document.instruction.repeated.weekday = this.repeatWeekdays.map(rw => {
      const weekday = new InstructionDocumentPeriodWeekday();
      weekday.updateFromNsi(rw);
      return weekday;
    });
    document.instruction.repeated.dayMonth = this.repeatDayOfMonth;
    document.instruction.repeated.numberWeek = this.repeatWeekNumber;
    document.instruction.repeated.Month = this.repeatMonth.map(rw => {
      const month = new InstructionDocumentPeriodMonth();
      month.updateFromNsi(rw);
      return month;
    });

    if (this.repeated) {
      if (document.documentId.length === 0) {
        document.instruction.primaryDate = document.instruction.planDate;

        const planDateRepeat = new InstructionDocumentPlanDateRepeat();
        planDateRepeat.planDate = document.instruction.planDate;
        if (document.instruction.repeated.repeatedSign) { planDateRepeat.numberRepeat = 1; }
        document.instruction.repeated.planDateRepeat = [planDateRepeat];
        document.instruction.repeated.currentNumberRepeat = 1;
      } else if (document.instruction.repeated.planDateRepeat.length === 1 && !document.instruction.repeated.planDateRepeat[0].reportAccepted) {
        document.instruction.repeated.planDateRepeat[0].planDate = document.instruction.planDate;
        document.instruction.repeated.planDateRepeat[0].numberRepeat = 1;
      }
    }
  }

  updateModelBeforeEditing(document: InstructionDocument, dicts: InstructionDictsModel) {
    this.repeated = document.instruction.repeated.repeatedSign;
    this.currentNumberRepeat = document.instruction.repeated.currentNumberRepeat;
    this.repeatPeriodEndDate = document.instruction.repeated.dateEndPeriodRepeat;
    if (document.instruction.repeated.periodRepeat.code) {
      this.repeatPeriod = dicts.repeatPeriods.find(rp => {
        return rp.code === document.instruction.repeated.periodRepeat.code;
      });
    }
    this.repeatPeriodicity = document.instruction.repeated.periodicity;
    this.repeatWorkingDay = document.instruction.repeated.RepeatWorkingDay;
    this.repeatWeekdays = document.instruction.repeated.weekday.map(w => {
      return dicts.weekdays.find(wd => wd.code === w.code);
    });
    this.repeatDayOfMonth = document.instruction.repeated.dayMonth;
    this.repeatWeekNumber = document.instruction.repeated.numberWeek;
    this.repeatMonth = document.instruction.repeated.Month.map(m => {
      return dicts.months.find(month => month.code === m.code);
    });
    this.repeatPlanDates = document.instruction.repeated.planDateRepeat.map(pd => {
      return {numberRepeat: pd.numberRepeat, planDate: pd.planDate, reportAccepted: pd.reportAccepted};
    })
  }
}

export class EditInstructionRepeatPlanDateModel {
  numberRepeat: number = null;
  planDate: Date = null;
  reportAccepted: boolean;
}
