import { UserBean } from '@reinform-cdp/nsi-resource';

export class InstructionExecutor {
    creator: UserBean;
    executor: UserBean[];
    assistant: UserBean[];
    assistantview?: UserBean[];

    constructor() {
      this.creator = new UserBean();
      this.executor = [];
      this.assistant = [];
      this.assistantview = [];
    }
}
