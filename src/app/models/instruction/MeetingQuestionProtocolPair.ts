import {SearchResultDocument} from '@reinform-cdp/search-resource';

export interface MeetingQuestionProtocolPair {
   question: SearchResultDocument;
   protocol: SearchResultDocument;
}
