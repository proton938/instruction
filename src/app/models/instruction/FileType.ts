export class FileType {
  idFile: string;
  typeFile: string;
  nameFile: string;
  sizeFile: string;
  signed: boolean;
  dateFile: Date;
  classFile: string; // mimeType

  static create(idFile?: string, nameFile?: string, sizeFile?: string, signed?: boolean, dateFile?: Date, typeFile?: string, classFile?: string): FileType {
    let result: FileType = new FileType();

    result.idFile = idFile;
    result.nameFile = nameFile;
    result.sizeFile = sizeFile;
    result.signed = signed;
    result.dateFile = dateFile;
    result.typeFile = typeFile;
    result.classFile = classFile;

    return result;
  }

}
