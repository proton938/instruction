import {IInstructionApprovalList, InstructionApprovalList} from './InstructionApprovalList';
import {DocumentBase} from '../core/DocumentBase';
import {IDocumentBase} from '../core/abstracts/IDocumentBase';

export interface IInstructionApproval extends IDocumentBase {
  approvalCycle: IInstructionApprovalList;
}

export class InstructionApproval extends  DocumentBase<IInstructionApproval, InstructionApproval> {
  approvalCycle: InstructionApprovalList = new InstructionApprovalList();

  build(i?: IInstructionApproval) {
    super.build(i);
    if (i) {
      this.approvalCycle.build(i.approvalCycle);
    }
  }

  public static create(num: number): InstructionApproval {
    const approval = new InstructionApproval();
    approval.approvalCycle.approvalCycleDate = new Date().toISOString();
    approval.approvalCycle.approvalCycleNum =  '' + num;
    return approval;
  }
}
