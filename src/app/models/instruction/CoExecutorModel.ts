import {UserBean} from '@reinform-cdp/nsi-resource';

export class CoExecutor {
    numberRepeat?: number = null; // Номер цикла повторяющегося поручения
    bpmTaskId?: string = ''; // ID задачи процесса
    reviewBy?: CoExecutorUser = new CoExecutorUser();
    factReviewBy?: CoExecutorUser = new CoExecutorUser();
    reviewFactDate?: Date = null;
    reviewNote?: string = '';
    fileId: string[] = [];

    constructor(i?: any) {
      this.numberRepeat = i && i.numberRepeat ? i.numberRepeat : null;
      this.bpmTaskId = i && i.bpmTaskId ? i.bpmTaskId : '';
      this.reviewBy = new CoExecutorUser(i ? i.reviewBy : null);
      this.factReviewBy = new CoExecutorUser(i ? i.factReviewBy : null);
      this.reviewFactDate = i && i.reviewFactDate ? i.reviewFactDate : null;
      this.reviewNote = i && i.reviewNote ? i.reviewNote : '';
      this.fileId = i && i.fileId ? i.fileId : [];
    }

    min() {
        return this;
    }

    isEmptyObject() {
        return !(this.reviewBy.login && this.reviewBy.fioFull);
    }
}

export class CoExecutorUser {
    login: string = '';
    fioFull: string = '';
    post?: string = '';

    constructor(i?: any) {
      if (i) {
        this.login = i.login || i.accountName;
        this.fioFull = i.fioFull || i.displayName;
        this.post = i.post || i.post;
      }
    }
}
