import {ExFileType} from "@reinform-cdp/widgets";

export interface IInstructionResultModel {
   reportFiles: ExFileType[];
   reportDate: Date;
   executorFIOFull: string;
   reportContent: string;
   acceptDate: Date;
   acceptStatus: string;
   acceptorFIOFull: string;
   acceptContent: string;
   acceptFiles: ExFileType[];
}
