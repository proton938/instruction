import {UserBean} from "@reinform-cdp/nsi-resource";

export class User {
    accountName: string;
    fio: string;
    post: string;
    email: string;
    phoneNumber: string;
    iofShort: string;
    fioShort?: string;
    department?: string;

    fioEmail?: string;
    fioPost?: string;
    fioPostPhone?: string;

    public static fromUserBean(userBean: UserBean) {
        return User.processUser({
            accountName: userBean.accountName,
            fio: userBean.displayName,
            post: userBean.post,
            email: userBean.mail,
            phoneNumber: userBean.telephoneNumber,
            iofShort: userBean.iofShort,
            fioShort: userBean.fioShort,
            department: userBean.departmentFullName
        });
    }

    public static processUser(user: User): User {
        user.fioEmail = `${user.fio} (${user.email ? user.email : ''})`;
        user.fioPost = `${user.fio} (${user.post ? user.post : ''})`;
        user.fioPostPhone = `${user.fio} (${user.post ? user.post : ''}, ${user.phoneNumber ? user.phoneNumber : ''})`;
        return user;
    }

    public static cleanUser(user: User): User {
        delete user.fioEmail;
        delete user.fioPost;
        delete user.fioPostPhone;
        return user;
    }
}