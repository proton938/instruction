import {NSIInstructionDifficulty} from '../nsi/NSIInstructionDifficulty';
import {NSIInstructionTheme} from '../nsi/NSIInstructionTheme';
import {NSIInstructionStatus} from '../nsi/NSIInstructionStatus';
import {NSIInstructionPriority} from '../nsi/NSIInstructionPriority';
import {DateRangeFilter} from '../DateRangeFilter';
import {NSIInstructionThematicHeading} from '../nsi/NSIInstructionThematicHeading';
import {formatDate} from '@angular/common';
import * as _ from 'lodash';

export class InstructionFilter {

  constructor() {}

  conditionInst: NSIInstructionStatus[] = [];
  creatorLogInst: any;
  assistantLogInst: any;
  docDateInst: DateRangeFilter = new DateRangeFilter();
  themeInst: NSIInstructionThematicHeading[] = [];
  subjectInst: NSIInstructionTheme[] = [];
  contentInst: string = '';
  descriptionInst: string = '';
  priorityInst: NSIInstructionPriority[] = [];
  difficultyInst: NSIInstructionDifficulty[] = [];
  planDateInst: DateRangeFilter = new DateRangeFilter();
  overduePlanDateInst: boolean = false;
  privateInst: boolean = false;
  executorLogInst: any;
  coExecutorsLogInst: any;
  forInformationLogin: any;
  docNumberInst: string = '';
  reportDateInst: DateRangeFilter = new DateRangeFilter();
  acceptDateInst: DateRangeFilter = new DateRangeFilter();
  repeatedSign: boolean = null;

  clear() {
    this.conditionInst = [];
    this.creatorLogInst = null;
    this.assistantLogInst = null;
    this.docDateInst.clear();
    this.themeInst = [];
    this.subjectInst = [];
    this.contentInst = '';
    this.descriptionInst = '';
    this.priorityInst = [];
    this.difficultyInst = [];
    this.planDateInst.clear();
    this.overduePlanDateInst = false;
    this.privateInst = false;
    this.executorLogInst = null;
    this.coExecutorsLogInst = null;
    this.forInformationLogin = null;
    this.docNumberInst = '';
    this.reportDateInst.clear();
    this.acceptDateInst.clear();
    this.repeatedSign = null;
  }

  isEmpty(): boolean {
    return this.conditionInst.length === 0 &&
      !this.creatorLogInst &&
      !this.assistantLogInst &&
      this.docDateInst.isEmpty() &&
      this.themeInst.length === 0 &&
      this.subjectInst.length === 0 &&
      this.contentInst === '' &&
      this.descriptionInst === '' &&
      this.priorityInst.length === 0 &&
      this.difficultyInst.length === 0 &&
      this.planDateInst.isEmpty() &&
      this.overduePlanDateInst === false &&
      this.privateInst === false &&
      !this.executorLogInst &&
      !this.coExecutorsLogInst &&
      !this.forInformationLogin &&
      this.docNumberInst === '' &&
      this.reportDateInst.isEmpty() &&
      this.acceptDateInst.isEmpty() &&
      !this.repeatedSign;
  }

  count(): number {
    return (this.conditionInst.length === 0 ? 0 : 1) +
      (this.creatorLogInst ? 1 : 0) +
      (this.assistantLogInst ? 1 : 0) +
      (this.docDateInst.isEmpty() ? 0 : 1) +
      (this.themeInst.length === 0 ? 0 : 1) +
      (this.subjectInst.length === 0 ? 0 : 1) +
      (this.contentInst === '' ? 0 : 1) +
      (this.descriptionInst === '' ? 0 : 1) +
      (this.priorityInst.length === 0 ? 0 : 1) +
      (this.difficultyInst.length === 0 ? 0 : 1) +
      (this.planDateInst.isEmpty() ? 0 : 1) +
      (this.overduePlanDateInst === false ? 0 : 1) +
      (this.privateInst === false ? 0 : 1) +
      (this.executorLogInst ? 1 : 0) +
      (this.coExecutorsLogInst ? 1 : 0) +
      (this.forInformationLogin ? 1 : 0) +
      (this.docNumberInst === '' ? 0 : 1) +
      (this.reportDateInst.isEmpty() ? 0 : 1) +
      (this.acceptDateInst.isEmpty() ? 0 : 1) +
      (!this.repeatedSign ? 0 : 1);
  }

  toString(login): string {
    let parts: string[] = [];

    if (this.conditionInst.length > 0) {
      parts.push(this.queryPart('statusCode', this.conditionInst.map(i => i.code), 'strict'));
    }

    if (this.creatorLogInst) {
      parts.push(this.queryPart(this.creatorLogInst.accountName ? 'creatorLogin' : 'creatorFioFull',
        this.creatorLogInst.accountName ? this.creatorLogInst.accountName : this.creatorLogInst, 'strict'));
    }

    if (this.assistantLogInst) {
      parts.push(this.queryPart(this.assistantLogInst.accountName ? 'assistantLogin' : 'assistantFioFull',
        this.assistantLogInst.accountName ? this.assistantLogInst.accountName : this.assistantLogInst, 'strict'));
    }

    if (!this.docDateInst.isEmpty()) {
      parts.push(this.queryPart('beginDate', this.docDateInst, 'dateRange'));
    }

    if (this.themeInst.length > 0) {
      parts.push(this.queryPart('themeName', this.themeInst.map(i => i.name), 'strict'));
    }

    if (this.subjectInst.length > 0) {
      parts.push(this.queryPart('subjectName', this.subjectInst.map(i => i.name), 'strict'));
    }

    if (this.contentInst) {
      parts.push(this.queryPart('content', this.contentInst));
    }

    if (this.docNumberInst) {
      parts.push(this.queryPart('instructionNumber', this.docNumberInst));
    }

    if (this.descriptionInst) {
      parts.push(this.queryPart('description', this.descriptionInst));
    }

    if (this.priorityInst.length > 0) {
      parts.push(this.queryPart('priorityName', this.priorityInst.map(i => i.name), 'strict'));
    }

    if (this.difficultyInst.length > 0) {
      parts.push(this.queryPart('difficultyName', this.difficultyInst.map(i => i.name), 'strict'));
    }

    if (!this.planDateInst.isEmpty()) {
      parts.push(this.queryPart('planDate', this.planDateInst, 'dateRange'));
    }

    if (this.privateInst) {
      parts.push(this.queryPart('confident', this.privateInst, 'boolean'));
    }

    if (this.overduePlanDateInst) {
      let today = new Date();
      let _yesterday = today.setDate(today.getDate() - 1);
      let yesterday = `${formatDate(_yesterday, 'yyyy-MM-dd', 'en')}T00:00:00Z`;
      parts.push(`((-factDate:[* TO *] AND planDate:[* TO ${yesterday}]) OR (factDate:[* TO *] AND overdueCloseInst:true))`);
    }

    if (this.executorLogInst) {
      parts.push(this.queryPart(this.executorLogInst.accountName ? 'executorLogin' : 'executorFioFull',
        this.executorLogInst.accountName ? this.executorLogInst.accountName : this.executorLogInst, 'strict'));
    }

    if (this.coExecutorsLogInst) {
      parts.push(this.queryPart(this.coExecutorsLogInst.accountName ? 'coExecutorLogin' : 'coExecutorFioFull',
        this.coExecutorsLogInst.accountName ? this.coExecutorsLogInst.accountName : this.coExecutorsLogInst, 'strict'));
    }

    if (this.forInformationLogin) {
      parts.push(this.queryPart(this.forInformationLogin.accountName ? 'forInformationLogin' : 'forInformationLogin',
        this.forInformationLogin.accountName ? this.forInformationLogin.accountName : this.forInformationLogin, 'strict'));
    }

    if (!this.reportDateInst.isEmpty()) {
      parts.push(this.queryPart('reportDate', this.reportDateInst, 'dateRange'));
    }

    if (!this.acceptDateInst.isEmpty()) {
      parts.push(this.queryPart('acceptDate', this.acceptDateInst, 'dateRange'));
    }

    if (this.repeatedSign) {
      parts.push(this.queryPart('repeatedSign', this.repeatedSign, 'boolean'));
    }

    parts = _.compact(parts); // Удаление возможных пустых записей
    const result = parts.join(' AND ');
    return parts.length > 1 ? `(${result})` : result;
  }

  queryPart(fieldName: string, value: any, type?: string): string {
    let result: string = '';
    const regExpDate = /\d{2}(.|\/)\d{2}(.|\/)\d{4}/;
    const makeItem = v => {
      let r = '';
      if (fieldName) {
        switch (type) {
          case 'date':
            let date: string;
            if (v) {
              if (v.getTime) {
                date = formatDate(v, 'yyyy-MM-dd', 'en');
              } else if (v.match(regExpDate)) {
                date = v.replace(/^(\d{2}).(\d{2}).(\d{4})/, '$3-$2-$1');
              }
              r = `${fieldName}:[${date}T00:00:00Z TO ${date}T23:59:59Z]`;
            }
            break;

          case 'dateRange':
            if (v) {
              let minDate = v.from ? `${formatDate(v.from, 'yyyy-MM-dd', 'en')}T00:00:00Z` : '*';
              let maxDate = v.to ? `${formatDate(v.to, 'yyyy-MM-dd', 'en')}T23:59:59Z` : '*';
              r = `${fieldName}:[${minDate} TO ${maxDate}]`;
            }
            break;

          case 'strong':
          case 'strict':
            r = `${fieldName}:("${this.escapingSpecialCharacters(v)}")`;
            break;

          case 'boolean':
            r = `${fieldName}:${v}`;
            break;

          default:
            r = `${fieldName}:(*${this.escapingSpecialCharacters(v).split(/\s+/).join('* AND *')}*)`;
        }
      }
      return r;
    };
    if (_.isArray(value)) {
      result = value.map(v => makeItem(v)).join(' OR ');
    } else {
      result = makeItem(value);
    }
    return result ? '(' + result + ')' : '';
  }

  private regExpSpecialCharacters(): any[] {
    return [/\\/g, /\+/g, /-/g, /\!/g, /\(/g, /\)/g, /:/g, /\^/g, /\[/g, /\]/g,
      /\{/g, /\}/g, /~/g, /\?/g, /\|/g, /&/g, /;/g, /\//g, /"/g];
  }

  private specialCharacters(): string [] {
    return ['\\\\', '\\+', '\\-', '\\!', '\\(', '\\)', '\\:', '\\^', '\\[', '\\]',
      '\\{', '\\}', '\\~', '\\?', '\\|', '\\&', '\\;', '\\/', '\\"'];
  }

  private escapingSpecialCharacters(value: string): string {
    let result = value;
    const regExpSpecialCharacters: string[] = this.regExpSpecialCharacters();
    const specialCharacters: string[] = this.specialCharacters();
    regExpSpecialCharacters.forEach((ch, index) => {
      result = result.replace(ch, specialCharacters[index]);
    });
    return result;
  }
}
