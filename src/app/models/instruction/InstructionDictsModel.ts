import {InstructionExecutor} from "./InstructionExecutor";
import {NSIInstructionPriority} from "../nsi/NSIInstructionPriority";
import {NSIInstructionDifficulty} from "../nsi/NSIInstructionDifficulty";
import {NSIInstructionStatus} from "../nsi/NSIInstructionStatus";
import {NSIInstructionTheme} from "../nsi/NSIInstructionTheme";
import {NSIThematicHeading} from "../nsi/NSIThematicHeading";
import {NSISettings} from "../nsi/NSISettings";
import {NSIPzzDeveloper} from "../nsi/NSIPzzDeveloper";
import {NSIMeetingTypes} from "../nsi/NSIMeetingTypes";
import {NSIRepeatPeriod} from '../nsi/NSIRepeatPeriod';
import {NSIWeekday} from '../nsi/NSIWeekday';
import {NSIMonth} from '../nsi/NSIMonth';
import {InstructionWeekNumber} from "./InstructionWeekNumber";

export class InstructionDictsModel {
   creators: InstructionExecutor[];
   executors: InstructionExecutor[];
   priorities: NSIInstructionPriority[];
   pzzDevelopers: NSIPzzDeveloper[];
   difficulties: NSIInstructionDifficulty[];
   statuses: NSIInstructionStatus[];
   themes: NSIThematicHeading[];
   meetingTypes: NSIMeetingTypes[];
   settings: NSISettings[];
   _themes: NSIInstructionTheme[];
   repeatPeriods: NSIRepeatPeriod[];
   weekdays: NSIWeekday[];
   months: NSIMonth[];
   weekNumbers: InstructionWeekNumber[];
}
