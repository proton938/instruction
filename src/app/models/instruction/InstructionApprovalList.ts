import {UserBean} from "@reinform-cdp/nsi-resource";
import {FileType} from './FileType';
import * as angular from 'angular';
import {IDocumentBase} from '../core/abstracts/IDocumentBase';
import {DocumentBase} from '../core/DocumentBase';
import * as _ from 'lodash';

export interface IInstructionApprovalAdditionalAgreed extends IDocumentBase {
  accountName?: string;
  fio?: string;
  iofShort?: string;
}

export interface IInstructionApprovalAdded extends IDocumentBase {
  additionalAgreed?: IInstructionApprovalAdditionalAgreed;
  addedNote?: string;
}

export interface IInstructionApprovalListItemSignature extends IDocumentBase {
  agreedDsLastName?: string;
  agreedDsName?: string;
  agreedDsPosition?: string;
  agreedDsCN?: string;
  agreedDsFrom?: string;
  agreedDsTo?: string;
}

export interface IInstructionApprover extends IDocumentBase {
  post?: string;
  fioFull?: string;
  accountName?: string;
  fioShort?: string;
  iofShort?: string;
  phone?: string;
  fioPost?: string;
}

export interface IInstructionApprovalListItem extends IDocumentBase {
  approvalNum?: string;
  approvalTime?: string;
  agreedBy?: IInstructionApprover;
  factAgreedBy?: IInstructionApprover;
  approvalPlanDate?: string;
  approvalFactDate?: string;
  approvalResult?: string;
  approvalType?: string;
  approvalTypeCode?: string;
  approvalNote?: string;
  fileApproval?: FileType[];
  agreedDs?: IInstructionApprovalListItemSignature;
  added?: {
    additionalAgreed: {
      accountName: string;
      fio: string;
      iofShort: string;
    }
    addedNote: string;
  }[];
}

export interface IInstructionApprovalList extends IDocumentBase {
  approvalCycleNum?: string;
  approvalCycleDate?: string;
  agreed?: IInstructionApprovalListItem[];
}

export interface IInstructionApprovalHistoryList extends IDocumentBase {
  approvalCycleNum?: string;
  approvalCycleDate?: string;
  agreed?: IInstructionApprovalListItem[];
  approvalCycleFile?: FileType[];
}

export interface IInstructionApprovalHistory extends IDocumentBase {
  approvalCycle?: IInstructionApprovalHistoryList[];
}

export class InstructionApprovalList extends DocumentBase<IInstructionApprovalList, InstructionApprovalList> {
  approvalCycleNum: string = '';
  approvalCycleDate: string = '';
  agreed: InstructionApprovalListItem[] = [];

  build(i?: IInstructionApprovalList) {
    super.build(i);
    if (i) {
      this.agreed = InstructionApprovalListItem.buildArray(i.agreed);
    }
  }
}

export class AddedInstructionApprovalListItem {
  approvalNum: string;
  approvalTime: string;
  agreedBy: InstructionApprover;
  approvalType: string;
  approvalTypeCode: string;
  note: string;
  approvalPlanDate: Date;
}

export class InstructionApprovalListItem extends DocumentBase<IInstructionApprovalListItem, InstructionApprovalListItem> {
  approvalNum: string = '';
  approvalTime: string = '';
  agreedBy: InstructionApprover = null;
  factAgreedBy: InstructionApprover = null;
  approvalPlanDate: Date = null;
  approvalFactDate: Date = null;
  approvalResult: string = '';
  approvalType: string = '';
  approvalTypeCode: string = '';
  approvalNote: string = '';
  fileApproval: FileType[] = [];
  agreedDs: InstructionApprovalListItemSignature = null;
  added: InstructionApprovalAdded[] = [];

  build(i?: IInstructionApprovalListItem) {
    super.build(i);
    if (i) {
      if (i.agreedBy) {
        this.agreedBy = new InstructionApprover();
        this.agreedBy.build(i.agreedBy);
      }
      if (i.factAgreedBy) {
        this.factAgreedBy = new InstructionApprover();
        this.factAgreedBy.build(i.factAgreedBy);
      }
      this.fileApproval = i.fileApproval || [];
      if (i.agreedDs) {
        this.agreedDs = new InstructionApprovalListItemSignature();
        this.agreedDs.build(i.agreedDs);
      }
      this.added = InstructionApprovalAdded.buildArray(i.added);
    }
  }

  static buildArray(i: IInstructionApprovalListItem[]): InstructionApprovalListItem[] {
    return (i && i.length) ? _.map(i, r => {
      let result = new InstructionApprovalListItem();
      result.build(r);
      return result;
    }) : [];
  }
}

export class InstructionApprovalAdditionalAgreed extends DocumentBase<IInstructionApprovalAdditionalAgreed, InstructionApprovalAdditionalAgreed> {
  accountName?: string = '';
  fio?: string = '';
  iofShort?: string = '';

  build(i?: IInstructionApprovalAdditionalAgreed) {
    super.build(i);
  }
}

export class InstructionApprovalAdded extends DocumentBase<IInstructionApprovalAdded, InstructionApprovalAdded> {
  additionalAgreed: InstructionApprovalAdditionalAgreed = new InstructionApprovalAdditionalAgreed();
  addedNote: string = '';

  build(i?: IInstructionApprovalAdded) {
    super.build(i);
    if (i) {
      this.additionalAgreed.build(i.additionalAgreed);
    }
  }

  static buildArray(i: IInstructionApprovalAdded[]): InstructionApprovalAdded[] {
    return (i && i.length) ? _.map(i, r => {
      let result = new InstructionApprovalAdded();
      result.build(r);
      return result;
    }) : [];
  }
}

export class InstructionApprover extends DocumentBase<IInstructionApprover, InstructionApprover> {
  post: string = '';
  fioFull: string = '';
  accountName: string = '';
  fioShort: string = '';
  iofShort: string = '';
  phone: string = '';
  fioPost: string = '';

  build(i?: IInstructionApprover) {
    super.build(i);
  }

  static fromUserBean(user: UserBean): InstructionApprover {
    let approver = new InstructionApprover();
    approver.build({
      post: user.post,
      fioFull: user.displayName,
      accountName: user.accountName,
      fioShort: user.fioShort,
      iofShort: user.iofShort,
      phone: user.telephoneNumber,
    });
    return InstructionApprover.processInstructionApprover(approver);
  }

  public static processInstructionApprover(approver: InstructionApprover): InstructionApprover {
    approver.fioPost = `${approver.fioFull} (${approver.post ? approver.post : ''})`;
    return approver;
  }

  public static cleanInstructionApprover(approver: InstructionApprover): InstructionApprover {
    delete approver.fioPost;

    if (!approver.post) {
      delete approver.post;
    }

    if (!approver.phone) {
      delete approver.phone;
    }

    return approver;
  }
}

export class InstructionApprovalHistory extends DocumentBase<IInstructionApprovalHistory, InstructionApprovalHistory> {
  approvalCycle: InstructionApprovalHistoryList[] = [];

  build(i?: IInstructionApprovalHistory) {
    super.build(i);
    if (i) {
      this.approvalCycle = InstructionApprovalHistoryList.buildArray(i.approvalCycle);
    }
  }
}

export class InstructionApprovalHistoryList extends DocumentBase<IInstructionApprovalHistoryList, InstructionApprovalHistoryList> {
  approvalCycleNum: string = '';
  approvalCycleDate: string = '';
  agreed: InstructionApprovalListItem[] = [];
  approvalCycleFile: FileType[] = [];

  build(i?: IInstructionApprovalHistoryList) {
    super.build(i);
    if (i) {
      this.agreed = InstructionApprovalListItem.buildArray(i.agreed);
      this.approvalCycleFile = i.approvalCycleFile || [];
    }
  }

  static buildArray(i: IInstructionApprovalList[]): InstructionApprovalHistoryList[] {
    return (i && i.length) ? _.map(i, r => {
      let result = new InstructionApprovalHistoryList();
      result.build(r);
      return result;
    }) : [];
  }
}

export class InstructionApprovalListItemSignature extends DocumentBase<IInstructionApprovalListItemSignature, InstructionApprovalListItemSignature> {
  agreedDsLastName: string = '';
  agreedDsName: string = '';
  agreedDsPosition: string = '';
  agreedDsCN: string = '';
  agreedDsFrom: Date = null;
  agreedDsTo: Date = null;

  build(i?: IInstructionApprover) {
    super.build(i);
  }
}
