import {NSIInstructionExecutors} from "./../nsi/NSIInstructionExecutors";
import {SessionStorage} from "@reinform-cdp/security";

export class ShowcaseMEMOTabBuilder {

   constructor(private executors: NSIInstructionExecutors[], private session: SessionStorage) {
   }

   build(): any[] {
      let result: any[] = [];

      let statuses = `(statusCodeMemo:send OR statusCodeMemo:review)`;

      if (true) {
         result.push({
            alias: 'memoAll',
            title: 'Служебные записки',
            filter: this.buildMemoAllFilter()
         });
      }

      if (true) {
         result.push({
            alias: 'memoFromMe',
            title: 'Направленные мной',
            filter: `((statusCodeMemo:send OR statusCodeMemo:review) AND (creatorLogMemo:${this.session.login()} OR assistantLogMemo:${this.session.login()}))`
         });
      }

      if (true) {
         result.push({
            alias: 'memoForMe',
            title: 'Направленные мне',
            filter: `((statusCodeMemo:send OR statusCodeMemo:review) AND executorLogMemo:${this.session.login()})`
         });
      }

      if (this.showMemoMyBoss()) {
         result.push({
            alias: 'memoFromMyBoss',
            title: 'Направленные моим руководителем',
            filter: `((statusCodeMemo:send OR statusCodeMemo:review) AND ${this.memoFromMyBoss()})`
         });
         result.push({
            alias: 'memoForMyBoss',
            title: 'Направленные моему руководителю',
            filter: `((statusCodeMemo:send OR statusCodeMemo:review) AND ${this.memoForMyBoss()})`
         });
      }

      if (true) {
         result.push({
            alias: 'memoClosed',
            title: 'Рассмотренные',
            filter: `((statusCodeMemo:instruction OR statusCodeMemo:viewed) AND ${this.memoClosed()})`
         });
      }

      if (true) {

         result.push({
            alias: 'assistantLogMemo',
            title: 'По подразделению',
            filter: `creatorLogMemo:${this.session.login()} OR assistantLogMemo:${this.session.login()}`
         });
      }

      return result;
   }

   private buildMemoAllFilter(): string {
      return '(statusCodeMemo:send OR statusCodeMemo:viewed)';
   }

   private showMemoMyBoss(): boolean {
      let login = this.session.login().trim();
      let show = false;
      this.executors.forEach(e => {
         if (e.assistantview && e.assistantview.length) {
            e.assistantview.forEach(a => {
               if (a.trim() === login) {
                  show = true;
               }
            })
         }
      });

      return show;
   }

   private memoFromMyBoss(): string {
      let creators: string[] = [];
      let login = this.session.login().trim();

      this.executors.forEach(e => {
         if (e.assistantview && e.assistantview.length) {
            e.assistantview.forEach(a => {
               if (a.trim() === login) {
                  creators.push(`creatorLogMemo:${e.creator}`);
               }
            })
         }
      });

      return (creators.length > 0) ? `(${creators.join(' OR ')})` : '';
   }

   private memoForMyBoss(): string {
      let creators: string[] = [];
      let login = this.session.login().trim();

      this.executors.forEach(e => {
         if (e.assistantview && e.assistantview.length) {
            e.assistantview.forEach(a => {
               if (a.trim() === login) {
                  creators.push(`executorLogMemo:${e.creator}`);
               }
            })
         }
      });

      return (creators.length > 0) ? `(${creators.join(' OR ')})` : '';
   }

   private memoClosed(): string {
      let memoFromMyBoss = this.memoFromMyBoss();
      memoFromMyBoss = (memoFromMyBoss) ? `OR ${memoFromMyBoss}` : '';
      let memoForMyBoss = this.memoForMyBoss();
      memoForMyBoss = (memoForMyBoss) ? `OR ${memoForMyBoss}` : '';
      return `(creatorLogMemo:${this.session.login()} OR executorLogMemo:${this.session.login()} OR assistantLogMemo:${this.session.login()} ${this.memoFromMyBoss()} ${this.memoForMyBoss()})`.trim();
   }
}
