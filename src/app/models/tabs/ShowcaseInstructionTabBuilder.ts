import {NSIInstructionExecutors} from "./../nsi/NSIInstructionExecutors";
import {AuthorizationService, SessionStorage} from "@reinform-cdp/security";

export class InstructionShowcaseTabModel {
  alias: string;
  title: string;
  filter: string;

  constructor(alias: string, title: string, filter: string) {
    this.alias = alias;
    this.title = title;
    this.filter = filter;
  }
}


export class InstructionShowcaseTabBuilderFactory {
  static getBuilder(id: string) {
    if (id === 'mka') {
      return new MkaShowcaseInstructionTabBuilder();
    } else {
      return new GeneralShowcaseInstructionTabBuilder();
    }
  }
}

export interface ShowcaseInstructionTabBuilder {
  build(authorizationService: AuthorizationService, session: SessionStorage, executors: NSIInstructionExecutors[]): InstructionShowcaseTabModel[];
}

export class MkaShowcaseInstructionTabBuilder {

  constructor() {}

  build(authorizationService: AuthorizationService, session: SessionStorage): InstructionShowcaseTabModel[] {
    let result: InstructionShowcaseTabModel[] = [];

    if (session.groups() && session.hasPermission('INSTRUCTION_MKA')) {
      result.push({
        alias: 'assignmentsMKA',
        title: 'Все поручения МКА',
        filter: '(receivedFromMKA:true)'
      });
    }

    return result;
  }
}

export class GeneralShowcaseInstructionTabBuilder implements ShowcaseInstructionTabBuilder{

   constructor() {}

   build(authorizationService: AuthorizationService, session: SessionStorage, executors: NSIInstructionExecutors[]): InstructionShowcaseTabModel[] {
      let result: InstructionShowcaseTabModel[] = [];

      let statuses = `(statusCode:inwork OR statusCode:check OR statusCode:assign)`;

      if (authorizationService.check('INSTRUCTION_INSTRUCTION_VIEW_ALL')) {
         result.push({
            alias: 'assignmentsAll',
            title: 'Все поручения',
            filter: this.buildAssignmentsAllFilter(session)
         });
      }

      if (true) {
         result.push({
            alias: 'assignmentsFromMe',
            title: 'Назначенные мной',
            filter: `((${statuses} AND creatorLogin:${session.login()}) OR (${statuses} AND confident:false AND assistantLogin:${session.login()})) AND -receivedFromMKA:true`
         });
      }

      if (true) {
         result.push({
            alias: 'assignmentsForMe',
            title: 'Порученные мне',
            filter: `((${statuses} AND (executorLogin:${session.login()} OR forInformationLogin:${session.login()} OR coExecutorLogin:${session.login()})) OR (confident:false AND ${statuses} AND (forInformationLogin:${session.login()} OR coExecutorLogin:${session.login()})) OR (confident:true AND (coExecutorLogin:${session.login()} OR forInformationLogin:${session.login()}) OR -statusCode:closed OR -statusCode:refused OR -statusCode:notExecuted )) AND -receivedFromMKA:true`
         });
      }


      if (this.ifAssistantView(session, executors)) {
         if (this.assistantCreators(session, executors)) {
            result.push({
               alias: 'assignmentsFromMyBoss',
               title: 'Порученные моим руководителем',
               filter: `${statuses} AND confident:false ${this.assistantCreators(session, executors)} AND -receivedFromMKA:true`.trim()
            });
         }
         if (this.assistantExecutors(session, executors)) {
            result.push({
               alias: 'assignmentsForMyBoss',
               title: 'Порученные моему руководителю',
               filter: `${statuses} AND confident:false AND (${this.assistantExecutors(session, executors)} OR ${this.coExecutorLogin(session, executors)}) AND -receivedFromMKA:true`.trim()
            });
         }
      }

      let statusesClosed = `(statusCode:closed OR statusCode:refused OR statusCode:notExecuted)`;

      if (true) {
    //   if (this.session.permissions() && this.session.hasPermission('INSTRUCTION_REESTER_CLOSED')) {
         let filter1 = `(${statusesClosed} AND creatorLogin:${session.login()})`;
         let filter2 = `(${statusesClosed} AND executorLogin:${session.login()})`;
         let filter3 = `(${statusesClosed} AND assistantLogin:${session.login()} AND confident:false)`;
         let filter4 = (this.assistantCreators(session, executors)) ? `OR (${statusesClosed} AND confident:false ${this.assistantCreators(session, executors)})`.trim() : '';
         let filter5 = (this.assistantExecutors(session, executors)) ? `OR (${statusesClosed} AND confident:false ${this.assistantExecutors(session, executors)})`.trim() : '';
         result.push({
            alias: 'assignmentsClosed',
            title: 'Закрытые',
            filter: `(${filter1} OR ${filter2} OR ${filter3} ${filter4} ${filter5}  OR ( confident:true AND ( statusCode:closed  OR  statusCode:refused  OR  statusCode:notExecuted )  AND  ( coExecutorLogin:${session.login()} OR forInformationLogin:${session.login()} ) )  ) AND -receivedFromMKA:true`.trim()
         });
      }

     if (authorizationService.check('INSTRUCTION_INSTRUCTION_DRAFT')) {
         let statusesDraft = `(statusCode:draft)`;
         result.push({
            alias: 'assignmentsDraft',
            title: 'Черновики',
            filter: `(${statusesDraft}) AND -receivedFromMKA:true`.trim()
         });
      }

      return result;
   }

   private ifAssistantView(session: SessionStorage, executors: NSIInstructionExecutors[]): boolean {
      let result = false;

      executors.forEach(e => {
         if (e.assistantview && e.assistantview.length && e.assistantview.length > 0) {
            e.assistantview.forEach(v => {
               if (session.login() === v) {
                  result = true;
               }
            });
         }
      });

      return result;
   }

   private buildAssignmentsAllFilter(session: SessionStorage): string {
      if (session.groups() && session.hasPermission('OASI_INSTRUCTION_ADMIN')) {
         return `(statusCode:inwork OR statusCode:check OR statusCode:assign OR statusCode:closed OR statusCode:refused OR statusCode:notExecuted)`;
      } else {
         let first = `-creatorLogin:${session.login()} AND -executorLogin:${session.login()} AND confident:false AND (statusCode:inwork OR statusCode:check OR statusCode:assign OR statusCode:closed OR statusCode:refused OR statusCode:notExecuted)`;
         let second = `(${session.login()} OR forInformationLogin:${session.login()} OR coExecutorLogin:${session.login()} ) AND (statusCode:inwork OR statusCode:check OR statusCode:assign OR statusCode:closed OR statusCode:refused OR statusCode:notExecuted)`;
         let all = `(statusCode:inwork OR statusCode:check OR statusCode:assign OR statusCode:closed OR statusCode:refused OR statusCode:notExecuted)`;
        return `((confident:false AND ${all})  OR (confident:true AND (creatorLogin:${session.login()} OR executorLogin: ${session.login()} OR coExecutorLogin:${session.login()} OR forInformationLogin:${session.login()}))) AND -receivedFromMKA:true`;
      }
   }

   private assistantCreators(session: SessionStorage, executors: NSIInstructionExecutors[]): string {
      let result = [];

      executors.forEach(e => {
         if (e.assistantview && e.assistantview.length && e.assistantview.length > 0) {
            e.assistantview.forEach(v => {
               if (session.login() === v) {
                  result.push(`creatorLogin:${e.creator}`);
               }
            });
         }
      });

      return this.toString(result);
   }

   private assistantExecutors(session: SessionStorage, executors: NSIInstructionExecutors[]): string {
      let result = [];

      executors.forEach(e => {
         if (e.assistantview && e.assistantview.length && e.assistantview.length > 0) {
            e.assistantview.forEach(v => {
               if (session.login() === v) {
                  result.push(`executorLogin:${e.creator}`);
               }
            });
         }
      });

      return this.toString(result);
   }

   private coExecutorLogin(session: SessionStorage, executors: NSIInstructionExecutors[]): string {
      let result = [];

      executors.forEach(e => {
         if (e.assistantview && e.assistantview.length && e.assistantview.length > 0) {
            e.assistantview.forEach(v => {
               if (session.login() === v) {
                  result.push(`coExecutorLogin:${e.creator}`);
                  result.push(`forInformationLogin:${e.creator}`);
               }
            });
         }
      });
      return this.toString(result);
   }

   private toString(values: string[]): string {
      let str = values.join(' OR ');
      return str;
   }
}
