import {DocumentBase} from "./DocumentBase";
import {DocumentUser} from './DocumentUser';
import {IDocumentComment} from "./abstracts/IDocumentComment";
import * as _ from 'lodash';

// @dynamic
export class DocumentComment extends DocumentBase<IDocumentComment, DocumentComment> {
  commentDate: Date = null; // дата комментария
  commentUser: DocumentUser = new DocumentUser(); // пользователь, оставивший комментарий
  commentText: string = ''; // текст комментария

  build(i?: IDocumentComment) {
    super.build(i);
    this.commentUser.build(i.commentUser);
  }

  static buildArray(comments: IDocumentComment[]): DocumentComment[] {
    return (comments && comments.length) ? _.map(comments, u => {
      let comment = new DocumentComment();
      comment.build(u);
      return comment;
    }) : [];
  }
}
