import {DocumentBase} from './DocumentBase';
import {IDocumentStatus} from "./abstracts/IDocumentStatus";

export class DocumentStatus extends DocumentBase<IDocumentStatus, DocumentStatus> {
  code: string = ''; // код статуса
  name: string = ''; // наименование
  color: string = ''; // стиль статуса
}
