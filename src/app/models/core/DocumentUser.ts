import {IDocumentUser} from "../core/abstracts/IDocumentUser";
import {DocumentBase} from "./DocumentBase";
import * as _ from 'lodash';
import {UserBean} from "@reinform-cdp/nsi-resource";

export class UserDepartment {
  code: string;
  name: string;
}

// @dynamic
export class DocumentUser extends DocumentBase<IDocumentUser, DocumentUser>{
  post: string = '';
  login: string = '';
  fioFull: string = '';
  phoneNumber: string = '';
  email: string = '';
  department: UserDepartment;

  updateFromUserBean(user: UserBean) {
    if (user) {
      this.post = user.post;
      this.login = user.accountName;
      this.fioFull = user.displayName;
      this.phoneNumber = user.telephoneNumber;
      this.email = user.mail;
      this.department = {
        code: user.departmentCode,
        name: user.departmentFullName,
      };
    }
  }

  toUserBean(): UserBean {
    let user = new UserBean();
    user.accountName = this.login;
    user.post = this.post;
    user.displayName = this.fioFull;
    user.mail = this.email;
    user.telephoneNumber = this.phoneNumber;
    user.departmentCode = this.department ? this.department.code : null;
    user.departmentFullName = this.department ? this.department.name : null;

    return user;
  }

  static buildArray(users: IDocumentUser[]): DocumentUser[] {
    return (users && users.length) ? _.map(users, u => {
      let user = new DocumentUser();
      user.build(u);
      return user;
    }) : [];
  }
}
