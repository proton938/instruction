import {IDocumentBase} from "./IDocumentBase";

export interface IDocumentFromMKA extends IDocumentBase {
  FromMKA?: boolean;
  receivedDate?: Date;
}
