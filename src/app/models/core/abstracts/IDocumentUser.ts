import {IDocumentBase} from "./IDocumentBase";

export interface IDocumentUser extends IDocumentBase {
  post?: string;
  login?: string;
  fioFull?: string;
}
