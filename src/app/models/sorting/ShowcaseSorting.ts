export class ShowcaseSorting {
  code: string;
  value: string;
  name: string;
  width: any;
  wrap: boolean;

  constructor(code: string, value: string, name: string, width: any, wrap: boolean) {
    this.code = code;
    this.value = value;
    this.name = name;
    this.width = width;
    this.wrap = wrap;
  }
}
