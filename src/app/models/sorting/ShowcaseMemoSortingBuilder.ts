import {ShowcaseSorting} from "./ShowcaseSorting";

export class ShowcaseMemoSortingBuilder {

  build(): ShowcaseSorting[] {
    let result: ShowcaseSorting[] = [];

    result.push(new ShowcaseSorting('statusMemo', 'none', 'Статус', {}, false));
    result.push(new ShowcaseSorting('docDateMemo', 'desc', 'Дата', {}, false));
    result.push(new ShowcaseSorting('docNumberMemo', 'none', 'Номер', {'min-width': '150px'}, false));
    result.push(new ShowcaseSorting('creatorMemo', 'none', 'От кого', {}, false));
    result.push(new ShowcaseSorting('contentMemo', 'none', 'Тема', {'min-width': '150px'}, false));
    result.push(new ShowcaseSorting('executorMemo', 'none', 'Кому', {}, false));

    return result;
  }
}
