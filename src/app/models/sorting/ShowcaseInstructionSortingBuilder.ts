import {ShowcaseSorting} from "./ShowcaseSorting";

export class ShowcaseInstructionSortingBuilder {

  build(): ShowcaseSorting[] {
    let result: ShowcaseSorting[] = [];

    result.push(new ShowcaseSorting('statusName', 'none', 'Статус', {}, false));
    result.push(new ShowcaseSorting('beginDate', 'desc', 'Дата', {}, false));
    result.push(new ShowcaseSorting('instructionNumber', 'none', 'Номер', {'min-width': '150px'}, false));
    result.push(new ShowcaseSorting('creatorFioFull', 'none', 'Автор', {}, false));
    result.push(new ShowcaseSorting('content', 'none', 'Содержание', {'min-width': '150px'}, false));
    result.push(new ShowcaseSorting('executorFioFull', 'none', 'Ответственный', {}, false));
    result.push(new ShowcaseSorting('priorityName', 'none', 'Приоритет', {}, false));
    result.push(new ShowcaseSorting('planDate', 'none', 'Исполнение (план)', {}, true));
    result.push(new ShowcaseSorting('reportDate', 'none', 'Исполнение (факт)', {}, true));
    result.push(new ShowcaseSorting('acceptDate', 'none', 'Дата принятия', {}, false));
   // result.push(new ShowcaseSorting('acceptStatus', 'none', 'Статус принятия', {}, false));

    return result;
  }
}
