export interface IJsonPatchData {
    op: string;
    path: string;
    value: any;
}