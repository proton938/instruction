import * as moment_ from 'moment';
const moment = moment_;
import * as _ from 'lodash';

export class ShowcaseField {
  code = '';
  type: string = 'text';
  name = '';
  isArray = false;
  dictionary: string = '';
  dictionaryCode = '';
  dictionaryText = '';
  hierarchical = false;
  valueOnLeaves = true;
  sortable = false;
  filterable = false;
  defaultSorting: string = 'NONE';
  isSearchable = false;
  permissions: string[] = [];
  expanded = false;
  dict: any;
  searchRule = '';

  constructor(i?: any) {
    if (i) {
      this.code = i.code;
      this.type = i.type;
      this.name = i.name;
      if (_.isBoolean(i.isArray))       { this.isArray = i.isArray; }
      if (_.isString(i.dictionary))     { this.dictionary = i.dictionary; }
      if (_.isString(i.dictionaryCode)) { this.dictionaryCode = i.dictionaryCode; }
      if (_.isString(i.dictionaryText)) { this.dictionaryText = i.dictionaryText; }
      if (_.isBoolean(i.hierarchical))  { this.hierarchical = i.hierarchical; }
      if (_.isBoolean(i.valueOnLeaves)) { this.valueOnLeaves = i.valueOnLeaves; }
      if (_.isBoolean(i.sortable))      { this.sortable = i.sortable; }
      if (_.isBoolean(i.filterable))    { this.filterable = i.filterable; }
      if (_.isString(i.defaultSorting)) { this.defaultSorting = i.defaultSorting; }
      if (_.isBoolean(i.isSearchable))  { this.isSearchable = i.isSearchable; }
      if (_.isArray(i.permissions))     { this.permissions = i.permissions; }
      if (_.isString(i.searchRule))     { this.searchRule = i.searchRule; }
    }
  }
}

export class ShowcaseFilter {
  code = '';
  name = '';
  field: ShowcaseField = null;
  multiple = false;
  template: string = '';
  placeholder = '';
  usingDictionaryLinks = false;
  expanded = false;
  dict = null;
  placement = 'bottom';
  loginFromStorage = false;
  notEmpty = false;
  presetFilter = '';
  userprofileFilter = '';
  strictMode = false;
  searchPartOfText = false;

  constructor(i?: any, fields?: ShowcaseField[]) {
    if (i) {
      this.code = i.code;
      this.name = i.name;
      if (fields) { this.field = _.find(fields, f => f.code === i.code); }
      if (_.isBoolean(i.multiple)) { this.multiple = i.multiple; }
      if (_.isString(i.template)) { this.template = i.template; }
      if (_.isString(i.placeholder)) { this.placeholder = i.placeholder; }
      if (_.isBoolean(i.usingDictionaryLinks)) { this.usingDictionaryLinks = i.usingDictionaryLinks; }
      if (_.isString(i.placement)) { this.placement = i.placement; }
      if (_.isBoolean(i.loginFromStorage)) { this.loginFromStorage = i.loginFromStorage; }
      if (_.isBoolean(i.notEmpty)) { this.notEmpty = i.notEmpty; }
      if (_.isString(i.userprofileFilter)) { this.userprofileFilter = i.userprofileFilter; }
      if (_.isString(i.presetFilter)) { this.presetFilter = i.presetFilter; }
      if (_.isBoolean(i.strictMode)) { this.strictMode = i.strictMode; }
      if (_.isBoolean(i.searchPartOfText)) { this.searchPartOfText = i.searchPartOfText; }
    }
  }
}

export class ShowcaseTableColumn {
  code = '';
  name = '';
  width = '';
  field: ShowcaseField = null;
  template = '';
  sortable = false;
  filterable = false;
  visible = true;
  expanded = false;
  filter: ShowcaseFilter = null;

  constructor(i?: any, fields?: ShowcaseField[]) {
    if (i) {
      this.code = i.code;
      this.name = i.name;
      if (_.isString(i.width)) { this.width = i.width; }
      if (fields) {
        const f = _.find(fields, ff => ff.code === i.code);
        if (f) { this.field = f; }
      } else if (i.field) {
        this.field = i.field;
      }
      if (_.isString(i.template)) { this.template = i.template; }
      if (_.isBoolean(i.sortable)) { this.sortable = i.sortable; }
      if (_.isBoolean(i.filterable)) { this.filterable = i.filterable; }
      if (_.isBoolean(i.visible)) { this.visible = i.visible; }
    }
  }
}

export class ShowcaseSettingsDeep {
  theme = 'blue';
  fields: ShowcaseField[] = [];
  filters: ShowcaseFilter[] = [];
  defaultFilters: Object = {};
  defaultView: string = 'table';
  tableSorting = true;
  separateSorting = false;
  tableFilter = false;
  separateFilter = true;
  disableCommonSearch = false;
  placeholderForCommonSearch = '';
  numberOfElementPerPage: number = null;
  changeNumberOfElementsPerPage = false;
  isNewSearch = true;
  tableColumns: ShowcaseTableColumn[] = [];

  constructor(i?: any, dicts?: any[]) {
    if (i.theme) { this.theme = i.theme; }
    this.fields = _.map(i.fields, f => new ShowcaseField(f));
    if (_.isArray(i.filters)) {
      this.filters = _.map(i.filters, f => new ShowcaseFilter(f, this.fields));
    }
    if (_.isObject(i.defaultFilters)) { this.defaultFilters = i.defaultFilters; }
    if (_.isString(i.defaultView)) { this.defaultView = i.defaultView; }
    if (_.isBoolean(i.tableSorting)) { this.tableSorting = i.tableSorting; }
    if (_.isBoolean(i.separateSorting)) { this.separateSorting = i.separateSorting; }
    if (_.isBoolean(i.tableFilter)) { this.tableFilter = i.tableFilter; }
    if (_.isBoolean(i.separateFilter)) { this.separateFilter = i.separateFilter; }
    if (_.isBoolean(i.disableCommonSearch)) { this.disableCommonSearch = i.disableCommonSearch; }
    if (_.isString(i.placeholderForCommonSearch)) { this.placeholderForCommonSearch = i.placeholderForCommonSearch; }
    if (_.isNumber(i.numberOfElementPerPage)) { this.numberOfElementPerPage = i.numberOfElementPerPage; }
    if (_.isBoolean(i.changeNumberOfElementsPerPage)) { this.changeNumberOfElementsPerPage = i.changeNumberOfElementsPerPage; }
    if (_.isBoolean(i.isNewSearch)) { this.isNewSearch = i.isNewSearch; }
    if (_.isArray(i.tableColumns)) {
      this.tableColumns = _.map(i.tableColumns, c => new ShowcaseTableColumn(c, this.fields));
      this.tableColumns.forEach(i => {
        if (i.code && i.filterable && i.field) {
          i.filter = _.find(this.filters, f => f.code === i.code);
        }
      });
    }
  }
}

export interface IShowcaseSettingsConfig {
  document: string; // Код документа
  doNotUseLocalstorage?: boolean; // Отключение localstorage
  filters?: any[]; // Предустановленные фильтры
}

export class ShowcaseSettings {
  commonSearch = '';
  documentCode: string;
  dicts: any = {};
  pagination = {
    totalItems: 0,
    itemsPerPage: 20,
    currentPage: 1
  };
  selectedFilters = {};
  loading: boolean;
  selectedFiltersNumber = 0;
  settings: ShowcaseSettingsDeep = null;
  config: IShowcaseSettingsConfig;

  constructor(i?: any, dicts?: any[]) {
    this.documentCode =   i ? i.documentCode  : '';
    this.settings = i ? new ShowcaseSettingsDeep(i.settings, dicts) : new ShowcaseSettingsDeep();
    this.pagination.itemsPerPage = this.settings.numberOfElementPerPage;
  }

  sortingsToString(): string {
    let result = '';
    this.settings.fields.forEach(f => {
      let fieldName: string = f.type === 'DICT' ? f.code + 'Value' : f.code;
      if (f.defaultSorting !== 'NONE') {
        result = `${fieldName} ${f.defaultSorting.toLowerCase()}`;
      }
    });
    result = result ? result : undefined;
    return result;
  }

  filtersToString(): string {
    let result = '';
    let countLoginFilters = this.settings.filters.filter(f => f.loginFromStorage).length;
    let i = 0;
    let resultLoginFilters = '';
    this.presetFilters();
    this.settings.filters.forEach(f => {
      if (f.multiple) {
        if (f.field.type === 'dictionary') {
          if (f.notEmpty) {
            result += result ? ' AND ' : '';
            result += `(${f.field.code}:[* TO *])`;
          } else if (this.selectedFilters[f.field.code].length > 0) {
            result += result ? ' AND ' : '';
            result += '(';
            this.selectedFilters[f.field.code].forEach((v, index, array) => {
              if (this.settings.isNewSearch) {
                if ((f.presetFilter || f.userprofileFilter) && _.isArray(v) ) {
                  result += '(' + v.map( (vv, index) => {
                    return f.searchPartOfText ? `(${f.field.code}:*${vv}*)` : f.strictMode ? `(${f.field.code}:${vv})` : `(${f.field.code}:\"${this.escapingSpecialCharacters(vv)}\")`
                  }).join(", ") + ')';
                } else {
                  result += f.searchPartOfText ? `(${f.field.code}:*${v}*)` : f.strictMode ? `(${f.field.code}:${v})` : `(${f.field.code}:\"${this.escapingSpecialCharacters(v)}\")`;
                }

              } else {
                if ((f.presetFilter || f.userprofileFilter) && _.isArray(v) ) {
                  result += '(' + v.map( (vv, index) => {
                    let value = this.getValueForOldSolr(vv);
                    return f.searchPartOfText ? `(${f.field.code}:*${value}*)` : f.strictMode ? `(${f.field.code}:${value})` : `(${f.field.code}:\"${this.escapingSpecialCharacters(value)}\")`
                  }).join(", ") + ')';
                } else {
                  let value = this.getValueForOldSolr(v);
                  result += f.searchPartOfText ? `(${f.field.code}:*${value}*)` : f.strictMode ? `(${f.field.code}:${value})` : `${f.field.code}:(${this.escapingSpecialCharacters(value)})`; //в старом SOLR не ищет фразу с кавычками
                }
              }
              result += (index < (array.length - 1)) ? ` OR ` : '';
            });
            result += ')';
          }
        } else {
          if (f.notEmpty) {
            result += result ? ' AND ' : '';
            result += `(${f.field.code}:[* TO *])`;
          } else if (this.selectedFilters[f.field.code].length > 0) {
            result += result ? ' AND ' : '';
            result += this._multipleValue(f.field.code, this.selectedFilters[f.field.code]);
          }
        }
      } else {
        if (f.notEmpty) {
          result += result ? ' AND ' : '';
          result += `(${f.field.code}:[* TO *])`;
        } else if ( f.loginFromStorage  &&
          _.isString(this.selectedFilters[f.field.code]) ) { // если ищем по логину, то поиск с OR, а не с AND
          resultLoginFilters += ( i == 0 ) ? ' ( ' : ' OR ';
          resultLoginFilters += this._onceValue(f.field.code, this.selectedFilters[f.field.code], false);
          resultLoginFilters += ( i == countLoginFilters - 1 ) ? ' ) ' : '';
          i++;
        } else if ( (f.presetFilter || f.userprofileFilter) && _.isArray(this.selectedFilters[f.field.code]) && f.field.type !== 'date') {
          result += result ? ' AND ' : '';
          result += '(' + this.selectedFilters[f.field.code].map( (vv, index) => {
            return f.searchPartOfText ? `(${f.field.code}:*${vv}*)` :
              f.strictMode ? `(${f.field.code}:${vv})` : `(${f.field.code}:"${this.escapingSpecialCharacters(vv)}")`
          }).join(" OR ") + ')';
        } else if ( (f.presetFilter || f.userprofileFilter) && _.isArray(this.selectedFilters[f.field.code]) && f.field.type === 'date') {
          result += result ? ' AND ' : '';
          result += '(' + this.selectedFilters[f.field.code].map( (vv, index) => {
            return this.dataToString(vv, f)
          }).join(" OR ") + ')';
        } else if ((f.field.type === 'text' || f.field.type === 'user') &&
          _.isString(this.selectedFilters[f.field.code]) &&
          this.selectedFilters[f.field.code].trim()) {
          result += result ? ' AND ' : '';
          result += this._onceValue(f.field.code, this.selectedFilters[f.field.code], false);
        }
        if ((f.field.type === 'dictionary') &&
          _.isString(this.selectedFilters[f.field.code]) &&
          this.selectedFilters[f.field.code].trim()) {
          result += result ? ' AND ' : '';
          if (this.settings.isNewSearch) {
            result += `(${f.field.code}:\"${this.escapingSpecialCharacters(this.selectedFilters[f.field.code])}\")`;
          } else {
            result += `(${f.field.code}:\\"${this.escapingSpecialCharacters(this.selectedFilters[f.field.code])}\\")`;
          }
        }
        if (f.field.type === 'number' && this.selectedFilters[f.field.code] && this.isNumber(this.selectedFilters[f.field.code]) ) {
          result += result ? ' AND ' : '';
          result += this._onceNumberValue(f.field.code, this.selectedFilters[f.field.code].toString());
        }
        if ((f.field.type === 'DICT') &&
          _.isString(this.selectedFilters[f.field.code]) &&
          this.selectedFilters[f.field.code].trim()) {
          result += result ? ' AND ' : '';
          result += this._onceValue(f.field.code+"Value", this.selectedFilters[f.field.code], false);
        }
        if (f.field.type === 'boolean' && _.isBoolean(this.selectedFilters[f.field.code])) {
          result += result ? ' AND ' : '';
          result += this._onceValue(f.field.code, this.selectedFilters[f.field.code].toString());
        }
        if (f.field.type === 'date' && _.isArray(this.selectedFilters[f.field.code]) && !(f.presetFilter || f.userprofileFilter)) {
          result += result ? ' AND ' : '';
          const formatedMinDate = (this.selectedFilters[f.field.code][0]) ?
            `${moment(this.selectedFilters[f.field.code][0]).format('YYYY-MM-DD')}T00:00:00Z` :
            '*';
          const formatedMaxDate = (this.selectedFilters[f.field.code][1]) ?
            `${moment(this.selectedFilters[f.field.code][1]).format('YYYY-MM-DD')}T00:00:00Z` :
            '*';
          result += `(${f.field.code}:[${formatedMinDate} TO ${formatedMaxDate}])`;
        }
        if (f.field.type === 'DOCNUMBER' && _.isString(this.selectedFilters[f.field.code])) {
          result += result ? ' AND ' : '';
          result += this._docNumberValue(f.field.code, this.selectedFilters[f.field.code]);
        }
      }
    });
    result = (resultLoginFilters) ? (result ) ?  result + ' AND ' + resultLoginFilters : resultLoginFilters : result ;
    result = (result) ? result : undefined;
    /*if (this.currentTab) {
      result = (result) ? `(${result}) AND (${this.currentTab.query})` : <string>this.currentTab.query;
    }*/
    if (this.commonSearch.trim()) {
      const common = this.filtersCommonToString(this.commonSearch);
      if (result) {
        result = `(${result}) AND (${common})`;
      } else {
        result = `${common}`;
      }
    }
    return result;
  }

  filtersCommonToString(query): string {
    let result = '';
    query = query.trim();
    let filters = this.settings.filters.some(i => i.field.isSearchable) ? this.settings.filters.filter(i => i.field.isSearchable) : this.settings.filters;
    filters.forEach((f) => {
      switch (f.field.type) {
        case 'date':
          if (query.match(/^(\d{2})[.\/](\d{2})[.\/](\d{4})/) && (moment(query,'DD.MM.YYYY', true).isValid() || moment(query,'MM/DD/YYYY', true).isValid())  ) {
            result += result ? ' OR ' : '';
            const formatedDate = `${moment(query.replace(/^(\d{2})[.\/](\d{2})[.\/](\d{4})/, '$3-$2-$1')).format('YYYY-MM-DD')}T00:00:00Z`;
            result += `(${f.field.code}:[${formatedDate} TO ${formatedDate}])`;
          }
          break;
        case 'number':
          if (this.isNumber(query.toString())) {
            result += result ? ' OR ' : '';
            result += this._onceNumberValue(f.field.code, query.toString(), false);
          }
          break;
        case 'DOCNUMBER':
          if (query.toString()) {
            result += result ? ' OR ' : '';
            result += this._docNumberValue(f.field.code, query.toString());
          }
          break;
        case 'DICT':
          result += result ? ' OR ' : '';
          result += this._onceValue(f.field.code+"Value", query.toString(), false);
          break;
        default:
          result += result ? ' OR ' : '';
          result += this._onceValue(f.field.code, query.toString(), false);
          break;
      }
    });

    return result;
  }

  toQueryRequestParams(): any {
    let result = {
      sort: this.sortingsToString(),
      query: this.filtersToString(),
      page: this.pagination.currentPage - 1,
      pageSize: this.pagination.itemsPerPage,
      types: [this.documentCode.toUpperCase()]
    };
    /*if (this.settings.documentTypes.length > 0) {
      this.settings.documentTypes.forEach(t => {
        result.types.push(t);
      });
    }*/
    return result;
  }

  calculateSelectedFilters(): number {
    let result = 0;
    let resultIncrement = (fieldCode: string) => {
      const fieldFilter = this.settings.filters.find(filter => filter.field.code == fieldCode);
      let notCalculateSelectedFilters = fieldFilter.loginFromStorage
        || fieldFilter.presetFilter
        || fieldFilter.userprofileFilter;
      if (_.isEmpty(this.settings.defaultFilters[fieldCode]) && !notCalculateSelectedFilters) result += 1;
    };
    this.settings.filters.forEach(f => {
      if (f.multiple) {
        if (this.selectedFilters[f.field.code].length > 0) {
          // if (!_.find(this.settings.filters, _f => {
          //     return _f.code === f.field.code
          //   })) {
            resultIncrement(f.field.code);
          // }
        }
      } else {
        if ((f.field.type === 'text' || f.field.type === 'user' || f.field.type === 'dictionary') &&
          _.isString(this.selectedFilters[f.field.code]) &&
          this.selectedFilters[f.field.code].trim()) {
          // if (!_.find(this.settings.filters, _f => {
          //     return _f.code === f.field.code
          //   })) {
            resultIncrement(f.field.code);
          // }
        }
        if (f.field.type === 'boolean' && _.isBoolean(this.selectedFilters[f.field.code])) {
          // if (!_.find(this.settings.filters, _f => {
          //     return _f.code === f.field.code
          //   })) {
            resultIncrement(f.field.code);
          // }
        }
        if (f.field.type === 'date' && _.isArray(this.selectedFilters[f.field.code])) {
          // if (!_.find(this.settings.filters, _f => {
          //     return _f.code === f.field.code
          //   })) {
            resultIncrement(f.field.code);
          // }
        }
      }
    });
    return result;
  }

  presetFilters() {
    this.settings.filters.forEach(f => {
      /*if (this.config.filters) {
        const _f = _.find(this.config.filters, ff => {
          return ff.code === f.field.code;
        });
        if (_f) {
          this.selectedFilters[_f.code] = _f.value;
        }
      }*/
      if (f.presetFilter !== '') {
        this.selectedFilters[f.field.code] = f.presetFilter.split(';');
      }

      if (f.userprofileFilter !== '') {
        let valuesFilters = this.dicts['userprofile'];
        f.userprofileFilter.split('.').forEach( s => {
          valuesFilters = valuesFilters[s]
        });

        this.selectedFilters[f.field.code] = valuesFilters;
      }
    });
  }

  private _docNumberValue(propName: string, value: string, hardOneValue: boolean = true): string {
    let result = '';
    let field = this.settings.fields.find(f => f.code === propName);
    result += this.settings.isNewSearch
      ? '(' + propName + ':"' + value + '")'
      : '(' + propName + ':*' + this.escapingSpecialCharacters(value) + '*)';
    return result;
  }

  clearFilters(login?: string) {
    this.settings.filters.forEach(f => {
      if (this.showFilter(f)) {
        if (this.settings.defaultFilters[f.field.code]) {
          this.selectedFilters[f.field.code] = this.settings.defaultFilters[f.field.code];
        } else if (f.loginFromStorage) {
          this.selectedFilters[f.field.code] = login ? login.substring(1, login.length - 1) : '';
        } else if (f.multiple) {
          this.selectedFilters[f.field.code] = [];
        } else {
          this.selectedFilters[f.field.code] = null;
        }
      }
    });
  }

  showFilter(filter: any): boolean {
    // if (this.config.filters && this.config.filters.length && this.config.filters.length > 0) {
    //   return !(this.config.filters.find(f => {
    //     return f.code === filter.field.code
    //   }) && (filter.presetFilter === '') );
    // } else {
      return filter.presetFilter === '';
    // }
  }

  correctSearchResultData(item) {
    this.settings.fields.forEach(f => {
      if (f.type === 'boolean' && item[f.code] && _.isString(item[f.code])) {
        if (item[f.code] === 'true') {
          item[f.code] = true;
        } else if (item[f.code] === 'false') {
          item[f.code] = false;
        }
      }
      /*if (f.type === 'calculated') {
        const _f: any = _.find(this.config.calculatedFields, c => {
          return c.code === f.code;
        });
        if (_f) {
          item[f.code] = _f.func(item)
        }
      }*/
    });
  }

  fillInSearchResultItemInfoAboutDict(item) {
    _.forIn(item, (value, key) => {
      const f = _.find(this.settings.fields, _f => {
        return _f.code === key;
      });
      if (f) {
        if (f.isArray) {
          if (f.dictionary) {
            value.forEach((vv, i) => {
              item[key][i] = this.findInDictionary(this.dicts[f.dictionary], f.dictionaryCode, vv);
            });
          }
        } else {
          if (f.dictionary) {
            const __d = _.find(this.dicts[f.dictionary], dd => {
              return dd[f.dictionaryCode] === value;
            });
            item[key] = this.findInDictionary(this.dicts[f.dictionary], f.dictionaryCode, value);
          }
        }
      }
    });
  }

  public findInDictionary(items, code, value) {
    let result;
    items.forEach(d => {
      const isSynonym = _.find(d.synonyms, s => {
        return s === value;
      });
      if ((d[code] === value) || isSynonym) {
        result = d;
      } else {
        if (d.children && d.children.length && d.children.length > 0) {
          const _result = this.findInDictionary(d.children, code, value);
          if (_result) {
            result = _result;
          }
        }
      }
    });

    return result;
  }

  private dataToString(value, field): string {
    if (value.match(/^(\d{2})[.\/](\d{2})[.\/](\d{4})/) && (moment(value,'DD.MM.YYYY', true).isValid() || moment(value,'MM/DD/YYYY', true).isValid())  ) {
      const formatedDate = `${moment(value.replace(/^(\d{2})[.\/](\d{2})[.\/](\d{4})/, '$3-$2-$1')).format('YYYY-MM-DD')}T00:00:00Z`;
      return `(${field.field.code}:[${formatedDate} TO ${formatedDate}])`;
    }
  }

  private getValueForOldSolr(element): string {
    return (element.trim().match(/["']/g)) ?
      element.trim().replace(/["']/g, '').split(" ").map( (word, index) => (index > 0 ) ? 'AND *' + word + '*' :  '*' + word + '*' ).join(" ") : element;
  }

  private regExpSpecialCharacters(): any[] {
    return [/\\/g, /\+/g, /-/g, /\!/g, /\(/g, /\)/g, /:/g, /\^/g, /\[/g, /\]/g,
      /\{/g, /\}/g, /~/g, /\?/g, /\|/g, /&/g, /;/g, /\//g, /"/g];
  }

  private specialCharacters(): string [] {
    return ['\\\\', '\\+', '\\-', '\\!', '\\(', '\\)', '\\:', '\\^', '\\[', '\\]',
      '\\{', '\\}', '\\~', '\\?', '\\|', '\\&', '\\;', '\\/', '\\"'];
  }

  private escapingSpecialCharacters(value: string): string {
    let result = value;
    const regExpSpecialCharacters: string[] = this.regExpSpecialCharacters();
    const specialCharacters: string[] = this.specialCharacters();
    regExpSpecialCharacters.forEach((ch, index) => {
      result = result.replace(ch, specialCharacters[index]);
    });

    return result;
  }

  private isNumber(n) { return !isNaN(parseFloat(n)) && !isNaN(n - 0) }

  private _onceValue(propName: string, value: string, hardOneValue: boolean = true): string {
    let result = '';
    value = value.trim();
    let field = this.settings.fields.find(f => f.code === propName);
    if (field && field.searchRule && field.searchRule === 'KEYWORD') {
      value = '"' + value + '"';
    } else if (value.match(/\s+/)) {
      value = '(*' + this.escapingSpecialCharacters(value.replace(/\s+/g, '* AND *')) + '*)';
    } else if (!hardOneValue) {
      value = '*' + this.escapingSpecialCharacters(value) + '*';
    }
    result += this.settings.isNewSearch
      ? '(' + propName + ':' + value + ')'
      : '(' + propName + ':' + this.escapingSpecialCharacters(value) + ')';
    return result;
  }

  private _onceNumberValue(propName: string, value: string, hardOneValue: boolean = true): string {
    let result = '';
    value = value.trim();
    let field = this.settings.fields.find(f => f.code === propName);
    if (field && field.searchRule && field.searchRule === 'KEYWORD') {
      value = '"' + value + '"';
    } else if (value.match(/\s+/)) {
      value = '(' + this.escapingSpecialCharacters(value.replace(/\s+/g, '* AND *')) + ')';
    } else if (!hardOneValue) {
      value = this.escapingSpecialCharacters(value);
    }
    result += this.settings.isNewSearch
      ? '(' + propName + ':' + value + ')'
      : '(' + propName + ':' + this.escapingSpecialCharacters(value) + ')';
    return result;
  }

  private _multipleValue(propName: string, values: string[]): string {
    let result = '';
    values = values.map(v => {
      v = v.trim();
      if (v.match(/\s+/)) {
        v = '(*' + this.escapingSpecialCharacters(v.replace(/\s+/g, '* AND *')) + '*)';
      }
      return v;
    });
    result += '(' + propName + ':' + values.join(' OR ' + propName + ':') + ')';
    return result;
  }


}
