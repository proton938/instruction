import {DocumentBase} from "../core/DocumentBase";
import {IInstructionDocumentDifficulty} from "./abstracts/IInstructionDocumentDifficulty";
import {NSIInstructionDifficulty} from "../nsi/NSIInstructionDifficulty";

export class InstructionDocumentDifficulty extends DocumentBase<IInstructionDocumentDifficulty, InstructionDocumentDifficulty> {
  code: string = '';
  name: string = '';
  color: string = '';

  build(i?: IInstructionDocumentDifficulty) {
    super.build(i);
  }

  updateFromNsi(value: NSIInstructionDifficulty) {
    if (value) {
      this.code = value.code;
      this.name = value.name;
      this.color = value.color;
    }
  }
}
