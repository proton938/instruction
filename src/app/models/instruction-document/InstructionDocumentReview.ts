import {DocumentUser} from '../core/DocumentUser';
import {DocumentBase} from "../core/DocumentBase";
import * as _ from 'lodash';
import {IInstructionDocumentReview} from "./abstracts/IInstructionDocumentReview";

// @dynamic
export class InstructionDocumentReview extends DocumentBase<IInstructionDocumentReview, InstructionDocumentReview> {
  reviewBy: DocumentUser = new DocumentUser(); // Ознакомился
  factReviewBy: DocumentUser = new DocumentUser(); // Ознакомился фактически (делегант)
  sentReviewBy: DocumentUser = new DocumentUser(); // Кто отправил на ознакомление
  reviewPlanDate: Date = null; //Плановая дата ознакомления
  reviewFactDate: Date = null; //Фактическая дата ознакомления
  reviewNote: string = ''; //Комментарий

  build(i?: IInstructionDocumentReview) {
    super.build(i);
    this.reviewBy.build(i.reviewBy);
    this.factReviewBy.build(i.factReviewBy);
    this.sentReviewBy.build(i.sentReviewBy);
  }

  static buildArray(reviews: IInstructionDocumentReview[]): InstructionDocumentReview[] {
    return (reviews && reviews.length) ? _.map(reviews, r => {
      let review = new InstructionDocumentReview();
      review.build(r);
      return review;
    }) : [];
  }

}
