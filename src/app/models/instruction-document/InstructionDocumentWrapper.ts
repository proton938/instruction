import {InstructionDocument} from './InstructionDocument';
import {IInstructionDocumentWrapper} from "./abstracts/IInstructionDocumentWrapper";
import {DocumentBase} from "../core/DocumentBase";

export class InstructionDocumentWrapper extends DocumentBase<IInstructionDocumentWrapper, InstructionDocumentWrapper> {
  document: InstructionDocument = new InstructionDocument();

  build(i?: IInstructionDocumentWrapper) {
    super.build(i);
    this.document.build(i.document);
  }
}
