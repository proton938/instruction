import {DocumentBase} from "../core/DocumentBase";
import {IInstructionDocumentForInformation} from "./abstracts/IInstructionDocumentForInformation";
import {UserBean} from "@reinform-cdp/nsi-resource";
import {DocumentUser} from "../core/DocumentUser";
import * as _ from "lodash";

// @dynamic
export class InstructionDocumentForInformation extends DocumentBase<IInstructionDocumentForInformation, InstructionDocumentForInformation> {
  numberRepeat: number = null;
  bpmTaskId: string = '';
  reviewBy: DocumentUser = new DocumentUser();
  factReviewBy: DocumentUser = new DocumentUser();
  reviewPlanDate?: Date = null;
  reviewFactDate?: Date = null;
  reviewNote?: string = '';

  updateFromUserBean(user: UserBean) {
    if (user) {
      let documentUser = new DocumentUser();
      documentUser.updateFromUserBean(user);
      this.reviewBy = documentUser;
    }
  }


  build(i?: IInstructionDocumentForInformation) {
    super.build(i);
    this.reviewBy.build(i.reviewBy);
    this.factReviewBy.build(i.factReviewBy);
  }

  static buildArray(forInformation: IInstructionDocumentForInformation[]): InstructionDocumentForInformation[] {
    return (forInformation && forInformation.length) ? _.map(forInformation, r => {
      let result = new InstructionDocumentForInformation();
      result.build(r);
      return result;
    }) : [];
  }
}
