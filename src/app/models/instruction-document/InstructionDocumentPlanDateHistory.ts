import {DocumentBase} from "../core/DocumentBase";
import {DocumentUser} from '../core/DocumentUser';
import {IInstructionDocumentPlanDateHistory} from "./abstracts/IInstructionDocumentPlanDateHistory";
import * as _ from 'lodash';

// @dynamic
export class InstructionDocumentPlanDateHistory extends DocumentBase<IInstructionDocumentPlanDateHistory, InstructionDocumentPlanDateHistory> {
  planDateOld: Date = null; // значение поля "Срок исполнения" (document/instruction/planDate) до изменения
  user: DocumentUser = new DocumentUser(); // данные текущего пользователя
  changeDate: Date = null; // текущие дату и время

  build(i?: IInstructionDocumentPlanDateHistory) {
    super.build(i);
    this.user.build(i.user);
  }

  static buildArray(history: IInstructionDocumentPlanDateHistory[]): InstructionDocumentPlanDateHistory[] {
    return (history && history.length) ? _.map(history, u => {
      let h = new InstructionDocumentPlanDateHistory();
      h.build(u);
      return h;
    }) : [];
  }
}
