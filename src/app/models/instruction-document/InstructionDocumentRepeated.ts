import {DocumentBase} from "../core/DocumentBase";
import {
  IInstructionDocumentPeriodMonth, IInstructionDocumentPeriodRepeat, IInstructionDocumentPeriodWeekday,
  IInstructionDocumentPlanDateRepeat, IInstructionDocumentRepeated
} from './abstracts/IInstructionDocumentRepeated';
import * as _ from 'lodash';
import {NSIRepeatPeriod} from '../nsi/NSIRepeatPeriod';
import {NSIWeekday} from '../nsi/NSIWeekday';
import {NSIMonth} from '../nsi/NSIMonth';
import {InstructionDocumentNumberWeek} from './InstructionDocumentNumberWeek';

// @dynamic
export class InstructionDocumentRepeated extends DocumentBase<IInstructionDocumentRepeated, InstructionDocumentRepeated> {
  repeatedSign: boolean = null; //Признак регулярности
  currentNumberRepeat: number = null; // Номер текущего цикла повторяющегося поручения
  dateEndPeriodRepeat: Date = null; //Дата окончания периода повтора
  periodRepeat: InstructionDocumentPeriodRepeat = new InstructionDocumentPeriodRepeat(); //Период повтора
  periodicity: number = null; //Периодичность
  RepeatWorkingDay: boolean = null; //Повтор в рабочие дни
  dayMonth: number = null; //День месяца
  numberWeek: InstructionDocumentNumberWeek = null; //Номер недели
  weekday: InstructionDocumentPeriodWeekday[] = [];
  Month: InstructionDocumentPeriodMonth[] = [];
  planDateRepeat: InstructionDocumentPlanDateRepeat[] = [];

  build(i?: IInstructionDocumentRepeated) {
    super.build(i);
    if (i) {
      this.periodRepeat.build(i.periodRepeat);
      this.weekday = InstructionDocumentPeriodWeekday.buildArray(i.weekday);
      this.Month = InstructionDocumentPeriodMonth.buildArray(i.Month);
      this.planDateRepeat = InstructionDocumentPlanDateRepeat.buildArray(i.planDateRepeat);
      if (i.numberWeek) {
        this.numberWeek = new InstructionDocumentNumberWeek();
        this.numberWeek.build(i.numberWeek);
      }
    }
  }

}

export class InstructionDocumentPeriodRepeat extends DocumentBase<IInstructionDocumentPeriodRepeat, InstructionDocumentPeriodRepeat> {
  code: string = '';
  name: string = '';

  updateFromNsi(value: NSIRepeatPeriod) {
    if (value) {
      this.code = value.code;
      this.name = value.name;
    }
  }
}

export class InstructionDocumentPeriodWeekday extends DocumentBase<IInstructionDocumentPeriodWeekday, InstructionDocumentPeriodWeekday> {
  code: string = '';
  name: string = '';

  static buildArray(array: IInstructionDocumentPeriodWeekday[]): InstructionDocumentPeriodWeekday[] {
    return (array && array.length) ? _.map(array, u => {
      let h = new InstructionDocumentPeriodWeekday();
      h.build(u);
      return h;
    }) : [];
  }

  updateFromNsi(value: NSIWeekday) {
    if (value) {
      this.code = value.code;
      this.name = value.name;
    }
  }
}

export class InstructionDocumentPeriodMonth extends DocumentBase<IInstructionDocumentPeriodMonth, InstructionDocumentPeriodMonth> {
  code: string = '';
  name: string = '';

  updateFromNsi(value: NSIMonth) {
    if (value) {
      this.code = value.code;
      this.name = value.name;
    }
  }

  static buildArray(array: IInstructionDocumentPeriodMonth[]): InstructionDocumentPeriodMonth[] {
    return (array && array.length) ? _.map(array, u => {
      let h = new InstructionDocumentPeriodMonth();
      h.build(u);
      return h;
    }) : [];
  }
}

export class InstructionDocumentPlanDateRepeat extends DocumentBase<IInstructionDocumentPlanDateRepeat, InstructionDocumentPlanDateRepeat> {
  numberRepeat: number = null; // Номер цикла повторяющегося поручения
  planDate: Date = null; //Плановая дата исполнения периодического поручения
  reportAccepted: boolean = null; //Отчет принят
  weekday: InstructionDocumentPeriodWeekday = new InstructionDocumentPeriodWeekday();

  build(i?: IInstructionDocumentPlanDateRepeat) {
    super.build(i);
    if (i) {
      this.weekday.build(i.weekday);
    }
  }

  static buildArray(array: IInstructionDocumentPlanDateRepeat[]): InstructionDocumentPlanDateRepeat[] {
    return (array && array.length) ? _.map(array, u => {
      let h = new InstructionDocumentPlanDateRepeat();
      h.build(u);
      return h;
    }) : [];
  }

}

