import {DocumentBase} from "../core/DocumentBase";
import {DocumentUser} from '../core/DocumentUser';
import {InstructionDocumentTheme} from './InstructionDocumentTheme';
import {InstructionDocumentPriority} from './InstructionDocumentPriority';
import {InstructionDocumentDifficulty} from './InstructionDocumentDifficulty';
import {InstructionDocumentCadastral} from "./InstructionDocumentCadastral";
import {InstructionDocumentPlanDateHistory} from "./InstructionDocumentPlanDateHistory";
import {InstructionDocumentSubject} from "./InstructionDocumentSubject";
import {IInstructionDocumentInstruction} from "./abstracts/IInstructionDocumentInstruction";
import {CoExecutor} from "../instruction/CoExecutorModel";
import {InstructionDocumentRepeated} from './InstructionDocumentRepeated';
import {InstructionDocumentHistory} from "./InstructionDocumentHistory";

export class InstructionDocumentInstruction extends DocumentBase<IInstructionDocumentInstruction, InstructionDocumentInstruction> {
  number: string = ''; // Номер поручения
  beginDate: Date = null; // Дата поручения
  creator: DocumentUser = new DocumentUser(); // Автор поручения
  assistant: DocumentUser = new DocumentUser(); // Асистент
  theme: InstructionDocumentTheme = null; // Рубрикатор
  content: string = ''; // Содержание поручения
  description: string = ''; // Описание поручения
  fileId: string[] = []; // Идентификаторы файлов поручения
  priority: InstructionDocumentPriority = null; // Приоритет
  difficulty: InstructionDocumentDifficulty =  null; // Сложность
  private: boolean = null; // Признак личного поручения
  planDate: Date = null; // Плановый срок исполнения
  factDate: Date = null; // Фактический срок исполнения
  executor: DocumentUser = null; // Ответственный исполнитель
  coExecutorHistory: InstructionDocumentHistory[] = []; // История изменения Соисполнителей
  coExecutor: CoExecutor[] = []; // Соисполнители
  questionId: string = '';
  questionPrimaryID: string = '';
  expressMeetingID: string = '';
  cadastral: InstructionDocumentCadastral =  new InstructionDocumentCadastral(); // Сведения о проверке в кадастровой палате
  primaryDate: Date = null; // Первичный срок исполнения
  planDateHistory: InstructionDocumentPlanDateHistory[] = []; // История изменения сроков исполнения
  subject: InstructionDocumentSubject = null; // Тема
  memoId: string = '';  // Идентификатор служебной записки
  memoMKAID: string = '';  // Идентификатор служебной записки МКА
  memoView: boolean = null; // Доступ к служебной записке
  InstructionMKAID: string = ''; //Идентификатор поручения МКА
  OrderID: string = ''; //Идентификатор РД
  ProtocolID: string = ''; //Идентификатор протокола
  AgendaID: string = ''; //Идентификатор повестки
  kindInstruction: InstructionDocumentPriority = new InstructionDocumentPriority(); //Вид поручения
  recalled: boolean = null;
  taskAccepted: boolean = null;
  repeated: InstructionDocumentRepeated = new InstructionDocumentRepeated();

  build(i?: IInstructionDocumentInstruction) {
    super.build(i);
    if (i) {
      this.creator.build(i.creator);
      this.assistant.build(i.assistant);
      if (i.theme) {
        this.theme = new InstructionDocumentTheme();
        this.theme.build(i.theme);
      }
      if (i.executor) {
        this.executor = new DocumentUser();
        this.executor.build(i.executor);
      }
      if (i.priority) {
        this.priority = new InstructionDocumentPriority();
        this.priority.build(i.priority);
      }
      if (i.difficulty) {
        this.difficulty = new InstructionDocumentDifficulty();
        this.difficulty.build(i.difficulty);
      }
      this.cadastral.build(i.cadastral);
      if (i.subject) {
        this.subject = new InstructionDocumentSubject();
        this.subject.build(i.subject);
      }
      this.coExecutorHistory = InstructionDocumentHistory.buildArray(i.coExecutorHistory);
      this.coExecutor = i.coExecutor;
      this.planDateHistory = InstructionDocumentPlanDateHistory.buildArray(i.planDateHistory);
      this.fileId = (i && i.fileId && i.fileId.length) ? i.fileId : [];
      this.kindInstruction.build(i.kindInstruction);
      this.repeated.build(i.repeated);
    }

  }
}
