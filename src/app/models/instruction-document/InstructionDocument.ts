import {InstructionDocumentInstruction} from './InstructionDocumentInstruction';
import {InstructionDocumentResult} from './InstructionDocumentResult';
import {IInstructionDocument} from "./abstracts/IInstructionDocument";
import {DocumentBase} from "../core/DocumentBase";
import {DocumentStatus} from "../core/DocumentStatus";
import {DocumentComment} from "../core/DocumentComment";
import {DocumentFromMKA} from '../core/DocumentFromMKA';
import {InstructionDocumentReview} from "./InstructionDocumentReview";
import {InstructionDocumentForInformation} from "./InstructionDocumentForInformation";
import {InstructionDocumentHistory} from "./InstructionDocumentHistory";

export class InstructionDocument extends DocumentBase<IInstructionDocument, InstructionDocument> {
  parentId?: string = ''; // Идентификатор родительского поручения
  folderId?: string = ''; // Идентификатор папки в FN
  linkedId?: string = ''; // Идентификатор связанного поручения
  statusId?: DocumentStatus = new DocumentStatus(); // Статус поручения
  instruction?: InstructionDocumentInstruction = new InstructionDocumentInstruction(); // Поручение
  result?: InstructionDocumentResult[] = []; // Исполнение
  comment?: DocumentComment[] = []; // Комментарии
  removeBase?: string = ''; // Комментарий к снятию поручения
  previousId?: string = ''; // Предыдущее поручение
  receivedFromMKA: DocumentFromMKA = new DocumentFromMKA();
  review?: InstructionDocumentReview[] = []; //Ознакомление
  forInformation?: InstructionDocumentForInformation[] = []; //К сведению
  forInformationHistory?: InstructionDocumentHistory[] = [];
  filesRequired: boolean = null; //Файлы обязательны

  build(i?: IInstructionDocument) {
    super.build(i);
    this.statusId.build(i.statusId);
    this.instruction.build(i.instruction);
    this.receivedFromMKA.build(i.receivedFromMKA);
    this.result = InstructionDocumentResult.buildArray(i.result);
    this.comment = DocumentComment.buildArray(i.comment);
    this.review = InstructionDocumentReview.buildArray(i.review);
    this.forInformation = InstructionDocumentForInformation.buildArray(i.forInformation);
    this.forInformationHistory = InstructionDocumentHistory.buildArray(i.forInformationHistory);
  }
}
