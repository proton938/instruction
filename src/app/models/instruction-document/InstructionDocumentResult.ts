import {DocumentUser} from '../core/DocumentUser';
import {DocumentBase} from "../core/DocumentBase";
import * as _ from 'lodash';
import {IInstructionDocumentResult} from "./abstracts/IInstructionDocumentResult";

// @dynamic
export class InstructionDocumentResult extends DocumentBase<IInstructionDocumentResult, InstructionDocumentResult> {
  reportExecutor: DocumentUser = new DocumentUser(); // Фактический исполнитель поручения
  reportDate: Date = null; // Дата отчета по поручению
  reportContent: string = ''; // Содержание отчета по поручению
  reportFileId: string[] = []; // Идентификаторы файлов отчета по поручению
  reportAcceptor: DocumentUser = new DocumentUser(); // Пользователь, фактически принявший отчет
  acceptDate: Date = null; // Дата принятия отчета по поручению
  acceptStatus: string = ''; // Статус принятия отчета по поручению
  acceptContent: string = ''; // Содержание принятия отчета по поручению
  acceptFileId: string[] = []; // Идентификаторы файлов принятия отчета по поручению

  build(i?: IInstructionDocumentResult) {
    super.build(i);
    this.reportExecutor.build(i.reportExecutor);
    this.reportAcceptor.build(i.reportAcceptor);
    this.reportFileId = (i && i.reportFileId && i.reportFileId.length) ? i.reportFileId : [];
    this.acceptFileId = (i && i.acceptFileId && i.acceptFileId.length) ? i.acceptFileId : [];
  }

  static buildArray(results: IInstructionDocumentResult[]): InstructionDocumentResult[] {
    return (results && results.length) ? _.map(results, r => {
      let result = new InstructionDocumentResult();
      result.build(r);
      return result;
    }) : [];
  }
}
