import {DocumentBase} from '../core/DocumentBase';
import {DocumentUser} from '../core/DocumentUser';
import {IInstructionDocumentHistory} from './abstracts/IInstructionDocumentHistory';
import {InstructionDocumentForInformation} from './InstructionDocumentForInformation';
import {UserBean} from '@reinform-cdp/nsi-resource';
import {CoExecutor} from '../instruction/CoExecutorModel';
import * as _ from 'lodash';

export class InstructionDocumentHistory extends DocumentBase<IInstructionDocumentHistory, InstructionDocumentHistory> {
  numberRepeat?: number = null; // Номер цикла повторяющегося поручения
  forInformationOld?: InstructionDocumentForInformation[] = []; // Предыдущее значение поля forInformation
  coExecutorOld?: CoExecutor[] = []; // Предыдущее значение поля forInformation
  user?: DocumentUser = new DocumentUser(); // Кто изменил
  changeDate?: string = ''; // Дата изменения

  updateFromUserBean(user: UserBean) {
    if (user) {
      let documentUser = new DocumentUser();
      documentUser.updateFromUserBean(user);
      this.user = documentUser;
    }
  }

  build(i?: IInstructionDocumentHistory) {
    super.build(i);
    if (i) {
      this.forInformationOld = InstructionDocumentForInformation.buildArray(i.forInformationOld);
      this.coExecutorOld = i.coExecutorOld;
      this.user.build(i.user);
    }
  }

  static buildArray(i: IInstructionDocumentHistory[]): InstructionDocumentHistory[] {
    return i && i.length ? _.map(i, r => {
      let result = new InstructionDocumentHistory();
      result.build(r);
      return result;
    }) : [];
  }
}
