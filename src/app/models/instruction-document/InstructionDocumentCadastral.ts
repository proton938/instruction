import {DocumentBase} from "../core/DocumentBase";
import {IInstructionDocumentCadastral} from "./abstracts/IInstructionDocumentCadastral";

export class InstructionDocumentCadastral extends DocumentBase<IInstructionDocumentCadastral, InstructionDocumentCadastral> {
  author: string = ''; // запрос от
  request: string = ''; // номер обращения
  count: number = null; // количество территориальных зон
  checked: number = null; // количество территориальных зон успешно прошедших проверку
}
