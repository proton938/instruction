import {DocumentBase} from "../core/DocumentBase";
import {IInstructionDocumentNumberWeek} from "./abstracts/IInstructionDocumentNumberWeek";

export class InstructionDocumentNumberWeek extends DocumentBase<IInstructionDocumentNumberWeek, InstructionDocumentNumberWeek> {
  code: string = '';
  name: string = '';
  value: number = null;
}
