import {IDocumentBase} from "../../core/abstracts/IDocumentBase";

export interface IInstructionDocumentTheme extends IDocumentBase {
  code?: string;
  name?: string;
}
