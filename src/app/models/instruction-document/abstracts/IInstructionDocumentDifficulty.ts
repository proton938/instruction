import {IDocumentBase} from "../../core/abstracts/IDocumentBase";

export interface IInstructionDocumentDifficulty extends IDocumentBase {
  code?: string;
  name?: string;
  color?: string;
}
