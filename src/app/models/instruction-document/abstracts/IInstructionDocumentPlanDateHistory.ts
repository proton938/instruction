import {IDocumentUser} from '../../core/abstracts/IDocumentUser';
import {IDocumentBase} from "../../core/abstracts/IDocumentBase";

export interface IInstructionDocumentPlanDateHistory extends IDocumentBase {
  planDateOld?: string;
  user?: IDocumentUser;
  changeDate?: string;
}
