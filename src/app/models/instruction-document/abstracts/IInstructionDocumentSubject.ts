import {IDocumentBase} from "../../core/abstracts/IDocumentBase";

export interface IInstructionDocumentSubject extends IDocumentBase {
  code?: string;
  name?: string;
}
