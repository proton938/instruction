import {IDocumentStatus} from "../../core/abstracts/IDocumentStatus";
import {IInstructionDocumentInstruction} from "./IInstructionDocumentInstruction";
import {IInstructionDocumentResult} from "./IInstructionDocumentResult";
import {IDocumentComment} from "../../core/abstracts/IDocumentComment";
import {IDocumentBase} from "../../core/abstracts/IDocumentBase";
import {IDocumentFromMKA} from '../../core/abstracts/IDocumentFromMKA';
import {IInstructionDocumentReview} from "./IInstructionDocumentReview";
import {IInstructionDocumentForInformation} from './IInstructionDocumentForInformation';
import {IInstructionDocumentHistory} from "./IInstructionDocumentHistory";

export interface IInstructionDocument extends IDocumentBase {
  parentId?: string;
  folderId?: string;
  statusId?: IDocumentStatus;
  instruction?: IInstructionDocumentInstruction;
  result?: IInstructionDocumentResult[];
  comment?: IDocumentComment[];
  removeBase?: string;
  previousId?: string;
  receivedFromMKA?: IDocumentFromMKA;
  review?: IInstructionDocumentReview[];
  forInformation?: IInstructionDocumentForInformation[];
  forInformationHistory?: IInstructionDocumentHistory[]; // История изменения - К сведению
  filesRequired?: boolean;
}
