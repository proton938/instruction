import {IDocumentBase} from "../../core/abstracts/IDocumentBase";
import {IInstructionDocumentNumberWeek} from "./IInstructionDocumentNumberWeek";

export interface IInstructionDocumentRepeated extends IDocumentBase {
  repeatedSign?: boolean;
  currentNumberRepeat?: number;
  dateEndPeriodRepeat?: Date;
  periodRepeat?: IInstructionDocumentPeriodRepeat;
  periodicity?: number;
  RepeatWorkingDay?: boolean;
  dayMonth?: number;
  numberWeek?: IInstructionDocumentNumberWeek;
  weekday?: IInstructionDocumentPeriodWeekday[];
  Month?: IInstructionDocumentPeriodMonth[];
  planDateRepeat?: IInstructionDocumentPlanDateRepeat[];
}

export interface IInstructionDocumentPeriodRepeat extends IDocumentBase {
  code?: string;
  name?: string;
}

export interface IInstructionDocumentPeriodWeekday extends IDocumentBase {
  code?: string;
  name?: string;
}

export interface IInstructionDocumentPeriodMonth extends IDocumentBase {
  code?: string;
  name?: string;
}

export interface IInstructionDocumentPlanDateRepeat extends IDocumentBase {
  numberRepeat?: number;
  planDate?: Date;
  reportAccepted?: boolean;
  weekday?: IInstructionDocumentPeriodWeekday;
}

