import {IDocumentBase} from "../../core/abstracts/IDocumentBase";
import {IDocumentUser} from "../../core/abstracts/IDocumentUser";

export interface IInstructionDocumentForInformation extends IDocumentBase {
  numberRepeat?: number; // Номер цикла повторяющегося поручения
  bpmTaskId?: string; // ID задачи процесса
  reviewBy: IDocumentUser;
  factReviewBy?: IDocumentUser;
  reviewPlanDate?: Date;
  reviewFactDate?: Date;
  reviewNote?: string;
}
