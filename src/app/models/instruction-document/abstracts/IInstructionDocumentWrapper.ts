import {IDocumentBase} from "../../core/abstracts/IDocumentBase";
import {IInstructionDocument} from "./IInstructionDocument";

export interface IInstructionDocumentWrapper extends IDocumentBase {
  document?: IInstructionDocument;
}
