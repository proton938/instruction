import {IDocumentBase} from "../../core/abstracts/IDocumentBase";
import {IInstructionDocumentForInformation} from "./IInstructionDocumentForInformation";
import {IDocumentUser} from "../../core/abstracts/IDocumentUser";
import {CoExecutor} from "../../instruction/CoExecutorModel";

export interface IInstructionDocumentHistory extends IDocumentBase {
  numberRepeat?: number; // Номер цикла повторяющегося поручения
  forInformationOld?: IInstructionDocumentForInformation[]; // Предыдущее значение поля forInformation
  coExecutorOld?: CoExecutor[]; // Предыдущее значение поля coExecutor
  user?: IDocumentUser; // Кто изменил
  changeDate?: string; // Дата изменения
}
