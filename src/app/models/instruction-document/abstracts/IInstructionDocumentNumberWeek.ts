import {IDocumentBase} from '../../core/abstracts/IDocumentBase';

export interface IInstructionDocumentNumberWeek extends IDocumentBase { // Номер недели
  code?: string; // Код
  name?: string; // Наименование
  value?: number; // Значение
}
