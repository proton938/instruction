import {IDocumentUser} from '../../core/abstracts/IDocumentUser';
import {IDocumentBase} from "../../core/abstracts/IDocumentBase";

export interface IInstructionDocumentResult extends IDocumentBase {
  reportExecutor?: IDocumentUser;
  reportDate?: string;
  reportContent?: string;
  reportFileId?: string[];
  reportAcceptor?: IDocumentUser;
  acceptDate?: string;
  acceptStatus?: string;
  acceptContent?: string;
  acceptFileId?: string[];
}
