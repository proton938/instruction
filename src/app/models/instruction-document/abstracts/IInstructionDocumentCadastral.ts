import {IDocumentBase} from "../../core/abstracts/IDocumentBase";

export interface IInstructionDocumentCadastral extends IDocumentBase {
  author?: string;
  request?: string;
  count?: string;
  checked?: string;
}
