import {IDocumentUser} from '../../core/abstracts/IDocumentUser';
import {IDocumentBase} from "../../core/abstracts/IDocumentBase";

export interface IInstructionDocumentReview extends IDocumentBase {
  reviewBy?: IDocumentUser;
  factReviewBy?: IDocumentUser;
  sentReviewBy?: IDocumentUser
  reviewPlanDate?: string;
  reviewFactDate?: string;
  reviewNote?: string;
}
