import {IDocumentBase} from "../../core/abstracts/IDocumentBase";

export interface IInstructionDocumentPriority extends IDocumentBase {
  code?: string;
  name?: string;
  color?: string;
}
