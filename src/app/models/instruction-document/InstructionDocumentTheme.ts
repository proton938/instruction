import {DocumentBase} from "../core/DocumentBase";
import {IInstructionDocumentTheme} from "./abstracts/IInstructionDocumentTheme";
import {NSIThematicHeading} from "../nsi/NSIThematicHeading";

export class InstructionDocumentTheme extends DocumentBase<IInstructionDocumentTheme, InstructionDocumentTheme> {
  code: string = '';
  name: string = '';

  build(i?: IInstructionDocumentTheme) {
    super.build(i);
  }

  updateFromNsi(value: NSIThematicHeading) {
    if (value) {
      this.code = value.code;
      this.name = value.name;
    }
  }
}
