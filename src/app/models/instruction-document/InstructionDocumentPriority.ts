import {DocumentBase} from "../core/DocumentBase";
import {IInstructionDocumentPriority} from "./abstracts/IInstructionDocumentPriority";
import {NSIInstructionPriority} from "../nsi/NSIInstructionPriority";

export class InstructionDocumentPriority extends DocumentBase<IInstructionDocumentPriority, InstructionDocumentPriority> {
  code: string = '';
  name: string = '';
  color: string = '';

  build(i?: IInstructionDocumentPriority) {
    super.build(i);
  }

  updateFromNsi(value: NSIInstructionPriority) {
    if (value) {
      this.code = value.code;
      this.name = value.name;
      this.color = value.color;
    }
  }
}
