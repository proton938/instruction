import {DocumentBase} from "../core/DocumentBase";
import {IInstructionDocumentSubject} from "./abstracts/IInstructionDocumentSubject";
import {NSIInstructionTheme} from "../nsi/NSIInstructionTheme";

export class InstructionDocumentSubject extends DocumentBase<IInstructionDocumentSubject, InstructionDocumentSubject> {
  code: string = '';
  name: string = '';

  build(i?: IInstructionDocumentSubject) {
    super.build(i);
  }

  updateFromNsi(value: NSIInstructionTheme) {
    if (value) {
      this.code = value.code;
      this.name = value.name;
    }
  }
}
