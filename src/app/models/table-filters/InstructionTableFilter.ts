import {UserBean} from '@reinform-cdp/nsi-resource';
import {NSIInstructionStatus} from './../nsi/NSIInstructionStatus';
import {NSIInstructionPriority} from './../nsi/NSIInstructionPriority';
import {DateRangeFilter} from './../DateRangeFilter';

import * as _ from "lodash";


export class InstructionTableFilter {
  conditionInst: NSIInstructionStatus = null;
  docDateInst: DateRangeFilter = new DateRangeFilter();
  docNumberInst: string = "";
  creatorLogInst: any;
  contentInst: string = "";
  executorLogInst: any;
  priorityInst: NSIInstructionPriority = null;
  planDateInst: DateRangeFilter = new DateRangeFilter();
  reportDateInst: DateRangeFilter = new DateRangeFilter();
  acceptDateInst: DateRangeFilter = new DateRangeFilter();
  acceptStatus: string = null;

  clear() {
    this.conditionInst = null;
    this.docDateInst.clear();
    this.docNumberInst = "";
    this.creatorLogInst = null;
    this.contentInst = "";
    this.executorLogInst = null;
    this.priorityInst = null;
    this.planDateInst.clear();
    this.reportDateInst.clear();
    this.acceptDateInst.clear();
    this.acceptStatus = null;
  }

  isEmpty(): boolean {
    return !this.conditionInst &&
      this.docDateInst.isEmpty() &&
      this.docNumberInst === "" &&
      !this.creatorLogInst &&
      this.contentInst === "" &&
      !this.executorLogInst &&
      !this.priorityInst &&
      this.planDateInst.isEmpty() &&
      this.reportDateInst.isEmpty() &&
      this.acceptDateInst.isEmpty() &&
      !this.acceptStatus;
  }

  toString(): string {
    let result: string = "";

    if (this.conditionInst) {
      let conditions = [`statusCode:${this.conditionInst.code}`];
      result += `(${conditions.join(' OR ')})`
    }
    if (!this.docDateInst.isEmpty()) {
      result += (result) ? " AND " : "";
      result += `(beginDate:${this.docDateInst.toString()})`;
    }
    if (this.docNumberInst) {
      result += (result) ? " AND " : "";
      result += `instructionNumber:(*${this.docNumberInst}*)`;
    }
    if (this.creatorLogInst) {
      result += (result) ? " AND " : "";
      if (this.creatorLogInst.accountName) {
        result += `(creatorLogin:${this.creatorLogInst.accountName})`;
      }
      else {
        result += `(creatorFioFull:*${this.creatorLogInst}*)`;
      }
    }
    if (this.contentInst) {
      let contents = _.map(this.contentInst.split(' '), v => {
        return v.trim();
      });
      let content = contents.join('\\ ');
      result += (result) ? " AND " : "";
      result += `content:(*${content}*)`;
    }

    if (this.executorLogInst) {
      result += (result) ? " AND " : "";
      if (this.executorLogInst.accountName) {
        result += `(executorLogin:${this.executorLogInst.accountName})`;
      }
      else {
        result += `(executorFioFull:*${this.executorLogInst}*)`;
      }
    }

    if (this.priorityInst) {
      result += (result) ? " AND " : "";
      let conditions = [`priorityName:${this.priorityInst.name}`];
      result += `(${conditions.join(' OR ')})`
    }

    if (!this.planDateInst.isEmpty()) {
      result += (result) ? " AND " : "";
      result += `(planDate:${this.planDateInst.toString()})`;
    }
    if (!this.reportDateInst.isEmpty()) {
      result += (result) ? " AND " : "";
      result += `(reportDate:${this.reportDateInst.toString()})`;
    }
    if (!this.acceptDateInst.isEmpty()) {
      result += (result) ? " AND " : "";
      result += `(acceptDate:${this.acceptDateInst.toString()})`;
    }
    if (this.acceptStatus) {
      result += result ? ' AND ' : '';
      result += `acceptStatus:(*${this.acceptStatus}*)`;
    }

    return `(${result})`;
  }
}
