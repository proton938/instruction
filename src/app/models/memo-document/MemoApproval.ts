import {UserBean} from "@reinform-cdp/nsi-resource";
import {DocumentBase} from '../core/DocumentBase';
import * as _ from 'lodash';
import {
  IMemoApproval, IMemoApprovalAdded, IMemoApprovalAdditionalAgreed, IMemoApprovalHistory,
  IMemoApprovalHistoryList, IMemoApprovalList, IMemoApprovalListItem, IMemoApprovalListItemSignature, IMemoApprovalTerm,
  IMemoApprover
} from './abstracts/IMemoApproval';
import {FileType} from '../instruction/FileType';

export class MemoApprovalList extends DocumentBase<IMemoApprovalList, MemoApprovalList> {
  approvalCycleNum: string = '';
  approvalCycleDate: string = '';
  agreed: MemoApprovalListItem[] = [];
  approvalCycleFile: FileType[] = [];

  build(i?: IMemoApprovalList) {
    super.build(i);
    if (i) {
      this.agreed = MemoApprovalListItem.buildArray(i.agreed);
      this.approvalCycleFile = i.approvalCycleFile || [];
    }
  }
}

export class MemoApprovalTerm extends DocumentBase<IMemoApprovalTerm, MemoApprovalTerm> {
  code: string = '';
  name: string = '';
  duration: number = null;

  build(i?: IMemoApprovalTerm) {
    super.build(i);
  }
}

export class MemoApprovalListItem extends DocumentBase<IMemoApprovalListItem, MemoApprovalListItem> {
  approvalNum: string = '';
  approvalTime: string = '';
  agreedBy: MemoApprover = null;
  factAgreedBy: MemoApprover = null;
  approvalPlanDate: Date = null;
  approvalFactDate: Date = null;
  approvalResult: string = '';
  approvalType: string = '';
  approvalTypeCode: string = '';
  approvalNote: string = '';
  fileApproval: FileType[] = [];
  agreedDs: MemoApprovalListItemSignature = null;
  added: MemoApprovalAdded[] = [];
  approvalTerm: MemoApprovalTerm = null;

  build(i?: IMemoApprovalListItem) {
    super.build(i);
    if (i) {
      if (i.agreedBy) {
        this.agreedBy = new MemoApprover();
        this.agreedBy.build(i.agreedBy);
      }
      if (i.factAgreedBy) {
        this.factAgreedBy = new MemoApprover();
        this.factAgreedBy.build(i.factAgreedBy);
      }
      this.fileApproval = i.fileApproval || [];
      if (i.agreedDs) {
        this.agreedDs = new MemoApprovalListItemSignature();
        this.agreedDs.build(i.agreedDs);
      }
      this.added = MemoApprovalAdded.buildArray(i.added);
      if (i.approvalTerm) {
        this.approvalTerm = new MemoApprovalTerm();
        this.approvalTerm.build(i.approvalTerm);
      }
    }
  }

  static buildArray(i: IMemoApprovalListItem[]): MemoApprovalListItem[] {
    return (i && i.length) ? _.map(i, r => {
      let result = new MemoApprovalListItem();
      result.build(r);
      return result;
    }) : [];
  }
}

export class MemoApprovalAdditionalAgreed extends DocumentBase<IMemoApprovalAdditionalAgreed, MemoApprovalAdditionalAgreed> {
  accountName?: string = '';
  fio?: string = '';
  iofShort?: string = '';

  build(i?: IMemoApprovalAdditionalAgreed) {
    super.build(i);
  }
}

export class MemoApprovalAdded extends DocumentBase<IMemoApprovalAdded, MemoApprovalAdded> {
  additionalAgreed: MemoApprovalAdditionalAgreed = new MemoApprovalAdditionalAgreed();
  addedNote: string = '';

  build(i?: IMemoApprovalAdded) {
    super.build(i);
    if (i) {
      this.additionalAgreed.build(i.additionalAgreed);
    }
  }

  static buildArray(i: IMemoApprovalAdded[]): MemoApprovalAdded[] {
    return (i && i.length) ? _.map(i, r => {
      let result = new MemoApprovalAdded();
      result.build(r);
      return result;
    }) : [];
  }
}

export class MemoApprover extends DocumentBase<IMemoApprover, MemoApprover> {
  post: string = '';
  fioFull: string = '';
  accountName: string = '';
  fioShort: string = '';
  iofShort: string = '';
  phone: string = '';
  fioPost: string = '';

  build(i?: IMemoApprover) {
    super.build(i);
  }

  static fromUserBean(user: UserBean): MemoApprover {
    let approver = new MemoApprover();
    approver.build({
      post: user.post,
      fioFull: user.displayName,
      accountName: user.accountName,
      fioShort: user.fioShort,
      iofShort: user.iofShort,
      phone: user.telephoneNumber,
    });
    return MemoApprover.processInstructionApprover(approver);
  }

  public static processInstructionApprover(approver: MemoApprover): MemoApprover {
    approver.fioPost = `${approver.fioFull} (${approver.post ? approver.post : ''})`;
    return approver;
  }

  public static cleanInstructionApprover(approver: MemoApprover): MemoApprover {
    delete approver.fioPost;

    if (!approver.post) {
      delete approver.post;
    }

    if (!approver.phone) {
      delete approver.phone;
    }

    return approver;
  }
}

export class MemoApprovalHistory extends DocumentBase<IMemoApprovalHistory, MemoApprovalHistory> {
  approvalCycle: MemoApprovalHistoryList[] = [];

  build(i?: IMemoApprovalHistory) {
    super.build(i);
    if (i) {
      this.approvalCycle = MemoApprovalHistoryList.buildArray(i.approvalCycle);
    }
  }
}

export class MemoApproval extends  DocumentBase<IMemoApproval, MemoApproval> {
  approvalCycle: MemoApprovalList = new MemoApprovalList();

  build(i?: IMemoApproval) {
    super.build(i);
    if (i) {
      this.approvalCycle.build(i.approvalCycle);
    }
  }

  public static create(num: number): MemoApproval {
    const approval = new MemoApproval();
    approval.approvalCycle.approvalCycleDate = new Date().toISOString();
    approval.approvalCycle.approvalCycleNum =  '' + num;
    return approval;
  }
}

export class MemoApprovalHistoryList extends DocumentBase<IMemoApprovalHistoryList, MemoApprovalHistoryList> {
  approvalCycleNum: string = '';
  approvalCycleDate: string = '';
  agreed: MemoApprovalListItem[] = [];
  approvalCycleFile: FileType[] = [];

  build(i?: IMemoApprovalHistoryList) {
    super.build(i);
    if (i) {
      this.agreed = MemoApprovalListItem.buildArray(i.agreed);
      this.approvalCycleFile = i.approvalCycleFile || [];
    }
  }

  static buildArray(i: IMemoApprovalHistoryList[]): MemoApprovalHistoryList[] {
    return (i && i.length) ? _.map(i, r => {
      let result = new MemoApprovalHistoryList();
      result.build(r);
      return result;
    }) : [];
  }
}

export class MemoApprovalListItemSignature extends DocumentBase<IMemoApprovalListItemSignature, MemoApprovalListItemSignature> {
  agreedDsLastName: string = '';
  agreedDsName: string = '';
  agreedDsPosition: string = '';
  agreedDsCN: string = '';
  agreedDsFrom: string = null;
  agreedDsTo: string = null;

  build(i?: IMemoApprovalListItemSignature) {
    super.build(i);
  }
}
