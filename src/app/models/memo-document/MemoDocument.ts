import {DocumentBase} from "../core/DocumentBase";
import {DocumentStatus} from "../core/DocumentStatus";
import {DocumentComment} from "../core/DocumentComment";
import {DocumentUser} from "../core/DocumentUser";
import {IMemoDocument, IMemoDocumentUser} from "./abstracts/IMemoDocument";
import {MemoDocumentReview} from "./MemoDocumentReview";
import {MemoKind} from '../memo/MemoKind';
import {MemoDocumentResult} from "./MemoDocumentResult";
import {FileType} from '../instruction/FileType';
import {NSIEmailGroup} from '../nsi/NSIEmailGroups';
import {MemoHeadingTopic} from '../memo/MemoHeadingTopic';
import {MemoTrestActivityKind} from '../memo/MemoTrestActivityKind';
import {UserBean} from "@reinform-cdp/nsi-resource";
import * as _ from "lodash";
import {MemoReportTermDoc} from './MemoReportTermDoc';
import {MemoApproval, MemoApprovalHistory} from './MemoApproval';
import {MemoDs} from './MemoDs';

export class MemoDocumentUser extends DocumentUser {
  extensionPhoneNumber: string = '';
  phoneNumberReporter: string = '';

  updateFromUserBean(user: UserBean) {
    super.updateFromUserBean(user);
    if (user) {
      // @ts-ignore
      this.extensionPhoneNumber = user.otherPhone;
      const match = this.phoneNumber.match(/(\d{1})(\d{3})(\d{3})(\d{4})/);
      if (match) {
        this.phoneNumberReporter = `${match[1]}(${match[2]})${match[3]}-${match[4]}`
      }
    }
  }

  toUserBean(): UserBean {
    let user = super.toUserBean();
    // @ts-ignore
    user.otherPhone = this.extensionPhoneNumber;
    return user;
  }

  static buildArray(users: IMemoDocumentUser[]): MemoDocumentUser[] {
    return (users && users.length) ? _.map(users, u => {
      let user = new MemoDocumentUser();
      user.build(u);
      return user;
    }) : [];
  }
}

export class MemoDocument extends DocumentBase<IMemoDocument, MemoDocument> {
  folderId: string = '';
  IdDraftFilesRegInfo: string = '';
  statusId: DocumentStatus = new DocumentStatus();
  number: string = '';
  beginDate: Date = null;
  creator: MemoDocumentUser = new MemoDocumentUser();
  assistant: MemoDocumentUser = new MemoDocumentUser();
  executor: MemoDocumentUser[] = [];
  recipient: MemoDocumentUser = new MemoDocumentUser();
  content: string = '';
  description: string = '';
  fileId: string[] = [];
  comment: DocumentComment[] = [];
  result?: MemoDocumentResult[] = []; // Исполнение
  review?: MemoDocumentReview[] = []; // Ознакомление
  memoKind: MemoKind = new MemoKind();
  draftFiles: FileType[];
  attachedFile: FileType[];
  draftWithoutAtt: FileType[];
  attachment: FileType[];
  approval: MemoApproval = new MemoApproval();
  approvalHistory: MemoApprovalHistory = new MemoApprovalHistory();
  recipientGroup: NSIEmailGroup = null;
  // recipientGroup: NSIEmailGroup[] = [];
  headingTopic: MemoHeadingTopic = new MemoHeadingTopic();
  activityKind: MemoTrestActivityKind = new MemoTrestActivityKind();
  privacy: any = null;
  relateId: string[] = [];
  externalRelation: string = '';
  approvalElectronically: boolean = false;
  signingElectronically: boolean = false;
  note: string = '';
  reportTermDoc: MemoReportTermDoc = new MemoReportTermDoc();
  ds: MemoDs = new MemoDs();
  limitedAccess: boolean = null;

  build(i?: IMemoDocument) {
    super.build(i);
    this.statusId.build(i.statusId);
    this.creator.build(i.creator);
    this.assistant.build(i.assistant);
    this.recipient.build(i.recipient);
    this.memoKind.build(i.memoKind);
    this.beginDate = i.beginDate ? new Date(i.beginDate) : null;

    if (i.headingTopic) {
      this.headingTopic.build(i.headingTopic);
    }
    if (i.activityKind) {
      this.activityKind.build(i.activityKind);
    }
    if (i.privacy) {
      this.privacy = i.privacy;
    }
    this.description = i.description;
    this.externalRelation = i.externalRelation;
    this.relateId = i.relateId || [];
    this.executor = MemoDocumentUser.buildArray(i.executor);
    this.comment = DocumentComment.buildArray(i.comment);
    this.review = MemoDocumentReview.buildArray(i.review);
    this.result = MemoDocumentResult.buildArray(i.result);
    this.fileId = (i && i.fileId && i.fileId.length) ? i.fileId : [];
    this.draftFiles = (i && i.draftFiles && i.draftFiles.length) ? i.draftFiles : [];
    this.attachment = (i && i.attachment && i.attachment.length) ? i.attachment : [];
    this.draftWithoutAtt = (i && i.draftWithoutAtt && i.draftWithoutAtt.length) ? i.draftWithoutAtt : [];
    if (i.approval) {
      this.approval.build(i.approval);
    }
    if (i.approvalHistory) {
      this.approvalHistory.build(i.approvalHistory);
    }
    this.number = i.number || '';
    this.attachedFile = (i && i.attachedFile && i.attachedFile.length) ? i.attachedFile : [];
    this.approvalElectronically = i.approvalElectronically;
    this.signingElectronically = i.signingElectronically;
    this.IdDraftFilesRegInfo = i.IdDraftFilesRegInfo;
    this.note = i.note;
    if (i.reportTermDoc) {
      this.reportTermDoc.build(i.reportTermDoc)
    }
    if (i.ds) {
      this.ds.build(i.ds)
    }
    this.limitedAccess = i.limitedAccess;
  }
}
