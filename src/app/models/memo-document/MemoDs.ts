import {DocumentBase} from '../core/DocumentBase';
import {IMemoApprovalListItemSignature} from './abstracts/IMemoApproval';
import {IMemoDs} from './abstracts/IMemoDs';


export class MemoDs extends DocumentBase<IMemoDs, MemoDs> {
  dsLastName: string = '';
  dsName: string = '';
  dsPosition: string = '';
  dsCN: string = '';
  dsFrom: string = null;
  dsTo: string = null;

  build(i?: IMemoApprovalListItemSignature) {
    super.build(i);
  }
}
