import {DocumentBase} from '../core/DocumentBase';
import {IMemoReportTerm, IMemoReportTermDoc} from './abstracts/IMemoReportTermDoc';
import {NSIMemoReportTerm} from '../nsi/NSIMemoReportTerm';

// @dynamic
export class MemoReportTermDoc extends DocumentBase<IMemoReportTermDoc, MemoReportTermDoc> {
  reportTerm: MemoReportTerm = new MemoReportTerm();

  build(i?: IMemoReportTermDoc) {
    super.build(i);
    this.reportTerm.build(i.reportTerm)
  }

  updateFromNsiMemoReportTerm(nsi: NSIMemoReportTerm) {
      this.reportTerm.code = nsi ? nsi.Code : null;
      this.reportTerm.name = nsi ? nsi.Name : null;
      this.reportTerm.duration = nsi ? parseInt(nsi.Duration) : null;
  }

  toNsiMemoReportTerm() {
    let result: NSIMemoReportTerm = new NSIMemoReportTerm();
    result.Code = this.reportTerm.code;
    result.Name = this.reportTerm.name;
    result.Duration = "" + this.reportTerm.duration;
    return result;
  }
}

// @dynamic
export class MemoReportTerm extends DocumentBase<IMemoReportTerm, MemoReportTerm> {
  code: string = '';
  name: string = '';
  duration: number = null;

  build(i?: IMemoReportTerm) {
    super.build(i);
  }
}
