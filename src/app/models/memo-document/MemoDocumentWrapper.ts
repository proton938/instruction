import {MemoDocument} from './MemoDocument';
import {IMemoDocumentWrapper} from "./abstracts/IMemoDocumentWrapper";
import {DocumentBase} from "../core/DocumentBase";

export class MemoDocumentWrapper extends DocumentBase<IMemoDocumentWrapper, MemoDocumentWrapper> {
  document: MemoDocument = new MemoDocument();

  build(i?: IMemoDocumentWrapper) {
    super.build(i);
    this.document.build(i.document);
  }
}
