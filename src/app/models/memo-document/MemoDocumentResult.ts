import { DocumentUser } from '../core/DocumentUser';
import { DocumentBase } from '../core/DocumentBase';
import { IMemoDocumentResult } from './abstracts/IMemoDocumentResult';
import * as _ from 'lodash';

// @dynamic
export class MemoDocumentResult extends DocumentBase<IMemoDocumentResult, MemoDocumentResult> {
  reportExecutor: DocumentUser = new DocumentUser(); // Фактический исполнитель служебной записки
  reportDate: Date = null; // Дата рассмотрения
  reportContent = ''; // Комментарий
  reportFileId: string[] = []; // Идентификаторы файлов
  reportAcceptor: DocumentUser = new DocumentUser(); // Пользователь, принявший информацию по служебной записке
  acceptDate: Date = null; // Дата принятия информации
  acceptContent = ''; // Содержание принятия информации
  processId = ''; //Идентификатор процесса
  reportButton = ''; //Информация о кнопке, по которой завершена задача "Рассмотреть служебную записку"
  acceptFileId: string[] = []; //Идентификаторы файлов принятия информации по СЗ
  redirectedBy: DocumentUser[] = []; //Перенаправлена пользователям

  static buildArray(results: IMemoDocumentResult[]): MemoDocumentResult[] {
    return (results && results.length) ? results.map(function (r) {
      const result = new MemoDocumentResult();
      result.build(r);
      return result;
    }) : [];
  }

  build(i?: IMemoDocumentResult) {
    super.build(i);
    this.reportExecutor.build(i.reportExecutor);
    this.reportAcceptor.build(i.reportAcceptor);
    this.reportFileId = (i && i.reportFileId && i.reportFileId.length) ? i.reportFileId : [];
    this.acceptFileId = (i && i.acceptFileId && i.acceptFileId.length) ? i.acceptFileId : [];
    this.redirectedBy = DocumentUser.buildArray(i.redirectedBy);
  }
}
