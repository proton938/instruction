import {DocumentUser} from '../core/DocumentUser';
import {DocumentBase} from '../core/DocumentBase';
import * as _ from 'lodash';
import {IMemoDocumentReview} from './abstracts/IMemoDocumentReview';

// @dynamic
export class MemoDocumentReview extends DocumentBase<IMemoDocumentReview, MemoDocumentReview> {
  reviewBy: DocumentUser = new DocumentUser(); // Ознакомился
  factReviewBy: DocumentUser = new DocumentUser(); // Ознакомился фактически (делегант)
  sentReviewBy: DocumentUser = new DocumentUser(); // Кто отправил на ознакомление
  reviewPlanDate: Date = null; // Плановая дата ознакомления
  reviewFactDate: Date = null; // Фактическая дата ознакомления
  reviewNote = ''; // Комментарий

  static buildArray(reviews: IMemoDocumentReview[]): MemoDocumentReview[] {
    return (reviews && reviews.length) ? _.map(reviews, r => {
      const review = new MemoDocumentReview();
      review.build(r);
      return review;
    }) : [];
  }

  build(i?: IMemoDocumentReview) {
    super.build(i);
    this.reviewBy.build(i.reviewBy);
    this.factReviewBy.build(i.factReviewBy);
    this.sentReviewBy.build(i.sentReviewBy);
  }

}
