import {IDocumentBase} from '../../core/abstracts/IDocumentBase';

export interface IMemoReportTermDoc extends IDocumentBase {
  reportTerm?: IMemoReportTerm;
}

export interface IMemoReportTerm extends IDocumentBase {
  code?: string;
  name?: string;
  duration?: number;
}
