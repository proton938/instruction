import {IDocumentUser} from '../../core/abstracts/IDocumentUser';
import {IDocumentBase} from '../../core/abstracts/IDocumentBase';

export interface IMemoDocumentResult extends IDocumentBase {
  reportExecutor?: IDocumentUser;
  reportDate?: string;
  reportContent?: string;
  reportFileId?: string[];
  reportAcceptor?: IDocumentUser;
  acceptDate?: string;
  acceptContent?: string;
  processId?: string;
  reportButton: string;
  acceptFileId:  string[];
  redirectedBy: IDocumentUser[];
}
