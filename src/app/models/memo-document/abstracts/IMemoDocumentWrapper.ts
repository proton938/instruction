import {IDocumentBase} from "../../core/abstracts/IDocumentBase";
import {IMemoDocument} from "./IMemoDocument";

export interface IMemoDocumentWrapper extends IDocumentBase {
  document?: IMemoDocument;
}
