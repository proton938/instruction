import {IDocumentStatus} from '../../core/abstracts/IDocumentStatus';
import {IDocumentComment} from '../../core/abstracts/IDocumentComment';
import {IDocumentUser} from '../../core/abstracts/IDocumentUser';
import {IDocumentBase} from '../../core/abstracts/IDocumentBase';
import {IMemoDocumentReview} from './IMemoDocumentReview';
import {IMemoDocumentResult} from './IMemoDocumentResult';
import {IMemoKind} from '../../memo/abstracts/IMemoKind';
import {NSIEmailGroup} from '../../nsi/NSIEmailGroups';
import {MemoHeadingTopic} from '../../memo/MemoHeadingTopic';
import {MemoTrestActivityKind} from '../../memo/MemoTrestActivityKind';
import {FileType} from '../../instruction/FileType';
import {IMemoReportTermDoc} from './IMemoReportTermDoc';
import {IMemoApprovalHistory, IMemoApproval} from './IMemoApproval';
import {IMemoDs} from './IMemoDs';

export interface IMemoDocumentUser extends IDocumentUser {
  extensionPhoneNumber?: string;
}

export interface IMemoDocument extends IDocumentBase {
  folderId?: string; // Идентификатор папки в FN
  IdDraftFilesRegInfo?: string; // Идентификатор файла проекта служебной записки с рег данными
  statusId?: IDocumentStatus; // Статус служебной записки
  number?: string; // Номер служебной записки
  beginDate?: string; // Дата служебной записки (без времени)
  creator?: IMemoDocumentUser; // Автор служебной записки
  assistant?: IMemoDocumentUser; // Карточку создал
  executor?: IMemoDocumentUser[]; // Исполнитель служебной записки
  recipient?: IMemoDocumentUser; // Адресат служебной записки
  content?: string; // Тема служебной записки
  description?: string; // Описание служебной записки
  fileId?: string[]; // Идентификаторы файлов служебной записки
  comment?: IDocumentComment[]; // Комментарии
  review?: IMemoDocumentReview[]; // Ознакомление
  result?: IMemoDocumentResult[]; // Ознакомление
  memoKind: IMemoKind;
  attachedFile?: FileType[];
  draftWithoutAtt?: FileType[];
  attachment?: FileType[];
  draftFiles?: FileType[];
  approval?: IMemoApproval;
  approvalHistory?: IMemoApprovalHistory;
  recipientGroup?: NSIEmailGroup;
  headingTopic?: MemoHeadingTopic;
  activityKind?: MemoTrestActivityKind;
  privacy?: any;
  relateId?: string[];
  externalRelation?: string;
  approvalElectronically: boolean;
  signingElectronically: boolean;
  note?: string;
  reportTermDoc?: IMemoReportTermDoc;
  ds: IMemoDs;
  limitedAccess?: boolean
}
