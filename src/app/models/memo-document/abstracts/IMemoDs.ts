import {IDocumentBase} from '../../core/abstracts/IDocumentBase';

export interface IMemoDs extends IDocumentBase {
  dsLastName?: string;
  dsName?: string;
  dsPosition?: string;
  dsCN?: string;
  dsFrom?: string;
  dsTo?: string;
}
