import {FileType} from '../../instruction/FileType';
import {IDocumentBase} from '../../core/abstracts/IDocumentBase';

export interface IMemoApprovalAdditionalAgreed extends IDocumentBase {
  accountName?: string;
  fio?: string;
  iofShort?: string;
}

export interface IMemoApprovalAdded extends IDocumentBase {
  additionalAgreed?: IMemoApprovalAdditionalAgreed;
  addedNote?: string;
}

export interface IMemoApprovalListItemSignature extends IDocumentBase {
  agreedDsLastName?: string;
  agreedDsName?: string;
  agreedDsPosition?: string;
  agreedDsCN?: string;
  agreedDsFrom?: string;
  agreedDsTo?: string;
}

export interface IMemoApprover extends IDocumentBase {
  post?: string;
  fioFull?: string;
  accountName?: string;
  fioShort?: string;
  iofShort?: string;
  phone?: string;
}

export interface IMemoApprovalTerm extends IDocumentBase {
  code?: string;
  name?: string;
  duration?: number;
}

export interface IMemoApprovalListItem extends IDocumentBase {
  approvalNum?: string;
  approvalTime?: string;
  agreedBy?: IMemoApprover;
  factAgreedBy?: IMemoApprover;
  approvalPlanDate?: string;
  approvalFactDate?: string;
  approvalResult?: string;
  approvalType?: string;
  approvalTypeCode?: string;
  approvalNote?: string;
  fileApproval?: FileType[];
  agreedDs?: IMemoApprovalListItemSignature;
  added?: IMemoApprovalAdded[];
  approvalTerm?: IMemoApprovalTerm;
}

export interface IMemoApprovalList extends IDocumentBase {
  approvalCycleNum?: string;
  approvalCycleDate?: string;
  approvalCycleFile?: FileType[];
  agreed?: IMemoApprovalListItem[];
}

export interface IMemoApprovalHistoryList extends IDocumentBase {
  approvalCycleNum?: string;
  approvalCycleDate?: string;
  agreed?: IMemoApprovalListItem[];
  approvalCycleFile?: FileType[];
}

export interface IMemoApprovalHistory extends IDocumentBase {
  approvalCycle?: IMemoApprovalHistoryList[];
}

export interface IMemoApproval extends IDocumentBase {
  approvalCycle: IMemoApprovalList;
}
