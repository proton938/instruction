export class HistoryLogModel {
   dateEdit: number;
   userName: string;
   operation: string;
   path: string;
   value: any;

   constructor(_dateEdit: number, _userName: string, _operation: string, _path: string, _value: any) {
      this.dateEdit = _dateEdit;
      this.userName = _userName;
      this.operation = _operation;
      this.path = _path;
      this.value = _value;
   }
}
