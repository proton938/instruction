import {HistoryLogModel} from "./HistoryLogModel";

export class HistoryLogWrapper {
   type: string = '';
   typeName: string = '';
   id: string = '';
   docId: string = '';
   date: number = null;
   comment: string = '';
   user: string = '';
   logs: HistoryLogModel[] = [];

   constructor(d: HistoryLogWrapper) {
      this.type = d && d.type ? d.type : '';
      this.typeName = d && d.typeName ? d.typeName : '';
      this.id = d && d.id ? d.id : '';
      this.docId = d && d.docId ? d.docId : '';
      this.date = d && d.date ? d.date : null;
      this.comment = d && d.comment ? d.comment : '';
      this.user = d && d.user ? d.user : '';
      this.logs = d && d.logs ? d.logs.map(i => new HistoryLogModel(i.dateEdit, i.userName, i.operation, i.path, i.value)) : [];
   }
}
