import {HistoryLogFilter} from './HistoryLogFilter';
import {HistoryLogPagination} from './HistoryLogPagination';
import {HistoryLogWrapper} from "./HistoryLogWrapper";

export class HistoryAdvancedModel {
   logs: HistoryLogWrapper[];
   allLogs: HistoryLogWrapper[];
   filter: HistoryLogFilter;
   pagination: HistoryLogPagination;
}
