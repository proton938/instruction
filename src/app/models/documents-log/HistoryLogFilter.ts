import * as angular from 'angular';
import {HistoryLogModel} from './HistoryLogModel';
import * as _ from "lodash";
import {HistoryLogWrapper} from "./HistoryLogWrapper";

export class HistoryLogFilter {
   dateEdit: number = null;
   user: string = '';
   operation: string = '';
   path: string = '';
   value: string = '';
   dateEditMin: number;
   dateEditMax: number;
   users: string[] = [];
   operations: string[] = [];

   constructor(logs: HistoryLogModel[]) {
      this.fillAllowableValues(logs);
   }

   fillAllowableValues(logs: HistoryLogModel[]) {
      this.dateEditMin = _.min(_.map(logs, l => {
         return l.dateEdit;
      }));
      this.dateEditMax = _.max(_.map(logs, l => {
         return l.dateEdit;
      }));
      this.operations = _.map(_.uniqBy(logs, 'operation'), l => {
         return l.operation;
      });
      this.operations.push('');
      this.users = _.map(_.uniqBy(logs, 'userName'), l => {
         return l.userName;
      });
      this.users.push('');
   }

   clearFilter() {
      this.dateEdit = null;
      this.user = '';
      this.operation = '';
      this.path = '';
      this.value = '';
   }

   isEmpty(): boolean {
      return (this.dateEdit === null) && (this.user === '') && (this.operation === '') && (this.path === '') && (this.value === '');
   }

   run(logs: HistoryLogModel[]): HistoryLogModel[] {
      let result: HistoryLogModel[] = logs;

      if (this.operation.trim() !== '') {
         result = _.filter(result, r => {
            return r.operation === this.operation;
         });
      }

      if (this.user.trim() !== '') {
         result = _.filter(result, r => {
            return r.userName === this.user;
         });
      }

      if (this.dateEdit !== null) {
         result = _.filter(result, r => {
            let dl = new Date(r.dateEdit);
            dl.setHours(0, 0, 0, 0);
            return dl.getTime() === this.dateEdit;
         });
      }

      if (this.path.trim() !== '') {
         result = _.filter(result, r => {
            return r.path.indexOf(this.path) > -1;
         });
      }

      if (this.value.trim() !== '') {
         result = _.filter(result, r => {
            if (r.value) {
               let str = JSON.stringify(r.value);
               return str.indexOf(this.value) > -1;
            } else {
               return false;
            }
         });
      }

      return result;
   }

   runAdvanced(logWrappers: HistoryLogWrapper[]): HistoryLogWrapper[] {
      return _.map(logWrappers, w => {
         let result: HistoryLogWrapper = new HistoryLogWrapper(JSON.parse(angular.toJson(w)));
         result.logs = _.filter(w.logs, (log: HistoryLogModel) => {
            let r = true;
            if (r && this.operation.trim() !== '') r = log.operation === this.operation;
            if (r && this.user.trim() !== '') r = log.userName === this.user;
            if (r && this.dateEdit !== null) r = this._getDateTime(log.dateEdit) === this.dateEdit;
            if (r && this.path.trim() !== '') r = log.path.indexOf(this.path) > -1;
            if (r && this.value.trim() !== '') r = log.value ? angular.toJson(log.value).indexOf(this.value) > -1 : false;
            return r;
         });
         return result;
      });
   }

   private _getDateTime(dateString) {
      let d = new Date(dateString);
      d.setHours(0, 0, 0, 0);
      return d.getTime();
   }
}
