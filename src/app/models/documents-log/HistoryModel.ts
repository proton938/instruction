import {HistoryLogModel} from './HistoryLogModel';
import {HistoryLogSorting} from './HistoryLogSorting';
import {HistoryLogFilter} from './HistoryLogFilter';
import {HistoryLogPagination} from './HistoryLogPagination';

export class HistoryModel {
   logs: HistoryLogModel[];
   allLogs: HistoryLogModel[];
   sortings: HistoryLogSorting[];
   filter: HistoryLogFilter;
   pagination: HistoryLogPagination;
}
