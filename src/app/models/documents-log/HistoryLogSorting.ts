import {HistoryLogModel} from './HistoryLogModel';
import * as _ from "lodash";

export class HistoryLogSorting {
   name: string;
   value: string;
   text: string;
   width: string;

   constructor(n: string, v: string, t: string, w?: string) {
      this.name = n;
      this.value = v;
      this.text = t;
      if (w) {
         this.width = w;
      }
   }

   static run(model: HistoryLogModel[], sortings: HistoryLogSorting[]): HistoryLogModel[] {
      let result: HistoryLogModel[] = model;

      sortings.slice().reverse().forEach(sorting => {
         switch (sorting.value) {
            case 'desc':
               result = _.orderBy(result, [sorting.name], ['asc']);
               break;
            case 'asc':
               result = _.orderBy(result, [sorting.name], ['desc']);
               break;
            default:
               break;
         }
      });

      return result;
   }

   static updateSortings(columnName: string, sortings: HistoryLogSorting[]) {
      sortings.forEach(v => {
         v.value = (v.name !== columnName) ? 'none' : ((v.value === 'desc') ? 'asc' : 'desc');
      });
   }

   static fillSortings(): HistoryLogSorting[] {
      return [
         new HistoryLogSorting('operation', 'none', 'Операция'),
         new HistoryLogSorting('dateEdit', 'asc', 'Дата'),
         new HistoryLogSorting('userName', 'none', 'Пользователь'),
         new HistoryLogSorting('path', 'none', 'Поле'),
         new HistoryLogSorting('value', 'none', 'Значение')
      ];
   }
}

