import {HistoryLogModel} from './HistoryLogModel';
import * as _ from "lodash";

export class HistoryLogPagination {
   totalItems: number;
   currentPage: number;
   pageSize: number;

   constructor(totalItems: number, pageSize: number = 20) {
      this.totalItems = totalItems;
      this.currentPage = 0;
      this.pageSize = pageSize;
   }

   needOfNavigation(): boolean {
      return this.totalItems > this.pageSize;
   }

   update(totalItems: number) {
      this.totalItems = totalItems;
      this.currentPage = 0;
   }

   run(logs: HistoryLogModel[]): HistoryLogModel[] {
      let result: HistoryLogModel[] = logs;

      if (this.needOfNavigation()) {
         let startIndex = this.currentPage * this.pageSize;
         result = _.slice(result, startIndex, this.pageSize);
      }

      return result;
   }
}

