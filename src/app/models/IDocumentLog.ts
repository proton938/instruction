import {IJsonPatchData} from "./IJsonPatchData";
import {IInstructionDocumentWrapper} from "./instruction-document/abstracts/IInstructionDocumentWrapper";

export interface IDocumentLog {
  dateEdit: string;
  id: string;
  idDoc: string;
  jsonNew: IInstructionDocumentWrapper;
  jsonOld?: IInstructionDocumentWrapper;
  jsonPatch?: IJsonPatchData[];
  md5New: string;
  md5Old?: string;
  userName: string;
}
