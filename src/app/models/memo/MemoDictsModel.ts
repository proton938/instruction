import * as _ from 'lodash';
import {NSIInstructionExecutors} from "../nsi/NSIInstructionExecutors";
import {NSIMemoStatus} from "../nsi/NSIMemoStatus";
import {ApprovalType} from '../../models/instruction/ApprovalType';
import {NSIMemoReportTerm} from '../nsi/NSIMemoReportTerm';

export class MemoDictsModel {
   statuses: NSIMemoStatus[] = [];
   reportTerms: NSIMemoReportTerm[] = [];
   executors: NSIInstructionExecutors[] = [];
   approvalTypes: { [key: string]: ApprovalType };

   findStatusByCode(code:string): NSIMemoStatus {
      return _.find(this.statuses, s => { return s.code === code; });
   }
}
