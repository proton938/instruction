import { IMemoHeadingTopic } from './abstracts/IMemoHeadingTopic';
import { DocumentBase } from '../core/DocumentBase';

export class MemoHeadingTopic extends DocumentBase<IMemoHeadingTopic, MemoHeadingTopic> {
  code?: string;
  name?: string;

  build(i: IMemoHeadingTopic) {
    this.code = i.code;
    this.name = i.name;
  }
}
