import { IDocumentBase } from '../../core/abstracts/IDocumentBase';

export interface IMemoHeadingTopic extends IDocumentBase {
  code?: string;
  name?: string;
}
