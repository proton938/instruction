import { IDocumentBase } from '../../core/abstracts/IDocumentBase';

export interface IMemoKind extends IDocumentBase {
  code?: string;
  name?: string;
}
