import { IDocumentBase } from '../../core/abstracts/IDocumentBase';

export interface IMemoTrestActivityKind extends IDocumentBase {
  code?: string;
  name?: string;
}
