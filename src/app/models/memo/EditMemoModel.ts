import {MemoDictsModel} from "./MemoDictsModel";
import {UserBean} from "@reinform-cdp/nsi-resource";
import {MemoDocument, MemoDocumentUser} from "../memo-document/MemoDocument";
import {MemoKind} from './MemoKind';
import {MemoHeadingTopic} from './MemoHeadingTopic';
import {NSIEmailGroup} from '../nsi/NSIEmailGroups';
import {MemoTrestActivityKind} from './MemoTrestActivityKind';
import {InstructionApprover} from '../instruction/InstructionApprovalList';
import {NSIMemoReportTerm} from '../nsi/NSIMemoReportTerm';
import {MemoApproval, MemoApprovalHistory} from '../memo-document/MemoApproval';

export class EditMemoModel {
  creator: UserBean = null;
  assistant: UserBean = null;
  executor: UserBean[] = [];
  recipient: UserBean = null;
  head: UserBean = null;
  content: string = '';
  description: string = '';
  relateId: string[] = [];
  externalRelation: string = '';
  beginDate: Date = null;
  memoKind: MemoKind = null;
  headingTopic: MemoHeadingTopic = null;
  recipientGroup: NSIEmailGroup = null;
  // recipientGroup: NSIEmailGroup[] = [];
  activityKind: MemoTrestActivityKind = null;
  privacy: any = null;
  approval: MemoApproval;
  approvalHistory: MemoApprovalHistory;
  attachedFile: File[];
  draftWithoutAtt: File;
  attachment: File[];
  draftFiles: File[];
  signingElectronically: boolean = false;
  systemCode?: string;
  files: any[] = [];
  approvalElectronically: boolean = false;
  number: string = '';
  note: string = '';
  reportTerm: NSIMemoReportTerm;

  isValid(): boolean {
    if (!this.memoKind) {
      return false;
    }

    if (this.memoKind.code === '16' && this.executor && !this.executor.length) {
      return false;
    }

    if (this.memoKind.code === '16' && this.approvalElectronically && !this.description) {
      return false;
    }

    if (this.memoKind.code === '16' && this.approvalElectronically && this.description.trim() === '') {
      return false;
    }

    return !!(this.creator && this.assistant && this.content && this.recipient);
  }

  updateDocumentFromModel(document: MemoDocument, forSaving = false) {
    document.creator.updateFromUserBean(this.creator);
    document.assistant.updateFromUserBean(this.assistant);
    document.executor = [];
    this.executor.forEach(exc => {
      let e: MemoDocumentUser = new MemoDocumentUser();
      e.updateFromUserBean(exc);
      document.executor.push(e);
    });
    document.recipient.updateFromUserBean(this.recipient);
    document.content = this.content;
    document.description = this.description;
    document.approval = this.approval;

    if (forSaving && document.approval) {
      if (document.approval.approvalCycle) {
        if (document.approval.approvalCycle.agreed) {
          document.approval.approvalCycle.agreed.forEach((a) => {
            a.agreedBy = InstructionApprover.cleanInstructionApprover(a.agreedBy);

            if (a.factAgreedBy) {
              a.factAgreedBy = InstructionApprover.cleanInstructionApprover(a.factAgreedBy);
            }
          });
        }
      }
    }

    document.approvalHistory = this.approvalHistory;
    document.memoKind = this.memoKind;
    document.recipientGroup = this.recipientGroup;
    document.headingTopic = this.headingTopic;
    document.activityKind = this.activityKind;
    document.privacy = this.privacy;
    document.relateId = this.relateId;
    document.externalRelation = this.externalRelation;
    document.approvalElectronically = this.approvalElectronically;
    document.signingElectronically = this.signingElectronically;
    document.number = this.number;
    document.note = this.note;
    document.reportTermDoc.updateFromNsiMemoReportTerm(this.reportTerm);
  }

  updateModelBeforeEditing(document: MemoDocument, dicts: MemoDictsModel) {
    this.creator = document.creator.toUserBean();
    this.assistant = document.assistant.toUserBean();
    document.executor.forEach(exc => {
      let e: UserBean = new UserBean();
      e = exc.toUserBean();
      this.executor.push(e);
    });
    this.recipient = document.recipient.toUserBean();
    this.content = document.content;
    this.description = document.description;
    const memoKind = new MemoKind();
    memoKind.build(document.memoKind);
    this.memoKind = memoKind;
    this.recipientGroup = document.recipientGroup;
    const headingTopic = new MemoHeadingTopic();
    headingTopic.build(document.headingTopic);
    this.headingTopic = headingTopic;
    const activityKind = new MemoTrestActivityKind();
    activityKind.build(document.activityKind);
    this.activityKind = activityKind;
    this.privacy = document.privacy;
    this.approval = document.approval;
    this.approvalHistory = document.approvalHistory;
    this.relateId = document.relateId;
    this.externalRelation = document.externalRelation;
    this.approvalElectronically = document.approvalElectronically;
    this.signingElectronically = document.signingElectronically;
    this.number = document.number;
    this.note = document.note;
    this.reportTerm = document.reportTermDoc.toNsiMemoReportTerm();

  }
}
