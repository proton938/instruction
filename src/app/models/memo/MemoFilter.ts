import { DateRangeFilter } from "../DateRangeFilter";
import {
   NSIMemoStatus
} from '../nsi/NSIMemoStatus';
import * as _ from "lodash";
import { UserBean } from "@reinform-cdp/nsi-resource";

export class MEMOFilter {
   statusMemo: NSIMemoStatus[] = [];
   kindMemo: any[] = [];
   docNumberMemo: string = "";
   creatorMemo: UserBean = null;
   assistantMemo: UserBean = null;
   executorMemo: UserBean = null;
   recipientMemo: UserBean = null;
   headingTopicMemo: any[] = [];
   activityKindMemo: any[] = [];
   privacyMemo: any[] = [];
   externalRelationMemo: string = "";
   docDateMemo: DateRangeFilter = new DateRangeFilter();
   contentMemo: string = "";
   descriptionMemo: string = "";
   limitedAccess: boolean = null;

   clear() {
      this.statusMemo.length = 0;
      this.kindMemo.length = 0;
      this.headingTopicMemo.length = 0;
      this.activityKindMemo.length = 0;
      this.privacyMemo.length = 0;
      this.recipientMemo = null;
      this.docNumberMemo = "";
      this.externalRelationMemo = "";
      this.creatorMemo = null;
      this.assistantMemo = null;
      this.executorMemo = null;
      this.docDateMemo.clear();
      this.contentMemo = "";
      this.descriptionMemo = "";
      this.limitedAccess = null;
   }

   isEmpty(): boolean {
      return this.statusMemo.length === 0 &&
         this.kindMemo.length === 0 &&
         this.headingTopicMemo.length === 0 &&
         this.activityKindMemo.length === 0 &&
         this.privacyMemo.length === 0 &&
         this.recipientMemo === null &&
         this.externalRelationMemo === "" &&
         this.docNumberMemo === "" &&
         this.creatorMemo === null &&
         this.assistantMemo === null &&
         this.executorMemo === null &&
         this.docDateMemo.isEmpty() &&
         this.contentMemo === "" &&
         this.descriptionMemo === "";
   }

   count(): number {
      return (this.statusMemo.length === 0 ? 0 : 1) +
         (this.docNumberMemo === "" ? 0 : 1) +
         (this.creatorMemo === null ? 0 : 1) +
         (this.assistantMemo === null ? 0 : 1) +
         (this.executorMemo === null ? 0 : 1) +
         (this.docDateMemo.isEmpty() ? 0 : 1) +
         (this.contentMemo === "" ? 0 : 1) +
         (this.descriptionMemo === "" ? 0 : 1) +
         (this.kindMemo.length === 0 ? 0 : 1) +
         (this.headingTopicMemo.length === 0 ? 0 : 1) +
         (this.activityKindMemo.length === 0 ? 0 : 1) +
         (this.privacyMemo.length === 0 ? 0 : 1) +
         (this.recipientMemo === null ? 0 : 1) +
         (this.externalRelationMemo === "" ? 0 : 1);
   }

   toString(): string {
      let result: string = "";

      if (this.statusMemo.length > 0) {
         let conditions = _.map(this.statusMemo, c => { return `statusCodeMemo:${c.code}`; });
         result += `(${conditions.join(' OR ')})`
      }

      if (this.activityKindMemo.length > 0) {
         let conditions = _.map(this.activityKindMemo, c => { return `activityKindMemo:"${c.name}"`; });
         result += `(${conditions.join(' OR ')})`
      }

      if (this.kindMemo.length > 0) {
         let conditions = _.map(this.kindMemo, c => { return `kindMemo: "${c.name}"`; });
         result += `(${conditions.join(' OR ')})`
      }

      if (this.headingTopicMemo.length > 0) {
         let conditions = _.map(this.headingTopicMemo, c => { return `headingTopicMemo:"${c.name}"`; });
         result += `(${conditions.join(' OR ')})`
      }

      if (this.privacyMemo.length > 0) {
         let conditions = _.map(this.privacyMemo, c => { return `privacyMemo:"${c.name}"`; });
         result += `(${conditions.join(' OR ')})`
      }

      if (this.docNumberMemo) {
         let contents = _.map(this.docNumberMemo.split(' '), v => { return v.trim(); });
         let content = contents.join('\\\\ ');
         result += (result) ? " AND " : "";
         result += `docNumberMemo:(*${content}*)`;
      }

      if (this.creatorMemo !== null && this.creatorMemo !== undefined) {
         result += (result) ? " AND " : "";
         const creatorMemo = this.creatorMemo.displayName.split(' ');
         const subResult = '(' + creatorMemo.map((str: string, index: number): string => index !== creatorMemo.length - 1 ? `*${str}* AND ` : `*${str}*`).join('') + ')';
         result += `(creatorMemo:${subResult})`;
      }

      if (this.assistantMemo !== null && this.assistantMemo !== undefined) {
         result += (result) ? " AND " : "";
         const assistantMemo = this.assistantMemo.displayName.split(' ');
         const subResult = '(' + assistantMemo.map((str: string, index: number): string => index !== assistantMemo.length - 1 ? `*${str}* AND ` : `*${str}*`).join('') + ')';
         result += `(assistantMemo:${subResult})`;
      }

      if (this.executorMemo !== null && this.executorMemo !== undefined) {
         result += (result) ? " AND " : "";
         const executorMemo = this.executorMemo.displayName.split(' ');
         const subResult = '(' + executorMemo.map((str: string, index: number): string => index !== executorMemo.length - 1 ? `*${str}* AND ` : `*${str}*`).join('') + ')';
         result += `(executorMemo:${subResult})`;
      }

      if (this.recipientMemo !== null && this.recipientMemo !== undefined) {
         result += (result) ? " AND " : "";
         const recipientMemo = this.recipientMemo.displayName.split(' ');
         const subResult = '(' + recipientMemo.map((str: string, index: number): string => index !== recipientMemo.length - 1 ? `*${str}* AND ` : `*${str}*`).join('') + ')';
         result += `(recipientMemo:${subResult})`;
      }

      if (!this.docDateMemo.isEmpty()) {
         result += (result) ? " AND " : "";
         result += `(docDateMemo:${this.docDateMemo.toString()})`;
      }

      if (this.contentMemo) {
         let contents = _.map(this.contentMemo.split(' '), v => { return v.trim(); });
         let content = contents.join('\\\\ ');
         result += (result) ? " AND " : "";
         result += `contentMemo:(*${content}*)`;
      }

      if (this.descriptionMemo) {
         let contents = _.map(this.descriptionMemo.split(' '), v => { return v.trim(); });
         let content = contents.join('\\\\ ');
         result += (result) ? " AND " : "";
         result += `descriptionMemo:(*${content}*)`;
      }

      if (this.externalRelationMemo) {
         let contents = _.map(this.externalRelationMemo.split(' '), v => { return v.trim(); });
         let content = contents.join('\\\\ ');
         result += (result) ? " AND " : "";
         result += `externalRelationMemo:(*${content}*)`;
      }

     if (this.limitedAccess) {
       result += (result) ? " AND " : "";
       result += `(limitedAccessMemo:${this.limitedAccess})`;
     }

      return `(${result})`;
   }
}
