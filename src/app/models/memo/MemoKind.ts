
import { IMemoKind } from './abstracts/IMemoKind';
import { DocumentBase } from '../core/DocumentBase';

export class MemoKind extends DocumentBase<IMemoKind, MemoKind>  {
  code?: string;
  name?: string;

  build(i?: IMemoKind) {
    if (!i) {
      return;
    }

    this.code = i.code;
    this.name = i.name;
  }
}
