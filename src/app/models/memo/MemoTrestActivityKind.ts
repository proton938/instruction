import { IMemoTrestActivityKind } from './abstracts/IMemoTrestActivityKind';
import { DocumentBase } from '../core/DocumentBase';

export class MemoTrestActivityKind extends DocumentBase<IMemoTrestActivityKind, MemoTrestActivityKind> {
  code?: string;
  name?: string;

  build(i: IMemoTrestActivityKind) {
    this.code = i.code;
    this.name = i.name;
  }
}
