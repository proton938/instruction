export class NSIMemoAppDuration {
  Code?: string;
  Name?: string;
  Default?: boolean;
  Duration?: string;
}
