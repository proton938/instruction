// Приоритеты поручений
export class NSIInstructionPriority {
  code?: string; // Код
  name?: string; // Наименование
  color?: string; // Цвет
}
