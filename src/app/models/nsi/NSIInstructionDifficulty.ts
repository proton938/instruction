// Сложности поручений
export class NSIInstructionDifficulty {
  code?: string; // Код
  name?: string; // Наименование
  color?: string; // Цвет
}
