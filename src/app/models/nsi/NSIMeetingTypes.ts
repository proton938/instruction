import { NSIMeetingPlace } from "./NSIMeetingPlace";

// Виды совещаний
export class NSIMeetingTypes {
  meetingType?: string; // Код вида заседания
  meetingShortName?: string; // Название вида заседания краткое
  meetingFullName?: string; // Название вида заседания полное
  meetingWeekly?: boolean; // Еженедельность проведения
  meetingDay?: string; // Типовой день недели заседания (для еженедельно проводимых заседаний)
  meetingTimeStart?: string; // Типовое время начала заседания
  meetingTimeEnd?: string; // Типовое время окончания заседания
  meetingPlace?: NSIMeetingPlace; // Место проведения заседания
  meetingAgendaDay?: string; // Типовой день завершения формирования повестки заседания (для еженедельно проводимых заседаний)
  meetingAgendaTime?: string; // Время завершения формирования повестки заседания
  startNumber?: string; // Начальный номер совещания
  creatingOutside?: boolean; // Создание вопросов из внешней подсистемы
  planningMeeting?: boolean; // Градостроительное
  CreatingCopies?: boolean; // Создание копий
}
