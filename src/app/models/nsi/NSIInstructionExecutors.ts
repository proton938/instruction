// Авторы и исполнителей поручений
export class NSIInstructionExecutors {
  creator?: string; // Автор
  assistant?: string[]; // Помощник (выдача поручений)
  executor?: string[]; // Исполнитель
  assistantview?: string[]; // Помощник (просмотр поручений)
}
