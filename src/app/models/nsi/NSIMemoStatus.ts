// Статусы служебных записок
export class NSIMemoStatus {
  code?: string; // Код
  name?: string; // Наименование
  color?: string; // Цвет
}
