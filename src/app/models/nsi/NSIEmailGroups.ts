export class NSIEmailGroup {
  code: string;
  name: string;
  users: string[];
  sendSystem: {
    code: string;
  }[];
}
