export class NSIMemoReportTerm {
  Code?: string;
  Name?: string;
  Default?: boolean;
  Duration?: string;
}
