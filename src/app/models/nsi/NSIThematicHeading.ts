// Тематические рубрикаторы
export class NSIThematicHeading {
  code?: string; // Код
  name?: string; // Наименование
}
