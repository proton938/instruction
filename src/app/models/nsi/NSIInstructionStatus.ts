// Статусы поручений
export class NSIInstructionStatus {
  code?: string; // Код
  name?: string; // Наименование
  color?: string; // Цвет
}
