import { StateService } from '@uirouter/core';
import { Injectable } from '@angular/core';
import { BreadcrumbsService } from "@reinform-cdp/skeleton";

@Injectable({
  providedIn: 'root'
})
export class MemoBreadcrumbsService {

  constructor(private $state: StateService, private breadcrumbsService: BreadcrumbsService) {
  }

  newMemo() {
    this.breadcrumbsService.setBreadcrumbsChain([{
      title: 'Служебные записки',
      url: null
    }, {
      title: 'Сформировать проект внутреннего документа',
      url: null,
      internal: false
    }]);
  }

  memoCard(showcaseState: string, showcaseTitle: string) {
    console.log('breadcrumbs set');
    this.breadcrumbsService.setBreadcrumbsChain([{
      url: showcaseState,
      title: showcaseTitle,
      internal: false
    }, {
      title: 'Служебная записка',
      url: null,
      internal: false
    }]);
  }

}
