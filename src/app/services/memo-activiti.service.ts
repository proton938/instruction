import {ToastrService} from 'ngx-toastr';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as _ from 'lodash';
import {
  ActivityProcessHistoryManager, ActivityResourceService, IDefaultVar, IInfoAboutProcessInit, IProcessCategory,
  ITask, ITaskVariable
} from '@reinform-cdp/bpm-components';
import {MemoDocument} from '../models/memo-document/MemoDocument';
import {from, of, throwError} from 'rxjs/index';
import {catchError, mergeMap, tap} from 'rxjs/internal/operators';
import {Observable} from 'rxjs/Rx';
import {MemoApprovalListItem} from '../models/memo-document/MemoApproval';

@Injectable({
  providedIn: 'root'
})
export class MemoActivitiService {

  constructor(private activityRestService: ActivityResourceService,
              private activityProcessHistoryManager: ActivityProcessHistoryManager,
              private http: HttpClient,
              private toastr: ToastrService) {
  }

  startProcess(name: string, document: MemoDocument, vars?: ITaskVariable[]): Observable<any> {
    return from(this.activityRestService.getProcessHistoryByReqNum(document.documentId, 'EntityIdVar')).pipe(
      mergeMap(response => {
        // if (response.data.length > 0) {
        //   this.toastr.warning('По данной служебной записке уже запущен процесс!');
        //   return of(null);
        // } else {
        return from(this.activityRestService.getProcessDefinitions({key: name, latest: true})).pipe(
          mergeMap(response => {
            if (!vars) {
              vars = [
                {
                  'name': 'EntityIdVar', 'value': document.documentId
                }, {
                  'name': 'ResponsibleExecutor_VAR', 'value': document.executor.map((ex) => (ex.login)).join(','),
                }
              ];
            }
            return from(this.activityRestService.initProcess({
              processDefinitionId: response.data[0].id,
              variables: vars
            }));
          })
        );
        // }
      }),
      tap((response) => {
        // @ts-ignore
        if (response !== 'skip') {
          this.toastr.success('Процесс успешно запущен!');
        }
      }),
      catchError(error => {
        this.toastr.error('Ошибка при запуске процесса!');
        return throwError(error);
      })
    );
  }

  getProcessHistory(document: MemoDocument): Observable<IInfoAboutProcessInit> {
    const categories: IProcessCategory[] = [{
      name: 'Рассмотрение служебной записки',
      key: 'memo_',
      compareType: 'startsWith'
    }];

    const defaulVars: IDefaultVar[] = [{
      name: 'EntityIdVar',
      value: document.documentId
    }, {
      name: 'ResponsibleExecutor_VAR',
      value: document.executor.map((ex) => (ex.login)).join(','),
    }];

    return from(this.activityProcessHistoryManager.init('memo',
      categories,
      document.documentId,
      'EntityIdVar',
      defaulVars));
  }

  openDiagram(id: string, type: IInfoAboutProcessInit) {
    window.open(this.activityProcessHistoryManager.getDiagram(id, type), '_blank');
  }

  getEntityIdVar(task: ITask): Observable<string> {
    const _EntityIdVar = task.variables.find(v => {
      return v.name === 'EntityIdVar';
    });
    if (_EntityIdVar) {
      return of(<string>_EntityIdVar.value);
    } else {
      this.toastr.error('Отсутствует переменная задачи\'EntityIdVar\'');
      return throwError('Отсутствует переменная задачи\'EntityIdVar\'');
    }
  }

  addApprovers(id: number | string, agreed: MemoApprovalListItem[]): Observable<any> {
    const url = '/app/sdo/bpm/approval/changeApprovalPersons';

    const requestData = {
      taskId: id,
      usersCollectionVarName: 'ApprovalUsersVar',
      durationsCollectionVarName: 'ApprovalDurationsVar',
      elementVarName: 'ApprovalUserVar',
      approvalPersons: _.map(agreed, _ => {
        return {
          userName: _.agreedBy.accountName,
          duration: _.approvalTime,
        };
      })
    };

    return <Observable<any>>this.http.post(url, requestData);
  }

  getHistoricalProcesses(id: number | string): Observable<any> {
    const url = '/app/sdo/bpm/query/historic-task-instances';
    const requestData = {
      processVariables: [{
        name: 'EntityIdVar',
        value: id,
        operation: 'equals',
        type: 'string',
      }],
      size: 1000000,
      includeProcessVariables: true,
    };

    return <Observable<any>>this.http.post(url, requestData);
  }
}
