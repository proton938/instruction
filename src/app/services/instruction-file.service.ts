import {Injectable} from '@angular/core';
import {IFileInFolderInfo} from '@reinform-cdp/file-resource';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
//TODO: удалить после рализа платформы 3.8
export class InstructionFileService {
  root = `/filestore/v1`;

  constructor(private http: HttpClient) {
  }

  copyFiles(ids: string[], targetFolderGuid: string): Observable<IFileInFolderInfo[]> {
    let url: string = `${this.root}/files/copy/${targetFolderGuid}`;
    return <Observable<IFileInFolderInfo[]>>this.http.post(url, ids);
  }
}
