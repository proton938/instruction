import { Injectable } from '@angular/core';
import * as Excel from 'exceljs/dist/exceljs.min.js';
import * as fs from 'file-saver';
import { formatDate } from '@angular/common';
import * as _ from 'lodash';
import {SolrMediatorService} from "./solr-mediator.service";

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  parsedDocs: any;
  workbook: any;
  worksheet: any;
  mkaDocuments: any[] = [];
  mkaOrderDocumentsIds: any;
  mkaDocumentsIds: any;

  constructor(public solrMediator: SolrMediatorService) { }

  exportAsExcelFile(docs: any[], excelFileName: string): any {
    this.workbook = new Excel.Workbook();
    this.worksheet = this.workbook.addWorksheet(excelFileName);
    this.createHeaderRow();

    this.worksheet.getColumn(7).alignment = { wrapText: true };

    this.mkaOrderDocumentsIds = docs.filter((doc) => {
      if (doc.orderMKAID) {
        return doc.orderMKAID;
      }
    }).map((doc) => {
      return doc.orderMKAID;
    });

    this.mkaDocumentsIds = docs.filter((doc) => {
      if (doc.instructionMKAID || doc.agendaMKAID || doc.protocolMKAID || doc.memoMKAID) {
        return doc;
      }
    }).map((doc) => {
      return doc.instructionMKAID || doc.agendaMKAID || doc.protocolMKAID || doc.memoMKAID;
    });

    if (this.mkaOrderDocumentsIds.length || this.mkaDocumentsIds.length) {
      return this.loadMKADocuments(docs).then(() => {
        this.parsedDocs = this._preprocessDataForReport(docs);
        this.worksheet.addRows(this.parsedDocs);
      }).then(() => {
        return this.createXLSXFile(excelFileName);
      });
    }
    else {
      this.parsedDocs = this._preprocessDataForReport(docs);
      this.worksheet.addRows(this.parsedDocs);
      return this.createXLSXFile(excelFileName);
    }

  }

  loadMKADocuments(docs): any {
    const mkaOrderDocumentsIds = this.mkaOrderDocumentsIds;
    const mkaDocumentsIds = this.mkaDocumentsIds;

    if (mkaOrderDocumentsIds.length) {
      return this.solrMediator.getByIds([
        { ids: _.uniq(mkaOrderDocumentsIds), type: this.solrMediator.types.order }
      ]).toPromise().then(res => {
        const resDocs = this.solrMediator.getDocs(res);
        if (resDocs.length) {
          this.mkaDocuments = resDocs;
        }
        return this.solrMediator.getByIds(this.mkaByIdRequest);
      }).then(res => {
        const resDocs = this.solrMediator.getDocs(res);
        if (resDocs.length) {
          this.mkaDocuments = this.mkaDocuments.concat(resDocs);
        }
      });
    }
    else if (mkaDocumentsIds.length) {
      return this.solrMediator.getByIds(this.mkaByIdRequest).toPromise().then(res => {
        const resDocs = this.solrMediator.getDocs(res);
        if (resDocs.length) {
          this.mkaDocuments = this.mkaDocuments.concat(resDocs);
        }
      });
    }
    else {
      return;
    }
  }

  get mkaByIdRequest(): any[] {
    const ids = _.uniq(this.mkaDocumentsIds);
    return [
      { ids: ids, type: this.solrMediator.types.instruction },
      { ids: ids, type: this.solrMediator.types.order },
      { ids: ids, type: this.solrMediator.types.agenda },
      { ids: ids, type: this.solrMediator.types.protocol },
      { ids: ids, type: this.solrMediator.types.memoMKA }
    ]
  }

  createHeaderRow() {
    this.worksheet.columns = [
      { header: 'Номер поручения', key: '1', width: 20 },
      { header: 'Дата поручения', key: '2', width: 20 },
      { header: 'Контрольный срок', key: '3', width: 20 },
      { header: 'Исполнитель', key: '4', width: 25 },
      { header: 'Приоритет', key: '5', width: 20 },
      { header: 'Статус', key: '6', width: 20 },
      { header: 'Содержание', key: '7', width: 40 },
      { header: 'Номер документа', key: '8', width: 20 },
      { header: 'Дата документа в ОАСИ МКА', key: '9', width: 30 },
      { header: 'Кому', key: '10', width: 25 },
      { header: 'Организация', key: '11', width: 20 },
      { header: 'Кто подписал', key: '12', width: 20 }
    ];

    let header = this.worksheet.getRow(1);

    header.eachCell(function(cell) {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {
          argb: 'eeeeee'
        }
      };

      cell.height = 30;

      cell.font = {
        bold: true
      };

      cell.alignment = {
        horizontal: 'center',
        vertical: 'middle'
      };

      cell.border = {
        top: {
          style: 'thin'
        },
        left: {
          style: 'thin'
        },
        bottom: {
          style: 'thin'
        },
        right: {
          style: 'thin'
        }
      };
    });
  }

  createXLSXFile(excelFileName): any {
    return this.workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: EXCEL_TYPE });
      fs.saveAs(blob, `${excelFileName}${EXCEL_EXTENSION}`);
    });
  }

  private _preprocessDataForReport(docs) {
    return docs.map((doc:any) => this._parseDocument(doc));
  }

  private _parseDocument(doc) {
    let res = [doc.instructionNumber || '',
      doc.beginDate ? formatDate(doc.beginDate, 'dd.MM.yyyy HH:mm:ss', 'en') : '',
      doc.planDate ? formatDate(doc.planDate, 'dd.MM.yyyy HH:mm:ss', 'en') : '',
      doc.executorFioFull || '',
      doc.priorityName || '',
      doc.statusName || '',
      doc.content || ''];

    if (doc.instructionMKAID) {
      let instruction = this.mkaDocuments[0].find((d) => d.documentId === doc.instructionMKAID);
      if (instruction) {
        res[7] = instruction.instructionNumber;
        res[8] = instruction.beginDate
          ? formatDate(instruction.beginDate, 'dd.MM.yyyy HH:mm:ss', 'en')
          : '';
        res[9] = doc.executorFioFull;
        res[10] = 'Москомархитектура';
        res[11] = instruction.creatorFioFull;
      }
    }
    else if (doc.orderMKAID) {
      let order = this.mkaDocuments[1].find((d) => d.documentId === doc.orderMKAID);
      if (order) {
        res[7] = order.docNumber;
        res[8] = order.beginDate ? formatDate(order.beginDate, 'dd.MM.yyyy', 'en') : '';
        res[9] = doc.executorFioFull;
        res[10] = 'Москомархитектура';
        res[11] = order.agreedByOrd;
      }
    }
    else if (doc.agendaMKAID) {
      let agenda = this.mkaDocuments[2].find((d) => d.documentId === doc.agendaMKAID);
      if (agenda) {
        res[7] = agenda.meetingNumber;
        res[8] = agenda.meetingDate;
        res[9] = doc.executorFioFull;
        res[10] = 'Москомархитектура';
        res[11] = agenda.initiatorFioFull;
      }
    }
    else if (doc.protocolMKAID) {
      let protocol = this.mkaDocuments[3].find((d) => d.documentId === doc.protocolMKAID);
      if (protocol) {
        res[7] = protocol.meetingNumber;
        res[8] = protocol.meetingDate;
        res[9] = doc.executorFioFull;
        res[10] = 'Москомархитектура';
        res[11] = protocol.approvedByFioFull;
      }
    } else if (doc.memoMKAID) {
      let memo = this.mkaDocuments[4].find((d) => d.documentId === doc.memoMKAID);
      if (memo) {
        res[7] = memo.regNumber;
        res[8] = memo.regDate ? formatDate(memo.regDate, 'dd.MM.yyyy', 'en') : '';
        res[9] = memo.executor && memo.executor.length ? _.last(memo.executor) : '';
        res[10] = 'Москомархитектура';
        res[11] = memo.creator;
      }
    }
    return res;
  }

}

