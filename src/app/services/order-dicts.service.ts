import {Inject, Injectable} from '@angular/core';
import {SessionStorage} from "@reinform-cdp/security";
import {ToastrService} from "ngx-toastr";
import {InstructionDictsModel} from "../models/instruction/InstructionDictsModel";
import {NsiResourceService, UserBean} from "@reinform-cdp/nsi-resource";
import {NSIInstructionPriority} from "../models/nsi/NSIInstructionPriority";
import {NSIInstructionDifficulty} from "../models/nsi/NSIInstructionDifficulty";
import {NSIInstructionStatus} from "../models/nsi/NSIInstructionStatus";
import {NSIThematicHeading} from "../models/nsi/NSIThematicHeading";
import {NSIMeetingTypes} from "../models/nsi/NSIMeetingTypes";
import {NSISettings} from "../models/nsi/NSISettings";
import {NSIInstructionTheme} from "../models/nsi/NSIInstructionTheme";
import {NSIInstructionExecutors} from "../models/nsi/NSIInstructionExecutors";
import {InstructionExecutor} from "../models/instruction/InstructionExecutor";
import {from, throwError, of} from "rxjs/index";
import * as _ from "lodash";
import {catchError, map, mergeMap} from "rxjs/internal/operators";
import {Observable} from "rxjs/Rx";
import {fromArray} from "rxjs/internal/observable/fromArray";
import {XmppService} from '../xmpp/XmppService';
import {IXmppServiceSettings} from '../xmpp/IXmppServiceSettings';
import {NSIRepeatPeriod} from '../models/nsi/NSIRepeatPeriod';
import {NSIWeekday} from '../models/nsi/NSIWeekday';
import {NSIMonth} from '../models/nsi/NSIMonth';

@Injectable({
  providedIn: 'root'
})
export class OrderDictsService {
  cancelation: any;

  constructor(private nsiRestService: NsiResourceService,
              private toastr: ToastrService,
              private session: SessionStorage,
              private xmppService: XmppService) {
  }

  isAssistantview: boolean = true;

  getDicts(dicts?: string[], assistantview?: boolean): Observable<InstructionDictsModel> {
    this.isAssistantview = !assistantview ? assistantview : this.isAssistantview;
    let model = new InstructionDictsModel();
    let dictList = dicts ? dicts : [
      'InstructionExecutors',
      'ThematicHeading',
      'InstructionPriority',
      'InstructionDifficulty',
      'InstructionStatus',
      'mggt_meeting_MeetingTypes',
      'Settings',
      'mggt_instruction_memo_status',
      'InstructionTheme',
      'mggt_instruction_PeriodsRepeat',
      'mggt_weekday',
      'mggt_months',
      'mggt_numberWeek'];
    return from(this.nsiRestService.getDictsFromCache(dictList)
    ).pipe(
      mergeMap((response: {
        InstructionPriority: NSIInstructionPriority[],
        InstructionDifficulty: NSIInstructionDifficulty[],
        InstructionStatus: NSIInstructionStatus[], ThematicHeading: NSIThematicHeading[],
        MeetingTypes: NSIMeetingTypes[], Settings: NSISettings[], InstructionTheme: NSIInstructionTheme[],
        InstructionExecutors: NSIInstructionExecutors[], RepeatPeriods: NSIRepeatPeriod[],
        Weekdays: NSIWeekday[], Months: NSIMonth[]
      }) => {
        model.priorities = response.InstructionPriority;
        model.difficulties = response.InstructionDifficulty;
        model.statuses = response.InstructionStatus;
        model.themes = response.ThematicHeading;
        model.meetingTypes = response['mggt_meeting_MeetingTypes'];
        model.settings = response.Settings;
        model._themes = response.InstructionTheme;
        model.repeatPeriods = response['mggt_instruction_PeriodsRepeat'];
        model.weekdays = response['mggt_weekday'];
        model.months = response['mggt_months'];
        model.weekNumbers = response['mggt_numberWeek'];
        if (model.settings) {
          let settings = this.createXmppSettingsFromNsi(model.settings);
          this.xmppService.setSetting(settings);
        }
        return response.InstructionExecutors ? this.transformInstructionExecutor(response.InstructionExecutors, this.isAssistantview) : of([]);
      }),
      map((response: InstructionExecutor[]) => {
        model.executors = response;
        model.creators = model.executors.filter(r => {
          return r.creator.accountName === this.session.login();
        }).concat(model.executors.filter(r => {
          return r.assistant.findIndex(e => {
            return e.accountName === this.session.login();
          }) !== -1;
        }));
        return model;
      }),
      catchError((error, caught) => {
        console.log(error);

        this.toastr.error('Ошибка при считывании справочной информации!');
        return throwError(error);
      })
    );
  }

  getDelegate(login): Observable<any> {
	  return from(this.nsiRestService.getDelegateTasks(login, 'sdoassign'));
  }

  private createXmppSettingsFromNsi(settings: NSISettings[]): IXmppServiceSettings {
    let xmpp_BOSH_SERVICE = _.find(settings, (s: NSISettings) => {
      return s.code === 'xmpp_BOSH_SERVICE';
    });
    let xmpp_sender_jid = _.find(settings, (s: NSISettings) => {
      return s.code === 'xmpp_sender_jid';
    });
    let xmpp_sender_pswd = _.find(settings, (s: NSISettings) => {
      return s.code === 'xmpp_sender_pswd';
    });
    let xmpp_start_timeout_sec = _.find(settings, (s: NSISettings) => {
      return s.code === 'xmpp_start_timeout_sec';
    });
    let xmpp_close_timeout_sec = _.find(settings, (s: NSISettings) => {
      return s.code === 'xmpp_close_timeout_sec';
    });
    let xmpp_domain = _.find(settings, (s: NSISettings) => {
      return s.code === 'xmpp_domain';
    });
    let xmpp_switch = _.find(settings, (s: NSISettings) => {
      return s.code === 'xmpp_switch';
    });

    return <IXmppServiceSettings>{
      xmpp_BOSH_SERVICE: (xmpp_BOSH_SERVICE) ? xmpp_BOSH_SERVICE.value : '',
      xmpp_sender_jid: (xmpp_sender_jid) ? xmpp_sender_jid.value : '',
      xmpp_sender_pswd: (xmpp_sender_pswd) ? xmpp_sender_pswd.value : '',
      xmpp_start_timeout_sec: (xmpp_start_timeout_sec) ? xmpp_start_timeout_sec.value : 0,
      xmpp_close_timeout_sec: (xmpp_close_timeout_sec) ? xmpp_close_timeout_sec.value : 0,
      xmpp_domain: (xmpp_domain) ? xmpp_domain.value : '',
      xmpp_switch: (xmpp_switch) ? xmpp_switch.value : ''
    };
  }

  solicitorSearch(search: string, $select: any, coExecutors: UserBean[]) {
    if (search && search.length > 2) {
      if (this.cancelation) {
        clearTimeout(this.cancelation);
      }
      this.cancelation = setTimeout(() => {
        this.nsiRestService.searchUsers({fio: search}).then(_response => {
          coExecutors.length = 0;
          _response.forEach(r => {
            coExecutors.push(r);
          });
        }).catch(error => {
          this.toastr.error('Ошибка при получении информации из справочника!');
        });
      }, 500);
    }
  }

  private transformInstructionExecutor(executors: NSIInstructionExecutors[], isAssistantview: boolean): Observable<InstructionExecutor[]> {
    let logins: string[] = [];
    let currentUser = this.session.login();
    let executorsList  = executors.filter(e => (e.executor && e.executor.filter(ex => ex === currentUser).length > 0)
                            || (e.assistant && e.assistant.filter(ex => ex === currentUser).length > 0)
                            || (e.assistantview && e.assistantview.filter(ex => ex === currentUser).length > 0)
                            || (e.creator && e.creator === currentUser) );
    executorsList.forEach(e => {

      if (e.assistant && e.assistant.length > 0) {
        logins = logins.concat(e.assistant);
      }
      if (e.creator) {
        logins.push(e.creator);
      }
      if (e.executor && e.executor.length > 0) {
        logins = logins.concat(e.executor);
      }
      if (e.assistantview && e.assistantview.length > 0 && isAssistantview) {
        logins = logins.concat(e.assistantview);
      }
    });
    logins = _.uniq(logins);
    if (logins.length > 0) {
      return from(this.nsiRestService.getUsersFromCache(logins)).pipe(
        map(_response => {
          let maped = _response.map((r: any) => {
            return r.accountName;
          });
          logins.forEach(l => {
            let find = false;
            maped.forEach(m => {
              if (m === l) {
                find = true;
              }
            });
            if (!find) {
              console.log(l);
            }
          });
          let result: InstructionExecutor[] = executorsList.map(n => {
            return {
              creator: _response.find(r => {
                return r.accountName === n.creator;
              }),
              executor: _response.filter(r => {
                return n.executor ? n.executor.findIndex(e => {
                  return e === r.accountName;
                }) !== -1 : false;
              }),
              assistant: _response.filter(r => {
                return n.assistant ? n.assistant.findIndex(e => {
                  return e === r.accountName;
                }) !== -1 : false;
              }),
              assistantview: _response.filter(r => {
                return n.assistantview ? n.assistantview.findIndex(e => {
                  return e === r.accountName;
                }) !== -1 : false;
              })
            };
          });
          let filtered = result.filter(r => {
            return !!r.creator;
          });
          filtered.forEach(f => {
            f.executor = _.orderBy(f.executor, ['displayName'], ['asc']);
          });
          return filtered;
        })
      );

    } else {
      return fromArray([]);
    }
  }

}
