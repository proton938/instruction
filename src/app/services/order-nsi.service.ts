import {Observable} from "rxjs/index";
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {formatDate} from "@angular/common";

@Injectable({
    providedIn: 'root'
})
// TODO: move to platform nsi
export class OrderNsiService {

    constructor(private http: HttpClient) {
    }

    toLocalDateTime(dateTime: Date): Observable<string> {
        return <Observable<string>>this.http.get(`/mdm/api/v1/datetime/zonedDateTime/${dateTime}/toLocalDateTime`, {
            headers: {
                'Accept': 'text/plain'
            },
            responseType: 'text'
        });
    }

    addDuration(date: Date, duration: string): Observable<string> {
        let dateStr = formatDate(date, 'yyyy-MM-ddTHH:mm:ss', 'en');
        return <Observable<string>>this.http.get(`/mdm/api/v1/businessCalendar/addDuration/${dateStr}/${duration}`, {
            responseType: 'text'
        });
    }

    calcDuration(duration: string): Observable<string> {
        return <Observable<string>>this.http.get(`/mdm/api/v1/businessCalendar/calcDuration/${duration}`, {
            responseType: 'text'
        });
    }

    divideTimeBy(duration: string, divideBy: number): Observable<string> {
        return <Observable<string>>this.http.get(`/mdm/api/v1/datetime/duration/${duration}/divideBy/${divideBy}`, {
            responseType: 'text'
        });
    }

    getPlanWorkDate(days: number = 1, date: Date = new Date()): Observable<any> {
      let dateStr = formatDate(date, 'yyyy-MM-ddTHH:mm:ss', 'en');
      return <Observable<string>>this.http.get(`/mdm/api/v1/businessCalendar/addWorkDays/${dateStr}/${days}`, {
        responseType: 'text'
      });
    }

}
