import {StateService} from '@uirouter/core';
import {Injectable} from '@angular/core';
import {BreadcrumbsService} from "@reinform-cdp/skeleton";

@Injectable({
  providedIn: 'root'
})
export class OrderBreadcrumbsService {

  constructor(private $state: StateService, private breadcrumbsService: BreadcrumbsService) {
  }

  newOrder() {
    this.breadcrumbsService.setBreadcrumbsChain([{
      title: 'Поручения',
      url: null
    }, {
      title: 'Дать поручение',
      url: null
    }])
  }

  orderCard(isMKA) {
    if(isMKA) {
      this.breadcrumbsService.setBreadcrumbsChain([{
        url: this.$state.href('app.instruction.showcase-assignments-mka', {}),
        title: 'Реестр поручений МКА'
  
      }, {
        title: 'Поручение',
        url: null
      }]);
    } else {
      this.breadcrumbsService.setBreadcrumbsChain([{
        url: this.$state.href('app.instruction.showcase-assignments', {}),
        title: 'Реестр поручений'
  
      }, {
        title: 'Поручение',
        url: null
      }]);
    }
  }

}
