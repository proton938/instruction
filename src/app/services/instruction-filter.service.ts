import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";
import { InstructionFilter } from '../models/instruction/InstructionFilter';
import { InstructionTableFilter } from '../models/table-filters/InstructionTableFilter';

@Injectable({
  providedIn: 'root'
})
export class InstructionFilterService {

  login: string = '';

  private assignmentFilter:InstructionFilter;
  private mkaassignmentFilter:InstructionFilter;
  private assignmentTableFilter: InstructionTableFilter;
  private assignmentSearch:string;
  public assignmentActiveTab:string;
  constructor() {
  }

  clearAssignmentFilter(){
    this.assignmentFilter=new InstructionFilter();
    this.assignmentFilter.clear();
    this.assignmentTableFilter=new InstructionTableFilter();
    this.assignmentSearch='';
  }

  getAssignmentFilter(){
    if(this.assignmentFilter==null){
      this.assignmentFilter=new InstructionFilter();
      this.assignmentFilter.clear();
    }
    return this.assignmentFilter;
  }
  getAssignmentTableFilter(){
    if(this.assignmentTableFilter==null){
      this.assignmentTableFilter=new InstructionTableFilter();
    }
    return this.assignmentTableFilter;
  }
  getAssignmentSearch(){
    return !!this.assignmentSearch?this.assignmentSearch:'';
  }
  setAssignmentSearch(search:string){
    this.assignmentSearch=search;
  }

}
