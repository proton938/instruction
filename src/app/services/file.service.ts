import { BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import { FileResourceService, IFileInFolderInfo } from '@reinform-cdp/file-resource';
import { Observable, EMPTY, forkJoin, from, of, throwError } from 'rxjs';
import { catchError, map, mergeMap, tap, take } from 'rxjs/internal/operators';
import * as _ from 'lodash';
import * as angular from 'angular';
import { RenameFileModalComponent } from '../components/commons/rename-file/rename-file.modal';
import {ExFileType} from '@reinform-cdp/widgets';
import {HelperService} from './helper.service';
import {FormRenameFileModalComponent} from "../components/forms/rename-file/form-rename-file-modal.component";
import {HttpClient} from "@angular/common/http";
import { PlatformConfig } from '@reinform-cdp/core';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  public ex: {
    files: ExFileType[],
    filesToUpload: File[],
    fileToUploadChanges: File[]
  } = {
    files: [],
    filesToUpload: [],
    fileToUploadChanges: []
  };

  root = `/filestore/v2`;  

  private filesInFolder: IFileInFolderInfo[] = [];
  public ids: string[] = [];

  constructor(private fileHttpService: FileResourceService,
              private toastr: ToastrService,
              private modalService: BsModalService,
              private http: HttpClient,
              private platformConfig: PlatformConfig) {
  }

  updateAccess(fileId: string, login: string): Observable<any> {
    let url: string = `${this.root}/files/updateAccess/${fileId}?checkPrincipals=true&systemCode=${this.platformConfig.systemCode}`;
    return this.http.post(url, {
      "access": [
        {
          "permissions": [
            "Coordinator"
          ],
          "principalId": login
        }
      ]
    });
  }
  delegateAccess(fileId: string, login: string, formKey:string,bpPrefix:string): Observable<any> {
    let url: string = `/mdm/api/v1/delegate/file/requestAccess?systemCode=${this.platformConfig.systemCode}`;
    return this.http.post(url, {
      "bpPrefix": bpPrefix,
      "formKey": formKey,
      "guid": fileId,
      "login": login,
      "permission": "Coordinator"
    });
  }

  renameFile(oldName: string): Observable<string> {
    const fileNameSeparation = oldName.split('.');
    let fileExtension = fileNameSeparation.pop().toLowerCase();
    const options = {
      initialState: {
        oldName: oldName,
        extension: fileExtension
      },
      'class': 'modal-md'
    };
    const ref = this.modalService.show(RenameFileModalComponent, options);
    return this.modalService.onHide.pipe(
      take(1),
      mergeMap(() => {
        let component = <RenameFileModalComponent>ref.content;
        if (component.submit) {
          return of(component.newName + '.' + component.extension);
        } else {
          return throwError('');
        }
      })
    );
  }

  deleteFolder(id: string): Observable<any> {
    return from(this.fileHttpService.deleteFolder(id)).pipe(
      tap(response => {
        this.toastr.success(`Папка ${id} с файлами успешно удалена!`);
      }), catchError(error => {
        this.toastr.error('Ошибка при удалении папки!');
        return throwError(error);
      })
    );
  }

  deleteExFile(params: any) {
    if (params && params.fileInfo) {
      let index = this.ex.files.findIndex(f => f.idFile === params.fileInfo.idFile);
      if (index >= 0) this.ex.files.splice(index, 1);
    }
  }

  updateInfoAboutFilesFromFolder(folderId: string): Observable<any> {
    if (!folderId) {
      return of('');
    }

    return from(this.fileHttpService.getFullFolderInfo(folderId, false)).pipe(
      tap((response: any) => {
        this.filesInFolder = response.files;
      })
    );
  }

  updateInfoAboutFilesByIds(ids: string[]) {
    this.ex.files = [];
    this.ids = ids.filter(f => _.some(this.filesInFolder, i => i.versionSeriesGuid === f));
    this.ids.forEach(id => {
      let file = this.filesInFolder.find(f => f.versionSeriesGuid === id);
      if (file) {
        this.ex.files.push(this.convertToExFile(file));
      }
    });
  }

  getRealInfoAboutFilesByIds(ids: string[]): Observable<ExFileType[]> {
    if (!ids.length) {
      return of([]);
    }

    const observables = ids.map(id => {
      return this.fileHttpService.getFileInfo(id).then(f => this.convertToExFile(f));
    });

    return forkJoin(observables);
  }

  getInfoAboutFilesByIds(ids: string[]): ExFileType[] {
    let files = [];

    ids.forEach(id => {
      let file = this.filesInFolder.find(f => {
        return f.versionSeriesGuid === id;
      });
      if (file) {
        files.push(this.convertToExFile(file));
      }
    });

    return files;
  }

  saveFile(entityID: string, folderId: string, file: File, fileType: string = 'MkaDocOrder'): Observable<string> {
    let fd = new FormData();
    fd.append('file', file, (<any>file).nameFile || file.name);
    fd.append('folderGuid', folderId);
    fd.append('fileType', fileType);
    fd.append('docEntityID', entityID);
    fd.append('docSourceReference', 'UI');

    return from(this.fileHttpService.handleFileUploadNew(fd)).pipe(
      map(response => response.guid),
      catchError(error => {
        let errorJson = angular.fromJson(error.data);
        if (errorJson && errorJson.userMessages) {
          this.toastr.error(_.last(errorJson.userMessages));
        } else {
          this.toastr.error(`Ошибка при загрузке файла ${file.name}!`);
        }
        return throwError(error);
      })
    );
  }

  saveFiles(entityID: string, folderId: string, files: File[], fileType: string = 'MkaDocOrder'): Observable<string[]> {
    if (files.length > 0) {
      let observables = files.map(f => {
        return this.saveFile(entityID, folderId, f, fileType);
      });
      return forkJoin(observables).pipe(
        tap(response => {
          this.toastr.success(`Успешно загружено файлов: ${files.length}!`);
        })
      );
    } else {
      return of([]);
    }
  }

  syncExFiles(entityID: string, folderId: string, deleted: (ids: string[]) => void): Observable<any> {
    const ids = this.getExFileIdsToDelete();
    return this.deleteFiles(ids).pipe(
      mergeMap(response => {
        deleted(ids);
        return this.saveFiles(entityID, folderId, this.ex.filesToUpload);
      })
    );
  }

  saveFilesWithFileType(entityID: string, folderId: string, files: any) {
    if (files.length > 0) {
      let observables = files.map(f => {
        return this.saveFile(entityID, folderId, f.file, f.fileType);
      });
      return forkJoin(observables).pipe(
        tap(response => {
          this.toastr.success(`Успешно загружено файлов: ${files.length}!`);
        })
      );
    } else {
      return of([]);
    }
  }

  deleteFile(id: string): Observable<any> {
    return from(this.fileHttpService.deleteFile(id));
  }

  getExFileIdsToDelete(): string[] {
    return _.difference(this.ids, _.map(this.ex.files, f => f.idFile));
  }

  deleteFiles(ids: string[]): Observable<any> {
    if (ids.length > 0) {
      let observables = ids.map(id => {
        return this.deleteFile(id);
      });
      return forkJoin(observables).pipe(
        tap(response => {
          this.toastr.success(`Успешно удалено файлов: ${ids.length}!`);
        }), catchError(error => {
          this.toastr.error(`Ошибка при удалении файлов!`);
          return throwError(error);
        })
      );
    } else {
      return of([]);
    }
  }

  getFilesFromRecreatedDocument(folderId: string, ids: string[]): Observable<File[]> {
    if (ids.length > 0) {
      return this.updateInfoAboutFilesFromFolder(folderId).pipe(
        mergeMap(response => {
          let observables = _.map(ids, id => {
            return this.getFile(id);
          });
          return forkJoin(observables).pipe()
        }), catchError(error => {
          this.toastr.error(`Ошибка при получении файлов из копируемого поручения!`);
          return throwError(error);
        })
      );
    } else {
      return of([]);
    }
  }

  getFile(id: string, sysCode: string = 'INSTRUCTION'): Observable<File> {
    let blob: Blob;
    return from(this.fileHttpService.getFile(id, sysCode)).pipe(
      map(response => {
        blob = response;
        let fileInfo = this.filesInFolder.find(f => {
          return f.versionSeriesGuid === id;
        });
        let file = new File([blob], fileInfo.fileName);
        file['nameFile'] = fileInfo.fileName;
        file['sizeFile'] = fileInfo.fileSize;
        return file;
      })
    );
  }

  convertToExFile(f: IFileInFolderInfo): ExFileType {
    return ExFileType.create(f.versionSeriesGuid, f.fileName, f.fileSize,
      !!f.signGuid, f.dateCreated, f.fileType, f.mimeType);
  }

  checkFiles2Rename(fileType: string = 'default') {
    if (this.ex.filesToUpload.length >= this.ex.fileToUploadChanges.length) {
      const _file: any = _.last(this.ex.filesToUpload);
      if (_file) {
        if (!_file.typeFile && fileType) { _file.typeFile = fileType; }
        const num: number = this.getOrderFileIndex(_file.name, [].concat(this.filesInFolder
          .map(f => this.convertToExFile(f)), this.ex.fileToUploadChanges));
        if (num > 0) {
          const modalRef = this.modalService.show(FormRenameFileModalComponent, {
            class: 'modal-md rename-file-modal',
            ignoreBackdropClick: true,
            initialState: {
              file: {
                oldName: _file.name,
                numberOfFile: num
              },
              saveCallback: (result: string) => {
                _file['nameFile'] = result + '.' + _file.name.substring(_file.name.lastIndexOf('.') + 1);
                this._updateFileChanges();
              },
              rejectCallback: () => {
                this.ex.filesToUpload.splice(this.ex.filesToUpload.length - 1, 1);
              }
            }
          });
        } else {
          this._updateFileChanges();
        }
      } else {
        this._updateFileChanges();
      }
    } else {
      this._updateFileChanges();
    }
  }

  getOrderFileIndex(name: string, files: any[] = []): number {
    let num = 0;
    const similarNames: string[] = [];
    const nameBase = name.split('.').slice(0, -1).join('.');
    const nameExt = name.replace(nameBase + '.', '');
    const prepareReg = str => str.replace(/([\(\)\[\]\{\}\.\^\$\*\-\;\|\/])/g, '\\$1');
    if (_.some(files, i => i.nameFile === name)) {
      const rx = new RegExp(prepareReg(nameBase) + '(\\((\\d+)\\))?\\.' + nameExt + '$');
      files.filter(i => i.nameFile.match(rx)).forEach( i => {
        similarNames[+i.nameFile.replace(rx, '$2')] = i.nameFile;
      });
    }
    num = _.findIndex(similarNames, i => !i);
    if (num < 0) { num = similarNames.length || 0; }
    return num;
  }

  uploadFile(file, folderGuid: string, fileType: string, docId: string = '-'): Observable<any> {
    const fd = new FormData();
    fd.append('file', file, file.nameFile);
    if (folderGuid) { fd.append('folderGuid', folderGuid); }
    if (fileType) { fd.append('fileType', fileType); }
    fd.append('docEntityID', docId);
    fd.append('docSourceReference', 'TEST-001');
    return from(this.fileHttpService.handleFileUpload(fd));
  }

  private _updateFileChanges() {
    this.ex.fileToUploadChanges = this.ex.filesToUpload.slice();
  }

}
