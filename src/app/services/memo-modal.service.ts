import {BsModalService} from "ngx-bootstrap";
import {Injectable} from '@angular/core';
import {MemoDocument} from "../models/memo-document/MemoDocument";
import {Observable} from "rxjs/Rx";
import {of, throwError} from "rxjs/index";
import {mergeMap} from "rxjs/internal/operators";
import {SendMailModalComponent} from "../components/memo/memo-card/memo-card-menu/send-mail/send-mail.modal";

@Injectable({
  providedIn: 'root'
})
export class MemoModalService {

  constructor(private modalService: BsModalService) {
  }

  mail(document: MemoDocument): Observable<any> {
    const options = {
      initialState: {
        document: document
      },
      'class': 'modal-lg'
    };
    const ref = this.modalService.show(SendMailModalComponent, options);
    return this.modalService.onHide.pipe(
      mergeMap(() => {
        let component = <SendMailModalComponent> ref.content;
        if (component.submit) {
          return of('');
        } else {
          return throwError('');
        }
      })
    );
  }

}
