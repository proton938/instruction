import { formatDate } from "@angular/common";
import { ToastrService } from "ngx-toastr";
import { Injectable } from '@angular/core';
import { InstructionDocument } from "../models/instruction-document/InstructionDocument";
import {
  ActivityProcessHistoryManager, ActivityResourceService, IDefaultVar, IInfoAboutProcessInit, IProcessCategory,
  ITask, ITaskVariable
} from "@reinform-cdp/bpm-components";
import { Observable } from "rxjs/Rx";
import { forkJoin, from, of, throwError } from "rxjs/index";
import { catchError, mergeMap, tap } from "rxjs/internal/operators";
import { AlertService } from "@reinform-cdp/widgets";
import * as _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class OrderActivitiService {

  constructor(private toastr: ToastrService,
    private activityRestService: ActivityResourceService, private activityProcessHistoryManager: ActivityProcessHistoryManager,
    private alertService: AlertService) {
  }

  startProcess(name: string, document: InstructionDocument): Observable<any> {
    return from(this.activityRestService.getProcessHistoryByReqNum(document.documentId, "EntityIdVar")).pipe(
      mergeMap(response => {
        if (response.data.length > 0) {
          this.toastr.warning('По данному поручению уже запущен процесс!');
          return of(null);
        } else {
          return from(this.activityRestService.getProcessDefinitions({ key: name, latest: true })).pipe(
            mergeMap(response => {
              return from(this.activityRestService.initProcess({
                processDefinitionId: response.data[0].id,
                variables: [
                  { 'name': 'EntityIdVar', 'value': document.documentId },
                  { 'name': 'ResponsibleExecutor_VAR', 'value': document.instruction.executor.login },
                  { 'name': 'TaskTime_VAR', 'value': this.buildTime(document.instruction.planDate) },
                  { 'name': 'Author_VAR', 'value': document.instruction.creator.login }
                ]
              }))
            })
          );
        }
      }), tap(response => {
        this.toastr.success('Процесс успешно запущен!');
      }), catchError(error => {
        this.alertService.message({
          message: 'Произошла ошибка. Попробуйте сохранить поручение позже или обратитесь в службу технической поддержки',
          type: 'warning',
          size: 'md'
        });
        return throwError(error);
      })
    );
  }

  startProcessWithVariables(name: string, document: InstructionDocument, variables: any[]): Observable<any> {
    return from(this.activityRestService.getProcessHistoryByReqNum(document.documentId, "EntityIdVar")).pipe(
      mergeMap(response => {
        const isExist = response.data.some(action => action.startActivityId.includes(name));
        if (isExist) {
          this.toastr.warning('По данному поручению уже запущен процесс!');
          return of(null);
        } else {
          return from(this.activityRestService.getProcessDefinitions({ key: name, latest: true })).pipe(
            mergeMap(response => {
              return from(this.activityRestService.initProcess({
                processDefinitionId: response.data[0].id,
                variables
              }))
            })
          );
        }
      }),
      tap(response => {
        this.toastr.success('Процесс успешно запущен!');
      }),
      catchError(error => {
        this.alertService.message({
          message: 'Произошла ошибка. Попробуйте сохранить поручение позже или обратитесь в службу технической поддержки',
          type: 'warning',
          size: 'md'
        });
        return throwError(error);
      })
    );
  }

  startReviewProcess(name: string, document: InstructionDocument): Observable<any> {
    if (!document.forInformation || !document.forInformation.length) {
      return of('');
    }
    return from(this.activityRestService.getProcessDefinitions({ key: name, latest: true })).pipe(
      mergeMap(response => {
        return from(this.activityRestService.initProcess({
          processDefinitionId: response.data[0].id,
          variables: [
            { 'name': 'EntityIdVar', 'value': document.documentId },
            { 'name': 'UsersListVar', 'value': document.forInformation.map(i => i.reviewBy.login).join(",") },
            { 'name': 'ChangeDate', 'value': formatDate(new Date(), 'yyyy-MM-ddTHH:mm:ss.SSS', 'en')}
          ]
        }))
      }), tap(response => {
        this.toastr.success('Процесс успешно запущен!');
      }), catchError(error => {
        this.alertService.message({
          message: 'Произошла ошибка. Попробуйте сохранить поручение позже или обратитесь в службу технической поддержки',
          type: 'warning',
          size: 'md'
        });
        return throwError(error);
      })
    );
  }

  startCoExecutionProcess(name: string, document: InstructionDocument): Observable<any> {
    if (!document.instruction.coExecutor || !document.instruction.coExecutor.length) {
      return of('');
    }
    const variables = [];
    variables.push({ name: 'EntityIdVar', value: document.documentId });
    variables.push({ name: 'UsersListVar', value: document.instruction.coExecutor.map(coEx => coEx.reviewBy.login).join(',') });
    variables.push({ name: 'ChangeDate', value: formatDate(new Date(), 'yyyy-MM-ddTHH:mm:ss.SSS', 'en') });
    return this.startProcessWithVariables(name, document, variables);
  }

  getProcessHistory(document: InstructionDocument): Observable<IInfoAboutProcessInit> {
    let categories: IProcessCategory[] = [{
      name: 'Исполнение поручения',
      key: 'sdoassign_',
      compareType: 'startsWith'
    }];

    let defaulVars: IDefaultVar[] = [{
      name: 'EntityIdVar',
      value: document.documentId
    }, {
      name: 'ResponsibleExecutor_VAR',
      value: document.instruction.executor.login
    }];

    return from(this.activityProcessHistoryManager.init('assign',
      categories, document.documentId, 'EntityIdVar', defaulVars));
  }


  getProcessHistoryById(id: string): Observable<IInfoAboutProcessInit> {
    let categories: IProcessCategory[] = [{
      name: 'Исполнение поручения',
      key: 'sdoassign_',
      compareType: 'startsWith'
    }];

    return from(this.activityProcessHistoryManager.init('assign',
      categories, id, 'EntityIdVar', []));
  }

  openDiagram(id: string, type: IInfoAboutProcessInit) {
    window.open(this.activityProcessHistoryManager.getDiagram(id, type), '_blank');
  }

  getOpenedProcesses(info: IInfoAboutProcessInit): string[] {
    return info.processes.filter(p => {
      return p.endTime === null;
    }).map(p => {
      return p.processInstanceId;
    });
  }

  getOpenedTasks(info: IInfoAboutProcessInit, names: string[] = ['Исполнить поручение', 'Исправить замечание по поручению']): string[] {
    let tasks = [];

    let openedProcesses = info.processes.filter(p => {
      return p.endTime === null;
    });

    openedProcesses.forEach(p => {
      if (p.tasks.length > 0) {
        Array.prototype.push.apply(tasks, p.tasks);
      }
    });

    let openedTasks = tasks.filter(t => {
      return t.endTime === null;
    });
    let filteredByFormKeyTasks = openedTasks.filter(t => {
      return _.includes(names, t.name);
    });
    return (filteredByFormKeyTasks.length > 0) ? _.map(filteredByFormKeyTasks, t => {
      return t.id
    }) : [];
  }

  getEntityIdVar(task: ITask): Observable<string> {
    let _EntityIdVar = task.variables.find(v => {
      return v.name === 'EntityIdVar';
    });
    if (_EntityIdVar) {
      return of(<string>_EntityIdVar.value);
    } else {
      this.toastr.error('Отсутствует переменная задачи\'EntityIdVar\'');
      return throwError('Отсутствует переменная задачи\'EntityIdVar\'');
    }
  }

  deleteProcesses(ids: string[]): Observable<any> {
    let observables = _.map(ids, id => {
      return from(this.activityRestService.deleteProcess(id));
    });

    if (observables.length > 0) {
      return forkJoin(observables).pipe(
        tap(response => {
          if (observables.length === 1) {
            this.toastr.success('Процесс успешно удален!');
          } else {
            this.toastr.error('Процессы успешно удалены!');
          }
        }), catchError(error => {
          if (observables.length === 1) {
            this.toastr.error('Ошибка при удалении процесса!');
          } else {
            this.toastr.error('Ошибка при удалении процессов!');
          }
          return throwError(error);
        })
      );
    } else {
      return of('');
    }
  }


  private getVars(executorLogin: string, planDate: Date): ITaskVariable[] {
    let vars = [];

    if (executorLogin) {
      vars.push({
        name: 'ResponsibleExecutor_VAR',
        value: executorLogin
      });
    }

    if (planDate) {
      vars.push({
        name: 'TaskTime_VAR',
        value: this.buildTime(planDate)
      });
    }

    return vars;
  }

  changeProcesses(ids: string[], executorLogin: string, planDate: Date): Observable<any> {
    let vars = this.getVars(executorLogin, planDate);

    let observables = (vars.length > 0) ?
      _.map(ids, id => {
        return from(this.activityRestService.updateProcessVars(id, vars));
      }) :
      [];

    if (observables.length > 0) {
      return forkJoin(observables).pipe(
        tap(response => {
          if (observables.length === 1) {
            this.toastr.success('Процесс успешно изменен!');
          } else {
            this.toastr.error('Процессы успешно изменены!');
          }
        }), catchError(error => {
          if (observables.length === 1) {
            this.toastr.error('Ошибка при изменении процесса!');
          } else {
            this.toastr.error('Ошибка при именении процессов!');
          }
          return throwError(error);
        })
      );
    } else {
      return of('')
    }
  }


  private getVarsForTaskUpdate(executorLogin: string, planDate: Date): any {
    if (executorLogin && planDate) {
      return {
        assignee: executorLogin,
        dueDate: this.buildTime(planDate)
      };
    } else if (executorLogin && !planDate) {
      return {
        assignee: executorLogin
      };
    } else if (!executorLogin && planDate) {
      return {
        dueDate: this.buildTime(planDate)
      };
    }

    return null;
  }

  changeTasks(ids: number[], executorLogin: string, planDate: Date): Observable<any> {
    let vars = this.getVarsForTaskUpdate(executorLogin, planDate);
    let observables = (vars) ?
      _.map(ids, id => {
        return from(this.activityRestService.updateTask(id, vars));
      }) :
      [];

    if (observables.length > 0) {
      return forkJoin(observables).pipe(
        tap(response => {
          if (observables.length === 1) {
            this.toastr.success('Задача успешно изменена!');
          } else {
            this.toastr.error('Задачи успешно изменены!');
          }
        }), catchError(error => {
          if (observables.length === 1) {
            this.toastr.error('Ошибка при изменении задачи!');
          } else {
            this.toastr.error('Ошибка при именении задач!');
          }
          return throwError(error);
        })
      );
    } else {
      return of('');
    }
  }

  private buildTime(date: Date): string {
    let result = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    result += 'T' + (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
    result += ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    result += ':' + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()) + '.000';
    let _date = new Date(result);
    let pad = (value) => {
      return value < 10 ? '0' + value : value;
    };

    let createOffset = (date) => {
      let sign = (date.getTimezoneOffset() > 0) ? '-' : '+';
      let offset = Math.abs(date.getTimezoneOffset());
      let hours = pad(Math.floor(offset / 60));
      let minutes = pad(offset % 60);
      return sign + hours + ':' + minutes;
    };

    let offset = createOffset(_date);
    result += offset;
    return result;
  }

  getTask(id: string): Observable<ITask> {
    return from(this.activityRestService.getTask(id));
  }

}
