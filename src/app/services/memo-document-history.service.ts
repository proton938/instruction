import {Injectable} from '@angular/core';
import {HistoryModel} from "../models/documents-log/HistoryModel";
import {MemoRestService} from "./memo-rest.service";
import {HistoryLogModel} from "../models/documents-log/HistoryLogModel";
import {HistoryLogSorting} from "../models/documents-log/HistoryLogSorting";
import {HistoryLogFilter} from "../models/documents-log/HistoryLogFilter";
import {HistoryLogPagination} from "../models/documents-log/HistoryLogPagination";
import {Observable} from "rxjs/Rx";
import {tap} from "rxjs/internal/operators";

@Injectable({
  providedIn: 'root'
})
export class MemoDocumentHistoryService {
  private model: HistoryModel;

  constructor(private memoRestService: MemoRestService) {
  }

  getModel(): HistoryModel {
    return this.model;
  }

  updateInfoHistory(id: string, documentType?: string): Observable<any> {
    return this.memoRestService.log(id, documentType).pipe(
      tap(response => {
        let logs: HistoryLogModel[] = [];
        response.forEach(r => {
          let date = new Date(r.dateEdit);
          let ms = date.getTime();
          if (r.jsonPatch) {
            r.jsonPatch.forEach(a => {
              logs.push(new HistoryLogModel(ms, r.userName, a.op, a.path, a.value));
            });
          } else {
            logs.push(new HistoryLogModel(ms, r.userName, 'add', '/document', r.jsonOnInsert.document));
          }
        });
        this.model = {
          logs: logs,
          allLogs: logs,
          sortings: HistoryLogSorting.fillSortings(),
          filter: new HistoryLogFilter(logs),
          pagination: new HistoryLogPagination(logs.length)
        };
      })
    );
  }

}
