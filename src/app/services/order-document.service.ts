import { Inject, Injectable } from '@angular/core';
import { Observable, forkJoin, from, of, throwError } from 'rxjs';
import { InstructionRestService } from './instruction-rest.service';
import { catchError, map, mergeMap, tap } from 'rxjs/internal/operators';
import { InstructionExecutor } from '../models/instruction/InstructionExecutor';
import { InstructionDocument } from '../models/instruction-document/InstructionDocument';
import { NSIInstructionStatus } from '../models/nsi/NSIInstructionStatus';
import { InstructionDocumentWrapper } from '../models/instruction-document/InstructionDocumentWrapper';
import { ToastrService } from 'ngx-toastr';
import { compare, Operation } from 'fast-json-patch';
import { UserBean } from '@reinform-cdp/nsi-resource';
import { NSIMeetingTypes } from '../models/nsi/NSIMeetingTypes';
import { MeetingQuestionProtocolPair } from '../models/instruction/MeetingQuestionProtocolPair';
import { NSIPzzDeveloper } from '../models/nsi/NSIPzzDeveloper';
import { SessionStorage } from '@reinform-cdp/security';
import { InstructionDocumentPlanDateHistory } from '../models/instruction-document/InstructionDocumentPlanDateHistory';
import { DocumentUser } from '../models/core/DocumentUser';
import { formatDate } from '@angular/common';
import * as _ from 'lodash';
import * as angular from 'angular';
import { MemoDocument } from '../models/memo-document/MemoDocument';
import { MemoRestService } from './memo-rest.service';
import { InstructionDictsModel } from '../models/instruction/InstructionDictsModel';
import { OrderActivitiService } from './order-activiti.service';
import { XmppService } from '../xmpp/XmppService';
import { AddOperation } from 'fast-json-patch/lib/core';
import {SolrMediatorService} from "./solr-mediator.service";
import {CommonQueryService} from "./common-query.service";
import {
  InstructionDocumentPlanDateRepeat,
  InstructionDocumentRepeated
} from "../models/instruction-document/InstructionDocumentRepeated";
import {HelperService} from "./helper.service";

@Injectable({
  providedIn: 'root'
})
export class OrderDocumentService {

  startPostTime: boolean = false;
  repeatPeriodEndDate: Date = null;
  planDate: Date = null;
  errorMessage: string = '';

  constructor(private instructionRestService: InstructionRestService,
              private solrMediator: SolrMediatorService,
              private queryService: CommonQueryService,
              private memoRestService: MemoRestService,
              private toastr: ToastrService,
              private instructionActivityService: OrderActivitiService,
              private session: SessionStorage,
              private xmppService: XmppService,
              private helper: HelperService) {

  }

  sendMessage(executors: InstructionExecutor[], document: InstructionDocument): Observable<any> {
    if (this.xmppService.settings.xmpp_switch === 'on') {
      let receivers: string[] = [];
      receivers.push(document.instruction.executor.login);
      let coExecutorLogins = _.map(document.instruction.coExecutor, e => { return e.reviewBy.login; });
      let assistantsview = this.getAssistantsview(executors, document);
      receivers = receivers.concat(coExecutorLogins);
      receivers = receivers.concat(assistantsview);
      let message = `Поступило новое поручение: ${document.instruction.content} (автор поручения: ${document.instruction.creator.fioFull}, срок исполнения: ${formatDate(document.instruction.planDate, 'dd.MM.yyyy', 'en')})`;
      this.toastr.info('Производится рассылка уведомления исполнителям ...');
      return this.xmppService.notify(receivers, message).pipe(
        tap(response => {
          this.toastr.success('Уведомления отправлены на сервер рассылки!');
        }), catchError(error => {
          this.toastr.error(error.message);
          return of('');
        })
      );
    } else {
      return of('');
    }
  }

  getAssistants(executors: InstructionExecutor[], document: InstructionDocument): string[] {
    let result = [];

    executors.forEach(e => {
      if (e.creator.accountName === document.instruction.executor.login) {
        result = _.map(e.assistant, a => {
          return a.accountName
        });
      }
    });

    return result;
  }

  getAssistantsview(executors: InstructionExecutor[], document: InstructionDocument): string[] {
    let result = [];

    executors.forEach(e => {
      if (e.creator.accountName === document.instruction.executor.login) {
        result = _.map(e.assistantview, a => {
          return a.accountName
        });
      }
    });

    return result;
  }

  updateDocumentStatus(document: InstructionDocument, statuses: NSIInstructionStatus[], code: string) {
    let status = statuses.find(s => {
      return s.code === code;
    });
    document.statusId.code = status.code;
    document.statusId.name = status.name;
    document.statusId.color = status.color;
  }

  createDocument(document: InstructionDocument): Observable<any> {
    if (!document.documentId) {
      let wrapper = new InstructionDocumentWrapper();
      wrapper.document = document;
      let _documentWrapper = wrapper.min();

      return this.instructionRestService.create(_documentWrapper).pipe(
        tap(response => {
          document.documentId = response.document.documentId;
          document.folderId = response.document.folderId;
          document.instruction.number = response.document.instruction.number;
          this.toastr.success('Поручение успешно создано!');
        }), catchError(error => {
          this.toastr.error('Ошибка при создании поручения!');
          return throwError(error);
        })
      );
    } else {
      return of('');
    }
  }

  deleteDocument(id: string, number: string): Observable<void> {
    return this.instructionRestService.delete(id).pipe(tap(() => {
      this.toastr.success(`Поручение ${number} успешно удалено!`);
    }, (error) => {
      this.toastr.error('Ошибка при удалении поручения!');
    }));
  }

  getDeletedOrCanceledCandidates(document: InstructionDocument): Observable<InstructionDocument[]> {
    return this.getSubTasks(document.documentId).pipe(
      mergeMap(response => {
        let subTaskIds = response.map(r => {
          return r.documentId;
        });
        if (subTaskIds.length > 0) {

          let promises = subTaskIds.map(id => {
            return this.instructionRestService.get(id);
          });
          return forkJoin(promises).pipe(
            map(response => {
              return [document].concat(response.map((r: any) => {
                let doc = new InstructionDocument();
                doc.build(r.document);
                return doc;
              }));
            }), catchError(error => {
              this.toastr.error('Ошибка при определении кандидатов на удаление!');
              return throwError(error);
            })
          );
        } else {
          return of([document]);
        }
      }), catchError(error => {
        this.toastr.error('Ошибка при определении кандидатов на удаление!');
        return throwError(error);
      })
    );
  }

  updateDocumentFiles(id: string, left: InstructionDocument, right: InstructionDocument): Observable<any> {
    let _left = {
      document: left.min()
    };

    let _right = {
      document: right.min()
    };

    let diff: Operation[] = compare(_left, _right);
    if (diff.length > 0) {
      return this.instructionRestService.patch(`${id}?sendNotificationOnAttachedFilesChange=false`, <any>JSON.stringify(diff)).pipe(
        map(response => {
          this.toastr.success('Поручение успешно изменено!');
          return response;
        }), catchError(error => {
          this.toastr.error('Ошибка при изменении поручения!');
          return throwError(error);
        })
      );
    } else {
      return of('');
    }
  }

  updateDocument(id: string, left: InstructionDocument, right: InstructionDocument, from = 'edit'): Observable<any> {
    let _left = {
      document: left.min()
    };

    let _right = {
      document: right.min()
    };

    let diff: Operation[] = compare(_left, _right);
    if (diff.length > 0) {
      return this.instructionRestService.patch(id, <any>JSON.stringify(diff)).pipe(
        map(response => {
          this.toastr.success('Поручение успешно изменено!');
          return response;
        }), catchError(error => {
          this.toastr.error('Ошибка при изменении поручения!');
          return throwError(error);
        })
      );
    } else {
      return of('');
    }
  }

  getParentDocument(id: string): Observable<InstructionDocument> {
    if (id) {
      return this.instructionRestService.get(id).pipe(
        map(response => {
          let doc = new InstructionDocument();
          doc.build(response.document);
          return doc;
        })
      );
    } else {
      return from(Promise.resolve(null));
    }
  }

  getMemo(id: string): Observable<MemoDocument> {
    if (id) {
      return this.memoRestService.get(id).pipe(
        map(response => {
          let doc = new MemoDocument();
          doc.build(response.document);
          return doc;
        })
      );
    } else {
      return of(null);
    }
  }

  getMemoMKA(id: string): Observable<MemoDocument> {
    if (id) {
      return this.memoRestService.get(id, 'memomka').pipe(
        map(response => {
          let doc = new MemoDocument();
          doc.build(response.document);
          return doc;
        })
      );
    } else {
      return of(null);
    }
  }

  getRecreatedDocument(id: string): Observable<InstructionDocument> {
    if (id) {
      return this.instructionRestService.get(id).pipe(
        map(response => {
          let doc = new InstructionDocument();
          doc.build(response.document);
          return doc;
        })
      );
    } else {
      return of(null);
    }
  }

  getSubTasks(id: string): Observable<any[]> {
    return this.solrMediator.query({
      page: 0,
      pageSize: 10000,
      query: 'parentId:' + id,
      types: [ this.solrMediator.types.instruction ]
    }).pipe(
      map(response => {
        return response.docs || [];
      })
    );
  }

  getLinked(document: InstructionDocument): Observable<any[]> {
    let observables = [];
    observables.push(this.getLinkedRightInstructions(document.documentId));
    observables.push(this.getLinkedLeftInstructions(document.linkedId));
    return forkJoin(observables).pipe(
      map(response => {
        let result = [];
        response.forEach(r => {
          Array.prototype.push.apply(result, r);
        });
        return result;
      })
    )
  }

  getInsideInstructions(id: string): Observable<any[]> {
    return this.solrMediator.query({
      page: 0,
      pageSize: 10000,
      query: 'instructionMKAID:' + id,
      types: [ this.solrMediator.types.instruction ]
    }).pipe(
      map(response => response.docs || [])
    );
  }

  getLinkedRightInstructions(id: string): Observable<any[]> {
    return this.solrMediator.query({
      page: 0,
      pageSize: 10000,
      query: 'linkedID:' + id,
      types: [ this.solrMediator.types.instruction ]
    }).pipe(
      map(response => response.docs || [])
    );
  }

  removeLinkedId(id: string): Observable<any> {
    return this.solrMediator.query({
      page: 0,
      pageSize: 10000,
      query: 'linkedID:' + id,
      types: [ this.solrMediator.types.instruction ]
    }).pipe(
      mergeMap(res => of(this.solrMediator.getDocs(res))),
      mergeMap(docs => {
        if (docs.length > 0) {
          let observables = docs.map(d => this.removeLinkedIdOneDocument(d.documentId));
          return forkJoin(observables);
        } else {
          return of([]);
        }
      })
    );
  }

  removeLinkedIdOneDocument(id: string): Observable<any> {
    return this.getRecreatedDocument(id).pipe(
      mergeMap(response => {
        let document = response;
        let copyDocument = angular.copy(document);
        document.linkedId = '';
        return this.updateDocument(document.documentId, copyDocument, document);
      })
    );
  }

  getLinkedLeftInstructions(id: string): Observable<any[]> {
    if (id) {
      return this.solrMediator.query({
        page: 0,
        pageSize: 10000,
        query: 'documentId:' + id,
        types: [ this.solrMediator.types.instruction ]
      }).pipe(
        map(response => response.docs || [])
      );
    } else {
      return of([]);
    }
  }

  getCurrentUser(): UserBean {
    let user = new UserBean();

    user.accountName = this.session.login();
    user.displayName = this.session.fullName();
    user.post = this.session.post();

    return user;
  }

  getMeeting(expressMeetingID: string): Observable<any[]> {
    if (expressMeetingID) {
      return this.solrMediator.query({
        page: 0,
        pageSize: 10000,
        query: 'agendaID:' + expressMeetingID,
        types: [ this.solrMediator.types.agenda ]
        // fields: [
        //   new SearchExtDataItem('docTypeCode', 'AGENDA'),
        //   new SearchExtDataItem('agendaID', expressMeetingID)
        // ]
      }).pipe(
        map(response => {
          let docs: any[] = response.docs || [];
          if (docs.length === 0) {
            return [];
          }
          return docs;
        })
      );
    } else {
      return of([]);
    }
  }

  getMeetingQuestion(questionId: string): Observable<any[]> {
    if (questionId) {
      return this.solrMediator.query({
        page: 0,
        pageSize: 10000,
        query: 'questionID:' + questionId,
        types: [ this.solrMediator.types.question ]
        // fields: [
        //   new SearchExtDataItem('docTypeCode', 'QUESTION'),
        //   new SearchExtDataItem('questionId', questionId)
        // ]
      }).pipe(
        map(response => {
          let docs: any[] = response.docs || [];
          if (docs.length === 0) {
            return [];
          }
          return docs;
        })
      );
    } else {
      return of([]);
    }
  }

  getQuestions(document: InstructionDocument): Observable<any[]> {
    let documents: any[] = [];

    if (document.instruction.questionId) {
      return this.solrMediator.query({
        page: 0,
        pageSize: 10000,
        query: 'questionID:' + document.instruction.questionId,
        types: [ this.solrMediator.types.question ]
        // fields: [
        //   new SearchExtDataItem('docTypeCode', 'QUESTION'),
        //   new SearchExtDataItem('questionId', document.instruction.questionId)
        // ]
      }).pipe(
        mergeMap(response => {
          let docs: any[] = response.docs || [];
          if (docs.length === 0) {
            return of([]);
          }
          Array.prototype.push.apply(documents, docs);
          let observables = docs.map(d => {
            return this.solrMediator.query({
              page: 0,
              pageSize: 10000,
              query: [
                this.queryService.commonToQuery(d.meetingDate, [{code: 'meetingDate', type: 'date'}]),
                'meetingNumber:' +  d.meetingNumber,
                'meetingType:' + d.meetingType
              ].join(' AND '),
              types: [ this.solrMediator.types.protocol ]
              // fields: [
              //   new SearchExtDataItem('docTypeCode', 'PROTOCOL'),
              //   new SearchExtDataItem('meetingDate', d.meetingDate),
              //   new SearchExtDataItem('meetingNumber', d.meetingNumber),
              //   new SearchExtDataItem('meetingTypeProtocol', d.meetingTypeQuestion)
              // ]
            });
          });
          return forkJoin(observables);
        }), map(response => {
          response.forEach(r => {
            Array.prototype.push.apply(documents, r.docs);
          });
          return documents;
        })
      );
    } else {
      return of([]);
    }
  }

  buildPair(docs: any[], meetingTypes: NSIMeetingTypes[]): MeetingQuestionProtocolPair[] {
    if (docs.length > 0) {
      let protocols: any[] = docs.filter(q => {
        return q.docTypeCode === 'PROTOCOL';
      });
      let questions: any[] = docs.filter(q => {
        return q.docTypeCode === 'QUESTION';
      });
      let pair: MeetingQuestionProtocolPair[] = [];
      questions.forEach(q => {
        let _p = null;
        protocols.forEach(p => {
          if ((q.meetingTypeQuestion === (<any>p).meetingTypeProtocol) && (q.meetingDate === p.meetingDate) && (q.meetingNumber === p.meetingNumber)) {
            _p = p
          }
        });
        if (_p) {
          let m = meetingTypes.find(m => {
            return m.meetingType === _p.meetingTypeProtocol;
          });
          _p['meetingFullName'] = (m) ? m.meetingFullName : '';
          _p['link'] = `/sdo/meeting/#/app/meeting/protocol/${_p.documentId}`;
        } else {
          let m = meetingTypes.find(m => {
            return m.meetingType === q.meetingTypeQuestion;
          });
          q['meetingFullName'] = (m) ? m.meetingFullName : '';
        }

        q['link'] = `/sdo/meeting/#/app/meeting/question/${q.documentId}`;

        pair.push({
          question: q,
          protocol: _p
        })
      });

      return pair;

    } else {
      return [];
    }
  }

  getMeetingType(meetingTypeSS: string, meetingTypes: NSIMeetingTypes[]): NSIMeetingTypes {
    return meetingTypes.find(m => {
      return m.meetingType === meetingTypeSS;
    });
  }

  getCadastralAuthor(document: InstructionDocument, pzzDevelopers: NSIPzzDeveloper[]): string {
    if (document.instruction.cadastral && document.instruction.cadastral.author) {
      return pzzDevelopers.find(d => {
        return d.code === document.instruction.cadastral.author
      }).name;
    } else {
      return '';
    }
  }

  isAssistant(document: InstructionDocument, executors: InstructionExecutor[], target: 'executor' | 'creator'): boolean {
    let isAssistant = false;
    let e = executors.find(e => {
      return e.creator.accountName === document.instruction[target].login;
    });

    if (e) {
      let a = e.assistant.find(a => {
        return a.accountName === this.session.login();
      });
      if (a) {
        isAssistant = true;
      }
    }

    return isAssistant;
  }

  showSubTask(document: InstructionDocument, executors: InstructionExecutor[]): boolean {
    let onlyOneLevelSubTasks = !document.parentId;
    let currentUserIsExecutorOrAssistant = document.instruction.executor.login === this.session.login() ||
      this.isAssistant(document, executors, 'executor') ||
      document.instruction.creator.login === this.session.login() ||
      this.isAssistant(document, executors, 'creator');

    return onlyOneLevelSubTasks && currentUserIsExecutorOrAssistant;
  }

  isShowLinked(document: InstructionDocument, executors: InstructionExecutor[]): boolean {
    return ((document.instruction.creator.login === this.session.login()) ||
      this.isAssistant(document, executors, 'creator'));
  }

  showCancel(document: InstructionDocument) {
    let isOpenOrder = !document.instruction.factDate;
    let currentUserIsCreatorOrAssistant = document.instruction.creator.login === this.session.login() ||
      document.instruction.assistant.login === this.session.login();
    let orderInCorrectStatus = _.includes(['inwork', 'check', 'assign'], document.statusId.code);

    return currentUserIsCreatorOrAssistant && isOpenOrder && orderInCorrectStatus;
  }

  showEdit(document: InstructionDocument) {
    let isOpenOrder = !document.instruction.factDate;
    let currentUserIsCreatorOrAssistant = document.instruction.creator.login === this.session.login() ||
      document.instruction.assistant.login === this.session.login();
    let orderInCorrectStatus = _.includes(['inwork', 'check', 'assign'], document.statusId.code);

    return currentUserIsCreatorOrAssistant && isOpenOrder && orderInCorrectStatus;
  }

  showLinked(document: InstructionDocument, executors: InstructionExecutor[]): boolean {
    return (this.session.login() === document.instruction.creator.login) ||
      this.isAssistant(document, executors, 'creator');
  }

  showCopyOrder(document: InstructionDocument, executors: InstructionExecutor[]): boolean {
    return (this.session.login() === document.instruction.creator.login) ||
      this.isAssistant(document, executors, 'creator') ||
      (this.session.login() === document.instruction.executor.login);
  }

  sendNotify(id: string): Observable<void> {
    return this.instructionRestService.notify(id).pipe(
      tap(response => {
        this.toastr.success('Письмо с уведомлением успешно отправлено!');
      }), catchError(error => {
        this.toastr.error('Ошибка при отправке письма с уведомлением!');
        return throwError(error);
      })
    )
  }

  getExecutorLoginForUpdateBPM(left: InstructionDocument, right: InstructionDocument): string {
    return (left.instruction.executor.login !== right.instruction.executor.login) ? right.instruction.executor.login : null;
  }

  getPlanDateForUpdateBPM(left: InstructionDocument, right: InstructionDocument): Date {
    return (left.instruction.planDate.toString() !== right.instruction.planDate.toString()) ? right.instruction.planDate : null;
  }

  updatePrimaryDate(document: InstructionDocument, editedDocument: InstructionDocument) {
    if (this.planeDateChanged(document, editedDocument)) {
      if (!editedDocument.instruction.primaryDate) {
        editedDocument.instruction.primaryDate = document.instruction.planDate;
      }
    }
  }

  updatePrimaryDateChangedHistory(document: InstructionDocument, editedDocument: InstructionDocument) {
    // расчет history происходит целиком на стороне бекенда
    if (this.planeDateChanged(document, editedDocument)) {
      // let history = new InstructionDocumentPlanDateHistory();
      // history.oldDate = document.instruction.planDate;
      // history.newDate = editedDocument.instruction.planDate;
      // let currentUser = this.getCurrentUser();
      // let user = new DocumentUser();
      // user.updateFromUserBean(currentUser);
      // history.user = user;
      // history.changeDate = new Date();
      // editedDocument.instruction.planDateHistory.push(history);
    }
  }

  planeDateChanged(left: InstructionDocument, right: InstructionDocument) {
    let dateToString = (date) => {
      return formatDate(date, 'yyyy-MM-ddTHH:mm', 'en');
    };
    return (left.instruction.planDate && right.instruction.planDate && (dateToString(left.instruction.planDate) !== dateToString(right.instruction.planDate)));
  }

  cancelOrder(reason: string, document: InstructionDocument, comment: string, dicts: InstructionDictsModel): Observable<void> {
    let documentCopy = angular.copy(document);
    let status = (reason === 'Отменено') ? 'refused' : 'notExecuted';
    this.updateDocumentStatus(document, dicts.statuses, status);
    document.instruction.factDate = new Date();
    document.removeBase = comment;
    return this.updateDocument(document.documentId, documentCopy, document).pipe(
      mergeMap(response => {
        return this.instructionActivityService.getProcessHistoryById(document.documentId);
      }),
      mergeMap(response => {
        let openedProcesses: string[] = this.instructionActivityService.getOpenedProcesses(response);
        return this.instructionActivityService.deleteProcesses(openedProcesses);
      }),
      mergeMap(response => {
        return this.sendNotify(document.documentId);
      })
    );
  }

  beforeSave(model: InstructionDocument): void {
    const isRepeated = this.helper.getField('instruction.repeated.repeatedSign', model);
    const planDateRepeat = this.helper.getField('instruction.repeated.planDateRepeat', model) || [];
    if (isRepeated) { // instruction is repeated
      if (!model.documentId) { // prepare document to create
        model.instruction.primaryDate = model.instruction.planDate;
        const planDateRepeat = new InstructionDocumentPlanDateRepeat();
        planDateRepeat.planDate = model.instruction.planDate;
        if (model.instruction.repeated.repeatedSign) { planDateRepeat.numberRepeat = 1; }
        model.instruction.repeated.planDateRepeat = [planDateRepeat];
        model.instruction.repeated.currentNumberRepeat = 1;
      } else if (planDateRepeat.length === 1 && !planDateRepeat[0].reportAccepted) {
        planDateRepeat[0].planDate = model.instruction.planDate;
        planDateRepeat[0].numberRepeat = 1;
      }
    }
    if (!model.documentId) {
      model.instruction.recalled = false;
    }
  }

  isValidRepeated(model: InstructionDocumentRepeated): boolean {
    let r = true;
    if (!model.repeatedSign) {
      return true;
    }
    if (!model.periodicity || !model.periodRepeat.code) {
      return false;
    }
    switch (model.periodRepeat.code) {
      case 'weekly':
        r = model.weekday.length > 0;
        break;
      case 'monthly':
      case 'yearly':
        r = !!model.dayMonth || (model.weekday.length > 0 && !!model.numberWeek);
        if (!r) { this.errorMessage = 'Необходимо определить число месяца или день недели повтора поручения'; }
        break;
      default:
        return true;
    }

    if (r && model.periodRepeat.code === 'yearly') {
      r = model.Month.length > 0;
    }

    return r;
  }

  isValid(model: InstructionDocument): boolean {
    const instruction = model.instruction;
    let isCadastral = instruction.theme && instruction.theme.code === 'cadastral'
      ? !!instruction.cadastral.author && !!instruction.cadastral.request && instruction.cadastral.count !== null
      : true;
    return isCadastral && !!instruction.creator && !!instruction.beginDate
      && !!instruction.content && !!instruction.planDate
      && !!instruction.executor && !!instruction.priority
      && !!instruction.difficulty && this.isValidRepeated(instruction.repeated);
  }

  get errorMsg(): string {
    return this.errorMessage || 'Не заполнены обязательные поля!';
  }
}
