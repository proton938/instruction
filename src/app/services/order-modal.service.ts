import {BsModalService} from 'ngx-bootstrap';
import {Injectable} from '@angular/core';
import {Observable, from} from 'rxjs';
import {AddThemeModalComponent} from '../components/order/add-theme/add-theme.modal';
import {AddEmployeeModalComponent} from '../components/order/tasks/review-mka-doc/add-employee/add-employee.modal'
import {DocumentComment} from '../models/core/DocumentComment';
import {AddCommentModalComponent} from '../components/order/order-card/order-comments/add-comment.modal';
import {AlertService} from '@reinform-cdp/widgets';
import {CopyOrderModalComponent} from '../components/order/order-card/order-card-menu/copy-order/copy-order.modal';
import {CancelOrderModalComponent} from '../components/order/order-card/order-card-menu/cancel-order/cancel-order.modal';
import {InstructionDocument} from '../models/instruction-document/InstructionDocument';
import {EditOrderModalComponent} from '../components/order/order-card/order-card-menu/edit-order/edit-order.modal';
import {ReCreateOrderModalComponent} from '../components/order/order-card/order-card-menu/re-create-order/re-create-order.modal';

@Injectable({
  providedIn: 'root'
})
export class OrderModalService {

  constructor(private modalService: BsModalService, private alertService: AlertService) {
  }

  delete(): Observable<any> {
    return from(this.alertService.confirm({
      okButtonText: 'Ок',
      message: 'Вы действительно хотите удалить данное поручение и все дочерние?',
      type: 'warning',
      size: 'md',
      windowClass: 'zindex'
    }));
  }

  cancel(): Observable<any> {
    return Observable.create(subscriber => {
      const options = {
        initialState: {},
        'class': 'modal-md'
      };
      const ref = this.modalService.show(CancelOrderModalComponent, options);
      let subscr = this.modalService.onHide.subscribe(
        () => {
          let component = <CancelOrderModalComponent> ref.content;
          if (component.submit) {
            subscriber.next({
              reason: component.result,
              status: component.status,
              comment: component.comment.trim()
            });
          } else {
            subscriber.error('canceled');
          }
          subscr.unsubscribe();
        }
      );
    });
  }

  reCreate(reason, documentId): Observable<string> {
    return Observable.create(subscriber => {
      const options = {
        initialState: {
          documentId: documentId
        },
        'class': 'modal-lg'
      };
      const ref = this.modalService.show(ReCreateOrderModalComponent, options);
      let subscr = this.modalService.onHide.subscribe(() => {
        let component = <ReCreateOrderModalComponent> ref.content;
        if (component.submit) {
          reason.documentId = component.document.documentId;
          subscriber.next(reason);
        } else {
          subscriber.error('canceled');
        }
        subscr.unsubscribe();
      });
    });
  }

  edit(id: string): Observable<InstructionDocument> {
    return Observable.create(subscriber => {
      const options = {
        initialState: {
          id: id
        },
        'class': 'modal-lg'
      };
      const ref = this.modalService.show(EditOrderModalComponent, options);
      let subscr = this.modalService.onHide.subscribe(() => {
        let component = <EditOrderModalComponent> ref.content;
        if (component.submit) {
          subscriber.next(component.document);
        } else {
          subscriber.error('canceled');
        }
        subscr.unsubscribe();
      });
    });
  }

  editNew(id: string): Observable<InstructionDocument> {
    return Observable.create(subscriber => {
      const options = {
        initialState: {
          id: id
        },
        'class': 'modal-lg'
      };
      const ref = this.modalService.show(EditOrderModalComponent, options);
      let subscr = this.modalService.onHide.subscribe(() => {
        let component = <EditOrderModalComponent>ref.content;
        if (component.submit) {
          subscriber.next(component.document);
        } else {
          subscriber.error('canceled');
        }
        subscr.unsubscribe();
      });
    });
  }

  copy(): Observable<string> {
    return Observable.create(subscriber => {
      const options = {
        initialState: {},
        'class': 'modal-md'
      };
      const ref = this.modalService.show(CopyOrderModalComponent, options);
      let subscr = this.modalService.onHide.subscribe(() => {
        let component = <CopyOrderModalComponent> ref.content;
        if (component.submit) {
          subscriber.next(component.status);
        } else {
          subscriber.error('canceled');
        }
        subscr.unsubscribe();
      });
    });
  }

  addComment(): Observable<DocumentComment> {
    return Observable.create(subscriber => {
      const options = {
        initialState: {},
        'class': 'modal-md'
      };
      const ref = this.modalService.show(AddCommentModalComponent, options);
      let subscr = this.modalService.onHide.subscribe(() => {
        let component = <AddCommentModalComponent> ref.content;
        if (component.submit) {
          subscriber.next(component.newComment);
        } else {
          subscriber.error('canceled');
        }
        subscr.unsubscribe();
      });
    });
  }

  createNewTheme(): Observable<string> {
    return Observable.create(subscriber => {
      const options = {
        initialState: {},
        'class': 'modal-md'
      };
      const ref = this.modalService.show(AddThemeModalComponent, options);
      let subscr = this.modalService.onHide.subscribe(() => {
        let component = <AddThemeModalComponent> ref.content;
        if (component.submit) {
          subscriber.next(component.theme);
        } else {
          subscriber.error('canceled');
        }
        subscr.unsubscribe();
      });
    });
  }

  addEmployee(): Observable<string> {
    return Observable.create(subscriber => {
      const options = {
        initialState: {},
        'class': 'modal-md'
      };
      const ref = this.modalService.show(AddEmployeeModalComponent, options);
      let subscr = this.modalService.onHide.subscribe(() => {
        let component = <AddEmployeeModalComponent> ref.content;

        if (component.submit) {
          subscriber.next(component.employee);
        } else {
          subscriber.error('canceled');
        }
        subscr.unsubscribe();
      });
    });
  }

}
