import {compare, Operation} from 'fast-json-patch';
import * as _ from 'lodash';
import * as angular from 'angular';
import {ToastrService} from 'ngx-toastr';
import {Injectable} from '@angular/core';
import {SearchResultDocument} from '@reinform-cdp/search-resource';
import {MemoRestService} from './memo-rest.service';
import {SessionStorage} from '@reinform-cdp/security';
import {catchError, map, mergeMap, tap} from 'rxjs/internal/operators';
import {Observable, forkJoin, from, of, throwError} from 'rxjs';
import {MemoDocument} from '../models/memo-document/MemoDocument';
import {NSIMemoStatus} from '../models/nsi/NSIMemoStatus';
import {MemoDocumentWrapper} from '../models/memo-document/MemoDocumentWrapper';
import {UserBean} from '@reinform-cdp/nsi-resource';
import {IMemoDocumentWrapper} from '../models/memo-document/abstracts/IMemoDocumentWrapper';
import {SolrMediatorService} from "./solr-mediator.service";

@Injectable({
  providedIn: 'root'
})
export class MemoDocumentService {

  constructor(private solrMediator: SolrMediatorService,
              private memoRestService: MemoRestService,
              private toastr: ToastrService,
              private session: SessionStorage) {
  }

  updateDocumentStatus(document: MemoDocument, statuses: NSIMemoStatus[], code: string) {
    let status = statuses ? statuses.find(s => {
      return s.code === code;
    }) : null;
    if (status) {
      document.statusId.code = status.code;
      document.statusId.name = status.name;
      document.statusId.color = status.color;
    }
  }

  createDocument(document: MemoDocument): Observable<any> {
    return this._createDocument(document, (_documentWrapper) => this.memoRestService.create(_documentWrapper));
  }

  createDocumentInitial(document: MemoDocument): Observable<any> {
    return this._createDocument(document, (_documentWrapper) => this.memoRestService.createInitial(_documentWrapper));
  }

  private _createDocument(document: MemoDocument, save: (doc: IMemoDocumentWrapper) => Observable<IMemoDocumentWrapper>): Observable<any> {
    if (!document.documentId) {
      let wrapper = new MemoDocumentWrapper();
      wrapper.document = document;
      let _documentWrapper = wrapper.min();
      return save(_documentWrapper).pipe(
        tap(response => {
          document.documentId = response.document.documentId;
          document.folderId = response.document.folderId;
          document.number = response.document.number;
          this.toastr.success('Служебная записка успешно создана!');
        }), catchError(error => {
          this.toastr.error('Ошибка при создании служебной записки!');
          return throwError(error);
        })
      );
    } else {
      return of('');
    }
  }

  updateDocument(id: string, original: MemoDocument, changed: MemoDocument, checkRelatedId = true): Observable<any> {
    const _left = {document: original.min()};
    const _right = {document: changed.min()};
    const diff: Operation[] = compare(_left, _right);

    if (diff.length > 0) {

      const oldRelatedId = original.relateId;
      const newRelatedId = changed.relateId;

      const memoIdsToAddRelatedId = _.difference(newRelatedId, oldRelatedId);
      const memoIdsToDeleteRelatedId = _.difference(oldRelatedId, newRelatedId);

      return this.memoRestService.patch(id, <any>JSON.stringify(diff)).pipe(
        mergeMap(() => {
          const loadRequests = [];
          const memoIdsToLoad = [...memoIdsToAddRelatedId, ...memoIdsToDeleteRelatedId];

          if (!checkRelatedId || !memoIdsToLoad.length) {
            return of([]);
          }

          memoIdsToLoad.forEach((memoId) => {
            loadRequests.push(this.memoRestService.get(memoId));
          });

          return forkJoin(loadRequests).pipe(
            mergeMap((memos) => {
              const updateRequests = [];

              memos.forEach((memoRaw) => {
                const memo = new MemoDocument();
                memo.build(memoRaw.document);
                const copyMemo = angular.copy(memo);

                if (memoIdsToAddRelatedId.indexOf(memo.documentId) !== -1) {
                  memo.relateId = _.uniq([
                    ...(memo.relateId || []),
                    id,
                  ]);
                }

                if (memoIdsToDeleteRelatedId.indexOf(memo.documentId) !== -1) {
                  memo.relateId = (memo.relateId || []).filter((rid) => ( rid !== id));
                }

                updateRequests.push(this.updateDocument(memo.documentId, copyMemo, memo, false));
              });

              return forkJoin(updateRequests);
            }),
            mergeMap(() => {
              return of([]);
            })
          );
        }),
        tap(() => {
          if (checkRelatedId) {
            this.toastr.success('Внутренний документ успешно сохранён!');
          }
        }),
        catchError(error => {
          this.toastr.error('Ошибка при сохранении внутреннего документа!');
          return throwError(error);
        })
      );
    } else {
      return of('');
    }
  }

  getCurrentUser(): UserBean {
    let user = new UserBean();

    user.accountName = this.session.login();
    user.displayName = this.session.fullName();
    user.post = this.session.post();
    user.telephoneNumber = this.session.telephoneNumber();
    user.departmentCode = this.session.departmentCode();
    user.departmentFullName = this.session.departmentFullName();
    user.mail = this.session.mail();
    user.otherPhone = this.session.otherPhone();

    return user;
  }

  getInstructions(id: string, memoType: string = 'memo'): Observable<SearchResultDocument[]> {
    const field = memoType === 'memo' ? 'memoID' : 'memoMKAID';

    return this.solrMediator.query({
      page: 0,
      pageSize: 10000,
      query: field + ':' + id,
      types: [ this.solrMediator.types.instruction ]
    }).pipe(
      map(response => response.docs || [])
    );
  }

  getInsideInstructions(id: string, memoType: string = 'memo'): Observable<SearchResultDocument[]> {
    const field = memoType === 'memo' ? 'memoID' : 'memoMKAID';

    return this.solrMediator.query({
      page: 0,
      pageSize: 10000,
      query: field + ':' + id,
      types: [ this.solrMediator.types.instruction ]/*,
      fields: [
        new SearchExtDataItem(field, id),
        new SearchExtDataItem('docTypeCode', 'INSTRUCTION'),
      ]*/
    }).pipe(
      map(response => response.docs || [])
    );
  }

  deleteDocument(id: string, number: string): Observable<void> {
    return this.memoRestService.delete(id).pipe(tap(() => {
      this.toastr.success(`Служебная записка ${number} успешно удалено!`);
    }, (error) => {
      this.toastr.error('Ошибка при удалении служебной записки!');
    }));
  }

  getSubTasks(ids: string[]): Observable<SearchResultDocument[]> {
    if (ids.length) {
      return this.solrMediator.query({
        page: 0,
        pageSize: 10000,
        query: 'parentId:' + ids.join(' OR parentId:'),
        types: [ this.solrMediator.types.instruction ]
      }).pipe(map(res => res && res.docs ? res.docs : []));
    } else {
      return of([]);
    }
  }

  getRelatedMemos(ids: string[]): Observable<MemoDocument[]> {
    if (!ids || ids.length === 0) {
      return of([]);
    }

    const observables = [];

    ids.forEach((id) => {
      observables.push(from(this.memoRestService.get(id)));
    });

    return forkJoin(observables).pipe(
      map(response => response.map((r) => (r.document)))
    );

  }

  searchMemos(search: string): Observable<SearchResultDocument[]> {
    const searchParams = {
      sort: 'docDateMemo desc',
      page: 0,
      pageSize: 10000,
      types: [ this.solrMediator.types.memo ],
      query: `(docNumberMemo: *${search}*)`
    };

    return this.solrMediator.query(searchParams).pipe(map(res => res && res.docs ? res.docs : []));
  }

}
