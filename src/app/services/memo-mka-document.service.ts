import {compare, Operation} from "fast-json-patch";
import {ToastrService} from "ngx-toastr";
import {Injectable} from '@angular/core';
import {SearchExtDataItem, SearchResultDocument, SolarResourceService} from "@reinform-cdp/search-resource";

import {SessionStorage} from "@reinform-cdp/security";
import {catchError, map, tap} from "rxjs/internal/operators";
import {Observable} from "rxjs/Rx";
import {MemoDocument} from "../models/memo-document/MemoDocument";
import {NSIMemoStatus} from "../models/nsi/NSIMemoStatus";
import {MemoDocumentWrapper} from "../models/memo-document/MemoDocumentWrapper";
import {forkJoin, from, of, throwError} from "rxjs/index";
import {UserBean} from "@reinform-cdp/nsi-resource";
import { MemoMkaRestService } from "./memo-mka-rest.service";

@Injectable({
  providedIn: 'root'
})
export class MemoMkaDocumentService {

  constructor(private solarRestService: SolarResourceService,
              private memoMkaRestService: MemoMkaRestService,
              private toastr: ToastrService,
              private session: SessionStorage) {
  }

  updateDocumentStatus(document: MemoDocument, statuses: NSIMemoStatus[], code: string) {
    let status = statuses ? statuses.find(s => {
      return s.code === code;
    }) : null;
    if (status) {
      document.statusId.code = status.code;
      document.statusId.name = status.name;
      document.statusId.color = status.color;
    }
  }

  updateDocument(id: string, left: MemoDocument, right: MemoDocument): Observable<any> {
    let _left = {document: left.min()};
    let _right = {document: right.min()};
    let diff: Operation[] = compare(_left, _right);
    if (diff.length > 0) {
      return this.memoMkaRestService.patch(id, <any>JSON.stringify(diff)).pipe(
        tap(response => {
          this.toastr.success('Служебная записка успешно изменена!');
        }), catchError(error => {
          this.toastr.error('Ошибка при изменении служебной записки!');
          return throwError(error);
        })
      );
    } else {
      return of('');
    }
  }

  getCurrentUser(): UserBean {
    let user = new UserBean();

    user.accountName = this.session.login();
    user.displayName = this.session.fullName();
    user.post = this.session.post();

    return user;
  }

  getInstructions(id: string): Observable<SearchResultDocument[]> {
    return from(this.solarRestService.searchExt({
      page: 0,
      pageSize: 10000,
      fields: [
        new SearchExtDataItem('memoId', id)
      ]
    })).pipe(
      map(response => response.docs || [])
    );
  }

  deleteDocument(id: string, number: string): Observable<void> {
    return this.memoMkaRestService.delete(id).pipe(tap(() => {
      this.toastr.success(`Служебная записка ${number} успешно удалено!`);
    }, (error) => {
      this.toastr.error('Ошибка при удалении служебной записки!');
    }));
  }

  getSubTasks(ids: string[]): Observable<SearchResultDocument[]> {
    let observables = [];
    ids.forEach(i => {
      observables.push(from(this.solarRestService.searchExt({
        page: 0,
        pageSize: 10000,
        fields: [
          new SearchExtDataItem('parentId', i)
        ]
      })));
    });
    if (observables.length > 0) {
      return forkJoin(observables).pipe(
        map(response => {
          let result = [];
          response.forEach(r => {
            Array.prototype.push.apply(result, r.docs);
          });
          return result;
        })
      );
    } else {
      return of([]);
    }
  }


}
