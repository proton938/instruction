import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";
import {map} from "rxjs/internal/operators";
import { Injectable } from '@angular/core';
import {IMemoDocumentWrapper} from "../models/memo-document/abstracts/IMemoDocumentWrapper";
import {IJsonPatchData} from "../models/IJsonPatchData";
import {IDocumentLogExtended} from "../models/IDocumentLogExt";

@Injectable({
  providedIn: 'root'
})
export class MemoRestService {
  root = `/app/sdo/instruction`;

  constructor(private http: HttpClient) { }

  search(search): Observable<any> {
    return this.http.post('/search/v1/solr/query', search);
  }

  get(id: string, type?: string): Observable<IMemoDocumentWrapper> {
    type = type || 'memo';
    return this.http.get(`${this.root}/${type}/${id}`);
  }

  create(data: IMemoDocumentWrapper): Observable<IMemoDocumentWrapper> {
    return this.http.post(`${this.root}/memo`, data, {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    });
  }

  createInitial(data: IMemoDocumentWrapper): Observable<IMemoDocumentWrapper> {
    return this.http.post(`${this.root}/memo/initial`, data, {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    });
  }

  delete(id: string): Observable<void> {
    return this.http.delete(`${this.root}/memo/${id}`).pipe(map(response => { return; }));
  }

  patch(id: string, data: IJsonPatchData[]): Observable<void> {
    return this.http.patch(`${this.root}/memo/${id}`, data, {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    }).pipe(map(response => { return; }));
  }

  notify(logins: string[], documentUrl: string, creator: string): Observable<void> {
    return this.http.post(`${this.root}/notify/memo`, {
      logins: logins,
      documentUrl: documentUrl,
      creator: creator
    }).pipe(map(response => { return; }));
  }

  notifyAdditionalApproval(logins: string[] = [], taskId: string = ''): Observable<any> {
    return this.http.post(`${this.root}/notify/memo/additionalApproval`, { logins, taskId });
  }

  log(id: string, documentType?: string): Observable<IDocumentLogExtended<IMemoDocumentWrapper>[]> {
    documentType = documentType || 'MEMO';
    return this.http.get<IDocumentLogExtended<IMemoDocumentWrapper>[]>(`${this.root}/log/${id}/${documentType}/ASC`);
  }

  generateNumber(id: string): Observable<void> {
    return this.http.post<any>(`${this.root}/memo/${id}/regNum`, {});
  }

  sendToDocumentsDB(id: string): Observable<any> {
    return this.http.post<any>(`${this.root}/memo/${id}/sendToDocumentsDB`, null);
  }

  getDelegates(login: string): Observable<any> {
    return this.http.get<any>(`/mdm/api/v1/delegate/tasks/sysprefix/sdomemo/account/${login}`, {});
  }
}
