import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {copy} from 'angular';
import {formatDate} from '@angular/common';
import {AlertService} from '@reinform-cdp/widgets';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  beforeUnloadEvent = (e => {
    if (this.beforeUnload.saving) {
      e.returnValue = 'Внесенные изменения не сохранятся!';
    }
  }).bind(this);

  beforeUnload: any = {
    saving: false,
    init: () => {
      window.addEventListener('beforeunload', this.beforeUnloadEvent);
    },
    destroy: () => {
      window.removeEventListener('beforeunload', this.beforeUnloadEvent);
    },
    start: () => {
      this.beforeUnload.saving = true;
    },
    stop: () => {
      this.beforeUnload.saving = false;
    }
  };

  constructor(private alertService: AlertService,
              protected toastr: ToastrService) {
  }

  error(error: any): void {
    const err = error ? error.error : '';
    if (err && err.userMessages && err.userMessages.length) {
      err.userMessages.forEach(msg => this.toastr.error(msg));
    } else if (_.isString(error)) {
      this.toastr.error(error);
    }
  }

  success(message: string): void {
    this.toastr.success(message);
  }

  warning(message: string): void {
    this.toastr.warning(message);
  }

  toJson(obj: any) {
    let r: any = copy(obj);
    this.transformObject(r);
    return JSON.stringify(r);
  }

  get alert() {
    return this.alertService;
  }

  private transformObject(o) {
    if (_.isArray(o)) {
      o.forEach(i => this.transformObject(i));
    } else if (_.isObject(o)) {
      _.forIn(o, (val: any, key: string) => {
        if (_.isDate(val)) {
          o[key] = formatDate(val, 'yyyy-MM-ddTHH:mm:ss', 'en');
        } else if (_.isArray(val) || _.isObject(val)) {
          this.transformObject(o[key]);
        }
      });
    }
  }

  getField(path: string, context): any {
    let field: any = context;
    let pathParts: string[] = _.compact(path.split(/[\/\.]/));
    pathParts.forEach(i => {
      if (_.isArray(field)) {
        field = field.map(f => f && f[i] ? f[i] : null);
      } else {
        field = field[i];
      }
    });
    return field;
  }

  /**
   * [textMask]
   * Generate mask for number fields (\d+)
   */
  maskNumber(t: string): any[] {
    const result = [];
    let parts: any[] = [];

    if (t) { parts = t.split(''); }
    parts.forEach(i => {
      if (/\d/.test(i)) { result.push(/\d/); }
    });
    return result;
  }
}
