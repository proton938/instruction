import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";
import {map} from "rxjs/internal/operators";
import { Injectable } from '@angular/core';
import {IMemoDocumentWrapper} from "../models/memo-document/abstracts/IMemoDocumentWrapper";
import {IJsonPatchData} from "../models/IJsonPatchData";

@Injectable({
  providedIn: 'root'
})
export class MemoMkaRestService {
  root = `/app/sdo/instruction`;

  constructor(private http: HttpClient) { }

  get(id: string): Observable<IMemoDocumentWrapper> {
    return this.http.get(`${this.root}/memomka/${id}`);
  }

  delete(id: string): Observable<void> {
    return this.http.delete(`${this.root}/memomka/${id}`).pipe(map(response => { return; }));
  }

  patch(id: string, data: IJsonPatchData[]): Observable<void> {
    return this.http.patch(`${this.root}/memomka/${id}`, data, {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    }).pipe(map(response => { return; }));
  }

}
