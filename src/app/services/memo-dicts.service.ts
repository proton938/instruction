import {ToastrService} from "ngx-toastr";
import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Rx";
import {MemoDictsModel} from "../models/memo/MemoDictsModel";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {from, throwError} from "rxjs/index";
import {catchError, map} from "rxjs/internal/operators";

@Injectable({
  providedIn: 'root'
})
export class MemoDictsService {

  constructor(private nsiRestService: NsiResourceService,
              private toastr: ToastrService) {
  }


  getDicts(): Observable<MemoDictsModel> {
    const model = new MemoDictsModel();

    return from(this.nsiRestService.getDictsFromCache(['mggt_instruction_memo_status', 'mggt_instruction_memo_reportTerm',
      'InstructionExecutors'])).pipe(
      map(response => {
        model.statuses = response.mggt_instruction_memo_status;
        model.reportTerms = response.mggt_instruction_memo_reportTerm;
        model.executors = response.InstructionExecutors;

        return model;
      }), catchError(error => {
        console.log(error);
        this.toastr.error('Ошибка при считывании справочной информации!');
        return throwError(error);
      })
    );
  }
}
