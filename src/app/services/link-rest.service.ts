import {HttpClient} from '@angular/common/http';
import {forkJoin, Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {mergeMap} from 'rxjs/internal/operators';
import {SolrMediatorService} from './solr-mediator.service';
import * as _ from 'lodash';

@Injectable({providedIn: 'root'})
export class LinkRestService {
  root = `/app/sdo/link`;
  docType = 'LINK';

  constructor(private http: HttpClient,
              private solrMediator: SolrMediatorService) {}

  create(data): Observable<any> {
    return this.http.post<any>(this.root + '/crud/create/' + this.docType + '?flagReindex=true', {
      link: data
    });
  }

  cloneLinks(docIdFrom: string, docIdTo: string, types: string[] = []) {
    return this.findSdoLinks(docIdFrom).pipe(
      mergeMap(res => {
        const links = res ? res.filter(i => {
          return types && types.length
            ? _.some(types, t => t === [i.link.type1, i.link.type2][+(i.link.id1 === docIdFrom)])
            : true;
        }) : [];
        return links.length ? forkJoin(links.map(i => this.create({
          id1: i.link.id1,
          id2: docIdTo,
          type1: i.link.type1,
          type2: i.link.type2,
          linkKind: {},
          subsystem1: i.link.subsystem1,
          subsystem2: i.link.subsystem2
        }))) : of([]);
      })
    );
  }

  findSdoLinks(id: string): Observable<any[]> {
    let url: string = `${this.root}/link/sdo/${id}/find`;
    return <Observable<any[]>>this.http.get(url);
  }

  findFromSolr(id: string, types: string[] = []): Observable<any[]> {
    return Observable.create(obs => {
      this.findSdoLinks(id).subscribe(res => {
        const links = res && res.length
          ? res.filter(i => _.some([i.link.type1, i.link.type2], t => _.some(types, d => d === t)))
          : [];
        const linkIds: string[] = links.map(i => _.find([i.link.id1, i.link.id2], f => f !== id));
        if (linkIds.length) {
          this.solrMediator.getByIds(types.map(t => {
            return {ids: linkIds, type: t};
          })).pipe(
            mergeMap(res => this.solrMediator.getDocs(res))
          ).subscribe((res: any[]) => {
            obs.next(res || []);
            obs.complete();
          }, error => {
            obs.error(error);
            obs.complete();
          });
        }
      });
    });
  }

}

