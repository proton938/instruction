interface Array<T> {
    isEmpty(): boolean;
}

Array.prototype.isEmpty = function () {
    return this.length === 0;
};

interface String {
    isDateTime(): boolean;
    isDate(): boolean;
    isNumber(): boolean;
    isNull(): boolean;
    isUndefined(): boolean;
    isBoolean(): boolean;
    isEmpty(): boolean;
    stringToBoolean(): boolean;
    stringToNumber(): number;
    stringToDate(): Date;
    stringToDateTime(): Date;
    startsWith(searchString, position): boolean;
}

String.prototype.isDateTime = function () {
    return (/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*$/.exec(this)) ? true : false;
};
String.prototype.isDate = function () {
    return (/^\d{4}-\d{2}-\d{2}$/.exec(this)) ? true : false;
};

String.prototype.stringToDate = function () {
    let result = new Date(this);
    result.setHours(0);
    return result;
};

String.prototype.stringToDateTime = function () {
    return new Date(this);
};

String.prototype.isNumber = function () {
    let resul = parseFloat(this);
    return resul !== NaN;
};

String.prototype.isNull = function () {
    let v = _.toLower(this);
    return (v === 'null');
};

String.prototype.isEmpty = function () {
    return (this.trim() === '');
};

String.prototype.isUndefined = function () {
    let v = _.toLower(this);
    return (v === 'undefined');
};

String.prototype.isBoolean = function () {
    let v = _.toLower(this);
    return (v === 'true' || v === 'false');
};

String.prototype.stringToBoolean = function () {
    let v = _.toLower(this);
    return (v === 'true');
};

String.prototype.stringToNumber = function () {
    return parseFloat(this);
};

String.prototype.startsWith = function (searchString, position) {
    position = position || 0;
    return this.indexOf(searchString, position) === position;
};
