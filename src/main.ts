import {enableProdMode, NgZone} from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {UIRouter, UrlService} from '@uirouter/core';
import { environment } from './environments/environment';
import {InstructionSystem} from "./app/app.module";


if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(InstructionSystem).then(platformRef => {
  const urlService: UrlService = platformRef.injector.get(UIRouter).urlService;

  const startUIRouter = () => {
    urlService.listen();
    urlService.sync();
  };

  platformRef.injector.get<NgZone>(NgZone).run(startUIRouter);
});
